/*
 * Author: Nathanaël Prémillieu
 */

#ifndef __BASE_SETASSOCTABLE_HH__
#define __BASE_SETASSOCTABLE_HH__

#include <cassert>
#include <list>
#include <vector>

#include "arch/utility.hh"

/**
 * @file Declaration of a class that implement a set-associative table
 *  with a LRU replacement policy.
 *  Each set is a list where the head is the MRU and the tail is the LRU
 */

template <class T, class Key>
class SetAssocTable
{
  public:

    /** empty constructor
     *  use init after when this constructor is used
     */
    SetAssocTable() { };

    /** Create a Set-associative table that store elements of type T
     * @param nbSets the number of row of the table
     * @param assoc the associativity of the table
     */
    SetAssocTable(unsigned nbSets, unsigned assoc);

    /** Destructor */
    ~SetAssocTable();

    /** initialize the structure */
    void init(unsigned nbSets, unsigned assoc);

    /** reset the valid bit */
    void reset();

    /** insert/update an element in the table
     * @param elem the element to insert
     * @param key the key to identify the element inside the set
     */
    void update(T elem, unsigned index, Key key);

    /** verify that there is an element in the table at the address with the given key
     * @param index the index in the table (the set index)
     * @param key the key to identify the element inside the set
     */
    bool valid(unsigned index, Key key);

    /** return the element which is in the table at the address with the given key
     * Use the exist function before to verify that the element exist
     * This function also update the LRU information.
     * @param index the index in the table (the set index)
     * @param key the key to identify the element inside the set
     */
    T lookup(unsigned index, Key key);

  private:
    unsigned nbSets;
    unsigned assoc;

    struct setAssocEntry {
        Key key;
        T value;
        bool valid;

    };

    std::vector< std::list<setAssocEntry> > table;

    typedef typename std::list<setAssocEntry>::iterator setAssocEntryIt;

    /** verify that there is an element in the table at the address with the given key
     * @param index the index in the table (the set index)
     * @param key the key to identify the element inside the set
     */
    setAssocEntryIt exist(unsigned index, Key key);
};

template <class T, class Key>
SetAssocTable<T, Key>::SetAssocTable(unsigned _nbSets, unsigned _assoc)
                                : nbSets(_nbSets),
                                  assoc(_assoc)
{
    table.resize(nbSets);
    for(int i = 0; i < nbSets; ++i) {
        table[i].resize(assoc);
    }
}

template <class T, class Key>
SetAssocTable<T, Key>::~SetAssocTable()
{
}

template <class T, class Key>
void
SetAssocTable<T, Key>::init(unsigned _nbSets, unsigned _assoc)
{
    nbSets = _nbSets;
    assoc = _assoc;
    table.resize(nbSets);
    for(int i = 0; i < nbSets; ++i) {
      table[i].resize(assoc);
    }
    this->reset();
}

template <class T, class Key>
void
SetAssocTable<T, Key>::reset()
{

    for(int index = 0; index < nbSets; ++index) {
        for(setAssocEntryIt entry = table[index].begin();
            entry != table[index].end(); ++entry) {
            entry->valid = false;
        }
    }
}

template <class T, class Key>
typename SetAssocTable<T, Key>::setAssocEntryIt
SetAssocTable<T, Key>::exist(unsigned index, Key key)
{
        assert(index < table.size());
    for(setAssocEntryIt entry = table[index].begin();
        entry != table[index].end(); ++entry) {
        if(entry->valid &&
           entry->key == key) {
            //found
            return entry;
        }
    }
    //not found
    return table[index].end();
}

template <class T, class Key>
bool
SetAssocTable<T, Key>::valid(unsigned index, Key key)
{
        assert(index < table.size());
    setAssocEntryIt entry = exist(index, key);
    return (entry != table[index].end());
}

template <class T, class Key>
T
SetAssocTable<T, Key>::lookup(unsigned index, Key key)
{
        assert(index < table.size());
    setAssocEntryIt entry = exist(index, key);
    T value;
    if(entry == table[index].end()) {
        panic("entry should exist if you do a lookup");
    }
    value = entry->value;

    //put the entry in the MRU position
    table[index].push_front(*entry);
    table[index].erase(entry);

    assert(table[index].size() == assoc);

    return value;
}

template <class T, class Key>
void
SetAssocTable<T, Key>::update(T elem, unsigned index, Key key)
{
        assert(index < table.size());
    setAssocEntryIt rpl = exist(index, key);

    if(rpl == table[index].end()) {
        //element not found, take the LRU as a replacement entry
        --rpl;
    }
    rpl->value = elem;
    rpl->key = key;
    rpl->valid = true;
    //put the entry in the MRU position
    table[index].push_front(*rpl);
    table[index].erase(rpl);

    assert(table[index].size() == assoc);
}

#endif // __BASE_SETASSOCTABLE_HH__

