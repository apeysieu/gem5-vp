/*
 * Copyright (c) 2011-2012, 2014 ARM Limited
 * Copyright (c) 2010 The University of Edinburgh
 * Copyright (c) 2012 Mark D. Hill and David A. Wood
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Kevin Lim
 */

#include "cpu/pred/bpred_unit.hh"

#include <algorithm>

#include "arch/isa_traits.hh"
#include "arch/types.hh"
#include "arch/utility.hh"
#include "base/trace.hh"
#include "config/the_isa.hh"
#include "debug/Branch.hh"

BPredUnit::BPredUnit(const Params *params)
    : SimObject(params),
      numThreads(params->numThreads),
      predHist(numThreads),
      BTB(params->BTBEntries,
          params->BTBAssoc,
          params->BTBTagSize,
          params->instShiftAmt),
      RAS(numThreads),
      instShiftAmt(params->instShiftAmt)
{
    for (auto& r : RAS)
        r.init(params->RASSize);
}

void
BPredUnit::regStats()
{
    lookups
        .name(name() + ".lookups")
        .desc("Number of BP lookups")
        ;

    condPredicted
        .name(name() + ".condPredicted")
        .desc("Number of conditional branches predicted")
        ;

    condIncorrectCommitted
        .name(name() + ".condIncorrectCommitted")
        .desc("Number of committed conditional branches incorrect")
        ;

    condIncorrectTotal
        .name(name() + ".condIncorrectTotal")
        .desc("Total number of committed conditional branches incorrect")
        ;

    BTBLookups
        .name(name() + ".BTBLookups")
        .desc("Number of BTB lookups")
        ;

    BTBHits
        .name(name() + ".BTBHits")
        .desc("Number of BTB hits")
        ;

    BTBCorrect
        .name(name() + ".BTBCorrect")
        .desc("Number of correct BTB predictions (this stat may not "
              "work properly.")
        ;

    BTBHitPct
        .name(name() + ".BTBHitPct")
        .desc("BTB Hit Percentage")
        .precision(6);

    BTBHitPct = (BTBHits / BTBLookups) * 100;


    BTBIncorrectCommitted
                .name(name() + ".BTBIncorrectCommitted")
                .desc("Number of incorrect BTB predictions (for committed instructions).")
                ;

    BTBIncorrectTotal
        .name(name() + ".BTBIncorrectTotal")
        .desc("Total number of incorrect BTB predictions.")
        ;

    usedRASCommitted
        .name(name() + ".usedRASCommitted")
        .desc("Number of times the RAS was used to get a target (for committed instructions).")
        ;

    usedRASTotal
        .name(name() + ".usedRASTotal")
        .desc("Total number of times the RAS was used to get a target.")
        ;

    RASIncorrectCommitted
        .name(name() + ".RASInCorrect")
        .desc("Number of incorrect RAS predictions (for committed instructions).")
        ;

    RASIncorrectTotal
        .name(name() + ".RASInCorrectTotal")
        .desc("Total number of incorrect RAS predictions.")
        ;

}

ProbePoints::PMUUPtr
BPredUnit::pmuProbePoint(const char *name)
{
    ProbePoints::PMUUPtr ptr;
    ptr.reset(new ProbePoints::PMU(getProbeManager(), name));

    return ptr;
}

void
BPredUnit::regProbePoints()
{
    ppBranches = pmuProbePoint("Branches");
    ppMisses = pmuProbePoint("Misses");
}

void
BPredUnit::drainSanityCheck() const
{
    // We shouldn't have any outstanding requests when we resume from
    // a drained system.
    for (const auto& ph M5_VAR_USED : predHist)
        assert(ph.empty());
}

bool
BPredUnit::predict(const StaticInstPtr &inst, const InstSeqNum &seqNum,
                   TheISA::PCState &pc, ThreadID tid)
{
    // See if branch predictor predicts taken.
    // If so, get its target addr either from the BTB or the RAS.
    // Save off record of branch stuff so the RAS can be fixed
    // up once it's done.

    bool pred_taken = false;
    TheISA::PCState target = pc;

    ++lookups;
    ppBranches->notify(1);

    void *bp_history = NULL;

    if (inst->isUncondCtrl()) {
        DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i] Unconditional control.\n",
        tid, pc, seqNum);
        pred_taken = true;
        // Tell the BP there was an unconditional branch.
        uncondBranch(pc.instAddr(), bp_history);
        inst->setHighConfBranch();
    } else {
        ++condPredicted;
        pred_taken = lookup(inst, pc.instAddr(), bp_history);

        DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: Branch predictor"
                        " predicted %i\n",
                        tid, pc, seqNum,  pred_taken);
    }

    DPRINTF(Branch, "[tid:%i]: [sn:%i] Creating prediction history "
            "for PC %s\n", tid, seqNum, pc);

    PredictorHistory predict_record(seqNum, pc.instAddr(),
                                    pred_taken, bp_history, tid);

    //save the state of the RAS
    predict_record.RASIndex_before = RAS[tid].topIdx();
    predict_record.RASTarget_before = RAS[tid].top();
    predict_record.RASCDTos_before = RAS[tid].cdtos();
    predict_record.RASFirst_wrap_before = RAS[tid].firstwrap();

    DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: Saving RAS"
                 " tos: %i, addr: %s, cd_tos: %i, fw: %i\n", tid, pc, seqNum,
                 RAS[tid].topIdx(), RAS[tid].top(), RAS[tid].cdtos(), RAS[tid].firstwrap());


    // Now lookup in the BTB or RAS.
    if (pred_taken) {
        if (inst->isReturn() && !RAS[tid].isCorrupted()) {
                ++usedRASTotal;
            predict_record.wasReturn = true;

            // Record the top entry of the RAS, and its index.
            predict_record.usedRAS = true;


            TheISA::PCState rasTop = RAS[tid].top();
            target = TheISA::buildRetPC(pc, rasTop);

            DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: Return, "
                        "RAS predicted target: %s, RAS index: %i, cd_tos: %i, fw: %i.\n",
                        tid, pc, seqNum, target,
                        RAS[tid].topIdx(), RAS[tid].cdtos(), RAS[tid].firstwrap());


            RAS[tid].pop();

            predict_record.RASIndex_after = RAS[tid].topIdx();
            predict_record.RASTarget_after = RAS[tid].top();
            predict_record.RASCDTos_after = RAS[tid].cdtos();
            predict_record.RASFirst_wrap_after = RAS[tid].firstwrap();

            DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: Saving RAS"
                        " after Return, tos: %i, addr: %s\n", tid, pc, seqNum,
                        RAS[tid].topIdx(), RAS[tid].top());
        } else {
            ++BTBLookups;

            if (inst->isCall()) {
                RAS[tid].push(pc);

                // Record that it was a call so that the top RAS entry can
                // be popped off if the speculation is incorrect.
                predict_record.wasCall = true;

                predict_record.RASIndex_after = RAS[tid].topIdx();
                predict_record.RASTarget_after = RAS[tid].top();

                DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: "
                                "Call, adding %s to the RAS index: %i.\n",
                                tid, pc, seqNum, pc, RAS[tid].topIdx());

            }

            if (BTB.valid(pc.instAddr(), tid)) {
                ++BTBHits;
                predict_record.usedBTB = true;

                // If it's not a return, use the BTB to get the target addr.
                target = BTB.lookup(pc.instAddr(), tid);

                DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i] predicted: "
                                " target is %s.\n", tid, pc, seqNum, target);

            } else {
                 DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: BTB doesn't have a "
                                 "valid entry.\n",tid, pc, seqNum);
                pred_taken = false;
                // The Direction of the branch predictor is altered because the
                // BTB did not have an entry
                // The predictor needs to be updated accordingly
                if (!inst->isCall() && !inst->isReturn()) {
                      btbUpdate(pc.instAddr(), bp_history);
                      DPRINTF(Branch, "[tid:%i]:[sn:%i] btbUpdate"
                              " called for %s\n", tid, seqNum, pc);
                } else if (inst->isCall() && !inst->isUncondCtrl()) {
                        RAS[tid].restore(predict_record.RASIndex_before, predict_record.RASTarget_before,
                                        predict_record.RASCDTos_before, predict_record.RASFirst_wrap_before);
                }
                TheISA::advancePC(target, inst);
            }
        }
    } else {
        if (inst->isReturn()) {
           predict_record.wasReturn = true;
        }
        TheISA::advancePC(target, inst);
    }

    pc = target;

    predHist[tid].push_front(predict_record);

    DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i]: History entry added."
                "predHist.size(): %i\n", tid, pc, seqNum, predHist[tid].size());


    return pred_taken;
}

bool
BPredUnit::predictInOrder(const StaticInstPtr &inst, const InstSeqNum &seqNum,
                          int asid, TheISA::PCState &instPC,
                          TheISA::PCState &predPC, ThreadID tid)
{
    // See if branch predictor predicts taken.
    // If so, get its target addr either from the BTB or the RAS.
    // Save off record of branch stuff so the RAS can be fixed
    // up once it's done.

    using TheISA::MachInst;

    bool pred_taken = false;
    TheISA::PCState target;

    ++lookups;
    ppBranches->notify(1);

    DPRINTF(Branch, "[tid:%i] [sn:%i] %s ... PC %s doing branch "
            "prediction\n", tid, seqNum,
            inst->disassemble(instPC.instAddr()), instPC);

    void *bp_history = NULL;

    if (inst->isUncondCtrl()) {
        DPRINTF(Branch, "[tid:%i] Unconditional control.\n", tid);
        pred_taken = true;
        // Tell the BP there was an unconditional branch.
        uncondBranch(instPC.instAddr(), bp_history);

        if (inst->isReturn() && RAS[tid].empty()) {
            DPRINTF(Branch, "[tid:%i] RAS is empty, predicting "
                    "false.\n", tid);
            pred_taken = false;
        }
    } else {
        ++condPredicted;

        pred_taken = lookup(inst, predPC.instAddr(), bp_history);
    }

    PredictorHistory predict_record(seqNum, predPC.instAddr(), pred_taken,
                                    bp_history, tid);

    // Now lookup in the BTB or RAS.
    if (pred_taken) {
        if (inst->isReturn()) {
                 ++usedRASCommitted;

            // If it's a function return call, then look up the address
            // in the RAS.
            TheISA::PCState rasTop = RAS[tid].top();
            target = TheISA::buildRetPC(instPC, rasTop);

            // Record the top entry of the RAS, and its index.
            predict_record.usedRAS = true;

            //assert(predict_record.RASIndex < 16);

            RAS[tid].pop();

            DPRINTF(Branch, "[tid:%i]: Instruction %s is a return, "
                    "RAS predicted target: %s, RAS index: %i.\n",
                    tid, instPC, target,
                    RAS[tid].topIdx());
        } else {
            ++BTBLookups;

            if (inst->isCall()) {

                RAS[tid].push(instPC);

                // Record that it was a call so that the top RAS entry can
                // be popped off if the speculation is incorrect.
                predict_record.wasCall = true;

                DPRINTF(Branch, "[tid:%i]: Instruction %s was a call"
                        ", adding %s to the RAS index: %i.\n",
                        tid, instPC, predPC,
                        RAS[tid].topIdx());
            }

            if (inst->isCall() &&
                inst->isUncondCtrl() &&
                inst->isDirectCtrl()) {
                target = inst->branchTarget(instPC);
            } else if (BTB.valid(predPC.instAddr(), asid)) {
                ++BTBHits;

                // If it's not a return, use the BTB to get the target addr.
                target = BTB.lookup(predPC.instAddr(), asid);

                DPRINTF(Branch, "[tid:%i]: [asid:%i] Instruction %s "
                        "predicted target is %s.\n",
                        tid, asid, instPC, target);
            } else {
                DPRINTF(Branch, "[tid:%i]: BTB doesn't have a "
                        "valid entry, predicting false.\n",tid);
                pred_taken = false;
                inst->clearHighConfBranch();
            }
        }
    }

    if (pred_taken) {
        // Set the PC and the instruction's predicted target.
        predPC = target;
    }
    DPRINTF(Branch, "[tid:%i]: [sn:%i]: Setting Predicted PC to %s.\n",
            tid, seqNum, predPC);

    predHist[tid].push_front(predict_record);

    DPRINTF(Branch, "[tid:%i] [sn:%i] pushed onto front of predHist "
            "...predHist.size(): %i\n",
            tid, seqNum, predHist[tid].size());

    return pred_taken;
}

void
BPredUnit::update(const InstSeqNum &done_sn, ThreadID tid)
{
    DPRINTF(Branch, "[tid:%i]: Committing branches until "
            "[sn:%lli].\n", tid, done_sn);

    DPRINTF(Branch, "History size: %u, back is for [sn:%lu]\n", predHist[tid].size(), predHist[tid].back().seqNum);
    while (!predHist[tid].empty() &&
           predHist[tid].back().seqNum <= done_sn) {
        // Update the branch predictor with the correct results.
        if (!predHist[tid].back().wasSquashed) {
            update(predHist[tid].back().pc, predHist[tid].back().predTaken,
                predHist[tid].back().bpHistory, false);
        } else {
            retireSquashed(predHist[tid].back().bpHistory);
        }

        if(predHist[tid].back().mispredicted) {
                ++condIncorrectCommitted;
                ++condIncorrectTotal;
        }

        if(predHist[tid].back().RASmispred) {
                ++RASIncorrectCommitted;
                ++RASIncorrectTotal;
        }

        if (predHist[tid].back().BTBmispred) {
                ++BTBIncorrectCommitted;
                ++BTBIncorrectTotal;
        }

        if(predHist[tid].back().usedRAS) {
                ++usedRASCommitted;
        }

        predHist[tid].pop_back();
    }
}

void
BPredUnit::squash(const InstSeqNum &squashed_sn, ThreadID tid)
{
    History &pred_hist = predHist[tid];

    while (!pred_hist.empty() &&
           pred_hist.front().seqNum > squashed_sn) {
        if (pred_hist.front().usedRAS) {
                // Restore the RAS
                 RAS[tid].restore(pred_hist.front().RASIndex_before, pred_hist.front().RASTarget_before,
                                 pred_hist.front().RASCDTos_before, pred_hist.front().RASFirst_wrap_before);
        }

        DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction 0x%x [sn:%i]: Squashing"
                                                      ", restoring RAS tos: %i, addr: %s\n", tid,
                                                      pred_hist.front().pc, pred_hist.front().seqNum,
                                                      RAS[tid].topIdx(), RAS[tid].top());

        // This call should delete the bpHistory.
        squash(pred_hist.front().bpHistory);

        DPRINTF(Branch, "[tid:%i]: Removing history for [sn:%i] "
                "PC %s.\n", tid, pred_hist.front().seqNum,
                pred_hist.front().pc);

        pred_hist.pop_front();

        DPRINTF(Branch, "[tid:%i]: predHist.size(): %i\n",
                tid, predHist[tid].size());
    }
}

void
BPredUnit::squash(const InstSeqNum &squashed_sn,
                  const TheISA::PCState &corrTarget,
                  bool actually_taken, ThreadID tid)
{
    // Now that we know that a branch was mispredicted, we need to undo
    // all the branches that have been seen up until this branch and
    // fix up everything.
    // NOTE: This should be call conceivably in 2 scenarios:
    // (1) After an branch is executed, it updates its status in the ROB
    //     The commit stage then checks the ROB update and sends a signal to
    //     the fetch stage to squash history after the mispredict
    // (2) In the decode stage, you can find out early if a unconditional
    //     PC-relative, branch was predicted incorrectly. If so, a signal
    //     to the fetch stage is sent to squash history after the mispredict

    History &pred_hist = predHist[tid];

    ++condIncorrectTotal;
    ppMisses->notify(1);

    DPRINTF(Branch, "[tid:%i]: Squashing from sequence number %i, "
            "setting target to %s.\n", tid, squashed_sn, corrTarget);

    // Squash All Branches AFTER this mispredicted branch
    squash(squashed_sn, tid);

    // If there's a squash due to a syscall, there may not be an entry
    // corresponding to the squash.  In that case, don't bother trying to
    // fix up the entry.
    if (!pred_hist.empty()) {

        auto hist_it = pred_hist.begin();
        //hist_it = find(pred_hist.begin(), pred_hist.end(),
        //                       squashed_sn);

        assert(hist_it != pred_hist.end());
        if (pred_hist.front().seqNum != squashed_sn) {
            DPRINTF(Branch, "Front sn %i != Squash sn %i\n",
                    pred_hist.front().seqNum, squashed_sn);

            assert(pred_hist.front().seqNum == squashed_sn);
        }


        if ((*hist_it).usedRAS) {
            ++RASIncorrectTotal;
        }

        //TODO ajouter actually_taken dans hist
        fixGhist((*hist_it).pc, pred_hist.front().bpHistory, actually_taken);

        update((*hist_it).pc, actually_taken,
               pred_hist.front().bpHistory, true);

        pred_hist.front().mispredicted = true;

        hist_it->wasSquashed = true;

        if (actually_taken) {
                if (hist_it->usedRAS) {
                        ++RASIncorrectTotal;
                        pred_hist.front().RASmispred = true;
                        DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction 0x%x [sn:%i]: RAS misprediction,"
                                        "correct target: %s, predicted target: %s (RASIncorrect: %i)\n", tid, hist_it->pc,
                                        pred_hist.front().seqNum, corrTarget, hist_it->RASTarget_before, RASIncorrectTotal.value());
                }

            DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction %s [sn:%i] BPBTBUpdate"
                        " called\n",
                        tid, corrTarget, squashed_sn);

            BTB.update(hist_it->pc, corrTarget, tid);
            //Restore the RAS
            if(hist_it->wasCall || (hist_it->wasReturn && hist_it->usedRAS)) {
                RAS[tid].restore(hist_it->RASIndex_after, hist_it->RASTarget_after,
                                hist_it->RASCDTos_after, hist_it->RASFirst_wrap_after);
            } else {
                RAS[tid].restore(hist_it->RASIndex_before, hist_it->RASTarget_before,
                                hist_it->RASCDTos_before, hist_it->RASFirst_wrap_before);
            }

            DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction 0x%x [sn:%i]: Correcting"
                        " (actually taken), restoring RAS tos: %i, addr: %s\n", tid,
                        hist_it->pc, hist_it->seqNum,
                        RAS[tid].topIdx(), RAS[tid].top(), RAS[tid].cdtos(), RAS[tid].firstwrap());

            /** If it is a return that has not been predicted taken
             * but that is actually taken, the RAS should be popped
             */
            if(hist_it->wasReturn && !hist_it->usedRAS && !hist_it->predTaken) {
                RAS[tid].pop();
                DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction 0x%x [sn:%i]: Correcting"
                                " Return (actually taken, RAS not used), restoring RAS tos: %i, addr: %s\n", tid,
                                hist_it->pc, hist_it->seqNum,
                                RAS[tid].topIdx(), RAS[tid].top());
            }
        } else {
                //Actually not Taken
                RAS[tid].restore(hist_it->RASIndex_before, hist_it->RASTarget_before,
                                hist_it->RASCDTos_before, hist_it->RASFirst_wrap_before);

                hist_it->usedRAS = false;
                DPRINTF(Branch, "BranchPred: [tid:%i]: Instruction 0x%x [sn:%i]: Correcting"
                                " (actually not-taken), restoring RAS tos: %i, addr: %s\n", tid,
                                hist_it->pc, hist_it->seqNum,
                                RAS[tid].topIdx(), RAS[tid].top());
        }
    } else {
        DPRINTF(Branch, "[tid:%i]: [sn:%i] pred_hist empty, can't "
                        "update.\n", tid, squashed_sn);
    }
}

void
BPredUnit::dump()
{
    int i = 0;
    for (const auto& ph : predHist) {
        if (!ph.empty()) {
            auto pred_hist_it = ph.begin();

            cprintf("predHist[%i].size(): %i\n", i++, ph.size());

            while (pred_hist_it != ph.end()) {
                cprintf("[sn:%lli], PC:%#x, tid:%i, predTaken:%i, "
                        "bpHistory:%#x\n",
                        pred_hist_it->seqNum, pred_hist_it->pc,
                        pred_hist_it->tid, pred_hist_it->predTaken,
                        pred_hist_it->bpHistory);
                pred_hist_it++;
            }

            cprintf("\n");
        }
    }
}

