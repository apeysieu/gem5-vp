/*
 * Authors: NathanaÃ«l PrÃ©millieu
 */


#include "cpu/pred/TAGE.hh"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "base/intmath.hh"
#include "base/trace.hh"
#include "debug/Branch.hh"

TageBP::TageBP(const TageBPParams *params)
: BPredUnit(params),
  _name(params->name + ".TAGE"),
  numHistComponents(params->numHistComponents),
  baseHystShift(params->baseHystShift),
  useAltOnNA(0),
  logCTick(19),
  cTick((1 << (logCTick - 1))),
  phist(0),
  seed(0),
  sinceBimodalMispred(0),
  BPHistory_count(0),
  instShiftAmt(params->instShiftAmt)
{
    unsigned i;
    unsigned size = numHistComponents + 1;
    /** initialize structures */

    assert(instShiftAmt == 0);

    assert(instShiftAmt == 0);

    /** logg */
    logg.push_back(-1);
    logg.push_back(10);
    logg.push_back(10);
    logg.push_back(11);
    logg.push_back(11);
    logg.push_back(11);
    logg.push_back(11);
    logg.push_back(10);
    logg.push_back(10);
    logg.push_back(10);
    logg.push_back(10);
    logg.push_back(9);
    logg.push_back(9);

    tagWidth.resize(numHistComponents);
    tagWidth[0] =  7;
    tagWidth[1] =  7;
    tagWidth[2] =  8;
    tagWidth[3] =  8;
    tagWidth[4] =  9;
    tagWidth[5] =  10;
    tagWidth[6] =  11;
    tagWidth[7] =  12;
    tagWidth[8] =  12;
    tagWidth[9] =  13;
    tagWidth[10] =  14;
    tagWidth[11] =  15;

    /** m */
    m.resize(size);
    /** computes the geometric history lengths */
    m[1] = params->minHistSize;
    m[numHistComponents] = params->maxHistSize;

    for(i = 2; i < size; i++) {
        m[i] =
                (int) (((double)  params->minHistSize *
                        pow ((double) ( params->maxHistSize) / (double)  params->minHistSize,
                                (double) (i - 1) / (double) ((numHistComponents - 1)))) + 0.5);
        std::cerr << "TAGE: " << std::dec << m[i] << std::endl;
    }

    /** ch_i, ch_t */
    ch_i.resize(size);
    ch_t[0].resize(size);
    ch_t[1].resize(size);


    for(i = 1; i < size; ++i) {
        ch_i[i].init(m[i], logg[i]);
        ch_t[0][i].init(ch_i[i].olength,  tagWidth[i - 1]);
        ch_t[1][i].init(ch_i[i].olength,  tagWidth[i - 1] - 1);
    }

    /** bTable */
    bTable.resize(1 <<  params->numLogBaseEntry);

    /** gTable */
    gTable.resize(size);
    for(i = 1; i < size; ++i) {
        gTable[i].resize(1 << logg[i], Gentry( params->counterWidth));
    }

    /** Compute masks */
    baseMask = ((1 << ( params->numLogBaseEntry)) - 1);

    tagMask.resize(size);
    for(i = 1; i < size; ++i) {
        tagMask[i] = ((1 <<  tagWidth[i - 1]) - 1);
    }

    gMask.resize(size);
    for(i = 1; i < size; ++i) {
        gMask[i] = ((1 << logg[i]) - 1);
    }

    Hist h;
    h.dir = 0;
    h.id = 0;
    /** Initialize the global history */
    for(i = 0; i <=  params->maxHistSize; ++i) {
        globHist.push_front(h);
    }
#ifdef DEBUG
    nonspec = globHist.rbegin();
#endif
    std::cerr << std::hex << "Globhist of TAGE is at " << &globHist << std::endl;
}

void
TageBP::regStats()
{
           lookups
                .name(name() + ".lookups")
                .desc("Number of BP lookups")
                ;

            condPredicted
                .name(name() + ".condPredicted")
                .desc("Number of conditional branches predicted")
                ;

            condIncorrectCommitted
                 .name(name() + ".condIncorrectCommitted")
                 .desc("Number of committed conditional branches incorrect")
                 ;

             condIncorrectTotal
                 .name(name() + ".condIncorrectTotal")
                 .desc("Total number of committed conditional branches incorrect")
                 ;


            BTBLookups
                .name(name() + ".BTBLookups")
                .desc("Number of BTB lookups")
                ;

            BTBHits
                .name(name() + ".BTBHits")
                .desc("Number of BTB hits")
                ;

            BTBCorrect
                .name(name() + ".BTBCorrect")
                .desc("Number of correct BTB predictions (this stat may not "
                      "work properly.")
                ;

            BTBHitPct
                .name(name() + ".BTBHitPct")
                .desc("BTB Hit Percentage")
                .precision(6);

            BTBHitPct = (BTBHits / BTBLookups) * 100;

            usedRAS
                .name(name() + ".usedRAS")
                .desc("Number of times the RAS was used to get a target.")
                ;

            RASIncorrect
                .name(name() + ".RASInCorrect")
                .desc("Number of incorrect RAS predictions.")
                ;

            BTBIncorrectCommitted
                .name(name() + ".BTBIncorrectCommitted")
                .desc("Number of incorrect BTB predictions (for committed instructions).")
                ;

            BTBIncorrectTotal
                .name(name() + ".BTBIncorrectTotal")
                .desc("Total number of incorrect BTB predictions.")
                ;

            usedRASCommitted
                .name(name() + ".usedRASCommitted")
                .desc("Number of times the RAS was used to get a target (for committed instructions).")
                ;

            usedRASTotal
                        .name(name() + ".usedRASTotal")
                        .desc("Total number of times the RAS was used to get a target.")
                        ;

            RASIncorrectCommitted
                        .name(name() + ".RASInCorrectCommitted")
                        .desc("Number of incorrect RAS predictions (for committed instructions).")
                        ;

            RASIncorrectTotal
                        .name(name() + ".RASInCorrectTotal")
                        .desc("Total number of incorrect RAS predictions.")
                        ;

    taggedPred
        .name(name() + ".taggedPred")
        .desc("Number of tagged components predictions")
        ;

    bimodPred
        .name(name() + ".bimodPred")
        .desc("Number of base bimodal predictions")
        ;

    standardPred
        .name(name() + ".standardPred")
        .desc("Number of standard predictions")
        ;

    altPred
        .name(name() + ".altPred")
        .desc("Number of predictions given by the alternate prediction")
        ;

    highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

    highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

    highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

    medConf
        .name(name() + ".medConf")
        .desc("Number of medium confidence predictions")
        ;

    medConfHit
        .name(name() + ".medConfHit")
        .desc("Number of correct medium confidence predictions")
        ;

    medConfMiss
        .name(name() + ".medConfMiss")
        .desc("Number of incorrect medium confidence predictions.")
        ;

    lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

    lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

    lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

    bimHighConf
        .name(name() + ".bimHighConf")
        .desc("Number of high confidence base bimodal predictions")
        ;

    bimHighConfHit
        .name(name() + ".bimHighConfHit")
        .desc("Number of correct high confidence base bimodal predictions")
        ;

    bimHighConfMiss
        .name(name() + ".bimHighConfMiss")
        .desc("Number of incorrect high confidence base bimodal predictions.")
        ;

    bimMedConf
        .name(name() + ".bimMedConf")
        .desc("Number of medium confidence base bimodal predictions")
        ;

    bimMedConfHit
        .name(name() + ".bimMedConfHit")
        .desc("Number of correct medium confidence base bimodal predictions")
        ;

    bimMedConfMiss
        .name(name() + ".bimMedConfMiss")
        .desc("Number of incorrect medium confidence base bimodal predictions.")
        ;

    bimLowConf
        .name(name() + ".bimLowConf")
        .desc("Number of low confidence base bimodal predictions")
        ;

    bimLowConfHit
        .name(name() + ".bimLowConfHit")
        .desc("Number of correct low confidence base bimodal predictions")
        ;

    bimLowConfMiss
        .name(name() + ".bimLowConfMiss")
        .desc("Number of incorrect low confidence base bimodal predictions.")
        ;

    Stag
        .name(name() + ".Stag")
        .desc("Number of predictions with Stag set")
        ;

    StagHit
        .name(name() + ".StagHit")
        .desc("Number of correct predictions with Stag set")
        ;

    StagMiss
        .name(name() + ".StagMiss")
        .desc("Number of incorrect predictions with Stag set.")
        ;

    NStag
        .name(name() + ".NStag")
        .desc("Number of predictions with NStag set")
        ;

    NStagHit
        .name(name() + ".NStagHit")
        .desc("Number of correct predictions with NStag set")
        ;

    NStagMiss
        .name(name() + ".NStagMiss")
        .desc("Number of incorrect predictions with NStag set.")
        ;

    Wtag
        .name(name() + ".Wtag")
        .desc("Number of predictions with Wtag set")
        ;

    WtagHit
        .name(name() + ".WtagHit")
        .desc("Number of correct predictions with Wtag set")
        ;

    WtagMiss
        .name(name() + ".WtagMiss")
        .desc("Number of incorrect predictions with Wtag set.")
        ;

    NWtag
        .name(name() + ".NWtag")
        .desc("Number of predictions with NWtag set")
        ;

    NWtagHit
        .name(name() + ".NWtagHit")
        .desc("Number of correct predictions with NWtag set")
        ;

    NWtagMiss
        .name(name() + ".NWtagMiss")
        .desc("NNumber of incorrect predictions with NWtag set.")
        ;
}

unsigned
TageBP::F(unsigned hist, unsigned size, unsigned bank)
{
    Addr res, h1, h2;
    res = (Addr)hist;

    res = res & ((1 << size) - 1);
    h1 = (res & gMask[bank]);
    h2 = (res >> logg[bank]);
    h2 = ((h2 << bank) & gMask[bank]) + (h2 >> (logg[bank] - bank));
    res = h1 ^ h2;
    res = ((res << bank) & gMask[bank]) + (res >> (logg[bank] - bank));
    return (unsigned)res;
}

unsigned
TageBP::gIndex(Addr &addr, unsigned bank)
{
    Addr branch_addr = (addr >> instShiftAmt);
    Addr index;
    unsigned M = (m[bank] > 16) ? 16 : m[bank];
    index = branch_addr ^ (branch_addr >> (abs(logg[bank] - bank) + 1)) ^
            ch_i[bank].comp ^ F(phist, M, bank);
    return (unsigned)(index & gMask[bank]);
}

unsigned
TageBP::gTag(Addr &addr, unsigned bank)
{
    Addr branch_addr = (addr >> instShiftAmt);
    Addr tag = branch_addr ^ ch_t[0][bank].comp ^ (ch_t[1][bank].comp << 1);
    return (unsigned)(tag & tagMask[bank]);
}

bool
TageBP::getBimodalPred(Addr &branch_addr, bool &saturated)
{
    unsigned index = bIndex(branch_addr);
    unsigned inter = (bTable[index].pred << 1) + bTable[index >> baseHystShift].hyst;
    saturated = (inter == 0 || inter == 3);

    return (bTable[index].pred > 0);
}

void
TageBP::baseUpdate(Addr branch_addr, bool taken)
{
    unsigned index = bIndex(branch_addr);
    unsigned inter = (bTable[index].pred << 1) + bTable[index >> baseHystShift].hyst;
    unsigned old = inter;

    if(taken) {
        if(old < 3)
            ++inter;
    }
    else if(old > 0)
        --inter;

    bTable[index].pred = inter >> 1;
    bTable[index >> baseHystShift].hyst = (inter & 1);

    DPRINTF(Branch, "BranchPred: 0x%x, base update, index: %i, hyst_index: %i,"
    " pred: %i (counter: before: %i, after: %i)\n", branch_addr, index, index >> baseHystShift,
    bTable[index].pred, old, inter);
}

void
TageBP::btbUpdate(Addr branch_addr, void * &bp_history)
{
                assert(!globHist.empty());
        globHist.pop_front();
        recover(branch_addr, false, bp_history);
        DPRINTF(Branch, "Popping front history because reversing direction.\n");
}

void
TageBP::updateGlobHist(Addr &branch_addr, bool taken, void * &bp_history, bool save) {
    unsigned size = numHistComponents + 1;
    unsigned i;
    Hist h;
    h.dir = taken ? 1 : 0;
    h.id = BPHistory_count;
    assert(bp_history);

    BPHistory *history = static_cast<BPHistory *>(bp_history);

    history->hist_id = BPHistory_count++;
    DPRINTF(Branch, "History hist_id is %u\n", history->hist_id);

    /** add the direction in the global history */
    globHist.push_front(h);

    history->dir_ptr = &(globHist.front());

    /** Save the history if needed */
    if(save)
    {
        history->phistSave = phist;
        history->ch_i_comp.resize(size);
        history->ch_t1_comp.resize(size);
        history->ch_t0_comp.resize(size);

        for (i = 1; i < size; ++i)
        {
            history->ch_i_comp[i] = ch_i[i].comp;
            history->ch_t0_comp[i] = ch_t[0][i].comp;
            history->ch_t1_comp[i] = ch_t[1][i].comp;
        }
    }

    /** update the path history */
    phist = (phist << 1) | ((branch_addr >> instShiftAmt) & 1);
    phist = (phist & ((1 << 16) - 1));

    //prepare next index and tag computations
    for (i = 1; i < size; ++i)
    {

        ch_i[i].update(globHist);
        DPRINTF(Branch, "TAGE: Updated folded history %u: %lu\n", i, ch_i[i].comp);
        ch_t[0][i].update(globHist);
        ch_t[1][i].update(globHist);
        DPRINTF(Branch, "Updating ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
    }

    DPRINTF(Branch, "BranchPred: GlobHist size: %i, new phist: %x\n", globHist.size(), phist);
}

bool
TageBP::lookup(const StaticInstPtr &inst, Addr branch_addr, void * &bp_history)
{

    int i;
    unsigned size = numHistComponents + 1;
    unsigned hitBank = 0;
    unsigned altBank = 0;
    bool tagePred;
    bool altTaken;
    bool predTaken;
    bool bimodPred = false;
    bool bimodSaturated = false;

    // Create BPHistory
    BPHistory *history = new BPHistory(true);

    // TAGE prediction

    // computes the table addresses and the partial tags
    history->gI.resize(size);
    history->gTag.resize(size);
    for (i = 1; i < size; ++i)
    {
        history->gI[i] = gIndex(branch_addr, i);
        history->gTag[i] = gTag(branch_addr, i);
        DPRINTF(Branch, "folded history at predict time ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
    }

    //Look for the bank with longest matching history
    for (i = size - 1; i > 0; --i)
    {
        if (gTable[i][history->gI[i]].tag == history->gTag[i])
        {
            hitBank = i;

            DPRINTF(Branch, "BranchPred: 0x%x, entry found in bank: %i,"
            " gI: %i, gTag: %i\n", branch_addr, i, history->gI[i], history->gTag[i]);

            break;
        }
    }

    //Look for the alternate bank
    for (i = hitBank - 1; i > 0; --i)
    {
        if (gTable[i][history->gI[i]].tag == history->gTag[i])
            if ((useAltOnNA < 0) ||
                    (abs (2 * gTable[i][history->gI[i]].ctr.read() + 1) > 1))
            {
                altBank = i;

                DPRINTF(Branch, "BranchPred: 0x%x, alternate entry found in bank: %i,"
                " gI: %i, gTag: %i\n", branch_addr, i, history->gI[i], history->gTag[i]);
                break;
            }
    }

    //computes the prediction and the alternate prediction
    if (hitBank > 0)
    {
        if (altBank > 0) {
            altTaken = (gTable[altBank][history->gI[altBank]].ctr.read() >= 0);
        }
        else
        {
            altTaken = getBimodalPred(branch_addr, bimodSaturated);
            bimodPred = true;

            DPRINTF(Branch, "BranchPred: 0x%x, alternate prediction by base bimodal,"
            " index: %i, sat: %i, pred: %i\n", branch_addr, bIndex(branch_addr), bimodSaturated,
            altTaken);
        }
        //if the entry is recognized as a newly allocated entry and
        //useAltOnNA is positive  use the alternate prediction
        if ((useAltOnNA < 0)
                || (abs (2 * gTable[hitBank][history->gI[hitBank]].ctr.read() + 1) > 1))
        {
            tagePred = (gTable[hitBank][history->gI[hitBank]].ctr.read() >= 0);
            bimodPred = false;
            int ctr = gTable[hitBank][history->gI[hitBank]].ctr.read();
            if(ctr == 0 || ctr == -1)
            {
                history->Wtag = true;
            }
            if(ctr == 1 || ctr == -2)
            {
                history->NWtag = true;
            }
            if(ctr == 2 || ctr == -3)
            {
                history->NStag = true;
            }
            if(gTable[hitBank][history->gI[hitBank]].hi)
            {
                history->Stag = true;
                history->NStag = false;
            }
            history->usedTagged = true;
            history->usedStandard = true;

            DPRINTF(Branch, "BranchPred: 0x%x, standard prediction used, tagePred %i\n",
            branch_addr, tagePred);
        }
        else
        {
            tagePred = altTaken;
            if(bimodPred)
            {
                if(bimodSaturated)
                {
                    history->bimHighConf = true;
                }
                else
                {
                    history->bimLowConf = true;
                }
                history->usedBimod = true;
            }
            else
            {
                int ctr = gTable[altBank][history->gI[altBank]].ctr.read();
                if(ctr == 0 || ctr == -1)
                {
                        history->Wtag = true;
                }
                if(ctr == 1 || ctr == -2)
                {
                        history->NWtag = true;
                }
                if(ctr == 2 || ctr == -3)
                {
                        history->NStag = true;
                }
                if(gTable[altBank][history->gI[altBank]].hi)
                {
                    history->Stag = true;
                    history->NStag = false;
                }
                history->usedTagged = true;
            }
            history->usedAlt = true;

            DPRINTF(Branch, "BranchPred: 0x%x, alternate prediction used, tagePred %i,"
            " bimodPred: %i\n", branch_addr, tagePred, bimodPred);
        }
    }
    else
    {
        altTaken = getBimodalPred(branch_addr, bimodSaturated);
        tagePred = altTaken;
        bimodPred = true;
        if(bimodSaturated)
        {
            history->bimHighConf = true;
        }
        else
        {
            history->bimLowConf = true;
        }

        history->usedBimod = true;
        history->usedAlt = true;

        DPRINTF(Branch, "BranchPred: 0x%x, no tagged prediction, use base"
        " bimodal prediction, tagePred: %i, index: %i, sat: %i\n", branch_addr, tagePred,
        bIndex(branch_addr), bimodSaturated);
    }
    //end TAGE prediction

    //predloop = getloop (pc);	// loop prediction

    //pred_taken = ((WITHLOOP >= 0) && (LVALID)) ? predloop : tage_pred;

    predTaken = tagePred;

    /* save predictor state */
    if(!history)
        panic("No history data structure to save the predictor state");

    /* save prediction, hit/alt bank, GI and GTAG */
    history->hitBank = hitBank;
    history->altBank = altBank;
    history->tagePred = tagePred;
    history->altTaken = altTaken;
    history->predTaken = predTaken;
    history->actuallyTaken = predTaken;

    /* confidence */
    if(bimodPred && sinceBimodalMispred < 8 && history->bimHighConf)
    {
        history->bimMedConf = true;
        history->bimHighConf = false;
        history->bimLowConf = false;
    }

    if(history->bimHighConf || history->Stag)
    {
        history->highConf = true;
        inst->setHighConfBranch();
    }

    if(history->bimMedConf || history->NStag)
    {
        history->medConf = true;
    }

    if(history->bimLowConf || history->Wtag || history->NWtag)
    {
        history->lowConf = true;
    }

    sinceBimodalMispred++;

    DPRINTF(Branch, "BranchPred: 0x%x, hitBank: %i, altBank: %i, tagePred: %i,"
    " altTaken: %i, predTaken: %i\n", branch_addr, hitBank, altBank, tagePred,
    altTaken, predTaken);
    DPRINTF(Branch, "BranchPred: 0x%x, confidence:  high: %i, med: %i, low: %i"
    " (bimod: %i) (Stag: %i, NStag: %i, Wtag: %i, NWtag: %i)\n", branch_addr,
    history->highConf, history->medConf, history->lowConf,
    history->bimHighConf || history->bimMedConf || history->bimLowConf,
    history->Stag, history->NStag, history->Wtag, history->NWtag);

    bp_history = static_cast<void *>(history);

    /** update the global history */
    updateGlobHist(branch_addr, predTaken, bp_history, true);

    return predTaken;
}

void
TageBP::uncondBranch(Addr branch_addr, void * &bp_history)
{
    // Create BPHistory and pass it back to be recorded.
    BPHistory *history = new BPHistory(true);
    /** branch is unconditionally taken */
    history->predTaken = true;
    history->tagePred = true;
    history->uncond = true;
    history->actuallyTaken = true;
    history->highConf = true;

    bp_history = static_cast<void *>(history);
    /** update the global history */
    updateGlobHist(branch_addr, true, bp_history, true);
}

void
TageBP::updatePredictor(Addr branch_addr, bool taken, void *bp_history)
{
    unsigned i,j;
    unsigned size = numHistComponents + 1;
    unsigned nrand = myRandom();
    bool tagePred;
    bool altTaken;
    unsigned hitBank;
    unsigned altBank;
    BPHistory *history = static_cast<BPHistory *>(bp_history);

    tagePred = history->tagePred;
    altTaken = history->altTaken;
    hitBank = history->hitBank;
    altBank = history->altBank;

    // first update the loop predictor
    //loopupdate (pc, taken);

    /* if (LVALID) */
    /* 	if (tage_pred != predloop) */
    /* 	  ctrupdate (WITHLOOP, (predloop == taken), 7); */

    // TAGE UPDATE
    // try to allocate a  new entries only if prediction was wrong
    bool alloc = ((tagePred != taken) && (hitBank < numHistComponents));
    if (hitBank > 0)
    {
        // Manage the selection between longest matching and alternate matching
        // for "pseudo"-newly allocated longest matching entry
        bool LongestMatchPred = (gTable[hitBank][history->gI[hitBank]].ctr.read() >= 0);
        bool PseudoNewAlloc =
                ((abs (2 * gTable[hitBank][history->gI[hitBank]].ctr.read()) + 1) <= 1);
        // an entry is considered as newly allocated if its prediction counter is weak
        if (PseudoNewAlloc)
        {
            if (LongestMatchPred == taken)
                alloc = false;
            // if it was delivering the correct prediction, no need to allocate a new entry
            //even if the overall prediction was false
            if (LongestMatchPred != altTaken)
            {
                if (altTaken == taken)
                {
                    if (useAltOnNA < 7)
                        useAltOnNA++;
                }
                else if (useAltOnNA > -8)
                    useAltOnNA--;
            }
            if (useAltOnNA >= 0)
                tagePred = LongestMatchPred;
        }
    }

    DPRINTF(Branch, "BranchPred: 0x%x, update, hitBank: %i, altBank: %i, tagePred: %i,"
    " altTaken: %i, actuallyTaken: %i, ALLOC: %i\n", branch_addr, hitBank, altBank, tagePred,
    altTaken, taken, alloc);



    if (alloc)
    {
        // is there some "unuseful" entry to allocate
        unsigned min = 1;
        for (i = numHistComponents; i > hitBank; --i)
            if (gTable[i][history->gI[i]].u < min)
                min = gTable[i][history->gI[i]].u;

        // we allocate an entry with a longer history
        //to  avoid ping-pong, we do not choose systematically the next entry, but among the 3 next entries
        unsigned Y = nrand & ((1 << (numHistComponents - hitBank - 1)) - 1);
        unsigned X = hitBank + 1;
        if (Y & 1)
        {
            X++;
            if (Y & 2)
                X++;
        }
        //NO ENTRY AVAILABLE:  ENFORCES ONE TO BE AVAILABLE
        if(X >= gTable.size()) {
                X = gTable.size() - 1;
        }

        if (min > 0)
            gTable[X][history->gI[X]].u = 0;

        //Allocate only  one entry
        for (i = X; i < size; ++i)
            if (gTable[i][history->gI[i]].u == 0)
            {
                gTable[i][history->gI[i]].tag = history->gTag[i];
                gTable[i][history->gI[i]].ctr.set((taken) ? 0 : -1);
                gTable[i][history->gI[i]].u = 0;
                gTable[i][history->gI[i]].hi = false;

                DPRINTF(Branch, "BranchPred: 0x%x, new entry allocated,"
                " table: %i, index: %i, tag: %i, ctr: %i\n", branch_addr, i,
                history->gI[i], history->gTag[i], (taken) ? 0 : -1);

                break;
            }
    }
    //periodic reset of u: reset is not complete but bit by bit
    cTick++;
    if ((cTick & ((1 << logCTick) - 1)) == 0)
        // reset least significant bit
        // most significant bit becomes least significant bit
        for (i = 1; i < size; ++i)
            for (j = 0; j < (1 << logg[i]); ++j)
                gTable[i][j].u = gTable[i][j].u >> 1;

    if (hitBank > 0)
    {
        DPRINTF(Branch, "Updating entry gI: %u, gTag: %u\n", history->gI[hitBank], history->gTag[hitBank]);
        gTable[hitBank][history->gI[hitBank]].ctrupdate(taken);
        //if the provider entry is not certified to be useful also update the alternate prediction
        if (gTable[hitBank][history->gI[hitBank]].u == 0)
        {
            if (altBank > 0) {
                DPRINTF(Branch, "Updating alt entry gI: %u, gTag: %u\n", history->gI[altBank], history->gTag[altBank]);
                gTable[altBank][history->gI[altBank]].ctrupdate(taken);
            }
            if (altBank == 0)
                baseUpdate(branch_addr, taken);
        }
    }
    else
        baseUpdate(branch_addr, taken);

    // update the u counter
    if (tagePred != altTaken)
    {
        if (tagePred == taken)
        {
            if (gTable[hitBank][history->gI[hitBank]].u < 3)
                gTable[hitBank][history->gI[hitBank]].u++;
        }
        else
        {
            if (useAltOnNA < 0)
                if (gTable[hitBank][history->gI[hitBank]].u > 0)
                    gTable[hitBank][history->gI[hitBank]].u--;
        }
    }

}

void
TageBP::recover(Addr &branch_addr, bool taken, void *bp_history)
{
    unsigned size = numHistComponents + 1;
    unsigned i;

    /** Restore history */
    assert(bp_history);
    BPHistory *history = static_cast<BPHistory *>(bp_history);

    if ((history->tagePred != taken) &&
           (history->bimHighConf || history->bimMedConf || history->bimLowConf))
                sinceBimodalMispred = 0;

    DPRINTF(Branch, "BranchPred: 0x%x, recover: phist: %x, sinceBimodalMispred: %i\n",
    branch_addr, history->phistSave, sinceBimodalMispred);

    phist = history->phistSave;
    history->actuallyTaken = taken;

    for (i = 1; i < size; ++i)
    {
        ch_i[i].comp = history->ch_i_comp[i];
        ch_t[0][i].comp = history->ch_t0_comp[i];
        ch_t[1][i].comp = history->ch_t1_comp[i];
        DPRINTF(Branch, "Restoring ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
    }

    /** update history with the correct value */
    updateGlobHist(branch_addr, taken, bp_history, false);
}

void
TageBP::update(Addr branch_addr, bool taken, void *bp_history,
        bool squashed)
{
    BPHistory *history = static_cast<BPHistory *>(bp_history);

    if(!squashed)
    {
        assert(!globHist.empty());
        assert(history->dir_ptr->id == history->hist_id);
        assert(history->dir_ptr->dir == history->actuallyTaken);
        if(!history->uncond) {
            updatePredictor(branch_addr, history->actuallyTaken, bp_history);
        }

        globHist.pop_back();

#ifdef DEBUG
        //Sanity check on the non speculative part of the history
        for(auto it = globHist.rbegin(); it != globHist.rend(); ++it) {
                if(it->id == history->dir_ptr->id) {
                        nonspec = it;
                        break;
                }
        }
        assert(nonspec->id == history->dir_ptr->id);
#endif

        updateStats(bp_history);

        DPRINTF(Branch, "Deleting hist_id is %u\n", history->hist_id);
        delete history;
    }
    else
    {
        assert(!globHist.empty());
        assert(globHist.front().id == history->hist_id);
        globHist.pop_front();
        recover(branch_addr, taken, bp_history);
        history->actuallyTaken = taken;
        if(!history->uncond) {
                updatePredictor(branch_addr, history->actuallyTaken, bp_history);
        }
    }
#ifdef DEBUG
    nonspec = globHist.rbegin();
#endif
}

void
TageBP::fixGhist(Addr branch_addr, void *bp_history, bool actually_taken) {
          BPHistory *history = static_cast<BPHistory *>(bp_history);

          assert(!globHist.empty());
          assert(globHist.front().id == history->hist_id);
          globHist.pop_front();
          recover(branch_addr,  actually_taken, bp_history);
          history->actuallyTaken =  actually_taken;
}



void
TageBP::squash(void *bp_history)
{
        DPRINTF(Branch, "numHistCOmponents : %u\n", numHistComponents);
    unsigned size = numHistComponents + 1;
    unsigned i;
    BPHistory *history = static_cast<BPHistory *>(bp_history);

    assert(history);
    assert(!globHist.empty());
    assert(globHist.front().id == history->hist_id);

    globHist.pop_front();

    DPRINTF(Branch, "BranchPred: squash: GlobHist size: %i, phist: %x, hist_id is %u\n",
                globHist.size(), history->phistSave, history->hist_id);

    phist = history->phistSave;

    for (i = 1; i < size; ++i)
    {
        ch_i[i].comp = history->ch_i_comp[i];
        ch_t[0][i].comp = history->ch_t0_comp[i];
        ch_t[1][i].comp = history->ch_t1_comp[i];
        DPRINTF(Branch, "Restoring in squash ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
    }

    // Delete this BPHistory now that we're done with it.
    DPRINTF(Branch, "Deleting hist_id is %u\n", history->hist_id);
    delete history;

}

void
TageBP::updateStats(void *bp_history)
{
    BPHistory *history = static_cast<BPHistory *>(bp_history);

    DPRINTF(Branch, "BranchPred: update Stats: predTaken: %i, actuallyTaken: %i,"
    " hi: %i, med: %i, low: %i\n", history->predTaken, history->actuallyTaken,
    history->highConf, history->medConf, history->lowConf);

    if(history->usedTagged) {
        ++taggedPred;
    }
    if(history->usedBimod) {
        ++bimodPred;
    }
    if(history->usedStandard) {
        ++standardPred;
    }
    if(history->usedAlt) {
        ++altPred;
    }
    if(history->predTaken == history->actuallyTaken) {
        if(history->bimHighConf) {
            ++bimHighConf;
            ++bimHighConfHit;
        }
        if(history->Stag) {
            ++Stag;
            ++StagHit;
        }
        if(history->bimMedConf) {
            ++bimMedConf;
            ++bimMedConfHit;
        }
        if(history->NStag) {
            ++NStag;
            ++NStagHit;
        }
        if(history->bimLowConf) {
            ++bimLowConf;
            ++bimLowConfHit;
        }
        if(history->Wtag) {
            ++Wtag;
            ++WtagHit;
        }
        if(history->NWtag) {
            ++NWtag;
            ++NWtagHit;
        }

        if(history->highConf) {
            ++highConf;
            ++highConfHit;
        }
        if(history->medConf) {
            ++medConf;
            ++medConfHit;
        }
        if(history->lowConf) {
            ++lowConf;
            ++lowConfHit;
        }
    } else {
        if(history->bimHighConf) {
            ++bimHighConf;
            ++bimHighConfMiss;
        }
        if(history->Stag) {
            ++Stag;
            ++StagMiss;
        }
        if(history->bimMedConf) {
            ++bimMedConf;
            ++bimMedConfMiss;
        }
        if(history->NStag) {
            ++NStag;
            ++NStagMiss;
        }
        if(history->bimLowConf) {
            ++bimLowConf;
            ++bimLowConfMiss;
        }
        if(history->Wtag) {
            ++Wtag;
            ++WtagMiss;
        }
        if(history->NWtag) {
            ++NWtag;
            ++NWtagMiss;
        }

        if(history->highConf) {
            ++highConf;
            ++highConfMiss;
        }
        if(history->medConf) {
            ++medConf;
            ++medConfMiss;
        }
        if(history->lowConf) {
            ++lowConf;
            ++lowConfMiss;
        }
    }
}

void
TageBP::retireSquashed(void *bp_history)
{
    BPHistory *history = static_cast<BPHistory *>(bp_history);
    delete history;
}

void
TageBP::printHistory() {
    std::stringstream tmp;
    for(auto it = globHist.begin(); it != globHist.end(); ++it) {
            tmp << it->dir << "(" << it->id << ")";
    }
    tmp << std::endl;
    DPRINTF(Branch, "History: %s", tmp.str());
    std::cerr << tmp.str() << std::endl;
}

TageBP*
TageBPParams::create()
{
    return new TageBP(this);
}





#ifdef DEBUG
int
TageBP::BPHistory::newCount = 0;
#endif
