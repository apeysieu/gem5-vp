/*
 * Authors: NathanaÃ«l PrÃ©millieu
 */

#ifndef __CPU_O3_TAGE_PRED_HH__
#define __CPU_O3_TAGE_PRED_HH__

#include <cstdlib>
#include <deque>
#include <vector>

#include "base/statistics.hh"
#include "base/types.hh"
#include "cpu/pred/bpred_unit.hh"
#include "cpu/pred/sat_counter.hh"
#include "params/TageBP.hh"

/**
 * Implements a TAGE branch predictor, winner of
 * the 2nd JILP Championship Branch Prediction Competition.
 * AndrÃ© Seznec
 */
class TageBP : public BPredUnit
{
public:
        template <class T> friend class VPredUnit;
        friend class VTageVP;
        friend class PSVP;
        friend class FCM_PS;
        friend class VTAGE_FCM_PS;
        friend class FCM_VTAGE_Stride;
        friend class VTAGE_PS;
        friend class VTAGE_Stride;
        friend class VTAGE_FCM;
        friend class LimitedSimpleTAGE;
        friend class DiffVTAGE;
        friend class VTAGEComponent;
        friend class DVTAGE_VTAGE;

    /**
     * Default branch predictor constructor.
     */
    TageBP(const TageBPParams *params);

    /**
     * Registers statistics.
     */
    void regStats();

    /**
     * Looks up the given address in the branch predictor and returns
     * a true/false value as to whether it is taken.  Also creates a
     * BPHistory object to store any state it will need on squash/update.
     * @param branch_addr The address of the branch to look up.
     * @param bp_history Pointer that will be set to the BPHistory object.
     * @return Whether or not the branch is taken.
     */
    bool lookup(const StaticInstPtr &inst, Addr branch_addr, void * &bp_history);

    /**
     * Records that there was an unconditional branch, and modifies
     * the bp history to point to an object that has the previous
     * global history stored in it.
     * @param branch_addr The address of the branch to look up
     * @param bp_history Pointer that will be set to the BPHistory object.
     */
    void uncondBranch(Addr branch_addr, void * &bp_history);

    /**
     * Updates the branch predictor to Not Taken if a BTB entry is
     * invalid or not found.
     * @param branch_addr The address of the branch to look up.
     * @param bp_history Pointer to any bp history state.
     * @return Whether or not the branch is taken.
     */
    void btbUpdate(Addr branch_addr, void * &bp_history);

    /**
     * Updates the branch predictor with the actual result of a branch.
     * @param branch_addr The address of the branch to update.
     * @param taken Whether or not the branch was taken.
     * @param bp_history Pointer to the BPHistory object that was created
     * when the branch was predicted.
     * @param squashed is set when this function is called during a squash
     * operation.
     */
    void update(Addr branch_addr, bool taken, void *bp_history, bool squashed);
    void fixGhist(Addr branch_addr, void *bp_history, bool actually_taken);

    /**
     * Restores the global branch history on a squash.
     * @param bp_history Pointer to the BPHistory object that has the
     * previous global branch history in it.
     */
   void squash(void *bp_history);
   void retireSquashed(void *bp_history);

private:

    /** Index function for the bimodal table */
    inline unsigned bIndex(Addr &branch_addr) {
        return ((branch_addr >> instShiftAmt) & baseMask);
    }

    /**
     * The index functions for the tagged tables uses
     * path history as in the OGEHL predictor
     */
    /** F serves to mix path history */
    unsigned F(unsigned A, unsigned size, unsigned bank);

    /** gIndex computes a full hash of pc, ghist and phist */
    unsigned gIndex(Addr &branch_addr, unsigned bank);

    /** tags computation */
    unsigned gTag(Addr &branch_addr, unsigned bank);

    /** Base prediction (with the bimodal predictor) */
    bool getBimodalPred(Addr &branch_addr, bool &saturated);

    /**
     * Update the bimodal predictor: a hysteresis bit is shared
     * among 4 prediction bits
     */
    void baseUpdate(Addr branch_addr, bool taken);

    /**
     * Just a simple pseudo random number generator:
     * a 2-bit counter, used to avoid ping-pong phenomenon
     * on tagged entry allocations
     */
    inline unsigned myRandom()
    {
        ++seed;
        return seed & 3;
    }

    /** update the global history with the direction of the branch */
    void updateGlobHist(Addr &branch_addr, bool taken,
            void * &bp_history, bool save);

    /** Update the predictor with the correct direction */
    void updatePredictor(Addr branch_addr, bool taken, void *bp_history);

    /**
     * Recover from a branch misprediction:
     * correct the global history and the folded histories
     */
    void recover(Addr &branch_addr, bool taken, void *bp_history);

    void updateStats(void *bp_history);

    void printHistory();
public:
    struct Hist {
         unsigned dir;
         unsigned id;
     };



    struct BPHistory {

#ifdef DEBUG
        BPHistory()
        { newCount++; }
        ~BPHistory()
        { newCount--; }

        static int newCount;
#endif
        /** constructor */
        BPHistory(const bool init)
        : actuallyTaken(false), highConf(false), medConf(false), lowConf(false),
          bimHighConf(false), bimMedConf(false), bimLowConf(false),
          Stag(false), NStag(false), Wtag(false), NWtag(false),
          uncond(false), usedTagged(false), usedBimod(false), usedStandard(false),
          usedAlt(false)
        {
#ifdef DEBUG
            newCount++;
#endif
        }

        /** path history */
        unsigned phistSave;

        Hist* dir_ptr;

        /** Indexes to the different tables are computed only once */
        std::vector<unsigned> gI;

        /** Tags for the different tables are computed only once */
        std::vector<unsigned> gTag;

        /** ch_i.comp */
        std::vector<Addr> ch_i_comp;

        /** ch_t[0].comp */
        std::vector<Addr> ch_t0_comp;

        /** ch_t[1].comp */
        std::vector<Addr> ch_t1_comp;

        /** Correct direction */
        bool actuallyTaken;

        /** Prediction */
        bool predTaken;

        /** Alternate  TAGE prediction */
        bool altTaken;

        /** TAGE prediction */
        bool tagePred;

        /** Longest matching bank */
        unsigned hitBank;

        /** Alternate matching bank */
        unsigned altBank;

        /** confidence */
        bool highConf, medConf, lowConf;

        /** bimodal confidence */
        bool bimHighConf, bimMedConf, bimLowConf;

        /** type of the tagged prediction */
        bool Stag, NStag, Wtag, NWtag;

        /** Unconditional? */
        bool uncond;

        /** Prediction given by the tagged components */
        bool usedTagged;

        /** Prediction given by the base bimodal predictor */
        bool usedBimod;

        /** Standard prediction used */
        bool usedStandard;

        /** Alternated prediction used */
        bool usedAlt;

        unsigned hist_id;
    };



    /** this is the cyclic shift register for folding
     * a long global history into a smaller number of bits;
     * see P. Michaud's PPM-like predictor at CBP-1
     */
    class Folded_history
    {
    public:

        Addr comp;
        unsigned clength;
        unsigned olength;
        unsigned outpoint;

        Folded_history ()
        {
        }

        void init (int original_length, int compressed_length)
        {
            comp = 0;
            olength = original_length;
            clength = compressed_length;
            outpoint = olength % clength;
        }

        void update (std::deque<Hist> &globHist)
        {

            comp = (comp << 1) | (Addr)globHist[0].dir;
            comp ^= ((Addr)globHist[olength].dir << outpoint);
            comp ^= (comp >> clength);
            comp &= (1 << clength) - 1;
        }

    };

    /** TAGE bimodal table entry */
    class Bentry
    {
    public:
        unsigned hyst;
        unsigned pred;

        Bentry ()
        {
            pred = 0;
            hyst = 1;
        }
    };

    /** TAGE global table entry */
    class Gentry
    {
    public:
        SatCounter ctr;
        unsigned tag;
        unsigned u;
        bool hi;

        Gentry (unsigned counterWidth)
        : ctr(counterWidth, -(1 << (counterWidth - 1)))
        {
            ctr.set(0);
            tag = 0;
            u = 0;
            hi = false;
        }

        void ctrupdate (bool taken)
        {
            if (taken)
                ctr.increment();
            else
                ctr.decrement();

            if(ctr.saturated()) {
                if(((!hi) && (rand() % 128 == 0))) {
                        hi = true;
                } else if(!hi) {
                        if(taken) {
                                ctr.decrement();
                        } else {
                                ctr.increment();
                        }
                }
            }
            else {
                hi = false;
            }
        }
    };

    /** Parameters */

    const std::string _name;

    /** Number of Tagged Components */
    unsigned numHistComponents;

    /** sharing an hysteresis bit between 4 bimodal predictor entries */
    unsigned baseHystShift;

    /** Internal structures and variables */

    /** "Use alternate prediction on newly allocated":
     * a 4-bit counter  to determine whether the newly
     * allocated entries should be considered as
     * valid or not for delivering  the prediction */
    int useAltOnNA;

    /** Control counter for the smooth resetting of useful counters */
    unsigned logCTick, cTick;

    /** Use a path history as on  the OGEHL predictor */
    unsigned phist;

public:
    /** Log of number of entries  on each tagged component */
    std::vector<unsigned> logg;
    std::vector<unsigned> tagWidth;

    /** Global history */
    std::deque<Hist> globHist;

#ifdef DEBUG
    std::deque<Hist>::reverse_iterator nonspec;
#endif

    /** Utility for computing TAGE indices */
    std::vector<Folded_history> ch_i;

    /** Utility for computing TAGE tags */
    std::vector<Folded_history> ch_t[2];

    /** Bimodal TAGE table */
    std::vector<Bentry> bTable;

    /** Tagged TAGE tables */
    std::vector< std::vector<Gentry> > gTable;

    /** Used for storing the history lengths */
    std::vector<unsigned> m;

    /** For the pseudo-random number generator */
    unsigned seed;

    /** Mask to compute the index in the base bimodal predictor */
    Addr baseMask;

    /** Mask to compute the tag for the different tagged tables */
    std::vector<Addr> tagMask;

    /** Mask to compute the index in the different tagged tables */
    std::vector<Addr> gMask;

    /**
     * Count how many prediction have been made
     * since the last misprediction due to the base bimodal predictor
     */
    unsigned sinceBimodalMispred;

    /** Variable to give a unique id to the BPHistory structures created */
    unsigned BPHistory_count;

    /** Number of bits to shift the instruction over to get rid of the word
     *  offset.
     */
    unsigned instShiftAmt;

    /** Global Stats */
    /** Stat for number of predictions given by the tagged components. */
    Stats::Scalar taggedPred;
    /** Stat for number of predictions given by the base bimodal predictor. */
    Stats::Scalar bimodPred;
    /** Stat for number of standard predictions. */
    Stats::Scalar standardPred;
    /** Stat for number of predictions given by the alternate prediction. */
    Stats::Scalar altPred;

    /** Confidence Stats */
    /** Stat for number of high confidence predictions. */
    Stats::Scalar highConf;
    /** Stat for number of correct high confidence predictions. */
    Stats::Scalar highConfHit;
    /** Stat for the number of incorrect high confidence predictions */
    Stats::Scalar highConfMiss;
    /** Stat for number of medium confidence predictions. */
    Stats::Scalar medConf;
    /** Stat for number of correct medium confidence predictions. */
    Stats::Scalar medConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar medConfMiss;
    /** Stat for number of low confidence predictions. */
    Stats::Scalar lowConf;
    /** Stat for number of correct low confidence predictions. */
    Stats::Scalar lowConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar lowConfMiss;

    /** Stat for number of high confidence base bimodal predictions. */
    Stats::Scalar bimHighConf;
    /** Stat for number of correct high confidence base bimodal predictions. */
    Stats::Scalar bimHighConfHit;
    /** Stat for the number of incorrect high confidence base bimodal predictions */
    Stats::Scalar bimHighConfMiss;
    /** Stat for number of medium confidence base bimodal predictions. */
    Stats::Scalar bimMedConf;
    /** Stat for number of correct medium confidence base bimodal predictions. */
    Stats::Scalar bimMedConfHit;
    /** Stat for the number of incorrect medium confidence base bimodal predictions */
    Stats::Scalar bimMedConfMiss;
    /** Stat for number of low confidence base bimodal predictions. */
    Stats::Scalar bimLowConf;
    /** Stat for number of correct low confidence base bimodal predictions. */
    Stats::Scalar bimLowConfHit;
    /** Stat for the number of incorrect medium confidence base bimodal predictions */
    Stats::Scalar bimLowConfMiss;

    /** Stat for number of predictions with Stag set. */
    Stats::Scalar Stag;
    /** Stat for number of correct predictions with Stag set. */
    Stats::Scalar StagHit;
    /** Stat for the number of incorrect predictions with Stag set */
    Stats::Scalar StagMiss;

    /** Stat for number of predictions with NStag set. */
    Stats::Scalar NStag;
    /** Stat for number of correct predictions with NStag set. */
    Stats::Scalar NStagHit;
    /** Stat for the number of incorrect predictions with NStag set */
    Stats::Scalar NStagMiss;

    /** Stat for number of predictions with Wtag set. */
    Stats::Scalar Wtag;
    /** Stat for number of correct predictions with Wtag set. */
    Stats::Scalar WtagHit;
    /** Stat for the number of incorrect predictions with Wtag set */
    Stats::Scalar WtagMiss;

    /** Stat for number of predictions with NWtag set. */
    Stats::Scalar NWtag;
    /** Stat for number of correct predictions with NWtag set. */
    Stats::Scalar NWtagHit;
    /** Stat for the number of incorrect predictions with NWtag set */
    Stats::Scalar NWtagMiss;

    Stats::Scalar condIncorrect;

    Stats::Scalar usedRAS;
    Stats::Scalar RASIncorrect;
};

#endif // __CPU_O3_TOURNAMENT_PRED_HH__
