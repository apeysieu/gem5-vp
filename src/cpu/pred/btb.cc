/*
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Kevin Lim
 */

#include "base/intmath.hh"
#include "base/trace.hh"
#include "cpu/pred/btb.hh"
#include "debug/Fetch.hh"

SetAssocBTB::SetAssocBTB(unsigned _numEntries,
                            unsigned _assoc,
                            unsigned _tagBits,
                            unsigned _instShiftAmt)
    : btb(),
      numEntries(_numEntries),
      assoc(_assoc),
      tagBits(_tagBits),
      instShiftAmt(_instShiftAmt)
{
    DPRINTF(Fetch, "BTB: Creating BTB object. Size: %d, Associativity: %d\n",
                numEntries, assoc);

    if (!isPowerOf2(numEntries)) {
        fatal("BTB entries is not a power of 2!");
    }

    if (numEntries % assoc != 0) {
        fatal("BTB number of entries is not a multiple of the associativity."
                  "(Num Entries: %d, Associativity: %d)", numEntries, assoc);
    }

    numEntriesPerWay = numEntries/assoc;

    btb.init(numEntriesPerWay, assoc);

    idxMask = numEntriesPerWay - 1;

    tagMask = (1 << tagBits) - 1;

    tagShiftAmt = instShiftAmt + floorLog2(numEntriesPerWay);
}

void
SetAssocBTB::reset()
{
    btb.reset();
}

inline
unsigned
SetAssocBTB::getIndex(Addr instPC)
{
    // Need to shift PC over by the word offset.
    return (instPC >> instShiftAmt) & idxMask;
}

inline
Addr
SetAssocBTB::getTag(Addr instPC)
{
    return (instPC >> tagShiftAmt) & tagMask;
}

bool
SetAssocBTB::valid(Addr instPC, ThreadID tid)
{
    unsigned btb_idx = getIndex(instPC);

    BTBKey key;
    key.tag = getTag(instPC);
    key.tid = tid;

    assert(btb_idx < numEntriesPerWay);

    return btb.valid(btb_idx, key);
}

// @todo Create some sort of return struct that has both whether or not the
// address is valid, and also the address.  For now will just use addr = 0 to
// represent invalid entry.
TheISA::PCState
SetAssocBTB::lookup(Addr instPC, ThreadID tid)
{
    unsigned btb_idx = getIndex(instPC);

    BTBKey key;
    key.tag = getTag(instPC);
    key.tid = tid;

    assert(btb_idx < numEntriesPerWay);

    return btb.lookup(btb_idx, key);
}

void
SetAssocBTB::update(Addr instPC, const TheISA::PCState &target, ThreadID tid)
{
    BTBKey key;
    key.tag = getTag(instPC);
    key.tid = tid;
    btb.update(target, getIndex(instPC), key);
}
