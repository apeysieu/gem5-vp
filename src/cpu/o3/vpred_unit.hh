/*
 * Copyright (c) 2011 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Kevin Lim
 */

#ifndef __CPU_O3_VPRED_UNIT_HH__
#define __CPU_O3_VPRED_UNIT_HH__

#include <list>

#include "base/statistics.hh"
#include "base/types.hh"
#include "cpu/inst_seq.hh"
#include "cpu/pred/TAGE.hh"
#include "cpu/pred/bpred_unit.hh"
#include "cpu/vpred/Diff_VTAGE.hh"
#include "cpu/vpred/LVP.hh"
#include "cpu/vpred/PS.hh"
#include "cpu/vpred/VTAGE.hh"
#include "cpu/vpred/diff_fcm.hh"
#include "cpu/vpred/diff_vtage_alt.hh"
#include "cpu/vpred/dvtage_vtage.hh"
#include "cpu/vpred/fcm_ps.hh"
#include "cpu/vpred/fcm_spec.hh"
#include "cpu/vpred/fcm_stride.hh"
#include "cpu/vpred/fcm_vtage_ps.hh"
#include "cpu/vpred/fcm_vtage_stride.hh"
#include "cpu/vpred/gdiff.hh"
#include "cpu/vpred/stride.hh"
#include "cpu/vpred/vtage_fcm.hh"
#include "cpu/vpred/vtage_ps.hh"
#include "cpu/vpred/vtage_stride.hh"

struct DerivO3CPUParams;

/**
 * Basically a wrapper class to hold the value predictor.
 */

template<class Impl>
class VPredUnit
{
  public:
    typedef typename Impl::DynInstPtr DynInstPtr;

  public:
    enum PredType {
        VTAGE,
        DIFFVTAGE,
        Stride,
        PS,
        LVP,
        FCMSPEC,
        DIFFFCM,
        FCMSTRIDE,
        FCMPS,
        VTAGEPS,
        VTAGESTRIDE,
        VTAGEFCM,
        FCM_VTAGE_STRIDE,
        FCM_VTAGE_PS,
        DVTAGE_VTAGE,
        DVTAGE_ALT,
        gDiff,
        None
    };

    PredType predictor;

    const std::string _name;


    typedef typename Impl::O3CPU O3CPU;

    O3CPU *cpu;
    /**
     * @param params The params object, that has the size of the BP and BTB.
     */
    VPredUnit(DerivO3CPUParams *params , O3CPU * _cpu, TageBP  *bpred);

    const std::string &name() const { return _name; }

    /**
     * Registers statistics.
     */
    void regStats();

    void switchOut();

    void takeOverFrom();

    /**
     * Predicts whether or not the instruction is a taken branch, and the
     * target of the branch if it is taken.
     * @param inst The branch instruction.
     * @param PC The predicted PC is passed back through this parameter.
     * @param tid The thread id.
     * @return Returns if the branch is taken or not.
     */
    prediction_t predict(DynInstPtr &inst, TheISA::PCState &pc, ThreadID tid);

    /** DEPRECATED gDiff related **/
    prediction_t predictAtDispatch(DynInstPtr &inst, TheISA::PCState &pc, ThreadID tid);


    /**
     * Squashes all outstanding updates until a given sequence number.
     * First version to be called on a fault other than a value misprediction.
     * Second version to be called on a value misprediction.
     * @param squashed_sn The sequence number to squash any younger updates up
     * until.
     * @param tid The thread id.
     * @param non_spec True if the squash is triggered by a non speculative instruction
     */
    void squash(const InstSeqNum &squashed_sn, ThreadID tid);

    /** Squash on a value misprediction **/
    void squash(const InstSeqNum &squashed_sn, std::list<VPUpdateInfo> &actual_result, ThreadID tid);

    /** Squashes state of one instruction (the youngest one)
    * @param vp_history Placeholder for the underlying history object used by the actual predictor
    * @param remove True if the history should be thrown away.
    * @param recompute True if the instruction is a branch and the folded histories of TAGE-like predictors should be recomputed
    */
    void VPSquash(void *vp_history, bool remove, bool recompute);

    /**
     * Looks up a given PC in the BP to see if it the predictor has a prediction for it.
     * @param inst_PC The PC to look up.
     * @param vp_history Pointer that will be set to an object that
     * has the value predictor state associated with the lookup.
     * @return Whether the branch is taken or not taken.
     */
    prediction_t VPLookup(Addr instPC, MicroPC micropc, void * &vp_history, uint64_t seqnum = 0);

    /** Update the predictor for one instruction **/
    void UpdateVHist(const InstSeqNum &sn, Prediction val, bool squashed, ThreadID tid, DynInstPtr inst);

    /** Update the predictors for all instructions that were recently committed **/
    void update(const InstSeqNum &sn, std::list<VPUpdateInfo> &actual_result, ThreadID tid);

    /** DEPRECATED gDiff related, don't use **/
    void updateAtWB(const InstSeqNum &sn, Prediction &value, ThreadID tid);

    /** Update the histories of TAGE-like predictors, call it after the branch predictor has predicted **/
    void UpdateBHist(DynInstPtr &inst, TheISA::PCState &pc, ThreadID tid);

    /** Buffer actual results coming from commit **/
    void bufferResult(std::list<VPUpdateInfo> &actual_result);

    void dump();
    void dumpCorrect();

  private:

        struct PredictorVPHistory {
        /**
         * Makes a predictor history struct that contains any
         * information needed to update the predictor.
         */
        PredictorVPHistory(const InstSeqNum &seq_num, Addr instPC,
                         void *vp_history,
                         ThreadID _tid, bool branch, DynInstPtr inst)
                  : seqNum(seq_num), pc(instPC), tid(_tid), isBranch(branch), vpHistory(vp_history),  inst(inst), mispredicted(false) {}

        bool operator==(const PredictorVPHistory &entry) const {
            return this->seqNum == entry.seqNum;
        }



        /** The sequence number for the predictor history entry. */
        InstSeqNum seqNum;

        /** The PC associated with the sequence number. */
        Addr pc;
        ThreadID tid;
        bool isBranch;


        /** Pointer to the history object passed back from the branch
         * predictor.  It is used to update or restore state of the
         * branch predictor.
        **/
        void *vpHistory;

        prediction_t pred;
        DynInstPtr inst;

        /** Whether or not it has been mispredicted */
        bool mispredicted;
     };


     bool dumped;
     typedef std::deque<PredictorVPHistory> VPHistory;
     typedef typename VPHistory::iterator VPHistoryIt;

    /**
     * The per-thread predictor history. This is used to update the predictor
     * as instructions are committed, or restore it to the proper state after
     * a squash. Branch Prediction AND Value Prediction History.
     */

    //The Branch History (should be the same as that of the branch predictor)
    //typename BPredUnit<Impl>::History predHist[Impl::MaxThreads];
    VPHistory vpredHist[Impl::MaxThreads];

    /** The VTAGE value predictor */
    VTageVP *vtageVP;
    DiffVTAGE *diffVTAGE;

    FCM_Stride *fcm_stride;
    FCM_PS *fcm_ps;
    FCM_SPEC_VP *fcmsVP;
    VTAGE_PS *vtage_ps;
    VTAGE_Stride *vtage_stride;
    VTAGE_FCM *vtage_fcm;
    FCM_VTAGE_Stride *fcm_vtage_stride;
    VTAGE_FCM_PS *fcm_vtage_ps;
    StrideVP *strideVP;
    PSVP *psVP;
    LastVP *lVP;
    DiffFCM *diffFCM;
    DiffVTAGE_ALT *dvtage_alt;
    DVTAGE_VTAGE_VP *dvtage_vtage;
    gDiffVP *gdiff;

    //Buffer holding values coming from commit.
    //useful when we decide to limit the number of updates per cycle.
    std::list<VPUpdateInfo> updateBuffer;

  public:
    //Probability vector for confidence counters (increment only if rand() & (probability - 1) == 0)
    std::vector<unsigned> probability;

    //FHomebrew stats
    std::map<Addr, unsigned> correct_distribution;
    std::map<Addr, std::string> disassembly;
    unsigned long inst_to_execute;
    bool first_update;



    /** Stat for number of BP lookups. */
    Stats::Scalar lookups;

    Stats::Scalar squashDueToValueMispred;
    Stats::Scalar squashDueToBranchMispred;
    Stats::Scalar squashDueToOther;

    static bool vpcompare(const PredictorVPHistory &a, const PredictorVPHistory &b) {
        return a.seqNum < b.seqNum;
    }

};

#endif // __CPU_O3_VPRED_UNIT_HH__
