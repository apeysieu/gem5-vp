/*
 * Copyright (c) 2011 ARM Limited
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors:
 */

#include <algorithm>
#include <map>

#include "arch/isa_traits.hh"
#include "arch/types.hh"
#include "arch/utility.hh"
#include "base/trace.hh"
#include "config/the_isa.hh"
#include "cpu/o3/vpred_unit.hh"
#include "debug/Fetch.hh"
#include "debug/ValuePredictor.hh"


template<class Impl>
VPredUnit<Impl>::VPredUnit(DerivO3CPUParams *params, O3CPU *_cpu, TageBP *bpred)
: _name(params->name + ".VPredUnit")
  {
        probability.push_back(1);
        probability.push_back(16);
        probability.push_back(16);
        probability.push_back(16);
        probability.push_back(16);
        probability.push_back(32);
        probability.push_back(params->v_filterProbability);

        dumped = false;
        cpu = _cpu;
        inst_to_execute = params->max_insts_any_thread;

        first_update= false;


        if(params->v_strideLength == sizeof(Prediction::mask_pos) * 8) {
                Prediction::mask_pos = 0xFFFFFFFFFFFFFFFF;
        } else {
                unsigned long one = 1;
                Prediction::mask_pos = (one << (params->v_strideLength - 1)) - 1;
        }
        Prediction::mask_neg = (0xFFFFFFFFFFFFFFFF ^ Prediction::mask_pos);
        Prediction::index_sign = pow(2, params->v_strideLength - 1);

        std::cerr << params->v_strideLength << " - " << std::hex << Prediction::mask_pos << " - " << Prediction::mask_neg << std::endl;
        //Setup the selected predictor.
        if (params->v_predType == "vtage") {
                vtageVP = new VTageVP(params->name,
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_counterWidth,
                                params->v_instShiftAmt,
                                probability,
                                &(bpred->globHist),
                                &(bpred->phist),
                                bpred);
                predictor = VTAGE;
        } else if(params->v_predType == "dvtage-alt") {
                dvtage_alt = new DiffVTAGE_ALT(params->name,
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_counterWidth,
                                params->v_instShiftAmt,
                                probability,
                                &(bpred->globHist),
                                &(bpred->phist),
                                bpred);
                predictor = DVTAGE_ALT;
        } else if(params->v_predType== "dvtage-vtage") {
                dvtage_vtage = new DVTAGE_VTAGE_VP(params->name,
                                &(bpred->globHist),
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_LVT_size,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_instShiftAmt,
                                &(bpred->phist),
                                params->v_counterWidth,
                                probability);
                predictor = DVTAGE_VTAGE;
        } else if(params->v_predType == "diff-vtage") {
                diffVTAGE = new DiffVTAGE(params->name,
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_LVT_size,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_counterWidth,
                                params->v_instShiftAmt,
                                probability,
                                &(bpred->globHist),
                                &(bpred->phist),
                                bpred);
                predictor = DIFFVTAGE;
        } else if(params->v_predType == "stride") {
                strideVP = new StrideVP(params->name,
                                params->v_size_stride,
                                params->v_tag_size_stride,
                                params->v_associativity_stride,
                                params->v_counterWidth,
                                probability);
                std::cerr << "Using a stride predictor" << std::endl;
                predictor = Stride;
        } else if(params->v_predType == "fcm-spec") {
                fcmsVP = new FCM_SPEC_VP(params->name,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                params->v_counterWidth,
                                probability,
                                params->v_associativity_fcm);
                predictor = FCMSPEC;
        } else if(params->v_predType == "diff-fcm") {
                diffFCM = new DiffFCM(params->name,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                params->v_counterWidth,
                                probability,
                                params->v_associativity_fcm);
                predictor = DIFFFCM;
        } else if(params->v_predType == "ps") {
                psVP = new PSVP(params->name,
                                params-> v_size_vht_ps,
                                params->v_tag_vht_ps,
                                params->v_size_SHT_ps,
                                params->v_tag_sht_ps,
                                params->v_bhist_width_ps,
                                params->v_counterWidth,
                                probability,
                                params->v_associativity_ps_vht,
                                params->v_associativity_ps_sht,
                                &(bpred->globHist));
                predictor = PS;
        } else if(params->v_predType == "lvp") {
                lVP = new LastVP(params->name,
                                params->v_size_vht_lvp,
                                params->v_tag_vht_lvp,
                                params->v_counterWidth,
                                probability,
                                params->v_associativity_lvp
                );
                predictor = LVP;
        } else if(params->v_predType == "fcm-stride") {
                fcm_stride = new FCM_Stride(params->name,
                                params->v_size_stride,
                                params->v_tag_size_stride,
                                params->v_associativity_stride,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                params->v_counterWidth,
                                probability,
                                probability,
                                params->v_associativity_fcm);
                predictor = FCMSTRIDE;
        } else if(params->v_predType == "fcm-ps") {
                fcm_ps = new FCM_PS(params->name,
                                params->v_size_vht_ps,
                                params->v_size_SHT_ps,
                                params->v_associativity_ps_vht,
                                params->v_associativity_ps_sht,
                                params->v_tag_vht_ps,
                                params->v_tag_sht_ps,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                &(bpred->globHist),
                                params->v_bhist_width_ps,
                                params->v_counterWidth,
                                probability,
                                probability,
                                params->v_associativity_fcm);
                predictor = FCMPS;
        } else if(params->v_predType == "vtage-ps") {
                vtage_ps = new VTAGE_PS(params->name,
                                params->v_size_vht_ps,
                                params->v_size_SHT_ps,
                                params->v_tag_vht_ps,
                                params->v_tag_sht_ps,
                                params->v_bhist_width_ps,
                                params->v_associativity_ps_vht,
                                params->v_associativity_ps_sht,
                                &(bpred->globHist),
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_instShiftAmt,
                                &(bpred->phist),
                                params->v_counterWidth,
                                probability,
                                probability);
                predictor = VTAGEPS;
        } else if(params->v_predType == "vtage-stride") {
                vtage_stride = new VTAGE_Stride(params->name,
                                params->v_size_vht_ps,
                                params->v_tag_vht_ps,
                                params->v_associativity_stride,
                                &(bpred->globHist),
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_instShiftAmt,
                                &(bpred->phist),
                                params->v_counterWidth,
                                probability,
                                probability);
                predictor = VTAGESTRIDE;
        } else if(params->v_predType == "vtage-fcm") {
                vtage_fcm = new VTAGE_FCM(params->name,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                &(bpred->globHist),
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_instShiftAmt,
                                &(bpred->phist),
                                params->v_counterWidth,
                                probability,
                                probability,
                                params->v_associativity_fcm);
                predictor = VTAGEFCM;
        } else if(params->v_predType == "fcm-vtage-stride") {
                fcm_vtage_stride = new FCM_VTAGE_Stride(params->name,
                                params->v_size_stride,
                                params->v_tag_size_stride,
                                probability,
                                params->v_associativity_stride,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                probability,
                                params->v_associativity_fcm,
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_instShiftAmt,
                                &(bpred->globHist),
                                &(bpred->phist),
                                probability,
                                params->v_counterWidth);
                predictor = FCM_VTAGE_STRIDE;
        } else if(params->v_predType == "fcm-vtage-ps") {
                fcm_vtage_ps= new VTAGE_FCM_PS(params->name,
                                params->v_size_vht_ps,
                                params->v_size_SHT_ps,
                                params->v_tag_vht_ps,
                                params->v_tag_sht_ps,
                                params->v_bhist_width_ps,
                                probability,
                                params->v_associativity_ps_vht,
                                params->v_associativity_ps_sht,
                                params->v_size_vht_fcm,
                                params->v_size_ht_fcm,
                                params->v_tagWidth_fcm,
                                params->v_lhist_fcm,
                                probability,
                                params->v_associativity_fcm,
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_instShiftAmt,
                                &(bpred->globHist),
                                &(bpred->phist),
                                probability,
                                params->v_counterWidth);
                predictor = FCM_VTAGE_PS;
        } else if(params->v_predType == "gdiff") {
                gdiff = new gDiffVP(params->name,
                                params->v_order_gdiff,
                                params->v_size_gdiff,
                                params->v_tag_size_gdiff,
                                params->v_associativity_gdiff,
                                params->v_counterWidth,
                                probability,
                                params,
                                &(bpred->globHist),
                                &(bpred->phist),
                                bpred);
                predictor = gDiff;
        } else if(params->v_predType == "none") {
                //To disable value prediction
                predictor = None;
        } else {
                fatal("Invalid VP selected!");
        }
  }

template <class Impl>
void
VPredUnit<Impl>::regStats()
{
        lookups
        .name(name() + ".lookups")
        .desc("Number of BP lookups")
        ;

        squashDueToValueMispred
        .name(name() + ".squashDueToValueMispred")
        .desc("Number of squashes due to a value misprediction")
        ;

        squashDueToBranchMispred
        .name(name() + ".squashDueToBranchMispred")
        .desc("Number of squashes due to a branch misprediction")
        ;

        squashDueToOther
        .name(name() + ".squashDueToOther")
        .desc("Number of squashes due to a misc (memory order) misprediction")
        ;

        /*vpred_reads
        .init(0, 16, 1)
        .name(name() + ".vpredReads")
        .desc("Number of reads every predictor cycles")
        ;

    vpred_optimized_writes
    .init(0, 8, 1)
        .name(name() + ".vpredOptWrites")
        .desc("Number of writes when we remove useless ones")
        ;
    vpred_fetch_reads
    .init(0, 8, 1)
    .name(name() + ".vpredFetchReads")
    .desc("Number of fetch reads every predictor cycles")
    ;

    vpred_writes
        .init(0, 8, 1)
        .name(name() + ".vpredWrites")
        .desc("Number of writes every predictor cycles")
        ;

    vpred_commit_reads
          .init(0, 8, 1)
          .name(name() + ".vpredCommitReads")
          .desc("Number of commit reads every predictor cycles")
          ;
         */

        if (predictor == VTAGE) {
                vtageVP->regStats();
        } else if(predictor == DVTAGE_ALT) {
                dvtage_alt->regStats();
        } else if(predictor == DVTAGE_VTAGE) {
                dvtage_vtage->regStats();
        } else if(predictor == DIFFVTAGE) {
                diffVTAGE->regStats();
        } else if(predictor == Stride) {
                strideVP->regStats();
        } else if(predictor == PS) {
                psVP->regStats();
        } else if(predictor == LVP) {
                lVP->regStats();
        } else if(predictor == FCMSPEC) {
                fcmsVP->regStats();
        } else if(predictor == DIFFFCM) {
                diffFCM->regStats();
        } else if(predictor == FCMSTRIDE) {
                fcm_stride->regStats();
        } else if(predictor == FCMPS) {
                fcm_ps->regStats();
        } else if(predictor == VTAGEPS) {
                vtage_ps->regStats();
        } else if(predictor == VTAGESTRIDE) {
                vtage_stride->regStats();
        } else if(predictor == VTAGEFCM) {
                vtage_fcm->regStats();
        } else if(predictor == FCM_VTAGE_STRIDE) {
                fcm_vtage_stride->regStats();
        } else if(predictor == FCM_VTAGE_PS) {
                fcm_vtage_ps->regStats();
        } else if(predictor == gDiff) {
                gdiff->regStats();
        } else if(predictor == None) {
                //Nothing to do
        } else {
                panic("Unexpected value predictor type");
        }
}

template <class Impl>
void
VPredUnit<Impl>::switchOut()
{
        // Clear any state upon switch out.
        for (int i = 0; i < Impl::MaxThreads; ++i) {
                squash(0, i);
        }
}

template <class Impl>
void
VPredUnit<Impl>::takeOverFrom()
{
}

template <class Impl>
prediction_t
VPredUnit<Impl>::predict(DynInstPtr &inst, TheISA::PCState &pc, ThreadID tid)
{
        if(predictor == None || predictor == gDiff) {
                return prediction_t(Prediction(),0);
        }

        prediction_t pred;

        ++lookups;

        void *vp_history = NULL;
        pred = VPLookup(pc.instAddr(), pc.microPC(), vp_history, inst->seqNum);


        DPRINTF(ValuePredictor, "ValuePred: [tid:%i]: [sn:%lu] Creating value prediction history for PC %s, predicted %s with confidence %u\n",
                        tid, inst->seqNum, inst->pcState(), pred.first.tostring(), pred.second);

        PredictorVPHistory predict_record(inst->seqNum, pc.instAddr() ^ pc.microPC(),
                        vp_history, tid, false, inst);

        predict_record.pred = pred;
        predict_record.inst = inst;

        vpredHist[tid].push_back(predict_record);

        DPRINTF(ValuePredictor, "Pushed a new value history(value) because of inst [sn:%lu], %lu predictions in flight\n", inst->seqNum, vpredHist[tid].size());

        assert(pred.second >= 0 && pred.second <= 7);
        return pred;
}

template <class Impl>
prediction_t
VPredUnit<Impl>::predictAtDispatch(DynInstPtr &inst, TheISA::PCState &pc, ThreadID tid)
{
        if(predictor != gDiff) {
                return prediction_t(Prediction(),0);
        }

        prediction_t pred;

        ++lookups;

        void *vp_history = NULL;

        DPRINTF(ValuePredictor, "Value pred: trying to predict for inst [sn:%lu]\n", inst->seqNum);

        pred = VPLookup(pc.instAddr(), pc.microPC(), vp_history, inst->seqNum);


        DPRINTF(ValuePredictor, "ValuePred: [tid:%i]: [sn:%lu] Creating value prediction history for PC %s, predicted %s with confidence %u\n",
                        tid, inst->seqNum, inst->pcState(), pred.first.tostring(), pred.second);

        PredictorVPHistory predict_record(inst->seqNum, (pc.instAddr() << 4) ^ pc.microPC(),
                        vp_history, tid, false, inst);
        predict_record.pred = pred;
        predict_record.inst = inst;

        //TODO Take care to push/pop correctly
        vpredHist[tid].push_back(predict_record);
        DPRINTF(ValuePredictor, "Pushed a new value history(value) because of inst [sn:%lu], %lu predictions in flight\n", inst->seqNum, vpredHist[tid].size());

        assert(pred.second >= 0 && pred.second <= 7);

        std::sort(vpredHist[tid].begin(), vpredHist[tid].end(), VPredUnit::vpcompare);
        assert(vpredHist[tid].size() < 256);
        return pred;
}


template <class Impl>
void
VPredUnit<Impl>::VPSquash(void *vp_history, bool remove, bool recompute)
{
        if (predictor == VTAGE) {
                vtageVP->squash(vp_history, remove, recompute);
        } else if(predictor == DVTAGE_ALT) {
                dvtage_alt->squash(vp_history, remove, recompute);
        } else if(predictor == DVTAGE_VTAGE) {
                dvtage_vtage->squash(vp_history, remove, recompute);
        } else if(predictor == DIFFVTAGE) {
                diffVTAGE->squash(vp_history, remove, recompute);
        } else if(predictor == Stride) {
                strideVP->squash(vp_history, remove);
        } else if(predictor == PS) {
                psVP->squash(vp_history, remove);
        } else if(predictor == LVP) {
                lVP->squash(vp_history, remove);
        } else if(predictor == FCMSPEC) {
                fcmsVP->squash(vp_history, remove);
        } else if(predictor == DIFFFCM) {
                diffFCM->squash(vp_history, remove);
        } else if(predictor == FCMSTRIDE) {
                fcm_stride->squash(vp_history, remove);
        } else if(predictor == FCMPS) {
                fcm_ps->squash(vp_history, remove);
        }  else if(predictor == VTAGEPS) {
                vtage_ps->squash(vp_history, remove, recompute);
        } else if(predictor == VTAGESTRIDE) {
                vtage_stride->squash(vp_history, remove, recompute);
        } else if(predictor == VTAGEFCM) {
                vtage_fcm->squash(vp_history, remove, recompute);
        } else if(predictor == FCM_VTAGE_STRIDE) {
                fcm_vtage_stride->squash(vp_history, remove, recompute);
        } else if(predictor == FCM_VTAGE_PS) {
                fcm_vtage_ps->squash(vp_history, remove, recompute);
        } else if(predictor == gDiff) {
                gdiff->squash(vp_history, remove, recompute);
        } else if(predictor == None) {
        } else {
                panic("Predictor type is unexpected value!");
        }
}

template <class Impl>
void
VPredUnit<Impl>::squash(const InstSeqNum &squashed_sn, ThreadID tid)
{

        if(predictor == None) {
                return;
        }


        VPHistory &vpred_hist = vpredHist[tid];
        DPRINTF(ValuePredictor, "Squashing the value predictor history because instruction [sn:%lu] faulted (Other mispred)\n", squashed_sn);

        //Squash all histories after the considered faulting instruction.
        while (!vpred_hist.empty() && vpred_hist.back().seqNum > squashed_sn) {
                DPRINTF(ValuePredictor, "Popped a value history(squash): [sn:%lu] because of inst: [sn:%lu], %lu predictions in flight\n", vpred_hist.back().seqNum, squashed_sn, vpredHist[tid].size());
                // This call should delete the vpHistory.
                VPSquash(vpred_hist.back().vpHistory, true, false);
                vpred_hist.pop_back();

        }

        //We removed younger (than the faulting instruction) state. It is possible that we have a history for the
        //faulting instruction itself. If not then we have nothing to do. If yes, two cases:
        //1 - A branch faulted and we should restore the histories in TAGE-like predictors.
        //2 - Another instruction faulted but we still have state associated to it (in theory).
        if(!vpred_hist.empty() && vpred_hist.back().seqNum == squashed_sn)
        {
                if(vpred_hist.back().isBranch) {
                        DPRINTF(ValuePredictor, "The faulting inst is a branch -> inst: [sn:%lu] when squashing because of inst: [sn:%lu]\n", vpred_hist.back().seqNum, squashed_sn);
                        VPSquash(vpred_hist.back().vpHistory, false, true);
                        squashDueToBranchMispred++;
                } else {
                        DPRINTF(ValuePredictor, "The faulting inst is misc -> inst: [sn:%lu], %lu predictions in flight\n", squashed_sn, vpredHist[tid].size());
                        VPSquash(vpred_hist.back().vpHistory, false, false);
                        squashDueToOther++;
                }

        }
        DPRINTF(ValuePredictor, "Update on squash: Popping the actual result from commit for inst [sn:%lu], size of VPUnit hist is %u\n", squashed_sn,
                        vpred_hist.size());

}

template <class Impl>
void
VPredUnit<Impl>::squash(const InstSeqNum &squashed_sn, std::list<VPUpdateInfo> &actual_result, ThreadID tid)
{
        if(predictor == None) {
                return;
        }

        /** Might be some pending updates to do since we squash at commit **/
        bufferResult(actual_result);

        VPHistory &vpred_hist = vpredHist[tid];
        DPRINTF(ValuePredictor, "Squashing the value predictor history because instruction [sn:%lu] faulted (value mispred)\n", squashed_sn);
        unsigned i = 0;

        //Squash all histories after the considered faulting instruction.
        while (!vpred_hist.empty() && vpred_hist.back().seqNum > squashed_sn) {
                // This call should delete the vpHistory.
                VPSquash(vpred_hist.back().vpHistory, true, false);

                DPRINTF(ValuePredictor, "Popped a value history(squash): [sn:%lu] because of inst: [sn:%lu], %lu predictions in flight\n", vpred_hist.back().seqNum, squashed_sn, vpredHist[tid].size());

                vpred_hist.pop_back();

                i++;
        }


        //We removed younger (than the faulting instruction) state. It is possible that we have a history for the
        //faulting instruction itself. If not then we have nothing to do. If yes, two cases:
        //1 - A branch faulted and we should restore the histories in TAGE-like predictors.
        //2 - Another instruction faulted but we still have state associated to it (in theory).
        if(!vpred_hist.empty() && vpred_hist.back().seqNum == squashed_sn)
        {
                if(vpred_hist.back().isBranch) {
                        DPRINTF(ValuePredictor, "The faulting inst is a branch -> inst: [sn:%lu] when squashing because of inst: [sn:%lu]\n", vpred_hist.back().seqNum, squashed_sn);
                        VPSquash(vpred_hist.back().vpHistory, false, true);
                        squashDueToBranchMispred++;
                }
        }

        DPRINTF(ValuePredictor, "Update on squash: Popping the actual result from commit for inst [sn:%lu], size of commit"
                        " hist is %u, size of VPUnit hist is %u\n", squashed_sn, updateBuffer.size(), vpred_hist.size());
        DPRINTF(ValuePredictor, "The faulting inst is a value mispred -> inst: [sn:%lu], with [sn:%lu] is the last inst in history\n", squashed_sn, vpred_hist.back().seqNum);


        squashDueToValueMispred++;


        if(vpred_hist.size() != 0) {
                DPRINTF(ValuePredictor, "Oldest inst in VPUnit hist is [sn:%lu]\n", vpred_hist.front().seqNum);
        }
        if(updateBuffer.size() != 0) {
                DPRINTF(ValuePredictor, "Oldest inst in commit info is [sn:%lu]\n", updateBuffer.front().first);
        }


}


template <class Impl>
prediction_t
VPredUnit<Impl>::VPLookup(Addr instPC, MicroPC micropc, void * &vp_history, uint64_t seqnum)
{
        first_update = true;
        if(predictor == VTAGE) {
                return vtageVP->lookup(instPC, micropc, vp_history, seqnum);
        } else if(predictor == DVTAGE_ALT) {
                return dvtage_alt->lookup(instPC, micropc, vp_history);
        } else if(predictor == DVTAGE_VTAGE) {
                return dvtage_vtage->lookup(instPC, micropc, vp_history);
        } else if(predictor == DIFFVTAGE) {
                return diffVTAGE->lookup(instPC, micropc, vp_history);
        } else if(predictor == Stride) {
                return strideVP->lookup(instPC, micropc, vp_history);
        } else if(predictor == PS) {
                return psVP->lookup(instPC, micropc, vp_history);
        } else if(predictor == LVP) {
                return lVP->lookup(instPC, micropc, vp_history);
        } else if(predictor == FCMSPEC) {
                return fcmsVP->lookup(instPC, micropc, vp_history);
        } else if(predictor == DIFFFCM) {
                return diffFCM->lookup(instPC, micropc, vp_history);
        } else if(predictor == FCMSTRIDE) {
                return fcm_stride->lookup(instPC, micropc, vp_history);
        } else if(predictor == FCMPS) {
                return fcm_ps->lookup(instPC, micropc, vp_history);
        }  else if(predictor == VTAGEPS) {
                return vtage_ps->lookup(instPC, micropc, vp_history);
        } else if(predictor == VTAGESTRIDE) {
                return vtage_stride->lookup(instPC, micropc, vp_history);
        } else if(predictor == VTAGEFCM) {
                return vtage_fcm->lookup(instPC, micropc, vp_history);
        } else if(predictor == FCM_VTAGE_STRIDE) {
                return fcm_vtage_stride->lookup(instPC, micropc, vp_history);
        } else if(predictor == FCM_VTAGE_PS) {
                return fcm_vtage_ps->lookup(instPC, micropc,vp_history);
        } else if(predictor == gDiff) {
                return gdiff->lookup(instPC, micropc, vp_history, seqnum);
        } else if(predictor == None) {
                return prediction_t(Prediction(),0);
        } else {
                panic("Predictor type is unexpected value!");
        }
}

template <class Impl>
void
VPredUnit<Impl>::update(const InstSeqNum &sn, std::list<VPUpdateInfo> &actual_result, ThreadID tid)
{
        if(predictor == None) {
                return;
        }

        //May have several updates to do, buffer them.
        //This is probably useless since we cannot lose results as we can do as many updates as we want each cycle.
        //Keep it in case we want to change that.
        bufferResult(actual_result);

        DPRINTF(ValuePredictor, "Committing all results until [sn:%lu]\n", sn);


        //Remove the histories corresponding to branches (where no gI and gTag have been saved).
        while(!vpredHist[tid].empty() && vpredHist[tid].front().seqNum <= sn)
        {
                //We only save histories for branch in VTAGE, hence.
                if(predictor == VTAGE && vpredHist[tid].front().isBranch) {
                        vtageVP->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == DVTAGE_ALT &&  vpredHist[tid].front().isBranch) {
                        dvtage_alt->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == DVTAGE_VTAGE && vpredHist[tid].front().isBranch) {
                        dvtage_vtage->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == DIFFVTAGE && vpredHist[tid].front().isBranch) {
                        diffVTAGE->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == VTAGEPS && vpredHist[tid].front().isBranch) {
                        vtage_ps->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == VTAGESTRIDE && vpredHist[tid].front().isBranch) {
                        vtage_stride->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == VTAGEFCM && vpredHist[tid].front().isBranch) {
                        vtage_fcm->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                }
                else if(predictor == FCM_VTAGE_STRIDE && vpredHist[tid].front().isBranch) {
                        fcm_vtage_stride->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == FCM_VTAGE_PS && vpredHist[tid].front().isBranch) {
                        fcm_vtage_ps->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else if(predictor == gDiff && vpredHist[tid].front().isBranch) {
                        gdiff->flush_branch(vpredHist[tid].front().vpHistory);
                        DPRINTF(ValuePredictor, "Popped a value history(flush_branch): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum,  vpredHist[tid].size());
                        vpredHist[tid].pop_front();
                } else {
                                //This is a predictable instruction, update the predictor.
                        DPRINTF(ValuePredictor, "Update: committing inst [sn:%lu]\n", vpredHist[tid].front().seqNum);

                        assert(!updateBuffer.empty());
                        assert(updateBuffer.front().first == vpredHist[tid].front().seqNum);

                        DPRINTF(ValuePredictor, "Update: Popping the actual result from commit for inst [sn:%lu](should be [sn:%lu])\n", vpredHist[tid].front().seqNum,
                                        updateBuffer.front().first);

                        if(vpredHist[tid].front().pred.first == updateBuffer.front().second && vpredHist[tid].front().pred.second == 7) {
                                correct_distribution[vpredHist[tid].front().pc]++;
                                if(disassembly.find(vpredHist[tid].front().pc) == disassembly.end()) {
                                        disassembly[vpredHist[tid].front().pc] = vpredHist[tid].front().inst->staticInst->disassemble(vpredHist[tid].front().pc);
                                }
                        }

                        this->UpdateVHist(vpredHist[tid].front().seqNum, updateBuffer.front().second, false, tid, vpredHist[tid].front().inst);
                        updateBuffer.pop_front();
                }
        }

        //Dumping statistics on cerr at the end.
        if(cpu->commit.instsCommitted[tid].value() > (inst_to_execute - 5000) && !dumped) {
                std::cerr << "Dumping" << std::endl;
                dumpCorrect();
                dumped = true;
        }
}

template <class Impl>
void
VPredUnit<Impl>::updateAtWB(const InstSeqNum &sn, Prediction &value, ThreadID tid)
{
        if(predictor != gDiff) {
                return;
        }

        gdiff->updateGHistWB(sn, value);

}


template <class Impl>
void
VPredUnit<Impl>::UpdateVHist(const InstSeqNum &sn, Prediction value, bool squashed, ThreadID tid, DynInstPtr inst)
{
        if(predictor == None) {
                return;
        }
        assert(!squashed);
        DPRINTF(ValuePredictor, "Updating Value Predictor for inst [sn:%lu]. Squashed %u. %u values in VPUnit history\n", sn, squashed, vpredHist[tid].size());

        //Remove the histories corresponding to branches (where no gI and gTag have been saved).
        //TODO Ensure that the corresponding branch has effectively been commited.
        if(!vpredHist[tid].empty()) {

                assert((!squashed && vpredHist[tid].front().seqNum == sn) || (squashed && vpredHist[tid].back().seqNum == sn));

                if(predictor == VTAGE) {
                        vtageVP->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == DVTAGE_ALT) {
                        dvtage_alt->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == DVTAGE_VTAGE) {
                        dvtage_vtage->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == DIFFVTAGE) {
                        diffVTAGE->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == Stride) {
                        strideVP->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == PS) {
                        psVP->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == LVP) {
                        lVP->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == FCMSPEC) {
                        fcmsVP->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == DIFFFCM) {
                        diffFCM->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == FCMSTRIDE) {
                        fcm_stride->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == FCMPS) {
                        fcm_ps->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == VTAGEPS) {
                        vtage_ps->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == VTAGESTRIDE) {
                        vtage_stride->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, inst->predIsWrong(), squashed);
                } else if(predictor == VTAGEFCM) {
                        vtage_fcm->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == FCM_VTAGE_STRIDE) {
                        fcm_vtage_stride->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                }  else if(predictor == FCM_VTAGE_PS) {
                        fcm_vtage_ps->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else if(predictor == gDiff) {
                        gdiff->update(value, squashed ? vpredHist[tid].back().vpHistory : vpredHist[tid].front().vpHistory, squashed);
                } else {
                        panic("Unexpected type of predictor");
                }

                DPRINTF(ValuePredictor, "Popped a value history(value_update): [sn:%lu], %lu predictions in flight\n", vpredHist[tid].front().seqNum, vpredHist[tid].size());
                if(!squashed) {
                        vpredHist[tid].pop_front();
                } else {
                        //vpredHist[tid].pop_back();
                }

        } else {
                DPRINTF(ValuePredictor, "History is empty. Correctness?");
        }
                }

template <class Impl>
void
VPredUnit<Impl>::UpdateBHist(DynInstPtr &inst, TheISA::PCState &pc, ThreadID tid)
{
        if(predictor == None) {
                return;
        }

        if(predictor == VTAGE)
        {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                vtageVP->updateFoldedHist(true, vp_history, inst->seqNum);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == DVTAGE_ALT) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                dvtage_alt->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == DVTAGE_VTAGE) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                dvtage_vtage->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == DIFFVTAGE) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                diffVTAGE->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == PS) {
                //Nothing to do here (No state depending on the history like the folded hist in VTAGE)
        } else if(predictor == Stride) {
                //Nothing to do here
        } else if(predictor == LVP) {
                //Nothing to do here
        } else if(predictor == FCMSPEC || predictor == DIFFFCM) {
                //Still nothing to do here.
        } else if(predictor == FCMSTRIDE) {
                //Same
        } else if(predictor == FCMPS) {
                //Same
        } else if(predictor == gDiff) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                gdiff->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == VTAGEPS) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                vtage_ps->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == VTAGESTRIDE) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                vtage_stride->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == VTAGEFCM) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                vtage_fcm->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == FCM_VTAGE_STRIDE) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                fcm_vtage_stride->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else if(predictor == FCM_VTAGE_PS) {
                DPRINTF(ValuePredictor, "Calling for a branch history update because a branch was predicted\n");
                void *vp_history = NULL;
                DPRINTF(ValuePredictor, "vp_history created (0x%x)\n", vp_history);
                fcm_vtage_ps->updateFoldedHist(true, vp_history);
                DPRINTF(ValuePredictor, "Allocated a vp_history at 0x%x\n", vp_history);
                PredictorVPHistory predict_record(inst->seqNum, pc.instAddr(),
                                vp_history, tid, true, inst);
                vpredHist[tid].push_back(predict_record);
                DPRINTF(ValuePredictor, "Pushed a new value history(branch) because of inst [sn:%lu], size is: %lu\n", inst->seqNum, vpredHist[tid].size());
        } else {
                panic("Predictor type is unexpected value!");
        }
}

template <class Impl>
void
VPredUnit<Impl>::bufferResult(std::list<VPUpdateInfo> &actual_result)
{
        if(predictor == None) {
                return;
        }
        unsigned size = actual_result.size();
        for(unsigned i = 0; i < size; i++)
        {
                updateBuffer.push_back(actual_result.front());
                actual_result.pop_front();
        }
        assert(actual_result.empty());
}

template <class Impl>
void
VPredUnit<Impl>::dumpCorrect() {

        unsigned total = 0;
        for(auto it = correct_distribution.begin(); it != correct_distribution.end(); ++it) {
                total += it->second;
        }

        std::multimap<double, Addr> sorted;

        for(auto it = correct_distribution.begin(); it != correct_distribution.end(); ++it) {
                sorted.insert(std::pair<double, Addr>((double) (((double) it->second / (double) total) * 100.0l), it->first));
        }

        for(auto it = sorted.rbegin(); it != sorted.rend(); ++it) {
                std::cerr << std::hex << it->second << " : " << disassembly[it->second];
                std::cerr << " : " << std::dec << correct_distribution[it->second] << " (" << it->first << "%)" << std::endl;
        }

}
//FIXME Implement
template <class Impl>
void
VPredUnit<Impl>::dump()
{
        //     HistoryIt pred_hist_it;
        //
        //     for (int i = 0; i < Impl::MaxThreads; ++i) {
        //         if (!predHist[i].empty()) {
        //             pred_hist_it = predHist[i].begin();
        //
        //             cprintf("predHist[%i].size(): %i\n", i, predHist[i].size());
        //
        //             while (pred_hist_it != predHist[i].end()) {
        //                 cprintf("[sn:%lli], PC:%#x, tid:%i, predTaken:%i, "
        //                         "bpHistory:%#x\n",
        //                         pred_hist_it->seqNum, pred_hist_it->pc,
        //                         pred_hist_it->tid, pred_hist_it->predTaken,
        //                         pred_hist_it->bpHistory);
        //                 pred_hist_it++;
        //             }
        //
        //             cprintf("\n");
        //         }
        //     }
}
