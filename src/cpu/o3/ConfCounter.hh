/*
 * ConfCounter.hh
 *
 *  Created on: Mar 13, 2013
 *      Author: aperais
 */

#ifndef CONFCOUNTER_HH_
#define CONFCOUNTER_HH_


#include <cmath>
#include <iostream>

#include "base/misc.hh"
#include "base/types.hh"

/**
 * Private counter class for the internal saturating counters.
 * Implements an n bit saturating counter and provides methods to
 * increment, decrement, and read it.
 * @todo Consider making this something that more closely mimics a
 * built in class so you can use ++ or --.
 */
class ConfCounter
{
  public:
    /**
     * Constructor for the counter.
     */
    ConfCounter()
        : initialVal(0), counter(0), proba(NULL)
    { }

    /**
     * Constructor for the counter.
     * @param bits How many bits the counter will have.
     */
    ConfCounter(unsigned bits)
        : initialVal(0), maxVal((1 << bits) - 1), counter(0), proba(NULL)
    { }

    /**
     * Constructor for the counter.
     * @param bits How many bits the counter will have.
     * @param initial_val Starting value for each counter.
     */
    ConfCounter(unsigned bits, unsigned initial_val, std::vector<unsigned> *proba)
        : initialVal(initial_val), maxVal((1 << bits) - 1),
          counter(initial_val)
    {
        // Check to make sure initial value doesn't exceed the max
        // counter value.
        this->proba = proba;
        if (initial_val > maxVal) {
            fatal("BP: Initial counter value (%i) exceeds max size (%i).", initialVal, maxVal);
        }
    }

    /**
     * Sets the number of bits.
     */
    void setBits(unsigned bits) {maxVal = (1 << bits) - 1; }

    void set(unsigned val) {
                counter = val;
    }

    void reset() { counter = initialVal; }

    /**
     * Increments the counter's current value.
     */
    void increment()
    {
        auto &proba_ref = *proba;
        if (counter < maxVal) {
                if((rand() & (proba_ref[counter] - 1)) == 0) {
                        ++counter;
                }
        }
    }

    /**
     * Decrements the counter's current value.
     */
    void decrement()
    {
        if (counter > initialVal) {
            --counter;
        }
    }

    void updateConf(bool outcome)
    {
        if(outcome) {
                this->increment();
        } else {
#ifdef RESET_COUNTERS
                this->set(0);
#else
                this->decrement();
#endif
        }
    }

    /**
     * Read the counter's value.
     */
    const unsigned read() const
    { return counter; }

    /** True if the counter is saturated */
    const bool saturated() const
    { return (counter == initialVal) || (counter == maxVal); }

    const bool nearlyHi() const
    { return (counter == maxVal - 1); }

  private:
    unsigned initialVal;
    unsigned maxVal;
    unsigned counter;
    std::vector<unsigned> *proba;
};

#endif /* CONFCOUNTER_HH_ */
