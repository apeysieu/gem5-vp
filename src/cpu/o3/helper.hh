/*
 * helper.cc
 *
 *  Created on: Nov 15, 2012
 *      Author: aperais
 */

#ifndef HELPER_CC_
#define HELPER_CC_

#include <cstdio>
#include <string>
#include <vector>

#include "base/types.hh"

static inline Addr createUPCindex(Addr upc, unsigned length) {

//        Addr result = 0;
//        Addr tmp_upc = upc;
//        unsigned tmp = result;
//        for(int i = 0; i < length ; i++) {
//                tmp = tmp_upc & 0x1;
//                result <<= 1;
//                tmp_upc >>= 1;
//                if(tmp) {
//                        result ^= 0x1;
//                }
//
//        }
//        //printf("Reverted 0x%lx to 0x%lx\n", upc, result);
//        return result /*& ((1 << length) - 1)*/;
                return upc & ((1 << length) - 1);

}

__attribute__ ((unused)) static inline void split(const std::string &s, char delim, std::vector<std::string> & elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
          elems.push_back(item);
    }
}


template <typename T>
static unsigned findWay(std::vector<std::vector<T> > &ways, Addr tag, Addr index) {
        for(unsigned i = 0; i < ways.size(); i++) {
                if(ways[i][index].tagMatches(tag)) {
                        return i;
                }
        }
        return ways.size();
}

template <typename T>
static unsigned findLRU(std::vector<std::vector<T> > &ways, Addr index) {
        for(unsigned i = 0; i < ways.size(); i++) {
                if(ways[i][index].getLru() == 0) {
                        return i;
                }
        }
        assert(false);
        return ways.size();
}

template <typename T>
static void updateLRUInfo(std::vector<std::vector<T> > &ways, unsigned threshold, unsigned mru, Addr index) {
        for(unsigned i = 0; i < ways.size(); i++) {
                if(ways[i][index].getLru() > threshold && i != mru) {
                        ways[i][index].decLru();
                }
        }
}





#endif /* HELPER_CC_ */
