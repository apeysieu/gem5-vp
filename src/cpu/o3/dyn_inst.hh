/*
 * Copyright (c) 2010 ARM Limited
 * Copyright (c) 2013 Advanced Micro Devices, Inc.
 * All rights reserved
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2006 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Kevin Lim
 */

#ifndef __CPU_O3_DYN_INST_HH__
#define __CPU_O3_DYN_INST_HH__

#include <array>

#include "arch/isa_traits.hh"
#include "config/the_isa.hh"
#include "cpu/o3/cpu.hh"
#include "cpu/o3/isa_specific.hh"
#include "cpu/base_dyn_inst.hh"
#include "cpu/inst_seq.hh"
#include "cpu/reg_class.hh"
#include "debug/ValuePredictor.hh"

class Packet;

template <class Impl>
class BaseO3DynInst : public BaseDynInst<Impl>
{
  public:
    /** Typedef for the CPU. */
    typedef typename Impl::O3CPU O3CPU;

    /** Binary machine instruction type. */
    typedef TheISA::MachInst MachInst;
    /** Extended machine instruction type. */
    typedef TheISA::ExtMachInst ExtMachInst;
    /** Logical register index type. */
    typedef TheISA::RegIndex RegIndex;
    /** Integer register index type. */
    typedef TheISA::IntReg   IntReg;
    typedef TheISA::FloatReg FloatReg;
    typedef TheISA::FloatRegBits FloatRegBits;
    typedef TheISA::CCReg   CCReg;

    /** Misc register index type. */
    typedef TheISA::MiscReg  MiscReg;

    enum {
        MaxInstSrcRegs = TheISA::MaxInstSrcRegs,        //< Max source regs
        MaxInstDestRegs = TheISA::MaxInstDestRegs       //< Max dest regs
    };

  public:
    /** BaseDynInst constructor given a binary instruction. */
    BaseO3DynInst(const StaticInstPtr &staticInst, const StaticInstPtr &macroop,
                  TheISA::PCState pc, TheISA::PCState predPC,
                  InstSeqNum seq_num, O3CPU *cpu);

    /** BaseDynInst constructor given a static inst pointer. */
    BaseO3DynInst(const StaticInstPtr &_staticInst,
                  const StaticInstPtr &_macroop);

    ~BaseO3DynInst();

    /** Executes the instruction.*/
    Fault execute();

    /** Initiates the access.  Only valid for memory operations. */
    Fault initiateAcc();

    /** Completes the access.  Only valid for memory operations. */
    Fault completeAcc(PacketPtr pkt);

  private:
    /** Initializes variables. */
    void initVars();

  public:
    /** Values to be written to the destination misc. registers. */
    std::array<MiscReg, TheISA::MaxMiscDestRegs> _destMiscRegVal;


    Prediction prediction;

    std::deque<value_t> values;
    uint64_t predFlags [3];


  public:
    uint64_t issuedLoad, receivedData;
    bool L1miss, L2miss;

    unsigned doneInStage;
    Cycles dispatchCycle;




  protected:
    /** Indexes of the destination misc. registers. They are needed to defer
     * the write accesses to the misc. registers until the commit stage, when
     * the instruction is out of its speculative state.
     */
    std::array<short, TheISA::MaxMiscDestRegs> _destMiscRegIdx;

    /** Number of destination misc. registers. */
    uint8_t _numDestMiscRegs;

    std::bitset<64> dynFlags;

    enum DynInstFlags : long unsigned int {
        collapsed,	//Dummy inst in branch macroops
        validated,	//Has been validated by the validation stage
        usingSpec, //Read a speculative register
        ignoreSquash, //Ignore VP squash because predicted reg was not read before actual value was produced
        isPredicted, //Is value predicted
        wasReadSpec, //Predicted dest reg was read
        miss, //Cache miss
        useful, //Pred was read, kind of the same as was_read_spec
        vpMispred, //Value mispredicted (squash implied, unless pred was not read).
        predWrong, //Value pred is wrong but confidence was not high enough for it to be used, so no squash
        predictedFlags, //Flags were also predicted
        freeLimm, //Load Immediate that does not merge a previous copy of the register
        doneAtRename, //Can be early executed
        canBeLate, //Can be late executed
        isHighConfBranch, //High confidence branch
        forwarded
    };

    unsigned destination_size;


    /** Just a helper to split a string into a vector of tokens **/
    __attribute__ ((unused)) static inline void split(const std::string &s, char delim, std::vector<std::string> & elems) {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
              elems.push_back(item);
        }
    }

    /** Ideally this should be set in the ISA definition, but it's easier to do it here **/
    /** INT only, x86 only **/
    unsigned computeResultWidth() const
     {
         char delim = ' ';
         const std::string &s = this->staticInst->disassemble(this->instAddr());
         std::vector<std::string> tokens;
         split(s, delim, tokens);
         std::string dest;

         if(tokens.size() == 7) {
                 //Syscall and CPUID
                 dest = tokens.at(6);
         } else if(tokens.size() > 7) {
                 dest = tokens.at(7);
         } else {
                 return 0;
         }

         //microcode has temporary registers (t0/1/2)
         if(dest.find("t") != std::string::npos) {
                 if(dest.find("b") != std::string::npos) {
                         return 8;
                 } else if(dest.find("w") != std::string::npos) {
                         return 16;
                 } else if(dest.find("d") != std::string::npos) {
                         return 32;
                 } else {
                         return 64;
                 }
         } else {
                  //Regular x86 registers
                 if(dest.find("l") != std::string::npos || dest.find("h") != std::string::npos) {
                         return 8;
                 } else if(dest.find("r") != std::string::npos) {
                         if((dest.find("rdx") != std::string::npos) || (dest.find("rbx") != std::string::npos) || (dest.find("rbp") != std::string::npos) || (dest.find("rdi") != std::string::npos)) {
                                 return 64;
                         } else if(dest.find("b") != std::string::npos) {
                                 return 8;
                         } else if(dest.find("w") != std::string::npos) {
                                 return 16;
                         } else if(dest.find("d") != std::string::npos) {
                                 return 32;
                         } else {
                                 return 64;
                         }
                 } if(dest.find("e") != std::string::npos) {
                         return 32;
                 } else {
                         return 16;
                 }
         }
     }

    unsigned computeResultWidthFloat() const
    {
        char delim = ' ';
        const std::string &s = this->staticInst->disassemble(this->instAddr());
        std::vector<std::string> tokens;
        split(s, delim, tokens);

        if(tokens.size() > 7) {
                std::string dest = tokens.at(7);
        } else {
                return 0;
        }

        //Single precision scalar is 32 bits.
        if(s.find("SS_XMM") != std::string::npos) {
                return 32;
        } else {
                return 64;
        }
    }

  public:

#if TRACING_ON
    /** Tick records used for the pipeline activity viewer. */
    Tick fetchTick;	     // instruction fetch is completed.
    int32_t decodeTick;  // instruction enters decode phase
    int32_t renameTick;  // instruction enters rename phase
    int32_t dispatchTick;
    int32_t issueTick;
    int32_t completeTick;
    int32_t commitTick;
    int32_t storeTick;
#endif

    void setForwardedToLoad() {
        dynFlags.set(DynInstFlags::forwarded);
    }

    bool forwardedToLoad() const {
        return dynFlags.test(DynInstFlags::forwarded);
    }

    void setHighConfBranch() {
        dynFlags.set(DynInstFlags::isHighConfBranch);
    }

    bool highConfBranch() const {
        return dynFlags.test(DynInstFlags::isHighConfBranch);
    }

    void setCanBeLateExecuted() {
        dynFlags.set(DynInstFlags::canBeLate);
    }

    void clearCanBeLateExecuted() {
        dynFlags.reset(DynInstFlags::canBeLate);
    }

    bool canBeLateExecuted() const {
        return dynFlags.test(DynInstFlags::canBeLate);
    }

    void setEarlyExecuted() {
        dynFlags.set(DynInstFlags::doneAtRename);
    }

    bool isEarlyExecuted() {
        return dynFlags.test(DynInstFlags::doneAtRename);
    }

    bool isValidated() const {
        return dynFlags.test(DynInstFlags::validated);
    }

    void setValidated() {
        dynFlags.set(DynInstFlags::validated);
    }

    void setUsingPred() {
        dynFlags.set(DynInstFlags::usingSpec);
    }

    bool isUsingPred() const {
        return dynFlags.test(DynInstFlags::usingSpec);
    }

    bool ignoringSquash() const {
        return dynFlags.test(DynInstFlags::ignoreSquash);
    }

    void setFreeLimm() {
        dynFlags.set(DynInstFlags::freeLimm);
    }

    bool isFreeLimm() const {
        return dynFlags.test(DynInstFlags::freeLimm);
    }

    void setL1Miss() {
        dynFlags.set(DynInstFlags::miss);
    }

    bool isL1Miss() const {
        return dynFlags.test(DynInstFlags::miss);
    }

    unsigned getOperandSize() const {
        return destination_size;
    }

    bool isCollapsed() const {
        return dynFlags.test(DynInstFlags::collapsed);
    }

    void setCollapsed() {
        assert(!dynFlags.test(DynInstFlags::collapsed));
        dynFlags.set(DynInstFlags::collapsed);
    }

    //Get the actual value of the instruction (first dest reg)
    value_t getPredReg() {

        //First, look at the flags and mark the prediction as being wrong is the flags are wrong.
        //We could do it some other place, but getPredReg() is called at Commit, when the predictor is trained, so it works.
        for(unsigned i = 0; i < this->numDestRegs(); i++) {
                if(this->destRegIdx(i) == 184 && dynFlags.test(DynInstFlags::predictedFlags)) {
                        //Reg 184 is the architectural reg for some flags, including AF, but we don't care about AF, so mask it.
                        if(predFlags[0] != ((0xFFEF) & this->cpu->readCCReg(this->renamedDestRegIdx(i))))
                                dynFlags.set(DynInstFlags::predWrong);
                } else if(this->destRegIdx(i) == 185 && dynFlags.test(DynInstFlags::predictedFlags)) {
                        //Reg 185 contains some other flags (still user flags).
                        if(predFlags[1] != this->cpu->readCCReg(this->renamedDestRegIdx(i)))
                                dynFlags.set(DynInstFlags::predWrong);
                } else if(this->destRegIdx(i) >= 186 && this->destRegIdx(i) <= 188 && dynFlags.test(DynInstFlags::predictedFlags)) {
                        //Reg 186 contains the remaining flags (I think those are the user level microcode flags).
                        if(predFlags[2] != this->cpu->readCCReg(this->renamedDestRegIdx(i)))
                                dynFlags.set(DynInstFlags::predWrong);
                }
        }

        assert(this->numDestRegs() != 0);
        return values.front();
    }

    bool predIsWrong() const
    {
        return dynFlags.test(DynInstFlags::predWrong);
    }

    void setValueMispredicted()
    {
        dynFlags.set(DynInstFlags::vpMispred);
    }

    bool isValueMispredicted() const
    {
        return dynFlags.test(DynInstFlags::vpMispred);
    }


    bool isValuePredicted() const
    {
        return dynFlags.test(DynInstFlags::isPredicted);
    }

    void setValuePredicted()
    {
        dynFlags.set(DynInstFlags::isPredicted);
    }

    bool predValueWasRead(unsigned idx) const
    {
        assert(idx < this->numDestRegs());
        DPRINTF(ValuePredictor, "Checking if dest reg %u(%u) was speculatively read\n", this->flattenedDestRegIdx(idx), this->renamedDestRegIdx(idx));
        return this->cpu->scoreboard.wasRead(this->renamedDestRegIdx(idx));
    }

    void setFlags(uint64_t flags, unsigned i) {
        assert(i >= 0 || i <= 2);
        predFlags[i] = flags;
        dynFlags.set(DynInstFlags::predictedFlags);
    }

    void setPred(Prediction &pred)
    {
        prediction = pred;
        DPRINTF(ValuePredictor, "Prediction is : %s\n", pred.tostring());
    }


    Prediction &getPred()
    {
        return prediction;
    }

    /** Reads a misc. register, including any side-effects the read
     * might have as defined by the architecture.
     */
    MiscReg readMiscReg(int misc_reg)
    {
        return this->cpu->readMiscReg(misc_reg, this->threadNumber);
    }

    /** Sets a misc. register, including any side-effects the write
     * might have as defined by the architecture.
     */
    void setMiscReg(int misc_reg, const MiscReg &val)
    {
        /** Writes to misc. registers are recorded and deferred until the
         * commit stage, when updateMiscRegs() is called. First, check if
         * the misc reg has been written before and update its value to be
         * committed instead of making a new entry. If not, make a new
         * entry and record the write.
         */
        for (int idx = 0; idx < _numDestMiscRegs; idx++) {
            if (_destMiscRegIdx[idx] == misc_reg) {
               _destMiscRegVal[idx] = val;
               return;
            }
        }

        assert(_numDestMiscRegs < TheISA::MaxMiscDestRegs);
        _destMiscRegIdx[_numDestMiscRegs] = misc_reg;
        _destMiscRegVal[_numDestMiscRegs] = val;
        _numDestMiscRegs++;
    }

    /** Reads a misc. register, including any side-effects the read
     * might have as defined by the architecture.
     */
    TheISA::MiscReg readMiscRegOperand(const StaticInst *si, int idx)
    {
        return this->cpu->readMiscReg(
                si->srcRegIdx(idx) - TheISA::Misc_Reg_Base,
                this->threadNumber);
    }

    /** Sets a misc. register, including any side-effects the write
     * might have as defined by the architecture.
     */
    void setMiscRegOperand(const StaticInst *si, int idx,
                                     const MiscReg &val)
    {
        int misc_reg = si->destRegIdx(idx) - TheISA::Misc_Reg_Base;
        setMiscReg(misc_reg, val);
    }

    /** Called at the commit stage to update the misc. registers. */
    void updateMiscRegs()
    {
        // @todo: Pretty convoluted way to avoid squashing from happening when
        // using the TC during an instruction's execution (specifically for
        // instructions that have side-effects that use the TC).  Fix this.
        // See cpu/o3/dyn_inst_impl.hh.
        bool no_squash_from_TC = this->thread->noSquashFromTC;
        this->thread->noSquashFromTC = true;

        for (int i = 0; i < _numDestMiscRegs; i++)
            this->cpu->setMiscReg(
                _destMiscRegIdx[i], _destMiscRegVal[i], this->threadNumber);

        this->thread->noSquashFromTC = no_squash_from_TC;
    }

    void forwardOldRegs()
    {

        for (int idx = 0; idx < this->numDestRegs(); idx++) {
            PhysRegIndex prev_phys_reg = this->prevDestRegIdx(idx);
            TheISA::RegIndex original_dest_reg =
                this->staticInst->destRegIdx(idx);
            switch (regIdxToClass(original_dest_reg)) {
              case IntRegClass:
                this->setIntRegOperand(this->staticInst.get(), idx,
                                       this->cpu->readIntReg(prev_phys_reg));
                break;
              case FloatRegClass:
                this->setFloatRegOperandBits(this->staticInst.get(), idx,
                                             this->cpu->readFloatRegBits(prev_phys_reg));
                break;
              case CCRegClass:
                this->setCCRegOperand(this->staticInst.get(), idx,
                                      this->cpu->readCCReg(prev_phys_reg));
                break;
              case MiscRegClass:
                // no need to forward misc reg values
                break;
            }
        }
    }
    /** Calls hardware return from error interrupt. */
    Fault hwrei();
    /** Traps to handle specified fault. */
    void trap(const Fault &fault);
    bool simPalCheck(int palFunc);

    /** Emulates a syscall. */
    void syscall(int64_t callnum);

  public:

    // The register accessor methods provide the index of the
    // instruction's operand (e.g., 0 or 1), not the architectural
    // register index, to simplify the implementation of register
    // renaming.  We find the architectural register index by indexing
    // into the instruction's own operand index table.  Note that a
    // raw pointer to the StaticInst is provided instead of a
    // ref-counted StaticInstPtr to redice overhead.  This is fine as
    // long as these methods don't copy the pointer into any long-term
    // storage (which is pretty hard to imagine they would have reason
    // to do).

    IntReg readIntRegOperand(const StaticInst *si, int idx)
    {
        return this->cpu->readIntReg(this->_srcRegIdx[idx]);
    }

    FloatReg readFloatRegOperand(const StaticInst *si, int idx)
    {
        return this->cpu->readFloatReg(this->_srcRegIdx[idx]);
    }

    FloatRegBits readFloatRegOperandBits(const StaticInst *si, int idx)
    {
        return this->cpu->readFloatRegBits(this->_srcRegIdx[idx]);
    }

    CCReg readCCRegOperand(const StaticInst *si, int idx)
    {
        return this->cpu->readCCReg(this->_srcRegIdx[idx]);
    }

    /** @todo: Make results into arrays so they can handle multiple dest
     *  registers.
     */
    void setIntRegOperand(const StaticInst *si, int idx, IntReg val)
    {
        //Saving the actual result so we can train the value predictor.
        if(idx == 0) {
                values.push_back(val);
        }

        //To make sure that this is not an instruction with several destination registers (e.g., imul)
        if(dynFlags.test(DynInstFlags::isPredicted)  && this->cpu->scoreboard.getReg(this->renamedDestRegIdx(idx))) {
                DPRINTF(ValuePredictor, "Int: Checking if reg %u is mispredicted: value is: %lu, should be %lu\n", this->renamedDestRegIdx(idx), this->cpu->readIntReg(this->renamedDestRegIdx(idx)), val);
                if(val != this->cpu->readIntReg(this->renamedDestRegIdx(idx)) && this->predValueWasRead(idx)) {
                        DPRINTF(ValuePredictor, "Setting inst [sn:%lu] to mispredicted\n", this->seqNum);
                        this->setValueMispredicted();
                } else if(val != this->cpu->readIntReg(this->renamedDestRegIdx(idx)) && !this->predValueWasRead(idx)) {
                        dynFlags.set(DynInstFlags::ignoreSquash);
                }
        }
        this->cpu->setIntReg(this->_destRegIdx[idx], val);
        BaseDynInst<Impl>::setIntRegOperand(si, idx, val);
    }

    /** This function does not seem to be called, but setFloatRegOperandBits is (see below) **/
    void setFloatRegOperand(const StaticInst *si, int idx, FloatReg val)
    {

        union {
                FloatReg float_pred;
                IntReg int_pred;
        } tmp, tmp_val;

        //Read the prediction as integer.
        tmp.int_pred = this->cpu->readFloatRegBits(this->renamedDestRegIdx(idx));
        tmp_val.float_pred = val;

        if(idx == 0) {
                        //Saving FP result as integer without converting to FP representation
                values.push_back(tmp_val.int_pred);
        }

        //To make sure that this is not an instruction with several destination registers (e.g., imul)
        if(dynFlags.test(DynInstFlags::isPredicted) && this->cpu->scoreboard.getReg(this->renamedDestRegIdx(idx))) {
                DPRINTF(ValuePredictor, "Float: Checking if reg %u is mispredicted: value is: %lu, should be %lu\n", this->renamedDestRegIdx(idx), tmp.int_pred, val);
                if(val != tmp.float_pred  && this->predValueWasRead(idx)) {
                        DPRINTF(ValuePredictor, "Setting inst [sn:%lu] to mispredicted\n", this->seqNum);
                        this->setValueMispredicted();
                } else if(val != tmp.float_pred && !this->predValueWasRead(idx)) {
                        dynFlags.set(DynInstFlags::ignoreSquash);
                }
        }
        this->cpu->setFloatReg(this->_destRegIdx[idx], val);
        BaseDynInst<Impl>::setFloatRegOperand(si, idx, val);
    }

    void setFloatRegOperandBits(const StaticInst *si, int idx,
                                FloatRegBits val)
    {
         //Saving the actual result so we can train the value predictor.
         if(idx == 0) {
                 values.push_back(val);
         }

         //To make sure that this is not an instruction with several destination registers (e.g., imul)
         if(dynFlags.test(DynInstFlags::isPredicted)  && this->cpu->scoreboard.getReg(this->renamedDestRegIdx(idx))) {
                 DPRINTF(ValuePredictor, "FloatBits: Checking if reg %u is mispredicted: value is: %lu, should be %lu\n", this->renamedDestRegIdx(idx), this->cpu->readFloatRegBits(this->renamedDestRegIdx(idx)), val);
                 if(val != this->cpu->readFloatRegBits(this->renamedDestRegIdx(idx)) && this->predValueWasRead(idx)) {
                         DPRINTF(ValuePredictor, "Setting inst [sn:%lu] to mispredicted\n", this->seqNum);
                         this->setValueMispredicted();
                 } else if(val != this->cpu->readFloatRegBits(this->renamedDestRegIdx(idx)) && !this->predValueWasRead(idx)) {
                         dynFlags.set(DynInstFlags::ignoreSquash);
                 }
         }

        this->cpu->setFloatRegBits(this->_destRegIdx[idx], val);
        BaseDynInst<Impl>::setFloatRegOperandBits(si, idx, val);
    }

    void setCCRegOperand(const StaticInst *si, int idx, CCReg val)
    {
        //Saving the actual result so we can train the value predictor. Is not likely to happen
        if(idx == 0) {
                values.push_back(val);
        }

         //To make sure that this is not an instruction with several destination registers (e.g., imul)
        if(dynFlags.test(DynInstFlags::isPredicted) && this->cpu->scoreboard.getReg(this->renamedDestRegIdx(idx))) {
                //Ignoring the AF flag
                if(this->destRegIdx(idx) == 184) {
                        if((val & 0xFFEF) != this->cpu->readCCReg(this->renamedDestRegIdx(idx)) && this->predValueWasRead(idx)) {
                                this->setValueMispredicted();
                        } else if((val & 0xFFEF) != this->cpu->readCCReg(this->renamedDestRegIdx(idx)) && !this->predValueWasRead(idx)) {
                                dynFlags.set(DynInstFlags::ignoreSquash);
                        }
                } else {
                        if(val != this->cpu->readCCReg(this->renamedDestRegIdx(idx)) && this->predValueWasRead(idx)) {
                                this->setValueMispredicted();
                        } else if(val != this->cpu->readCCReg(this->renamedDestRegIdx(idx)) && !this->predValueWasRead(idx)) {
                                dynFlags.set(DynInstFlags::ignoreSquash);
                        }
                }
        }
        this->cpu->setCCReg(this->_destRegIdx[idx], val);

        //Arch reg 188 has the DF flag, which cannot be inferred from the value prediction, but can be approximated by what is the previous DF flag.
        //So we update a global DF flag in the CPU to help with that.
        if(this->destRegIdx(idx) == 188) {
                this->cpu->currentDF = val;
        }

        BaseDynInst<Impl>::setCCRegOperand(si, idx, val);
    }

#if THE_ISA == MIPS_ISA
    MiscReg readRegOtherThread(int misc_reg, ThreadID tid)
    {
        panic("MIPS MT not defined for O3 CPU.\n");
        return 0;
    }

    void setRegOtherThread(int misc_reg, MiscReg val, ThreadID tid)
    {
        panic("MIPS MT not defined for O3 CPU.\n");
    }
#endif

  public:
    /** Calculates EA part of a memory instruction. Currently unused,
     * though it may be useful in the future if we want to split
     * memory operations into EA calculation and memory access parts.
     */
    Fault calcEA()
    {
        return this->staticInst->eaCompInst()->execute(this, this->traceData);
    }

    /** Does the memory access part of a memory instruction. Currently unused,
     * though it may be useful in the future if we want to split
     * memory operations into EA calculation and memory access parts.
     */
    Fault memAccess()
    {
        return this->staticInst->memAccInst()->execute(this, this->traceData);
    }
};

#endif // __CPU_O3_ALPHA_DYN_INST_HH__

