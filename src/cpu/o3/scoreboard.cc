/*
 * Copyright (c) 2005-2006 The Regents of The University of Michigan
 * Copyright (c) 2013 Advanced Micro Devices, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Korey Sewell
 *          Kevin Lim
 */

#include "config/the_isa.hh"
#include "cpu/o3/scoreboard.hh"
#include "debug/Scoreboard.hh"

Scoreboard::Scoreboard(const std::string my_name,
                       unsigned _numLogicalIntRegs,
                       unsigned _numPhysicalIntRegs,
                       unsigned _numLogicalFloatRegs,
                       unsigned _numPhysicalFloatRegs,
                       unsigned _numMiscRegs,
                       PhysRegIndex _zeroRegIdx,
                       PhysRegIndex _fpZeroRegIdx)
    : numLogicalIntRegs(_numLogicalIntRegs),
      numPhysicalIntRegs(_numPhysicalIntRegs),
      numLogicalFloatRegs(_numLogicalFloatRegs),
      numPhysicalFloatRegs(_numPhysicalFloatRegs),
      numMiscRegs(_numMiscRegs),
      zeroRegIdx(_zeroRegIdx)
{
        _name = my_name;
    //Get Register Sizes
    numLogicalRegs = numLogicalIntRegs  + numLogicalFloatRegs;
    numPhysicalRegs = numPhysicalIntRegs  + numPhysicalFloatRegs;

    numTotalRegs = numPhysicalRegs + _numMiscRegs;

    //Resize scoreboard appropriately
    regScoreBoard.resize(numPhysicalRegs);

    //Initialize values
    for (int i=0; i < numPhysicalRegs; i++) {
        //assert(indexInBounds(i));
        regScoreBoard[i].ready = true;
        regScoreBoard[i].speculative = false;
        regScoreBoard[i].spec_read = false;
    }
}


bool
Scoreboard::getReg(PhysRegIndex phys_reg)
{
        assert(phys_reg < numTotalRegs);
#if THE_ISA == ALPHA_ISA
    // Always ready if int or fp zero reg.
    if (phys_reg == zeroRegIdx ||
        phys_reg == (zeroRegIdx + numPhysicalIntRegs)) {
        return 1;
    }
#else
    // Always ready if int zero reg.
    if (phys_reg == zeroRegIdx) {
        return 1;
    }
#endif

    if (phys_reg >= numPhysicalRegs) {
        // misc regs are always ready
        return true;
    }

    return regScoreBoard[phys_reg].ready;
}


void Scoreboard::consumeReg(PhysRegIndex phys_reg) {
        assert(phys_reg < numTotalRegs);
        if(phys_reg >= numPhysicalRegs) {
                return;
        }
        if(regScoreBoard[phys_reg].speculative) {
                DPRINTF(Scoreboard, "Scoreboard: Setting register %u to speculatively read: \n", phys_reg);
                regScoreBoard[phys_reg].spec_read = true;
        }
}

void
Scoreboard::setSpec(PhysRegIndex phys_reg)
{
        assert(phys_reg < numTotalRegs);
        if (phys_reg >= numPhysicalRegs) {
                // misc regs are always ready
                return;
        }
    DPRINTF(Scoreboard, "Setting reg %i as speculative\n", phys_reg);
    //assert(indexInBounds(phys_reg));
    regScoreBoard[phys_reg].speculative = true;
}

void
Scoreboard::setReg(PhysRegIndex phys_reg, bool speculative)
{
    assert(phys_reg < numTotalRegs);
    DPRINTF(Scoreboard, "Setting reg %i as ready. Speculative: %u\n", phys_reg, speculative);

    if (phys_reg >= numPhysicalRegs) {
        // misc regs are always ready
        return;
    }

    assert(phys_reg < numPhysicalRegs);

    //assert(indexInBounds(phys_reg));
    regScoreBoard[phys_reg].ready = true;
    regScoreBoard[phys_reg].speculative = speculative;
}


bool
Scoreboard::wasRead(PhysRegIndex phys_reg)
{
                assert(phys_reg < numTotalRegs);
        if(phys_reg >= numPhysicalRegs) {
                        return false;
        }
        DPRINTF(Scoreboard, "Register %u was speculatively read: %u\n", phys_reg, regScoreBoard[phys_reg].spec_read);
        return regScoreBoard[phys_reg].spec_read;
}


bool
Scoreboard::speculative(PhysRegIndex phys_reg) {
        assert(phys_reg < numTotalRegs);
        if(phys_reg >= numPhysicalRegs) {
                return false;
        }
        return regScoreBoard[phys_reg].speculative;
}

bool
Scoreboard::wasReadNotSpec(PhysRegIndex phys_reg)
{
                assert(phys_reg < numTotalRegs);
        if (phys_reg >= numPhysicalRegs) {
                return false;
        }
        assert(phys_reg < numPhysicalRegs);
        DPRINTF(Scoreboard, "Register %u was read: %u\n", phys_reg, regScoreBoard[phys_reg].spec_read);
        return regScoreBoard[phys_reg].read;
}



void
Scoreboard::unsetReg(PhysRegIndex ready_reg)
{
        assert(ready_reg < numTotalRegs);
#if THE_ISA == ALPHA_ISA
    if (ready_reg == zeroRegIdx ||
        ready_reg == (zeroRegIdx + numPhysicalIntRegs)) {
        // Don't do anything if int or fp zero reg.
        return;
    }
#else
    if (ready_reg == zeroRegIdx) {
        // Don't do anything if int zero reg.
        return;
    }
#endif

    if (ready_reg >= numPhysicalRegs) {
        // misc regs are always ready
        return;
    }


    //assert(indexInBounds(ready_reg));
    regScoreBoard[ready_reg].ready = false;
    regScoreBoard[ready_reg].speculative = false;
    regScoreBoard[ready_reg].spec_read = false;
    regScoreBoard[ready_reg].read = false;
}
