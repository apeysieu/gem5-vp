/*
 * Copyright (c) 2005-2006 The Regents of The University of Michigan
 * Copyright (c) 2013 Advanced Micro Devices, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Korey Sewell
 *          Kevin Lim
 *          Steve Reinhardt
 */

#ifndef __CPU_O3_SCOREBOARD_HH__
#define __CPU_O3_SCOREBOARD_HH__

#include <iostream>
#include <utility>
#include <vector>

#include "base/trace.hh"
#include "config/the_isa.hh"
#include "cpu/o3/comm.hh"
#include "debug/Scoreboard.hh"

/**
 * Implements a simple scoreboard to track which registers are
 * ready. This class operates on the unified physical register space,
 * so integer and floating-point registers are not distinguished.  For
 * convenience, it also accepts operations on the physical-space
 * mapping of misc registers, which are numbered starting after the
 * end of the actual physical register file.  However, there is no
 * actual scoreboard for misc registers, and they are always
 * considered ready.
 */

class Scoreboard
{
  private:
    /** The object name, for DPRINTF.  We have to declare this
     *  explicitly because Scoreboard is not a SimObject. */
    std::string _name;

    /**
     * The total number of registers which can be indexed, including
     * the misc registers that come after the physical registers and
     * which are hardwired to be always considered ready.
     */
    unsigned M5_CLASS_VAR_USED numTotalRegs;



    bool isZeroReg(PhysRegIndex idx) const
    {
        return (idx == zeroRegIdx ||
                (THE_ISA == ALPHA_ISA && idx == fpZeroRegIdx));
    }

  public:
    /** Constructs a scoreboard.
     *  @param _numPhysicalRegs Number of physical registers.
     *  @param _numMiscRegs Number of miscellaneous registers.
     *  @param _zeroRegIdx Index of the zero register.
     *  @param _fpZeroRegIdx Index of the FP zero register (if any, currently
     *                       used only for Alpha).
     */
    Scoreboard(const std::string my_name,
                unsigned _numLogicalIntRegs,
                unsigned _numPhysicalIntRegs,
                unsigned _numLogicalFloatRegs,
                unsigned _numPhysicalFloatRegs,
                unsigned _numMiscRegs,
                PhysRegIndex _zeroRegIdx,
                PhysRegIndex _fpZeroRegIdx);

    /** Destructor. */
    ~Scoreboard() {}

    /** Returns the name of the scoreboard. */
    std::string name() const { return _name; };

    /** Checks if the register is ready.
     *@param true_read	True if a FU or a structure is really reading the value, false if we
     * 					just need it for some tests/stats but it will ultimately not be used by the pipeline.
    **/
    bool getReg(PhysRegIndex ready_reg);

    /** Sets the register as ready.
    *@param speculative	True if we are setting the register because we predicted its value. False otherwise.
    **/
    void setReg(PhysRegIndex phys_reg, bool speculative = false);
    void setSpec(PhysRegIndex phys_reg);

    /** Checks if the register was read before being overwritten by the true result */
    bool wasRead(PhysRegIndex phys_reg);
    bool wasReadNotSpec(PhysRegIndex phys_reg);

    bool speculative(PhysRegIndex phys_reg);

    /** Sets the register as not ready. */
    void unsetReg(PhysRegIndex ready_reg);

    void consumeReg(PhysRegIndex phys_reg);

  private:

    struct RegisterState
    {
        bool ready;			//Is the register ready.
        bool speculative;	//Is the register value speculated.
        bool spec_read;		//Has the register been read while in speculative state.
        bool read;
    };
    /** Scoreboard of physical integer registers. */
    std::vector<RegisterState> regScoreBoard;

  public:
    /** Number of logical integer registers. */
    int numLogicalIntRegs, numPhysicalIntRegs, numLogicalFloatRegs, numPhysicalFloatRegs;

    /** Number of physical integer registers. */
    int numLogicalRegs, numPhysicalRegs;

    /** Number of miscellaneous registers. */
    int numMiscRegs;

    /** The index of the zero register. */
      PhysRegIndex zeroRegIdx;

      /** The index of the FP zero register. */
      PhysRegIndex fpZeroRegIdx;

    /*void
    resize(int newSize)
    {
        currentSize = newSize;
        regScoreBoard.resize(newSize);
    }

    bool
    indexInBounds(int idx)
    {
        return idx < currentSize;
    }*/
};

#endif
