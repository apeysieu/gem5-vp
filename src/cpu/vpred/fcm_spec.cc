/*
 * fcm_spec.cc
 *
 *  Created on: Oct 10, 2012
 *      Author: aperais
 */


#include "cpu/vpred/fcm_spec.hh"

#include "base/statistics.hh"
#include "cpu/o3/helper.hh"
#include "debug/ValuePredictor.hh"

std::string FCM_SPEC_VP::Entry::_name;
uint32_t FCM_SPEC_VP::Entry::_lhist_length;

FCM_SPEC_VP::FCM_SPEC_VP(std::string &name, unsigned size_vht, unsigned size_ht, unsigned tag_length_ht, unsigned lhist_length, unsigned counterWidth, std::vector<unsigned> &proba, unsigned assoc)
{
        this->_name = name + ".VPredUnit.FCM_SPEC";
        FCM_SPEC_VP::Entry::_lhist_length = lhist_length;
        this->_proba = proba;
        this->assoc = assoc;
        FCM_SPEC_VP::Entry::_name = this->_name + ".Entry";

        HT_INDEX = log2(size_ht);
        VHT_INDEX = log2(size_vht);
        TAG_LENGTH = tag_length_ht;

        this->fetch_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_ht, this->assoc, counterWidth, _proba);
        this->commit_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_ht, this->assoc, counterWidth, _proba);

        _low_conf_inflight.resize(size_ht, 0);
        inflight.resize(size_ht, 0);


        this->_VHT.resize(size_vht);
        this->_vht_tag.resize(size_vht);
        this->_last_wrong.resize(size_ht, false);

        this->_conf.resize(size_vht, ConfCounter(2,  0, &(this->_proba)));


}

prediction_t FCM_SPEC_VP::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);

        Addr base_upc = createUPCindex(micropc, (Addr)log2(this->commit_HT.size()));

        history->ht_index = (base_upc ^ ip) & (this->commit_HT.size() - 1);
        history->tag = (ip >> this->HT_INDEX) & ((((Addr) 1) << TAG_LENGTH) - 1);

        DPRINTF(ValuePredictor, "Index for the prediction: %lx, tag: %lx\n", history->ht_index, history->tag);

        history->vht_mismatch = false;
        history->ip = base_upc ^ ip;


        //Init
        history->low_conf_inflight = false;
        history->mismatch = false;

        history->way = this->commit_HT[history->ht_index].tagLookup(history->tag);
        history->mismatch = history->way == assoc;

        accessVHT++;


        if(history->mismatch) {
                DPRINTF(ValuePredictor, "Tag mismatch when getting prediction for inst at 0x%lx in entry %lu (tag %lu)\n",
                                ip, history->ht_index, history->tag);
                history->pred = prediction_t(Prediction(),0);
                return prediction_t(Prediction(),0);
        }

        accessVPT++;

        DPRINTF(ValuePredictor, "Accessing the fetch table\n");
        history->vht_index = this->fetch_HT[history->ht_index].getIndex(history->way, VHT_INDEX,  history->ip);


        DPRINTF(ValuePredictor, "Inflight: %i, vht_index: %lx\n", inflight[history->ht_index], history->vht_index);

        vpt_dist.sample(history->vht_index);

        history->pred = prediction_t(this->_VHT[history->vht_index], this->commit_HT[history->ht_index].getConfidence(history->way));

        DPRINTF(ValuePredictor, "Predicting %s with conf %i\n", history->pred.first.tostring(), (int) history->pred.second);


#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if( _low_conf_inflight.at(history->ht_index) > 0) {
                DPRINTF(ValuePredictor, "Pred is low conf\n");
                history->low_conf_inflight = true;
        }

        if((history->pred.second < 7 || history->low_conf_inflight)) {

                _low_conf_inflight.at(history->ht_index)++;
                DPRINTF(ValuePredictor, "One more low conf inflight: %i\n", _low_conf_inflight.at(history->ht_index));

        }
#endif

        if(this->_vht_tag[history->vht_index] != history->ht_index && !history->low_conf_inflight && history->pred.second == 7) {
                history->vht_mismatch = true;
        }

        inflight[history->ht_index]++;
        DPRINTF(ValuePredictor, "One more inflight for %lu, inflight: %i\n", history->ht_index, inflight.at(history->ht_index));

        //Update the speculative state
        this->fetch_HT[history->ht_index].update(history->pred.first, *history, history->way);

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if(!history->low_conf_inflight) {
#endif
                DPRINTF(ValuePredictor, "Returning pred\n");
                history->vht_mismatch =false;
                return history->pred;
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        } else {
                if(history->pred.second == 7) {
                        ignoredLowConfInflight++;
                }
                history->vht_mismatch = false;
                DPRINTF(ValuePredictor, "Returning dummy because low conf\n");
                return prediction_t(Prediction(),0);
        }
#endif
}

void FCM_SPEC_VP::updateVHT(uint16_t index, Prediction & value)
{
        if(this->_conf[index].read() == 0) {
                this->_VHT[index] = value;
        }
}

void FCM_SPEC_VP::update(Prediction & value, void *vp_history, bool mispred, bool squashed)
{
        assert(vp_history);
        VPSave *history = static_cast<VPSave*>(vp_history);


#ifdef NOREAD_AT_COMMIT
        unsigned commit_way_fcm = history->way;
#else
        unsigned commit_way_fcm = commit_HT[history->ht_index].tagLookup(history->tag);
#endif


        if(squashed) {
                //Need to reset the confidence counter in the entry who mispredicted. Update will be done in order.
                this->commit_HT[history->ht_index].update(value, NULL, history->tag, false, 0, true);
                return;
        }

        accessVHT++;



#ifndef NOREAD_AT_COMMIT
        if(!history->low_conf_inflight && this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip) != history->vht_index && history->pred.second == 7 && !(history->pred.first == value)) {
                indexDiffers++;
        }
        if(!history->low_conf_inflight && this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip) == history->vht_index && history->pred.second == 7 && (this->_VHT[this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip)] == value) && !(history->pred.first == value)) {
                DPRINTF(ValuePredictor, "Value in pred : %s, value in table : %s\n", history->pred.first.tostring(), this->_VHT[history->vht_index].tostring());
                valueDiffers++;
        }

        if(!history->low_conf_inflight && history->pred.second == 7 && history->pred.first != value && history->vht_mismatch) {
                vhtTagDiffers++;
        }

        if(commit_way_fcm != this->assoc) {
                history->vht_index = this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip);
        }
#endif

        if(commit_way_fcm != assoc) {
                accessVPT++;
                if(this->commit_HT[history->ht_index].getConfidence(commit_way_fcm) == 7) {
                        if(this->_VHT.at(history->vht_index) == value) {
                                commit_correct++;
                        } else {
                                commit_incorrect++;
                        }
                }
        }

#ifndef NOREAD_AT_COMMIT
        bool outcome = commit_way_fcm != assoc && this->_VHT[history->vht_index] == value && !mispred;
#else
        bool outcome = !history->mismatch && history->pred.first == value;
#endif

        DPRINTF(ValuePredictor, "Update, value predicted: %s, value in commit: %s, value: %s, outcome: %lu\n", history->pred.first.tostring(), this->_VHT[history->vht_index].tostring(), value.tostring(), outcome);

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
                _low_conf_inflight.at(history->ht_index)--;
                DPRINTF(ValuePredictor, "One less low conf in flight: %lu, at %lu\n", _low_conf_inflight.at(history->ht_index), history->ht_index);
                assert(_low_conf_inflight.at(history->ht_index) >= 0);
        }
#endif

        if(!history->mismatch) {
                inflight.at(history->ht_index)--;
                DPRINTF(ValuePredictor, "Accessing entry %lx, inflight: %i\n", history->ht_index, inflight.at(history->ht_index));
                assert(inflight.at(history->ht_index) >= 0);

        }

        if(commit_way_fcm != assoc) {
                this->updateVHT(history->vht_index, value);
                if(outcome) {
                        this->_conf[history->vht_index].increment();
                } else {
                        this->_conf[history->vht_index].decrement();
                }
        }

        DPRINTF(ValuePredictor, "Updating value predictor with value %s (outcome: %u). Accessing entry %lu (tag: %lu, mismatch: %u).\n",
                        value.tostring(), outcome, history->ht_index, history->tag, history->mismatch);

        this->commit_HT[history->ht_index].update(value, NULL, history->tag, outcome, history->pred.second);

        if(inflight[history->ht_index] == 0) {
                        unsigned way = this->commit_HT[history->ht_index].tagLookup(history->tag);
                DPRINTF(ValuePredictor, "Tag mismatch or not other instances in flight: setting speculative history to committed history\n");
                this->fetch_HT[history->ht_index].restore(this->commit_HT[history->ht_index].getSignatures(way), way);
        }

        updateStats(*history, !history->mismatch && (history->pred.first == value));
        delete history;
}

void FCM_SPEC_VP::squash(void *vp_history, bool remove)
{
        VPSave *history = static_cast<VPSave*>(vp_history);

        DPRINTF(ValuePredictor, "Squash, restoring fetch history for ht: %lx, vht %lx\n", history->ht_index, history->vht_index);

        if(remove) {
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
                if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
                        _low_conf_inflight.at(history->ht_index)--;
                        assert(_low_conf_inflight.at(history->ht_index) >= 0);
                }
#endif
                if(!history->mismatch) {
                        inflight.at(history->ht_index)--;
                        assert(inflight.at(history->ht_index) >= 0);
                }
        }

        if(!history->mismatch && inflight.at(history->ht_index) != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch history\n");
                accessVHT++;
                this->fetch_HT[history->ht_index].restore(history->signatures, history->way);
        } else if(!history->mismatch) {
                DPRINTF(ValuePredictor, "Restoring commit and fetch history\n");
                accessVHT++;
                this->fetch_HT[history->ht_index].restore(this->commit_HT[history->ht_index].getSignatures(history->way), history->way);
        }

        if(remove) {
                delete history;
        }


}

void FCM_SPEC_VP::regStats()
{


        vpt_dist
        .name(name() + ".VPTDist")
        .init(/* base value */ 0,
                        /* last value */ 8192,
                        /* bucket size */ 1)

                        .desc("Distribution of the VPT accesses");

        accessVHT
        .name(name() + ".accessesVHT")
        .desc("Accesses to the VHT")
        ;

        selectiveCascade
        .name(name() + ".selectiveCascade")
        .desc("Number of wrong prediction whose immediate next instance is also wrong (cascading mispreds)")
        ;

        accessVPT
        .name(name() + ".accessesVPT")
        .desc("Accesses to the VPT")
        ;


        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight")
        ;

        indexDiffers
        .name(name() + ".VHTIndexDiffers")
        .desc("Number of times the index of the VHT computed at fetch differed from the one computed at commit")
        ;

        valueDiffers
        .name(name() + ".VHTValueDiffers")
        .desc("Number of times the value in the VHT computed at fetch differed from the one computed at commit")
        ;

        vhtTagDiffers
        .name(name() + ".VHTTagDiffers")
        .desc("VHT entry was modified by a different entry than the one accessing it")
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        .flags(Stats::total)
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        .flags(Stats::total)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;

        missrate
        .name(name() + ".miss_all")
        .desc("Missrate of the predictor")
        .precision(5)
        ;

        miss
        .name(name() + ".misses")
        .desc("Total number of tag mismatches")
        ;


        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        .flags(Stats::total)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        .flags(Stats::total)
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        .flags(Stats::total)
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;


        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;

        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);

        ignoredIncorrect = ign_incorrect / attempted;
        missrate = miss / attempted;
}

void FCM_SPEC_VP::updateStats(VPSave &history, bool outcome)
{
        bool proceeded = history.pred.second == 7 && !history.vht_mismatch;
        miss += history.mismatch;
        ign_correct +=  proceeded && outcome && history.low_conf_inflight ;
        ign_incorrect += proceeded && !outcome &&  history.low_conf_inflight;
        attempted ++;
        correct += outcome && proceeded && !history.low_conf_inflight;
        if(!outcome && proceeded && !history.low_conf_inflight) {
                DPRINTF(ValuePredictor, "Inst was wrongly predicted with highconf\n");
        }
        incorrect += !outcome && proceeded && !history.low_conf_inflight;

        if(!outcome && proceeded && !history.low_conf_inflight && _last_wrong[history.ip & (this->commit_HT.size() - 1)]) {
                selectiveCascade++;
        } else if(!outcome && proceeded && !history.low_conf_inflight) {
                _last_wrong[history.ip & (this->commit_HT.size() - 1)] = true;
        } else {
                _last_wrong[history.ip & (this->commit_HT.size() - 1)] = false;
        }

        /** Stats  **/

        bool highConf = history.pred.second == 7;

        bool transient =  history.pred.second >= 1 && !highConf;

        bool low_conf =   history.pred.second == 0 && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;
}


