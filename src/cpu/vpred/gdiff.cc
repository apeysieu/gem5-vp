/*
 * gDiffVP.cc
 *
 *  Created on: Aug 17, 2013
 *      Author: aperais
 */


#include "cpu/vpred/gdiff.hh"

#include "cpu/o3/helper.hh"

/**
 * Constructor.
 *@param[in] size	Number of entries in the predictor (power of 2).
 *@param[in] assoc	Associativity of the table.
 **/
gDiffVP::gDiffVP(std::string &name, unsigned order, unsigned size_VHT, unsigned tag_vht, unsigned assoc,  unsigned counterwidth, std::vector<unsigned> &probability,
                DerivO3CPUParams *params,  std::deque<TageBP::Hist> *bpred_globHist,unsigned *bpred_phist, TageBP *tage)
{
        this->_name = name + ".VPredUnit.gDiff";
        this->proba = probability;
        this->assoc = assoc;

        this->fetch_VHT = AssociativeArray<gDiffVP, gDiffVP::Entry>(size_VHT, this->assoc, counterwidth, proba, order);
        this->commit_VHT = AssociativeArray<gDiffVP, gDiffVP::Entry>(size_VHT, this->assoc, counterwidth, proba, order);

        this->_low_conf_inflight.resize(size_VHT);
        this->inflight.resize(size_VHT);
        this->TAG_SIZE = tag_vht;
        fetch_gHistory.resize(order);
        commit_gHistory.resize(order);
        this->order = order;

        vtage = new  VTageVP(params->name,
                                params->v_numHistComponents,
                                params->v_numLogBaseEntry,
                                params->v_minHistSize,
                                params->v_maxHistSize,
                                params->v_baseHystShift,
                                params->v_counterWidth,
                                params->v_instShiftAmt,
                                probability,
                                &(tage->globHist),
                        &(tage->phist),
                        tage);
}


gDiffVP::~gDiffVP() {}


prediction_t
gDiffVP::lookup(Addr ip, MicroPC micropc, void *&vp_history, uint64_t seqnum)
{

        Addr base_upc = createUPCindex(micropc, (Addr)log2(this->commit_VHT.size()));
        Addr index_vht = base_upc ^ (ip & (this->fetch_VHT.size() - 1));


        Addr tag_VHT = ((ip >> (unsigned) log2(this->fetch_VHT.size())) & (((Addr) 1 << TAG_SIZE) - 1));


        VPSave *save = new VPSave();

        vp_history = static_cast<void*>(save);

        save->index_vht = index_vht;
        DPRINTF(ValuePredictor, "Trying to access entry %u with tag %u\n", index_vht, tag_VHT);

        save->tag_vht = tag_VHT;
        save->way = this->commit_VHT[index_vht].tagLookup(tag_VHT);
        save->mismatch = save->way == assoc;

        //Init
        save->low_conf_inflight = false;
        save->ip = ip;
        save->isBranch = false;
        save->micropc = micropc;

        prediction_t result;

        access++;

        for(unsigned i = 0; i < order; i++) {
                DPRINTF(ValuePredictor, "Saving fetch history for inst : %lu\n", fetch_gHistory[i].second.getValue());
                 save->fetch_hist.push_back(fetch_gHistory[i].second);
        }

        if(save->mismatch) {
                DPRINTF(ValuePredictor, "Tag mismatch, returning\n");
                prediction_t fromvtage = updateGHistDP(seqnum, *save);
                if(fromvtage.second == 7) {
                        save->pred = fromvtage;
                        save->tagePredicted = true;
                } else {
                        save->tagePredicted = false;
                }
                return save->pred;

        }


       Prediction val;
       unsigned offset = this->commit_VHT[index_vht].getOffset(save->way);

       val = this->commit_VHT[index_vht].getStride(save->way) + this->fetch_gHistory[offset].second;

        result = prediction_t(val, this->commit_VHT[index_vht].getConfidence(save->way));




        //Updating the global history with the prediction from stride
        prediction_t fromvtage = updateGHistDP(seqnum, *save);
        DPRINTF(ValuePredictor, "Accessing table, stride: %s\n", this->commit_VHT[index_vht].getStride(save->way).tostring());

        if(fromvtage.second == 7 /*&& result.second != 7*/) {
                save->pred = fromvtage;
                madeByVtage++;
                save->tagePredicted = true;
        } else {
                save->pred = result;
                madeByGDiff++;
                save->tagePredicted = false;
        }

        if(this->commit_VHT[index_vht].getStride(save->way).getValue() != 0) {
                nonZeroStride++;
        }

        if(this->_low_conf_inflight[index_vht] > 0) {
                        DPRINTF(ValuePredictor, "Low conf prediction inflight, ignoring this one \n");
                save->low_conf_inflight = true;
        }

#ifdef PARTIAL_TRACKING
        if((save->pred.second < 7)) {
#else
        if((save->pred.second < 7 || save->low_conf_inflight)) {
#endif
                this->_low_conf_inflight[index_vht]++;
        }

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        save->low_conf_inflight = false;
#endif

        inflight[save->index_vht]++;
        //DPRINTF(ValuePredictor, "Updating speculative history with %s, saving %s, inflight for entry %u: %u\n", save->pred.first.tostring(), this->fetch_VHT[save->index_vht].getPred(save->way).tostring(), save->index_vht, inflight[save->index_vht]);

        assert(save->index_vht == index_vht);
        //assert(this->fetch_VHT[index_vht].getPred(save->way) == save->pred.first);


        DPRINTF(ValuePredictor, "Predicting for inst at 0x%lx."
                        " Accessing entry (VHT) %lu (tag: %lu, mismatch: %u). Predicted %s with confidence %i\n",
                                ip, save->index_vht, save->tag_vht, save->mismatch,
                                 save->pred.first.tostring(), (int) save->pred.second);


        if(!save->low_conf_inflight) {
                return save->pred;
        } else {
                if(save->pred.second == 7) {
                        ignoredLowConfInflight++;
                }
                return prediction_t(Prediction(),0);
        }
}

void
gDiffVP::squash(void *vp_history, bool remove, bool recompute) {

        VPSave *history = static_cast<VPSave*>(vp_history);

        if(history->isBranch) {
                vtage->squash(history->tageHist, remove, recompute);
        } else {

                vtage->squash(history->tageHist, remove, false);

                if(remove) {
                        assert(!recompute);
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
                        if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
                                _low_conf_inflight[history->index_vht]--;
                                assert(_low_conf_inflight[history->index_vht] >= 0);
                        }
#endif
                        if(!history->mismatch) {
                                inflight[history->index_vht]--;
                                assert(inflight[history->index_vht] >= 0);
                        }
                        fetch_gHistory.pop_front();
                        fetch_gHistory.push_back(history->oldGValue);


                }


                if(!history->mismatch && inflight[history->index_vht] != 0) {
                        access++;
                        DPRINTF(ValuePredictor, "Restoring entry %u in fetch history with %s\n", history->index_vht, history->previous.tostring());
                        this->fetch_VHT[history->index_vht].restore(history->previous, history->way);
                } else if(!history->mismatch) {
                        Prediction tmp = this->commit_VHT[history->index_vht].getLastValue(history->way);
                        DPRINTF(ValuePredictor, "Restoring entry %u in fetch and commit histories\n", history->index_vht, tmp.tostring());
                        access++;

                        this->fetch_VHT[history->index_vht].restore(tmp, history->way);
                }
        }

        if(remove) {
                delete history;
        }
}




void
gDiffVP::update(Prediction & value, void *vp_history, bool squashed)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way = this->commit_VHT[history->index_vht].tagLookup(history->tag_vht);


        if(squashed) {
                std::vector<Prediction> dummy;
                this->commit_VHT[history->index_vht].update(dummy, value, NULL,history->tag_vht, false, 0, squashed);
                return;
        }

        access++;
//        if(commit_way != assoc) {
//                if(this->commit_VHT[history->index_vht].getConfidence(commit_way) == 7) {
//                        if(this->commit_VHT[history->index_vht].getPred(commit_way) + this->commit_VHT[history->index_vht].getStride(commit_way) == value) {
//                                commit_correct++;
//                        } else {
//                                commit_incorrect++;
//                        }
//                }
//        }

#ifdef PARTIAL_TRACKING
                 if(!history->mismatch && (history->pred.second < 7)) {
#else
                 if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
#endif
                  this->_low_conf_inflight[history->index_vht]--;
                  assert(this->_low_conf_inflight[history->index_vht] >= 0);
        }

//#ifndef NOREAD_AT_COMMIT
//        unsigned offset = this->commit_VHT[history->index_vht].getOffset(commit_way);
//        bool correct = commit_way != this->assoc && (commit_gHistory[offset] + this->commit_VHT[history->index_vht].getStride(commit_way)) == value;
///#else
        bool correct = (history->tagePredicted || !history->mismatch) && history->pred.first == value;
//#endif

        //assert(!vtage_hist.empty());
        vtage->update(value, history->tageHist, false, false);

        DPRINTF(ValuePredictor, "Updating predictor with %s (correct: %u)."
                        " Accessing entry (VHT) %lu (tag: %lu, mismatch: %u). Way: %u\n",
                         value.tostring(), correct, history->index_vht, history->tag_vht, history->mismatch, commit_way);

        //Update the SHT first because we need the value in the VHT to compute the stride.

        if(!history->mismatch) {
                inflight[history->index_vht]--;
                DPRINTF(ValuePredictor, "Inflight for entry %u : %u\n",history->index_vht, inflight[history->index_vht]);
        }


        std::vector<Prediction> new_strides;

#ifdef NOREAD_AT_COMMIT
        for(int i = 0; i < commit_gHistory.size(); i++){
                new_strides.push_back(value - history->fetch_hist[i]);
        }
#else
        for(int i = 0; i < commit_gHistory.size(); i++){
                new_strides.push_back(value - commit_gHistory[i]);
////        	std::cerr << "Stride " << i << " "  << (value - commit_gHistory[i])[0] << std::endl;
        }
#endif

//        std::cerr << "Value is " << value[0] << ", predicted " << history->pred.first[0] << std::endl;
//        std::cerr << "Stride is " << commit_VHT[history->index_vht].getStride(0)[0];
//        std::cerr << ", offset is " <<  commit_VHT[history->index_vht].getOffset(0) << std::endl;
//        for(unsigned i = 0; i < order; i++) {
//        	std::cerr << i << " : fetch : " << history->fetch_hist[i][0] << ", commit : " << commit_gHistory[i][0] << std::endl;
//        }
//        std::cerr << "----------------------------------" << std::endl;

        unsigned diffs = 0;
        for(unsigned i = 0; i < order; i++) {
                diffs += history->fetch_hist[i].getValue() != commit_gHistory[i].getValue();
        }
        diff_with_commit.sample(diffs);

        this->commit_VHT[history->index_vht].update(new_strides, value, &history->previous, history->tag_vht, correct, history->pred.second);


        if(commit_way != this->assoc && inflight[history->index_vht] == 0) {
                         DPRINTF(ValuePredictor, "Restoring entry %u in fetch history with %s\n", history->index_vht, value.tostring());
                this->fetch_VHT[history->index_vht].restore(value, commit_way);
        }

        //Updating the commit global value history
        commit_gHistory.pop_back();
        commit_gHistory.push_front(value);

        updateStats(*history, (history->tagePredicted || !history->mismatch) && history->pred.first == value);
        delete history;
}

prediction_t gDiffVP::updateGHistDP(uint64_t seqnum, VPSave &history) {
         void * hist;

         //Prediction value = fetch_VHT[history.index_vht].getPred(0);
         prediction_t pred  = vtage->lookup(history.ip, history.micropc, hist);


         history.oldGValue = fetch_gHistory.back();
         history.tageHist = hist;
         fetch_gHistory.pop_back();
         fetch_gHistory.push_front(gEntry(seqnum, pred.first));

         //this->fetch_VHT[history.index_vht].update(value, history, history.way);
         return pred;
}

void gDiffVP::updateGHistWB(uint64_t seqnum, Prediction &value) {
        for(unsigned i = 0; i < fetch_gHistory.size(); i++) {
                if(fetch_gHistory[i].first == seqnum) {
                        fetch_gHistory[i].second = value;
                        return;
                }
        }
}

void gDiffVP::updateFoldedHist(bool save, void *&vp_history) {
        VPSave * hist = new VPSave();
        hist->isBranch = true;

        vtage->updateFoldedHist(save, hist->tageHist);
        vp_history = static_cast<void*>(hist);
}

void
gDiffVP::flush_branch(void *vp_history)
{
        VPSave * hist = static_cast<VPSave*>(vp_history);
        vtage->flush_branch(hist->tageHist);
        delete hist;
}

void gDiffVP::regStats()
{
        diff_with_commit
        .name(name() + ".diffWithCommitHist")
        .init(0, 15, 1)
        .desc("Number of differences between the two histories due to delays");

        access
        .name(name() + ".accesses")
        .desc("Accesses to the VHT")
        ;

        madeByVtage
        .name(name() + ".madeByVtage")
        .desc("Pred made by vtage")
        ;

        madeByGDiff
        .name(name() + ".madeByGDiff")
        .desc("Pred made by Gdiff")
        ;

        nonZeroStride
        .name(name() + ".nonZeroStride")
        .desc("Number of predictions with a non zero stride")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        ignoredLowConfInflight
        .name(name() + "ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        missrate
        .name(name() + ".miss_all")
        .desc("Missrate of the predictor")
        .precision(5)
        ;

        miss
        .name(name() + ".misses")
        .desc("Total number of tag mismatches")
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;



        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;
        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);
        missrate = miss / attempted;

        vtage->regStats();
}

void gDiffVP::updateStats(VPSave &history, bool outcome)
{
        bool proceeded = history.pred.second == 7;

        miss += history.mismatch;
        ign_correct +=  proceeded && outcome && history.low_conf_inflight ;
        ign_incorrect += proceeded && !outcome &&  history.low_conf_inflight;
        attempted ++;

        DPRINTF(ValuePredictor, "Updating stat, outcome %u, proceeded: %u, low conf inflight: %u\n", outcome, proceeded, history.low_conf_inflight);
        if(outcome && proceeded && !history.low_conf_inflight) {
        } else if(outcome && proceeded && history.low_conf_inflight) {
                DPRINTF(ValuePredictor, "Committing low conf inflight predicton\n");
        }

        correct += outcome && proceeded && !history.low_conf_inflight;
        incorrect += !outcome && proceeded && !history.low_conf_inflight;

        /** Stats  **/

        bool highConf = history.pred.second == 7;

        bool transient =  history.pred.second >= 1 && !highConf;

        bool low_conf =   history.pred.second == 0 && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;
}

