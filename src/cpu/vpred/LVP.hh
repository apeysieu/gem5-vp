#ifndef __CPU_LastVP_HH__
#define __CPU_LastVP_HH__

#include <cmath>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"

class LastVP
{
public:
        struct VPSave {
                Addr index;
                Addr tag;
                prediction_t pred;
                bool mismatch;
                bool low_conf_inflight;
                Prediction previous;
                unsigned way;
        };

        class Entry {
        private:
                Prediction _value;
                Addr _tag;
                bool valid;

        public:

                ConfCounter _confidence;
                static std::string name;


                Entry() {}

                /**
                 * @param[counterwidth] Width of the confidence counter in bits.
                 * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
                 */
                Entry(unsigned counterwidth, std::vector<unsigned> &proba)
                {
                        this->_value = Prediction();
                        this->_tag = 0;
                        this->_confidence = ConfCounter(counterwidth, 0, &proba);
                }

                bool tagMatches(Addr tag)
                {
                        return this->_tag == tag;
                }

                unsigned getConfidence() const
                {
                        return this->_confidence.read();
                }

                Prediction getPred() const
                {
                        return this->_value;
                }

                /**
                 * @param[value] The speculative value to update the entry with.
                 * @param[save] Structure to save the "old" last value.
                 */
                void update(Prediction &value, VPSave &save) {
                        save.previous = this->_value;
                        this->_value = value;
                }

                /**
                 * @param[value] Restore a previously saved last value.
                 */
                void restore(Prediction &value) {
                        this->_value = value;
                }

                /**
                 * @param[value] The actual result of the instruction.
                 * @param[dummy] Reserved.
                 * @param[tag] The new tag.
                 * @param[outcome] True if the predictor predicted correctly, false otherwise.
                 * @param[conf] The confidence seen at fetch (may differ from the confidence currently in the entry).
                 * @param[squashed] True if this is an update on squash, false is this is an update at retire.
                 */
                void update(Prediction &value, Prediction *dummy, Addr tag, bool outcome, unsigned conf, bool squashed = false)
                {
                        if(this->tagMatches(tag)) {
#ifdef NOREAD_AT_COMMIT
                                _confidence.set(conf);
#endif
                                _confidence.updateConf(outcome);
                                if(!squashed) {
                                        this->_value = value;
                                }
                        } else {
                                this->_tag = tag;
                                this->_value = value;
                                this->_confidence.set(0);
                        }
                }
        };

    AssociativeArray<LastVP, LastVP::Entry> fetch_VHT;
    AssociativeArray<LastVP, LastVP::Entry> commit_VHT;

    std::string _name;
    std::vector<int> _low_conf_inflight;
    std::vector<int> inflight;
    std::vector<unsigned> proba;
    unsigned assoc;

    unsigned VHT_INDEX_WIDTH, VHT_TAG_WIDTH;

    /** Global Stats */
    Stats::Scalar attempted;
    Stats::Scalar correct;
    Stats::Scalar incorrect;

    Stats::Formula accuracy;
    Stats::Formula coverage;
    Stats::Scalar ignoredLowConfInflight;

    Stats::Formula ignoredCorrect, ignoredIncorrect;
    Stats::Scalar ign_correct, ign_incorrect;
    Stats::Scalar commit_correct, commit_incorrect;
    Stats::Formula accuracy_ideal, coverage_ideal;

    /** Confidence Stats */
    /** Stat for number of high confidence predictions. */
    Stats::Scalar highConf;
    /** Stat for number of correct high confidence predictions. */
    Stats::Scalar highConfHit;
    /** Stat for the number of incorrect high confidence predictions */
    Stats::Scalar highConfMiss;
    /** Stat for number of medium confidence predictions. */
    Stats::Scalar transientConf;
    /** Stat for number of correct medium confidence predictions. */
    Stats::Scalar transientConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar transientConfMiss;
    /** Stat for number of low confidence predictions. */
    Stats::Scalar lowConf;
    /** Stat for number of correct low confidence predictions. */
    Stats::Scalar lowConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar lowConfMiss;

    Stats::Scalar miss;
    Stats::Formula missrate;

    Stats::Distribution way_use;
    Stats::Scalar access;

  public:
    /**
     * @param[name] Name of the class (will appear in stats).
     * @param[size] Size of the VHT in entries.
     * @param[tag_width] In bits.
     * @param[counterWidth] In bits.
     * @param[proba] Probability vector used to control forward transition of confidence counters.
     * @param[assoc] Associativity of the VHT.
     */
    LastVP(std::string &name, unsigned size, unsigned tag_width, unsigned counterwidth, std::vector<unsigned> &proba, unsigned assoc = 1);

    ~LastVP();

    /**
     * @param[ip] Address of the instruction.
     * @param[micropc] uop index.
     * @param[vp_history] Pointer to modify.
     * @post vp_history points to the data structure of LVP describing the current prediction.
     * @return prediction of inst (ip,micropc)
     */
    prediction_t lookup(Addr ip, MicroPC micropc, void *&vp_history);

    /**
     * @param[value] Actual result of the instruction (dummy if update on squash).
     * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
     * @param[squash] True if update on squash, false if update at retire.
     */
    void update(Prediction & value, void *vp_history, bool mispred, bool squashed);

    /**
     * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
     * @param[remove] True if the data structure should be deleted, false otherwise.
     */
    void squash(void *vp_history, bool remove);

    const std::string &name() const { return _name; }

    /**
     * @pre Stats are registered with the gem5 infrastructure.
     */
    void regStats();

    /**
     * @param[history] Structure describing the prediction.
     * @param[outcome] True if the prediction was correct, false otherwise.
     */
    void updateStats(VPSave &history, bool outcome);


};

#endif
