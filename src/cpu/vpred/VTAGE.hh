/*
 * Authors: NathanaÃ«l PrÃ©millieu
 */

#ifndef __CPU_O3_VTAGE_VPRED_HH__
#define __CPU_O3_VTAGE_VPRED_HH__

#include <cstdlib>
#include <deque>
#include <vector>


#include "base/statistics.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"
#include "debug/ValuePredictor.hh"

class VTageVP
{
public:

    /**
     * Default branch predictor constructor.
     * @param[name] Name of the class (will appear in the stats). Gem5 Specific.
     * @param[numHistComponents] Number of tagged tables.
     * @param[numLogBaseEntry] Log2(#entries) of the base component.
     * @param[minHistSize] Minimum bhist length.
     * @param[maxHistSize] Max bhist length.
     * @param[baseHystShift] In case hysteresis counters have to be shared between entries. NOT IN VTAGE.
     * @param[counterWidth] Width of the confidence counters.
     * @param[instShiftAmt] Shifts the PC (useful only for RISC or aligned instructions).
     * @param[filterProbability] The probability vector to handle forward transitions of confidence counters.
     * @param[bpred_globHist] Pointer to the global branch history (handled in the branch predictor in gem5).
     * @param[bpred_phist] Pointer to the global path history (handled in the branch predictor in gem5).
     * @param[tage] Pointer to TAGE (to debug internal state of the folded registers).
     *
     * @pre assert(filterProbability.size()) == exp(2, counterWidth) - 1;
     */
    VTageVP(std::string &name,
            unsigned numHistComponents,
            unsigned numLogBaseEntry,
            unsigned minHistSize,
            unsigned maxHistSize,
            unsigned baseHystShift,
            unsigned counterWidth,
            unsigned instShiftAmt,
            std::vector<unsigned> & filterProbability,
            std::deque<TageBP::Hist> *bpred_globHist,
            unsigned *bpred_phist,
            TageBP *tage = NULL);

    //Gem5 specific.
    const std::string &name() const { return _name; }

        /** this is the cyclic shift register for folding
         * a long global history into a smaller number of bits;
         * see P. Michaud's PPM-like predictor at CBP-1
         */
        class Folded_history
        {
        public:

            Addr comp;
            unsigned clength;
            unsigned olength;
            unsigned outpoint;

            Folded_history ()
            {
            }

            void init (int original_length, int compressed_length)
            {
                comp = 0;
                olength = original_length;
                clength = compressed_length;
                outpoint = olength % clength;
            }

            void update (std::deque<TageBP::Hist> &globHist)
            {

                comp = (comp << 1) | (Addr)globHist[0].dir;
                comp ^= ((Addr)globHist[olength].dir << outpoint);
                comp ^= (comp >> clength);
                comp &= (1 << clength) - 1;
            }

        };

    /** We still have to save the cyclic register when a branch is encountered because the
     * branch predictor does not necessarily use registers of the same size.
     * We also have to save the index and the tag of the entry,
     * as well as the bank and the altbank and which one made the prediction.
     * This is only needed for speculative execution.
     */
    struct VPSave {
                        //Copy of the folded registers to be able to restore them on a squash.
                std::vector<Addr> ch_i_comp;
                std::vector<Addr> ch_t_comp[2];

                //Indexes and tags used to access the components.
                std::vector<unsigned> gI;
                std::vector<unsigned> gTag;

                //Selected bank and altBank
                unsigned bank;
                unsigned altBank;

                //In VTAGE, altPred is only used to update the u counter, it is never used as the prediction.
                prediction_t tagePred;
                prediction_t altPred;

                //Branch instructions create histories as they update the internal folded history registers.
                bool isBranch;

                Addr instAddr;

                bool usedAlt;
    };

    /**
     * Registers statistics. Gem5 specific.
     */
    void regStats();


    /**
     * Looks up the given address in the branch predictor and returns
     * a pair <value, confidence>.  Also creates a
     * BPHistory object to store any state it will need on squash/update.
     * @param[branch_addr] The address of instruction to look up.
     * @param[micropc] The uop index.
     * @param[vp_history Pointer that will point to the VPSave object.
     * @param[seqNum] The sequence number of the instruction causing the lookup.
     * @return The predicted result of the instruction.
     */
    prediction_t lookup(Addr &branch_addr, MicroPC micropc, void * &vp_history, uint64_t seqNum = 0);

    /**
     * Updates the value predictor with the actual result of an instruction.
     * @param[val] Actual result.
     * @param[vp_history] Pointer to the VPSave object that was created
     * when the value was predicted.
     * @param[mispred] The prediction is incorrect.
     * @param[squashed] is set when this function is called during a squash
     * operation.
     */
    void update(Prediction & val, void *vp_history, bool mispred, bool squashed);

    /**
     * Restores the global value history on a squash.
     * @param[vp_history] Pointer to the VPHistory object that has the
     * previous cyclic registers in it.
     * @param[remove] True if the VPSave corresponding to the instruction should be deleted, false otherwise.
     * @param[recompute] True if the cyclic registers should be recomputed (squashing a branch).
     */
    void squash(void *vp_history, bool remove, bool recompute);

    /** Computes the new folded history when a branch is encountered.
    * @param[save] True if the cyclic registers should be saved.
    * @param[vp_history] Pointer to the VPSave object that has the
    * previous cyclic registers in it.
    * @pre The buffer managing the branch history has been updated by the branch predictor.
    * @post Folded histories are ready to compute new indexes.
    **/
    void updateFoldedHist(bool save, void *&vp_history, uint64_t seqnum = 0);

    /** Update the branch predictor with the committed value.
     * @param[val] The actual result of the instruction.
     * @param[outcome] True if the predicition was correct, false otherwise.
     * @param[vp_history] Pointer to the VPSave object that has the
     * previous cyclic registers in it.
     * @param[squashed] True if the instruction caused a squashed, false otherwise (cannot be true if the predictor is not updated on squashes).
     */
    void updatePredictor(Prediction & val, bool outcome, void *vp_history, bool squashed);

    std::string dump();

    //Basically delete the saved folded history for a branch instruction when they are no longer
    //necessary (because we are committing/updating the predictor with a younger instruction).
    void flush_branch(void *vp_history);

private:


    /** Index function for the base table */
    inline unsigned bIndex(Addr &branch_addr) {
        return ((branch_addr >> instShiftAmt) & baseMask);
    }


    /**
     * The index functions for the tagged tables uses
     * path history as in the OGEHL predictor
     */
    /** F serves to mix path history */
    unsigned F(unsigned A, unsigned size, unsigned bank);

    /** gIndex computes a full hash of pc, ghist and phist */
    unsigned gIndex(Addr &branch_addr, unsigned bank);

    /** tags computation */
    unsigned gTag(Addr &branch_addr, unsigned bank);

    /** Base prediction (with the base predictor) */
    prediction_t getBasePred(Addr &branch_addr, bool &saturated);

    /**
     * Update the base predictor.
     */
    void baseUpdate(Addr branch_addr, Prediction &  val, bool taken, unsigned conf, bool squashed = false);

    /**
     * Just a simple pseudo random number generator:
     * a 2-bit counter, used to avoid ping-pong phenomenon
     * on tagged entry allocations
     */
    inline unsigned myRandom()
    {
        ++seed;
        return seed & 3;
    }



    /**
     * Recover from a misprediction: correct the global branch history.
     * @param[vp_history] Pointer to the VPSave object coressponding to the faulting instruction.
     */
    void recoverBHist(void *vp_history, bool recompute);
    void recoverVHist(void *vp_history);


    void updateStats(VPSave &history, bool outcome);




    /** VTAGE base table entry */
    class Bentry
    {
    public:
        ConfCounter hyst;
        Prediction pred;

        /**
         * @param[counterwidth] Width of the confidence counter in bits.
         * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
         */
        Bentry (unsigned counterwidth, std::vector<unsigned> &proba)
        {
            pred = Prediction();
            hyst = ConfCounter(counterwidth,  0, &proba);
        }

        /**
         * Updates the hysteresis/confidence counter
         * @param[outcome] True if the prediction was correct, false otherwise.
         * @param[val] The actual result of the instruction.
         * @param[conf] The confidence read at fetch.
         * @param[squashed] True is update on squash, false otherwise.
        **/
        void ctrupdate (bool outcome, Prediction & val, unsigned conf)
        {
#ifdef NOREAD_AT_COMMIT_VTAGE
                hyst.set(conf);
#endif

                if(hyst.read() == 0) {
                        pred = val;
                }

                hyst.updateConf(outcome);
        }
    };

    /** VTAGE global table entry */
    class Gentry
    {
    public:
        ConfCounter hyst;
        unsigned tag;
        unsigned u;
        Prediction pred;

        /**
         * @param[counterwidth] Width of the confidence counter in bits.
         * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
         */
        Gentry (unsigned counterwidth, std::vector<unsigned> & proba)
        {
            hyst = ConfCounter(counterwidth, 0, &proba);
            pred = Prediction();
            tag = 0;
            u = 0;
        }

        /**
         * Updates the hysteresis/confidence counter
         * @param[outcome] True if the prediction was correct, false otherwise.
         * @param[val] The actual result of the instruction.
         * @param[conf] The confidence read at fetch.
         * @param[squashed] True is update on squash, false otherwise.
        **/
        void ctrupdate (bool outcome, Prediction & val, unsigned conf)
        {
#ifdef NOREAD_AT_COMMIT_VTAGE
                hyst.set(conf);
#endif
                if(hyst.read() == 0) {
                        pred = val;
                }

                hyst.updateConf(outcome);
        }
    };

    /** Parameters */

    const std::string _name;

    /** Number of Tagged Components */
    unsigned numHistComponents;

    /** sharing an hysteresis bit between 4 bimodal predictor entries */
    unsigned baseHystShift;

    /** Internal structures and variables */

    uint64_t created, deleted;

    /** "Use alternate prediction on newly allocated":
     * a 4-bit counter  to determine whether the newly
     * allocated entries should be considered as
     * valid or not for delivering  the prediction */
    int useAltOnNA;

    /** Control counter for the smooth resetting of useful counters */
    unsigned logCTick, cTick;

    /** Use a path history as on  the OGEHL predictor. Use that of the branch predictor. */
    unsigned *phist;
    /** Global history. Use that of the branch predictor. */
    std::deque<TageBP::Hist> *globHist;

    /** Log of number of entries  on each tagged component */
    std::vector<unsigned> logg;

    /** Utility for computing TAGE indices */
    std::vector<Folded_history> ch_i;

    /** Utility for computing TAGE tags */
    std::vector<Folded_history> ch_t[2];

    /** Base VTAGE table */
    std::vector<Bentry> bTable;

    /** Tagged VTAGE tables */
    std::vector< std::vector<Gentry> > gTable;

    /** Used for storing the history lengths */
    std::vector<unsigned> m;


    /** Map to store VPSave objects ie track all the inflight state of each prediction */
    std::map<void*, uint64_t> hists;


    /** For the pseudo-random number generator */
    unsigned seed;

    /** Mask to compute the index in the base bimodal predictor */
    Addr baseMask;

    /** Mask to compute the tag for the different tagged tables */
    std::vector<Addr> tagMask;

    /** Mask to compute the index in the different tagged tables */
    std::vector<Addr> gMask;
    std::vector<unsigned> proba;
    /**
     * Count how many prediction have been made
     * since the last misprediction due to the base predictor
     */
    unsigned sinceBaseMispred;

    /** Number of bits to shift the instruction over to get rid of the word
     *  offset.
     */
    unsigned instShiftAmt;



    /** Global Stats. Gem5 specific */

    //Stats for frontend predictions
    Stats::Scalar taggedHit;
    Stats::Scalar altTaggedHit;
    Stats::Distribution table;

    //Stats for backend predictions
    /** Stat for number of predictions given by the tagged components. */
    Stats::Scalar taggedPred;
    /** Stat for number of predictions given by the base bimodal predictor. */
    Stats::Scalar basePred;
    /** Stat for number of standard predictions. */
    Stats::Scalar standardPred;
    /** Stat for number of predictions given by the alternate prediction. */
    Stats::Scalar altPred;

    Stats::Scalar alloc;

    Stats::Scalar attempted;
    Stats::Scalar correct;
    Stats::Scalar incorrect;

    Stats::Formula accuracy;
    Stats::Formula coverage;
    Stats::Formula nonTagged;
    Stats::Scalar correctBasePred;
    Stats::Scalar commit_correct, commit_incorrect;
    Stats::Formula accuracy_ideal, coverage_ideal;

    /** Confidence Stats */
    /** Stat for number of high confidence predictions. */
    Stats::Scalar highConf;
    /** Stat for number of correct high confidence predictions. */
    Stats::Scalar highConfHit;
    /** Stat for the number of incorrect high confidence predictions */
    Stats::Scalar highConfMiss;
    /** Stat for number of medium confidence predictions. */
    Stats::Scalar transientConf;
    /** Stat for number of correct medium confidence predictions. */
    Stats::Scalar transientConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar transientConfMiss;
    /** Stat for number of low confidence predictions. */
    Stats::Scalar lowConf;
    /** Stat for number of correct low confidence predictions. */
    Stats::Scalar lowConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar lowConfMiss;

    /** Stat for number of high confidence base predictions. */
    Stats::Scalar baseHighConf;
    /** Stat for number of correct high confidence base bimodal predictions. */
    Stats::Scalar baseHighConfHit;
    /** Stat for the number of incorrect high confidence base bimodal predictions */
    Stats::Scalar baseHighConfMiss;
    /** Stat for number of medium confidence base bimodal predictions. */
    Stats::Scalar baseTransientConf;
    /** Stat for number of correct medium confidence base bimodal predictions. */
    Stats::Scalar baseTransientConfHit;
    /** Stat for the number of incorrect medium confidence base bimodal predictions */
    Stats::Scalar baseTransientConfMiss;
    /** Stat for number of low confidence base bimodal predictions. */
    Stats::Scalar baseLowConf;
    /** Stat for number of correct low confidence base bimodal predictions. */
    Stats::Scalar baseLowConfHit;
    /** Stat for the number of incorrect medium confidence base bimodal predictions */
    Stats::Scalar baseLowConfMiss;

    Stats::Distribution accesses;

    TageBP *tage;

};

#endif //  __CPU_O3_VTAGE_VPRED_HH__
