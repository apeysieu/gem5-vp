/*
 * stride.hh
 *
 *  Created on: Oct 12, 2012
 *      Author: aperais
 */

#ifndef STRIDE_HH_
#define STRIDE_HH_

#include <string>
#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"

/**
* Class implementing a finite PS predictor.
**/
class StrideVP
{

public:

        struct VPSave {
                Addr index_vht;
                Addr tag_vht;
                prediction_t pred;
                bool mismatch, low_conf_inflight;
                Prediction previous;
                bool spec_update;
                unsigned way;
                Addr ip;
        };

        class Entry
        {
          private:
            Addr _tag;
            Prediction _pred;
            Prediction _stride1;
            Prediction _stride2;
          public:

            ConfCounter _confidence;

            static std::string name;

            Entry()
            {
            }

            /**
             * @param[counterwidth] Width of the confidence counter in bits.
             * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
             */
            Entry(unsigned counterwidth, std::vector<unsigned> &proba)
            {
                this->_stride1 = Prediction();
                this->_stride2 = Prediction();
                this->_pred = Prediction();
                this->_tag = 0;
                this->_confidence = ConfCounter(counterwidth, 0, &proba);
            }

            Prediction getPred()
            {
              return this->_pred;
            }

            Prediction getStride() {
                return this->_stride1;
            }

            Prediction getStride2() {
                return this->_stride2;
            }

            bool tagMatches(Addr tag) const
            {
              return this->_tag == tag;
            }

            unsigned getConfidence() const
            {
              return this->_confidence.read();
            }


            /**
             * @param[value] The speculative value to update the entry with.
             * @param[save] Structure to save the "old" last value.
             */
            void update(Prediction &value, VPSave &save) {
                save.previous = this->_pred;
                this->_pred = value;
            }

            /**
             * @param[value] Restore a previously saved last value.
             */
            void restore(Prediction &value) {
                this->_pred = value;
            }

            /**
             * @param[value] The actual result of the instruction.
             * @param[fetch_value] Value seen at fetch to compute the stride.
             * @param[tag] The new tag.
             * @param[outcome] True if the predictor predicted correctly, false otherwise.
             * @param[conf] The confidence seen at fetch (may differ from the confidence currently in the entry).
             * @param[squashed] True if this is an update on squash, false is this is an update at retire.
             */
            void update(Prediction &value, Prediction *fetch_value, Addr tag, bool outcome, int conf, bool squashed = false)
            {
              if(!tagMatches(tag)) {
                this->_confidence.set(0);
                if(!squashed) {
                        this->_tag = tag;
                        this->_pred = value;
                        this->_stride1 = Prediction();
                        this->_stride2 = Prediction();
                }
              } else {
                  if(!squashed) {
#ifdef NOREAD_AT_COMMIT
                      Prediction stride = value - *fetch_value;
#else
                      Prediction stride = value - _pred;
#endif
                      if(this->_stride2 == stride) {
                          this->_stride1 = this->_stride2;
                      } else {
                          this->_stride2 = stride;
                      }
                      this->_pred = value;

                  }
#ifdef NOREAD_AT_COMMIT
                  _confidence.set(conf);
#endif
                  _confidence.updateConf(outcome);
              }
            }

        };

        /** PRIVATE MEMBERS **/

        AssociativeArray<StrideVP, StrideVP::Entry> fetch_VHT;
        AssociativeArray<StrideVP, StrideVP::Entry> commit_VHT;
        unsigned TAG_SIZE_VHT;
        std::string _name;
        unsigned assoc;
        std::vector<int> _low_conf_inflight;
        std::vector<int> inflight;
        std::vector<unsigned> proba;


        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct;
        Stats::Scalar incorrect;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;
        Stats::Scalar commit_correct, commit_incorrect;
        Stats::Scalar miss;
        Stats::Formula accuracy_ideal, coverage_ideal;
        Stats::Formula missrate;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;

        Stats::Scalar access;
        Stats::Scalar nonZeroStride;


public:
        /**
         * @param[name] Name of the class (will appear in stats).
         * @param[size_VHT] Size of the VHT in entries.
         * @param[tag_vht In bits.
         * @param[assoc] Associativity of the VHT.
         * @param[counterWidth] In bits.
         * @param[probability] Probability vector used to control forward transition of confidence counters.
         */
        StrideVP(std::string &name, unsigned size_VHT, unsigned tag_vht, unsigned assoc,  unsigned counterwidth, std::vector<unsigned> &probability);

        ~StrideVP();

        /**
         * @param[ip] Address of the instruction.
         * @param[micropc] uop index.
         * @param[vp_history] Pointer to modify.
         * @post vp_history points to the data structure of LVP describing the current prediction.
         * @return prediction of inst (ip,micropc)
         */
        prediction_t lookup(Addr ip, MicroPC micropc, void *&vp_history);

        /**
         * @param[value] Actual result of the instruction (dummy if update on squash).
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[squash] True if update on squash, false if update at retire.
         */
        void update(Prediction & value, void *vp_history, bool mispred, bool squashed);

        /**
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[remove] True if the data structure should be deleted, false otherwise.
         */
        void squash(void *vp_history, bool remove);

        const std::string &name() const { return _name; }

        /**
         * @pre Stats are registered with the gem5 infrastructure.
         */
        void regStats();

        /**
         * @param[history] Structure describing the prediction.
         * @param[outcome] True if the prediction was correct, false otherwise.
         */
        void updateStats(VPSave &history, bool outcome);

};

#endif /* STRIDE_HH_ */
