/*
 * fcm_vtage_ps.hh
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */

#ifndef VTAGE_FCM_PS_HH_
#define VTAGE_FCM_PS_HH_

#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/vpred/PS.hh"
#include "cpu/vpred/VTAGE.hh"
#include "cpu/vpred/associative_array.hh"
#include "cpu/vpred/fcm_spec.hh"
#include "debug/ValuePredictor.hh"

class VTAGE_FCM_PS
{
protected:


        struct VPSave {
                        enum Type {
                                FCM,
                                PS,
                                VTAGE,
                                FCM_VTAGE,
                                FCM_PS,
                                PS_VTAGE,
                                All,
                                None
                        };

                Type who;
                bool isBranch;
                FCM_SPEC_VP::VPSave fcm_save;
                PSVP::VPSave ps_save;
                VTageVP::VPSave *vtage_save;
        };

        enum ArbiterPair {
                FCM_VTAGE,
                PS_VTAGE,
                FCM_PS
        };



public:
        /**
         * Constructor.
         *@param[in] threshold		The threshold of the 2level predictor.
         *@param[in] size		Number of entries in the VHT (power of 2).
         **/
        VTAGE_FCM_PS(std::string &name,
                        //PS
                        unsigned size_ps_vht,
                        unsigned size_ps_sht,
                        unsigned tag_ps_vht_size,
                        unsigned tag_ps_sht_size,
                        unsigned bhist_length,
                        std::vector<unsigned> &proba_ps,
                        unsigned associativity_vht,
                        unsigned associativity_sht,
                        //FCM
                        unsigned size_fcm_vht,
                        unsigned size_fcm_ht,
                        unsigned tag_fcm_size,
                        unsigned lhist_length,
                        std::vector<unsigned> &proba_fcm,
                        unsigned assoc_fcm,
                        //VTAGE
                        unsigned numHistComponents,
                        unsigned numLogBaseEntry,
                        unsigned minHistSize,
                        unsigned maxHistSize,
                        unsigned baseHystShift,
                        unsigned instShiftAmt,
                        std::deque<TageBP::Hist> *bhist,
                        unsigned *phist,
                        std::vector<unsigned> &proba_vtage,
                        //Misc
                        unsigned countWidth
                        );

        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        void flush_branch(void *vp_history);

        void updateFoldedHist(bool save, void *&vp_history);

        void updateVHT(uint16_t index, Prediction & value);

        void update(Prediction & value, void *vp_history, bool squashed);

        void squash(void *vp_history, bool remove, bool recompute);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);

        prediction_t chooseBetweenFCMAndVTage(VPSave *history, prediction_t &vtage_pred);
        prediction_t chooseBetweenPSAndVTage(VPSave *history, prediction_t &vtage_pred);
        prediction_t chooseBetweenFCMAndPS(VPSave *history);

        Addr hashPC(Addr ip);

private:

        /** PRIVATE MEMBERS **/
        unsigned FCM_HT_INDEX, FCM_VHT_INDEX, PS_VHT_INDEX, PS_VHT_TAG_LENGTH, PS_SHT_INDEX, PS_SHT_TAG_LENGTH, FCM_TAG_LENGTH, BHIST_LENGTH;

        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fcm_fetch_HT;
        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fcm_commit_HT;
        std::vector<Prediction> fcm_VHT;
        std::vector<ConfCounter> fcm_conf;
        std::vector<unsigned> fcm_proba;

        AssociativeArray<PSVP, PSVP::VHTEntry> ps_fetch_VHT;
        AssociativeArray<PSVP, PSVP::VHTEntry> ps_commit_VHT;
        AssociativeArray<PSVP, PSVP::SHTEntry> ps_SHT;
        std::vector<unsigned> ps_proba;

        VTageVP *tage;
        std::deque<TageBP::Hist> *bhist;
        unsigned assoc_fcm, assoc_ps_vht, assoc_ps_sht;

        std::string _name;

        std::vector<int> ps_low_conf_inflight, fcm_low_conf_inflight, ps_inflight, fcm_inflight;


        std::vector<int> arbiter;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct, correct_ps, correct_fcm, correct_vtage, correct_ps_vtage, correct_fcm_vtage, correct_fcm_ps, correct_all;
        Stats::Scalar incorrect, incorrect_ps, incorrect_fcm, incorrect_vtage, incorrect_ps_vtage, incorrect_fcm_vtage, incorrect_fcm_ps, incorrect_all;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Scalar made_by_ps, made_by_fcm, made_by_vtage;
        Stats::Scalar spec_update_fcm, spec_update_ps, spec_update_vtage;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
              Stats::Scalar ign_correct, ign_incorrect;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;
};

#endif /* VTAGE_FCM_PS_HH_ */
