/*
 * vtage_stride.hh
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */



#ifndef VTAGE_STRIDE_HH_
#define VTAGE_STRIDE_HH_



#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"
#include "cpu/vpred/VTAGE.hh"
#include "cpu/vpred/associative_array.hh"
#include "cpu/vpred/stride.hh"
#include "debug/ValuePredictor.hh"

#define TOURNAMENT_VPRED
#undef TOURNAMENT_VPRED

class VTAGE_Stride
{
protected:


        struct VPSave {
                        enum Type {
                                VTAGE,
                                STRIDE,
                                All,
                                None
                        };

                Type who;
                bool isBranch;
                StrideVP::VPSave stride_save;
                VTageVP::VPSave *vtage_save;

        };

public:
        /**
         * Constructor.
         *@param[in] threshold		The threshold of the 2level predictor.
         *@param[in] size		Number of entries in the VHT (power of 2).
         **/
        VTAGE_Stride(std::string &name,
                        unsigned size_stride_vht,
                        unsigned tag_stride_vht_size,
                        unsigned assoc_stride,
                        std::deque<TageBP::Hist> *bhist,
                        unsigned numHistComponents,
                        unsigned numLogBaseEntry,
                        unsigned minHistSize,
                        unsigned maxHistSize,
                        unsigned baseHystShift,
                        unsigned instShiftAmt,
                        unsigned *phist,
                        unsigned counterwidth,
                        std::vector<unsigned> &proba_vtage,
                        std::vector<unsigned> &proba_stride);

        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        void flush_branch(void *vp_history);

        void updateFoldedHist(bool save, void *&vp_history);

        void update(Prediction & value, void *vp_history, bool mispred, bool squashed);

        void squash(void *vp_history, bool remove, bool recompute);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);


private:

        /** PRIVATE MEMBERS **/
        unsigned STRIDE_VHT_INDEX, STRIDE_TAG_VHT;


        AssociativeArray<StrideVP, StrideVP::Entry> stride_fetch_VHT;
        AssociativeArray<StrideVP, StrideVP::Entry> stride_commit_VHT;
        std::vector<unsigned> stride_proba;
        unsigned assoc_stride;

        VTageVP *tage;

        std::string _name;

        std::vector<int> stride_low_conf_inflight, stride_inflight;

#ifdef TOURNAMENT_VPRED
        std::vector<int> arbiter;
#else
        int arbiter;
#endif

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct, correct_stride, correct_vtage, correct_all;
        Stats::Scalar incorrect, incorrect_stride, incorrect_vtage, incorrect_all;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Scalar made_by_stride, made_by_vtage;
        Stats::Scalar spec_update_vtage;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;



        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;
};

#endif /* VTAGE_STRIDE_HH_ */

