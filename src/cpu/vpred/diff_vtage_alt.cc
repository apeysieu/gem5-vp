/*
 * diff_vtage_alt.cc
 *
 *  Created on: Jun 24, 2013
 *      Author: aperais
 */


#include "cpu/vpred/diff_vtage_alt.hh"

#include <cmath>
#include <cstdlib>
#include <sstream>

#include "base/intmath.hh"
#include "base/trace.hh"
#include "cpu/o3/helper.hh"
#include "cpu/pred/TAGE.hh"
#include "debug/TAGEAlloc.hh"
#include "debug/ValuePredictor.hh"

DiffVTAGE_ALT::DiffVTAGE_ALT(std::string &name,
                unsigned _numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned _baseHystShift,
                unsigned counterWidth,
                unsigned _instShiftAmt,
                std::vector<unsigned> & _filterProbability,
                std::deque<TageBP::Hist> *bpred_globHist,
                unsigned *bpred_phist,
                TageBP *tage)
: _name(name + ".VPredUnit.DVTAGE"),
  numHistComponents(_numHistComponents),
#ifdef X86_ISA
  baseHystShift(0),
#else
  baseHystShift(_baseHystShift),
#endif
  useAltOnNA(0),
  logCTick(19),
  cTick((1 << (logCTick - 1))),
  phist(bpred_phist),
  globHist(bpred_globHist),
  seed(0),
#ifdef X86_ISA
  instShiftAmt(0)
#else
instShiftAmt(_instShiftAmt)
#endif
{
    proba = _filterProbability;


    unsigned i;
    unsigned size = numHistComponents + 1;

    /** initialize structures */
    this->tage = tage;
    /** logg */
    logg.resize(size);
    for(i = 1; i < size; ++i) {
        logg[i] = 10;
    }
    /** m */
    m.resize(size);
    /** computes the geometric history lengths */
    m[1] = minHistSize;
    m[numHistComponents] = maxHistSize;

    for(i = 2; i < size; i++) {
        m[i] =
                (int) (((double) minHistSize *
                        pow ((double) (maxHistSize) / (double) minHistSize,
                                (double) (i - 1) / (double) ((numHistComponents - 1)))) + 0.5);
    }

    /** ch_i, ch_t */
    ch_i.resize(size);
    ch_t[0].resize(size);
    ch_t[1].resize(size);

    for(i = 1; i < size; ++i) {
        ch_i[i].init(m[i], 10);
        ch_t[0][i].init(ch_i[i].olength, i+12);
        ch_t[1][i].init(ch_i[i].olength, i+11);
    }


    /** bTable */
    bTable.resize(1 << numLogBaseEntry, Bentry(counterWidth, proba));
    std::cerr << bTable.size() << std::endl;

    /** gTable */
    gTable.resize(size);
    for(i = 1; i < size; ++i) {
        gTable[i].resize(1 << 10, Gentry(counterWidth, proba));
        std::cerr << gTable[i].size() << std::endl;
    }



    /** Compute masks */
    baseMask = ((1 << (numLogBaseEntry)) - 1);


    tagMask.resize(size);
    for(i = 1; i < size; ++i) {
        tagMask[i] = ((1 << (i+12)) - 1);
    }

    gMask.resize(size);
    for(i = 1; i < size; ++i) {
        gMask[i] = ((1 << 10) - 1);
    }

}


void
DiffVTAGE_ALT::regStats()
{
        zero_stride
        .name(name() + ".ZeroStride")
        .desc("Number of predictions made with a stride 0")
        ;
        table
        .name(name() + ".TableSpread")
        .init(0,6,1)
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        taggedHit
        .name(name() + ".taggedHit")
        .desc("Number of hits in a tagged table during a lookup for the standard pred")
        ;

        altTaggedHit
        .name(name() + ".altTaggedHit")
        .desc("Number of hits in a tagged table during a lookup for the altpred")
        ;

        alloc
        .name(name() + ".alloc")
        .desc("Number of allocations in tagged components")
        ;


        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        ;

        nonTagged
        .name(name() + ".nonTagged")
        .desc("Ratio of non tagged predictions out of potential predictions")
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        taggedPred
        .name(name() + ".taggedPred")
        .desc("Number of tagged components predictions")
        ;

        basePred
        .name(name() + ".basePred")
        .desc("Number of base bimodal predictions")
        ;

        standardPred
        .name(name() + ".standardPred")
        .desc("Number of standard predictions")
        ;

        altPred
        .name(name() + ".altPred")
        .desc("Number of predictions given by the alternate prediction")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        baseHighConf
        .name(name() + ".baseHighConf")
        .desc("Number of high confidence base predictions")
        ;

        baseHighConfHit
        .name(name() + ".baseHighConfHit")
        .desc("Number of correct high confidence base redictions")
        ;

        baseHighConfMiss
        .name(name() + ".baseHighConfMiss")
        .desc("Number of incorrect high confidence base predictions.")
        ;

        baseTransientConf
        .name(name() + ".baseTransientConf")
        .desc("Number of transient confidence base predictions")
        ;

        baseTransientConfHit
        .name(name() + ".baseTransientConfHit")
        .desc("Number of correct medium confidence base predictions")
        ;

        baseTransientConfMiss
        .name(name() + ".baseTransientConfMiss")
        .desc("Number of incorrect medium confidence base predictions.")
        ;

        baseLowConf
        .name(name() + ".baseLowConf")
        .desc("Number of low confidence base predictions")
        ;

        baseLowConfHit
        .name(name() + ".baseLowConfHit")
        .desc("Number of correct low confidence base bimodal predictions")
        ;

        baseLowConfMiss
        .name(name() + ".baseLowConfMiss")
        .desc("Number of incorrect low confidence base bimodal predictions.")
        ;

        correctBasePred
        .name(name() + ".correctBasPred")
        .desc("Number of correct predictions that flowed from the base predictors")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        nonTagged = correctBasePred / attempted;

        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);

}

unsigned
DiffVTAGE_ALT::F(unsigned hist, unsigned size, unsigned bank)
{
    Addr res, h1, h2;
    res = (Addr)hist;

    res = res & ((1 << size) - 1);
    h1 = (res & gMask[bank]);
    h2 = (res >> logg[bank]);
    h2 = ((h2 << bank) & gMask[bank]) + (h2 >> (logg[bank] - bank));
    res = h1 ^ h2;
    res = ((res << bank) & gMask[bank]) + (res >> (logg[bank] - bank));
    return (unsigned)res;
}

unsigned
DiffVTAGE_ALT::gIndex(Addr &branch_addr, unsigned bank)
{
    Addr index;
    unsigned M = (m[bank] > 16) ? 16 : m[bank];
    index = branch_addr ^ (branch_addr >> (abs(logg[bank] - bank) + 1)) ^
            ch_i[bank].comp ^ F(*phist, M, bank);
    return (unsigned)(index & gMask[bank]);
}

unsigned
DiffVTAGE_ALT::gTag(Addr &branch_addr, unsigned bank)
{
    Addr tag = branch_addr ^ ch_t[0][bank].comp ^ (ch_t[1][bank].comp << 1);
    return (unsigned)(tag & tagMask[bank]);
}

prediction_t
DiffVTAGE_ALT::getBasePred(Addr &branch_addr, bool &saturated)
{
    unsigned index = bIndex(branch_addr);
    saturated = bTable[index].hyst.read() == 7;
    if(bTable[index].stride == Prediction()) {
        zero_stride++;
    }
        DPRINTF(ValuePredictor, "Index for base: %u, Stride is %s, Value is %s\n",  index, bTable[index].stride.tostring(), bTable[index].last_value_fetch.tostring());
    return prediction_t(bTable[index].last_value_fetch + bTable[index].stride, bTable[index].hyst.read());


}

void
DiffVTAGE_ALT::baseUpdate(Addr branch_addr, Prediction & val, Prediction &value_fetch, bool outcome, unsigned conf, bool count, bool squashed)
{
    unsigned index = bIndex(branch_addr);

    bTable[index].ctrupdate(outcome, val, &value_fetch, conf, count, squashed);
}


prediction_t
DiffVTAGE_ALT::lookup(Addr &addr, MicroPC micropc, void *&vp_history)
{

    int i;
    unsigned size = numHistComponents + 1;
    unsigned hitBank = 0;
    unsigned altBank = 0;
    prediction_t tagePred;
    prediction_t altPred;

    bool choseAlt = false;
    bool baseSaturated = false;

    // Create DiffVTAGE_ALT::VPSave. This is the history for a new instruction.
    VPSave *history = new VPSave();

    // TAGE prediction
    // computes the table addresses and the partial tags
    history->instAddr = addr;

    history->gI.resize(size);
    history->gTag.resize(size);



    Addr base_upc = createUPCindex(micropc, (Addr) log2(bTable.size()));
    Addr tagged_upc =createUPCindex(micropc, (Addr) log2(gTable[1].size()));



    DPRINTF(ValuePredictor, "Predicting for address %lx, hashing to 0x%lx for base and 0x%lx for tagged\n", addr, addr ^ base_upc, addr ^ tagged_upc);

    Addr baddr = addr ^ base_upc;
    Addr taddr = addr ^ tagged_upc;


    history->gI[0] = bIndex(baddr);

    for (i = 1; i < size; ++i)
    {
        history->gI[i] = gIndex(taddr, i);
        history->gTag[i] = gTag(taddr, i);
    }

    assert(baddr == taddr);


    //Look for the bank with longest matching history
    for (i = size - 1; i > 0; --i)
    {

        if (gTable[i][history->gI[i]].tag == history->gTag[i])
        {
            hitBank = i;

            DPRINTF(ValuePredictor, "ValuePred: 0x%lx, entry found in bank: %i,"
            " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);

            break;
        }
    }

    //Look for the alternate bank
    for (i = hitBank - 1; i > 0; --i)
    {
        if (gTable[i][history->gI[i]].tag == history->gTag[i])
            //TODO Check this condition
            //if ((useAltOnNA < 0) || (gTable[i][history->gI[i]].hyst.read()) > 0)
            {
                altBank = i;

                DPRINTF(ValuePredictor, "ValuePred: 0x%lx, alternate entry found in bank: %i,"
                " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);
                break;
            }
    }

    //computes the prediction and the alternate prediction
    if (hitBank > 0)
    {
        taggedHit++;
        if (altBank > 0) {
                altTaggedHit++;
                altPred = prediction_t(gTable[altBank][history->gI[altBank]].last_value_fetch + gTable[altBank][history->gI[altBank]].stride, gTable[altBank][history->gI[altBank]].hyst.read());

        }
        else
        {
            altPred = getBasePred(baddr, baseSaturated);


        }

        tagePred = prediction_t(gTable[hitBank][history->gI[hitBank]].last_value_fetch + gTable[hitBank][history->gI[hitBank]].stride, gTable[hitBank][history->gI[hitBank]].hyst.read());
        history->valid = gTable[hitBank][history->gI[hitBank]].valid == 2;

        DPRINTF(ValuePredictor, "Stride is %s, Value is %s\n",  gTable[hitBank][history->gI[hitBank]].stride.tostring(), gTable[hitBank][history->gI[hitBank]].last_value_fetch.tostring());
        if(gTable[hitBank][history->gI[hitBank]].stride == Prediction()) {
                zero_stride++;
        }
    }
    else
    {
        altPred = getBasePred(baddr, baseSaturated);
        tagePred = prediction_t(altPred.first, altPred.second);
        choseAlt = false;
        history->valid = bTable[history->gI[0]].valid == 2;
        //TODO Fix DPRINTF
//        DPRINTF(ValuePredictor, "ValuePred: 0x%x, no tagged prediction, use base"
//        " base prediction, tagePred: %i, index: %i, sat: %i\n", addr, tagePred.first,
//        bIndex(addr), baseSaturated);
    }

//    history->usedBimod = choseBase;
    history->usedAlt = choseAlt;


    /* save predictor state */
    if(!history)
        panic("No history data structure to save the predictor state");

    /* save prediction, hit/alt bank, GI and GTAG */
    history->bank = hitBank;
    history->altBank = altBank;
    history->tagePred = tagePred;
    history->altPred = altPred;
    history->isBranch = false;
    history->last_value_tage = hitBank == 0 ? bTable[history->gI[0]].last_value_fetch : gTable[hitBank][history->gI[hitBank]].last_value_fetch;
    history->last_value_alt = altBank == 0 ? bTable[history->gI[0]].last_value_fetch : gTable[altBank][history->gI[altBank]].last_value_fetch;

    if(hitBank == 0) {
        bTable[history->gI[0]].updateSpecHist(tagePred.first, true);
        DPRINTF(ValuePredictor, "Inflight: %u, updating hit fetch hist to %s\n", bTable[history->gI[0]].inflight, tagePred.first.tostring());
     } else {
        gTable[hitBank][history->gI[hitBank]].updateSpecHist(tagePred.first, true);
        DPRINTF(ValuePredictor, "Inflight: %u, updating hit fetch hist to %s\n", gTable[hitBank][history->gI[hitBank]].inflight, tagePred.first.tostring());
        if(altBank == 0) {
                bTable[history->gI[0]].updateSpecHist(tagePred.first, true);
                DPRINTF(ValuePredictor, "Inflight: %u, updating alt fetch hist to %s\n", bTable[history->gI[0]].inflight, altPred.first.tostring());
        } else {
                gTable[altBank][history->gI[altBank]].updateSpecHist(tagePred.first, true);
                DPRINTF(ValuePredictor, "Inflight: %u, updating alt fetch hist to %s\n", gTable[altBank][history->gI[altBank]].inflight, altPred.first.tostring());
        }
    }


    /*
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, hitBank: %i, altBank: %i, tagePred: %i,"
    " altTaken: %i, predTaken: %i\n", branch_addr, hitBank, altBank, tagePred,
    altTaken, predTaken);
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, confidence:  high: %i, med: %i, low: %i"
    " (bimod: %i) (Stag: %i, NStag: %i, Wtag: %i, NWtag: %i)\n", branch_addr,
    history->highConf, history->medConf, history->lowConf,
    history->bimHighConf || history->bimMedConf || history->bimLowConf,
    history->Stag, history->NStag, history->Wtag, history->NWtag);*/

    DPRINTF(ValuePredictor, "VPSave :bank: %i, altbank: %i, choseAlt: %x\n",
                history->bank, history->altBank, choseAlt);

    vp_history = static_cast<void *>(history);


    return (choseAlt ? altPred : tagePred);
}


void
DiffVTAGE_ALT::updateFoldedHist(bool save, void *&vp_history)
{
        unsigned size = numHistComponents + 1;
        unsigned i = 0;
        DPRINTF(ValuePredictor, "Updating Folded History in VTAGE\n");

        if(save)
        {
               VPSave *new_record = new VPSave();




                new_record->ch_i_comp.resize(size);
                new_record->ch_t_comp[0].resize(size);
                new_record->ch_t_comp[1].resize(size);

                for (i = 1; i < size; ++i)
                {
                        //DPRINTF(ValuePredictor, "Saving folded history %u: %lu\n", i, ch_i[i].comp);

                        new_record->ch_i_comp[i] = ch_i[i].comp;
                        new_record->ch_t_comp[0][i] = ch_t[0][i].comp;
                        new_record->ch_t_comp[1][i] = ch_t[1][i].comp;
                }


//        	std::stringstream ss;
//        	for(unsigned j = 0; j < 64; j++) {
//        		ss << (*globHist)[j].dir;
//        	}
//        	DPRINTF(ValuePredictor, "History is: %s\n", ss.str());

                new_record->bank = 0;
                new_record->altBank = 0;
                new_record->tagePred = prediction_t(Prediction(),0);
                new_record->altPred = prediction_t(Prediction(),0);
                new_record->usedAlt = false;
                new_record->isBranch = true;

                vp_history = static_cast<void *>(new_record);
        }

    //prepare next index and tag computations
    for (i = 1; i < size; ++i)
    {
        ch_i[i].update(*globHist);
        //DPRINTF(ValuePredictor, "VTAGE: Updated folded history %u: %lu\n", i, ch_i[i].comp);
        ch_t[0][i].update(*globHist);
        ch_t[1][i].update(*globHist);
        DPRINTF(ValuePredictor, "Updating ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
    }

    DPRINTF(ValuePredictor, "ValuePred: GlobHist size: %i, new phist: %x\n", globHist->size(), *phist);
}


void
DiffVTAGE_ALT::updatePredictor(/*Addr &addr,*/ Prediction &val, bool outcome, void *vp_history, bool squashed)
{
    unsigned i,j;
    unsigned size = numHistComponents + 1;
    unsigned nrand = myRandom();



    unsigned hitBank;
    unsigned altBank;

    assert(vp_history);

    VPSave *history = static_cast<VPSave *>(vp_history);

    //Propagate the confidence instead of re reading the table.
    table.sample(history->bank);

    unsigned conf = history->usedAlt ?  history->altPred.second : history->tagePred.second;


    if(squashed) {
//         if(history->bank == 0) {
//                 baseUpdate(history->gI[0], val, false, true);
//         } else {
//                 gTable[history->bank][history->gI[history->bank]].ctrupdate(false, val, true);
//         }
         return;
     }

//    if(history->usedAlt) {
//        if(history->altBank == 0) {
//                commit_correct += bTable[history->gI[0]].pred == val && bTable[history->gI[0]].hyst.read() == 7;
//                commit_incorrect += !(bTable[history->gI[0]].pred == val) && bTable[history->gI[0]].hyst.read() == 7;
//        } else {
//                commit_correct += gTable[history->altBank][history->gI[history->altBank]].pred == val && gTable[history->altBank][history->gI[history->altBank]].hyst.read() == 7;
//                commit_incorrect += !(gTable[history->altBank][history->gI[history->altBank]].pred == val) && gTable[history->altBank][history->gI[history->altBank]].hyst.read() == 7;
//
//        }
//    } else {
//        if(history->bank == 0) {
//                commit_correct += bTable[history->gI[0]].pred == val && bTable[history->gI[0]].hyst.read() == 7 ;
//                commit_incorrect += !(bTable[history->gI[0]].pred == val) && bTable[history->gI[0]].hyst.read() == 7;
//        } else {
//                commit_correct += gTable[history->bank][history->gI[history->bank]].pred == val && gTable[history->bank][history->gI[history->bank]].hyst.read() == 7;
//                commit_incorrect += !(gTable[history->bank][history->gI[history->bank]].pred == val) && gTable[history->bank][history->gI[history->bank]].hyst.read() == 7;
//        }
//    }

    prediction_t &tagePred = history->tagePred;
    prediction_t &altPred = history->altPred;
    hitBank = history->bank;
    altBank = history->altBank;
#ifdef NOREAD_AT_COMMIT_VTAGE
    Prediction stride = val - history->last_value_tage;
#else
    Prediction stride = val - (hitBank == 0 ? bTable[history->gI[0]].last_value_commit : gTable[hitBank][history->gI[hitBank]].last_value_commit);
#endif
#ifndef NOREAD_AT_COMMIT_VTAGE
    if(hitBank == 0) {
        outcome = bTable[history->gI[0]].stride + bTable[history->gI[0]].last_value_commit == val;
    } else {
        outcome = gTable[hitBank][history->gI[hitBank]].stride + gTable[hitBank][history->gI[hitBank]].last_value_commit == val;
    }
#endif

    //TODO Temporary to remove branch instruction value history.
    assert(!history->isBranch);
    assert(!history->usedAlt);

    if(history->isBranch) return;


    // VTAGE UPDATE
    // try to allocate a  new entries only if prediction was wrong
    bool alloc = !outcome && (hitBank < numHistComponents) && history->valid;
    if (hitBank > 0)
    {
        // Manage the selection between longest matching and alternate matching
        // for "pseudo"-newly allocated longest matching entry
        //TODO Look in VTAGE code to fix that
        //bool LongestMatchPred = (gTable[hitBank][history->gI[hitBank]].ctr.read() >= 0);
        bool PseudoNewAlloc =
                (gTable[hitBank][history->gI[hitBank]].hyst.read() == 0);
                        // an entry is considered as newly allocated if its prediction counter is weak
        if (PseudoNewAlloc)
        {
            if (tagePred.first == val) {
                alloc = false;
            }
            // if it was delivering the correct prediction, no need to allocate a new entry
            //even if the overall prediction was false
            if(altPred.second == 7) {
                if (!(altPred.first == tagePred.first))
                {
                        if (altPred.first == val)
                        {
                                if (useAltOnNA < 7)
                                        useAltOnNA++;
                        }
                        else if (useAltOnNA > -8)
                                useAltOnNA--;
                }
            }
            //wtf
//            if (useAltOnNA >= 0)
//                tagePred = LongestMatchPred;
        }
    }

    if(alloc) {
        DPRINTF(TAGEAlloc, "Allocating entry for inst 0x%lx\n", history->instAddr);
    }

    DPRINTF(ValuePredictor, "ValuePred: update, hitBank: %i, altBank: %i, tagePred: %lu,"
    " altPred: %lu, ALLOC: %i\n", hitBank, altBank, tagePred.first.tostring(),
    altPred.first.tostring(), alloc);

    if (alloc)
    {
        // is there some "unuseful" entry to allocate
        unsigned min = 1;
        for (i = numHistComponents; i > hitBank; --i) {
            if (gTable[i][history->gI[i]].u < min) {
                min = gTable[i][history->gI[i]].u;
            }

        }
        // we allocate an entry with a longer history
        //to  avoid ping-pong, we do not choose systematically the next entry, but among the 3 next entries
        unsigned Y = nrand & ((1 << (numHistComponents - hitBank - 1)) - 1);
        unsigned X = hitBank + 1;
        if (Y & 1)
        {
            X++;
            if (Y & 2)
                X++;
        }
        //NO ENTRY AVAILABLE:  ENFORCES ONE TO BE AVAILABLE
        //TODO Do we need this?

        if (min > 0)
            gTable[X][history->gI[X]].u = 0;

        //Allocate only  one entry
        for (i = X; i < size; ++i) {
            if (gTable[i][history->gI[i]].u == 0)
            {
                this->alloc++;

                gTable[i][history->gI[i]].tag = history->gTag[i];
                gTable[i][history->gI[i]].hyst.set(0);
                gTable[i][history->gI[i]].u = 0;
                gTable[i][history->gI[i]].stride = stride;
                gTable[i][history->gI[i]].valid = 2;
                gTable[i][history->gI[i]].last_value_commit = val;
                gTable[i][history->gI[i]].last_value_fetch = val;

                //TODO Fix DPRINTF
                DPRINTF(ValuePredictor, "ValuePred: new entry allocated,"
                " table: %i, index: %i, tag: %i\n", i,
               history->gI[i], history->gTag[i]);

                break;
            }
        }
    }
    //periodic reset of u
    cTick++;
    if ((cTick & ((1 << logCTick) - 1)) == 0)
        // reset least significant bit
        // most significant bit becomes least significant bit
        for (i = 1; i < size; ++i)
            for (j = 0; j < (1 << logg[i]); ++j)
                gTable[i][j].u = 0;

    if (hitBank > 0)
    {
        DPRINTF(ValuePredictor, "Updating bank %u, inflight %i\n", hitBank, gTable[hitBank][history->gI[hitBank]].inflight);
        gTable[hitBank][history->gI[hitBank]].ctrupdate(tagePred.first == val, val, &history->last_value_tage, conf, true);
        DPRINTF(ValuePredictor, "last value: %s\n", gTable[hitBank][history->gI[hitBank]].last_value_commit.tostring());

        //if the provider entry is not certified to be useful also update the alternate prediction
        if (gTable[hitBank][history->gI[hitBank]].u == 0)
        {
            if (altBank > 0) {
                DPRINTF(ValuePredictor, "Updating altbank %u, inflight %i\n", altBank, gTable[altBank][history->gI[altBank]].inflight);
                gTable[altBank][history->gI[altBank]].ctrupdate(altPred.first == val, val, &history->last_value_alt, altPred.second, true);
                DPRINTF(ValuePredictor, "last value: %s\n", gTable[altBank][history->gI[altBank]].last_value_commit.tostring());
            }
            if (altBank == 0) {
                //baseUpdate(addr, val, altPred.first == val);
                DPRINTF(ValuePredictor, "Updating altbank %u index %u, inflight %i\n", altBank, history->gI[0], bTable[history->gI[0]].inflight);
                baseUpdate(history->gI[0], val, history->last_value_alt, altPred.first == val, altPred.second, true);
                DPRINTF(ValuePredictor, "last value: %s\n", bTable[history->gI[0]].last_value_commit.tostring());
            }
        }
    }
    else {
         DPRINTF(ValuePredictor, "Updating hitbank %u index %u, inflight %i\n", hitBank, history->gI[0], bTable[history->gI[0]].inflight);
        //baseUpdate(addr, val, altPred.first == val);
        baseUpdate(history->gI[0], val, history->last_value_tage, tagePred.first == val, conf, true);
        DPRINTF(ValuePredictor, "last value: %s\n", bTable[history->gI[0]].last_value_commit.tostring());
    }

    // update the u counter
    if (!(tagePred.first == altPred.first))
    {
        if(tagePred.first == val && hitBank > 0)
        {
            gTable[hitBank][history->gI[hitBank]].u = 1;
        } else {
           /* if (useAltOnNA < 0)*/ gTable[hitBank][history->gI[hitBank]].u = 0;
        }
    } else if(altPred.second != 7 && tagePred.first == val && hitBank > 0) {
         gTable[hitBank][history->gI[hitBank]].u = 1;
    }

    updateStats(*history, outcome);


}

void
DiffVTAGE_ALT::squash(void *vp_history, bool remove, bool recompute)
{
        VPSave *hist = static_cast<VPSave*>(vp_history);
        if(!remove && !recompute) {
                assert(!hist->isBranch);
        }
        if(hist->isBranch) {
                DPRINTF(ValuePredictor, "squashing a branch in value predictor\n");
                recoverBHist(vp_history, recompute);
        }
        if(remove) recoverVHist(vp_history);
}

void
DiffVTAGE_ALT::flush_branch(void *vp_history)
{
         VPSave *history = static_cast<VPSave *>(vp_history);
         DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
         delete history;
}

void
DiffVTAGE_ALT::recoverBHist(/*Addr &addr,*/ void *vp_history, bool recompute)
{
    unsigned size = numHistComponents + 1;
    unsigned i;

    VPSave *rollback = static_cast<VPSave*>(vp_history);

    /** Restore history */
    assert(vp_history);
    assert(rollback->isBranch);

    //TODO Fix DPRINTF
//    DPRINTF(ValuePredictor, "ValuePred: 0x%x, recover: phist: %x",
//    addr, phist);


    for (i = 1; i < size; ++i)
    {
        ch_i[i].comp = rollback->ch_i_comp[i];
        //DPRINTF(ValuePredictor, "Restoring folded history %u: %lu\n", i, ch_i[i].comp);
        ch_t[0][i].comp = rollback->ch_t_comp[0][i];
        ch_t[1][i].comp = rollback->ch_t_comp[1][i];

        DPRINTF(ValuePredictor, "Restoring ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
    }
//    DPRINTF(ValuePredictor, "History is:");
//    for(unsigned j = 0; j < 64; j++) {
//    	printf("%u", (*globHist)[j].dir);
//    }
//    printf("\n");

    //Call to recompute the cyclic registers with the correct global branch history
    //Only if needed.
    if(recompute) updateFoldedHist(false, vp_history);
}

void
DiffVTAGE_ALT::recoverVHist(/*Addr &addr,*/ void *vp_history)
{
        //Essentially we don't have much to do here.
        VPSave *history = static_cast<VPSave *>(vp_history);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);

        if(!history->isBranch) {
                if(history->bank == 0) {
                        bTable[history->gI[0]].inflight--;
                        bTable[history->gI[0]].last_value_fetch = history->last_value_tage;
                        if(bTable[history->gI[0]].inflight == 0) {
                                bTable[history->gI[0]].last_value_fetch = bTable[history->gI[0]].last_value_commit;
                        }
                        DPRINTF(ValuePredictor, "Setting last_value_fetch to %s\n", history->last_value_tage.tostring());
                } else {
                        gTable[history->bank][history->gI[history->bank]].inflight--;
                        gTable[history->bank][history->gI[history->bank]].last_value_fetch = history->last_value_tage;
                        if(gTable[history->bank][history->gI[history->bank]].inflight == 0) {
                                gTable[history->bank][history->gI[history->bank]].last_value_fetch = gTable[history->bank][history->gI[history->bank]].last_value_commit;
                        }
                        if(history->altBank == 0) {
                                bTable[history->gI[0]].inflight--;
                                bTable[history->gI[0]].last_value_fetch = history->last_value_alt;
                                if(bTable[history->gI[0]].inflight == 0) {
                                        bTable[history->gI[0]].last_value_fetch = bTable[history->gI[0]].last_value_commit;
                                }
                        } else {
                                gTable[history->altBank][history->gI[history->altBank]].inflight--;
                                gTable[history->altBank][history->gI[history->altBank]].last_value_fetch = history->last_value_alt;
                                if(gTable[history->bank][history->gI[history->bank]].inflight == 0) {
                                        gTable[history->altBank][history->gI[history->altBank]].last_value_fetch = gTable[history->altBank][history->gI[history->altBank]].last_value_commit;
                                }
                        }
                }
        }
        delete history;
}

void
DiffVTAGE_ALT::update(Prediction &  val, void *vp_history,
        bool squashed)
{

        VPSave *history = static_cast<VPSave *>(vp_history);
        assert(!history->isBranch);

        if(history->usedAlt) DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->altPred.first.tostring());
        else DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->tagePred.first.tostring());

        updatePredictor(/*addr,*/ val, (history->usedAlt ? (val == history->altPred.first) : (val == history->tagePred.first)), vp_history, squashed);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        if(!squashed) {
                delete history;
        }

}


//TODO Modify stats
void
DiffVTAGE_ALT::updateStats(VPSave &history, bool outcome)
{
        /** Base stats **/

        bool proceeded = (history.usedAlt && history.altPred.second == 7) || (!history.usedAlt && history.tagePred.second == 7);
        attempted ++;

        correct += outcome && proceeded;
        incorrect += !outcome && proceeded;

        if(history.usedAlt)
        {
                altPred++;
                if(history.altBank == 0)
                {
                        basePred++;
                        if(outcome && proceeded) {
                                correctBasePred++;
                        }
                } else {
                        taggedPred++;
                }
        } else {
                standardPred++;
                if(history.bank == 0)
                {
                        basePred++;
                        if(outcome && proceeded) {
                                correctBasePred++;
                        }
                } else {
                        taggedPred++;
                }
        }

        /** Stats for the tagged components **/

        bool highConf = (!history.usedAlt && history.bank != 0 && history.tagePred.second >= 7)
                                        || (history.usedAlt && history.altBank != 0 && history.altPred.second >= 7);

        bool transient = ((!history.usedAlt && history.bank != 0 && history.tagePred.second >= 1)
                                                        || (history.usedAlt && history.altBank != 0 && history.altPred.second >= 1)) && !highConf;

        bool low_conf = ((!history.usedAlt && history.bank != 0 && history.tagePred.second == 0)
                                                                || (history.usedAlt && history.altBank != 0 && history.altPred.second == 0)) && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;


         //Stats for the base component.

        highConf = (!history.usedAlt && history.bank == 0 && history.tagePred.second >= 7)
                                                                || (history.usedAlt && history.altBank == 0 && history.altPred.second >= 7);

        transient = ((!history.usedAlt && history.bank == 0 && history.tagePred.second >= 1)
                        || (history.usedAlt && history.altBank == 0 && history.altPred.second >= 1)) && !highConf;

        low_conf = ((!history.usedAlt && history.bank == 0 && history.tagePred.second == 0)
                        || (history.usedAlt && history.altBank == 0 && history.altPred.second == 0)) && !highConf && !transient;

        baseHighConf += highConf;
        baseHighConfHit += highConf && outcome;
        baseHighConfMiss += highConf && !outcome;

        baseTransientConf += transient;
        baseTransientConfHit += transient && outcome;
        baseTransientConfMiss += transient && !outcome;

        baseLowConf += low_conf;
        baseLowConfHit += low_conf && outcome;
        baseLowConfMiss += low_conf && !outcome;

}

std::string
DiffVTAGE_ALT::dump()
{
        std::ostringstream oss;
        oss << "VPred: Branch history: ";
        for(std::deque<TageBP::Hist>::iterator it = globHist->begin(); it != globHist->end(); ++it)
        {
                oss << it->dir;
        }
        oss << std::endl;
        return oss.str();
}

