/*
 * fcm_ps.cc
 *
 *  Created on: Nov 6, 2012
 *      Author: aperais
 */


#include "cpu/vpred/fcm_ps.hh"

#include "cpu/o3/helper.hh"

FCM_PS::FCM_PS(std::string &name, unsigned size_ps_vht, unsigned size_ps_sht, unsigned ps_associativity_vht, unsigned ps_associativity_sht, unsigned tag_ps_vht_size, unsigned tag_ps_sht_size,
        unsigned size_fcm_vht, unsigned size_fcm_ht, unsigned tag_fcm_size,
        unsigned lhist_length, std::deque<TageBP::Hist> *bhist, unsigned bhist_length, unsigned counterwidth, std::vector<unsigned> &proba_fcm, std::vector<unsigned> &proba_ps, unsigned assoc_fcm)
{

       this->_name = name + ".VPredUnit.FCM_PS";
       FCM_SPEC_VP::Entry::_lhist_length = lhist_length;

       this->assoc_fcm = assoc_fcm;
       this->assoc_ps_vht = ps_associativity_vht;
       this->assoc_ps_sht =  ps_associativity_sht;

       fcm_proba = proba_fcm;
       ps_proba = proba_ps;

       FCM_HT_INDEX = log2(size_fcm_ht);
       FCM_VHT_INDEX = log2(size_fcm_vht);
       PS_VHT_INDEX = log2(size_ps_vht);
       PS_SHT_INDEX = log2(size_ps_sht);
       PS_TAG_SHT = tag_ps_sht_size;
       PS_TAG_VHT = tag_ps_vht_size;
       FCM_TAG_LENGTH = tag_fcm_size;
       BHIST_LENGTH = bhist_length;
       this->bhist = bhist;

       this->fcm_fetch_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);
       this->fcm_commit_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);

       fcm_VHT.resize(size_fcm_vht);
       fcm_conf.resize(size_fcm_vht);
       for(unsigned i = 0; i < size_fcm_vht; i++) {
           fcm_conf[i] = ConfCounter(2, 0, &fcm_proba);
       }


       this->ps_fetch_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_ps_vht, this->assoc_ps_vht, counterwidth, ps_proba);
       this->ps_commit_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_ps_vht, this->assoc_ps_vht, counterwidth, ps_proba);

       this->ps_SHT = AssociativeArray<PSVP, PSVP::SHTEntry>(size_ps_sht, this->assoc_ps_sht, counterwidth, ps_proba);



       arbiter = 0;
       ps_low_conf_inflight.resize(size_ps_vht);
       ps_inflight.resize(size_ps_vht);
       fcm_low_conf_inflight.resize(size_fcm_ht);
       fcm_inflight.resize(size_fcm_ht);



       //This assert is here because of the way we compute the index of the VHT.
       //assert(pow((double) 2, (double) (8 + lhist_length - 1)) == size_fcm_vht);
}

Addr
FCM_PS::hashPC(Addr ip)
{
        std::bitset<640> tmp;
        for(unsigned i = 0; i < BHIST_LENGTH; i++) {
                tmp[i] = (*bhist)[i].dir;
        }
        return ((ip << BHIST_LENGTH) + tmp.to_ulong());
}

prediction_t
FCM_PS::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{


        Addr base_upc_vht_ps = createUPCindex(micropc, (Addr) log2(this->ps_commit_VHT.size()));
        Addr base_upc_sht_ps =  createUPCindex(micropc, (Addr) log2(this->ps_SHT.size()));

        Addr base_upc_fcm = createUPCindex(micropc, (Addr)log2(this->fcm_commit_HT.size()));

        Addr fcm_ht_index = (base_upc_fcm ^ ip) & (this->fcm_commit_HT.size() - 1);
        Addr fcm_tag = (ip >> this->FCM_HT_INDEX) & ((((Addr) 1) << FCM_TAG_LENGTH) - 1);

        Addr ps_vht_index = (base_upc_vht_ps ^ ip) & (this->ps_commit_VHT.size() - 1);
        Addr ps_sht_index =  (hashPC(ip ^ base_upc_sht_ps) % (this->ps_SHT.size()));

        Addr ps_vht_tag = (ip >> (unsigned) log2(this->ps_commit_VHT.size())) & (((Addr) 1 << PS_TAG_VHT) - 1);
        Addr ps_sht_tag = (ip >> (unsigned) log2(this->ps_SHT.size())) & (((Addr) 1 << PS_TAG_SHT) -1);
        //Addr ps_sht_tag = (hashPC(ip) >> (unsigned) log2(this->ps_SHT.size())) & (((Addr) 1 << PS_TAG_SHT) -1);



        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);


        history->fcm_save.spec_update = false;
        history->ps_save.spec_update = false;
        history->ps_save.low_conf_inflight = false;
        history->fcm_save.low_conf_inflight = false;
        history->fcm_save.ip =  ip ^ base_upc_fcm;


        history->fcm_save.ht_index = fcm_ht_index;
        history->fcm_save.tag = fcm_tag;
        history->fcm_save.way = fcm_commit_HT[fcm_ht_index].tagLookup(fcm_tag);
        history->fcm_save.mismatch =  history->fcm_save.way == assoc_fcm;

        history->ps_save.index_vht = ps_vht_index;
        history->ps_save.tag_vht = ps_vht_tag;
        history->ps_save.index_sht = ps_sht_index;
        history->ps_save.tag_sht = ps_sht_tag;
        history->ps_save.way_vht = ps_commit_VHT[ps_vht_index].tagLookup(ps_vht_tag);
        history->ps_save.mismatch_vht = history->ps_save.way_vht == assoc_ps_vht;

        history->ps_save.way_sht = ps_SHT[ps_sht_index].tagLookup(ps_sht_tag);
        history->ps_save.mismatch_sht = history->ps_save.way_sht == assoc_ps_sht;

        DPRINTF(ValuePredictor, "FCM predicts using entry %u\n", history->fcm_save.ht_index);

        prediction_t pred_fcm, pred_ps;
        if(fcm_inflight[history->fcm_save.ht_index] == 0 && !history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_commit_HT[fcm_ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
        } else if(!history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_fetch_HT[fcm_ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
        }


        pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));


        if(ps_inflight[history->ps_save.index_vht] == 0 && !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                pred_ps = prediction_t(this->ps_commit_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht) + this->ps_SHT[history->ps_save.index_sht].getStride(history->ps_save.way_sht),this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(history->ps_save.way_vht));
        } else if (!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                pred_ps = prediction_t(this->ps_fetch_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht) + this->ps_SHT[history->ps_save.index_sht].getStride(history->ps_save.way_sht),this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(history->ps_save.way_vht));
        }



        if((history->ps_save.mismatch_sht || history->ps_save.mismatch_vht) && history->fcm_save.mismatch) {
                history->who = VPSave::None;
                history->fcm_save.pred = prediction_t(Prediction(),0);
                history->ps_save.pred = prediction_t(Prediction(), 0);
                return prediction_t(Prediction(), 0);
        } else if (history->ps_save.mismatch_sht || history->ps_save.mismatch_vht) {
                history->ps_save.pred = prediction_t(Prediction(), 0);
                //If only PS misses, FCM has one more in flight.
                fcm_inflight[history->fcm_save.ht_index]++;
        } else if(history->fcm_save.mismatch) {
                history->fcm_save.pred = prediction_t(Prediction(), 0);
                //If only FCM misses, PS has one more in flight;
                ps_inflight[history->ps_save.index_vht]++;
        } else {
                //No miss
                fcm_inflight[history->fcm_save.ht_index]++;
                ps_inflight[history->ps_save.index_vht]++;
        }





        history->fcm_save.pred = (history->fcm_save.mismatch ? prediction_t(Prediction(),0) : pred_fcm);
        history->ps_save.pred = (history->ps_save.mismatch_sht || history->ps_save.mismatch_vht) ? prediction_t(Prediction(),0) : pred_ps;

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && ps_low_conf_inflight[history->ps_save.index_vht] > 0) {
                history->ps_save.low_conf_inflight = true;
        }
        if(!history->fcm_save.mismatch && fcm_low_conf_inflight[history->fcm_save.ht_index] > 0) {
                history->fcm_save.low_conf_inflight = true;
        }

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                ps_low_conf_inflight[history->ps_save.index_vht]++;
        }
        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]++;
        }

        //Arbitration if needed

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->fcm_save.low_conf_inflight = false;
        history->ps_save.low_conf_inflight = false;
#endif

        //bool fcm = false;
        bool fcm_high = history->fcm_save.pred.second == 7 && !history->fcm_save.low_conf_inflight;
        bool ps_high = history->ps_save.pred.second == 7 && !history->ps_save.low_conf_inflight;


        if(ps_high && fcm_high) {
                if(history->fcm_save.pred.first == history->ps_save.pred.first) {
                        history->who = VPSave::All;
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_fcm.first, history->ps_save, history->ps_save.way_vht);
                        return pred_fcm;
                } else {
//                        if(arbiter >= 0) {
//                                fcm = true;
//                        } else {
//                                fcm = false;
//                        }
//
//                        if(fcm) {
//                                //Chose FCM so update everybody speculatively with its value
//                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
//                                this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_fcm.first, history->ps_save, history->ps_save.way_vht);
//
//                                history->fcm_save.spec_update = history->fcm_save.pred.first != history->ps_save.pred.first;
//
//                                history->who = VPSave::FCM;
//
//                                made_by_fcm++;
//                                spec_update_fcm += history->fcm_save.spec_update;
//
//                                return pred_fcm;
//
//                        } else {
//                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_ps.first, history->fcm_save, history->fcm_save.way);
//                                this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_ps.first, history->ps_save, history->ps_save.way_vht);
//
//                                history->ps_save.spec_update = history->fcm_save.pred.first != history->ps_save.pred.first;
//
//                                history->who = VPSave::PS;
//                                made_by_ps++;
//                                spec_update_ps += history->ps_save.spec_update;
//                                return pred_ps;
//
//                        }
                        if(!history->ps_save.mismatch_vht && !history->ps_save.mismatch_sht) {
                                this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_ps.first, history->ps_save, history->ps_save.way_vht);
                        }
                        if(!history->fcm_save.mismatch) {
                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                        }

                        history->who = VPSave::None;

                        return prediction_t(Prediction(),0);
                }
        } else if(ps_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_ps.first, history->fcm_save, history->fcm_save.way);
                this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_ps.first, history->ps_save, history->ps_save.way_vht);

                history->ps_save.spec_update = true;

                history->who = VPSave::PS;
                made_by_ps++;
                spec_update_ps += history->ps_save.spec_update;


                return pred_ps;

        } else if(fcm_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_fcm.first, history->ps_save, history->ps_save.way_vht);

                history->fcm_save.spec_update = true;

                history->who = VPSave::FCM;
                made_by_fcm++;
                spec_update_fcm += history->fcm_save.spec_update;


                return pred_fcm;


        } else {
                if(!history->ps_save.mismatch_vht && !history->ps_save.mismatch_sht) {
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(pred_ps.first, history->ps_save, history->ps_save.way_vht);
                }
                if(!history->fcm_save.mismatch) {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                }

                history->who = VPSave::None;

                return prediction_t(Prediction(),0);
        }
}



void
FCM_PS::updateVHT(uint16_t index, Prediction & value)
{
        if(this->fcm_conf[index].read() == 0) {
                this->fcm_VHT[index] = value;
         }
}

void
FCM_PS::update(Prediction & value, void *vp_history, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way_fcm = fcm_commit_HT[history->fcm_save.ht_index].tagLookup(history->fcm_save.tag);

        if(squashed) {
                if(history->who == VPSave::FCM) {
                         this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, false, 0,true);
                } else if(history->who == VPSave::PS) {
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL, history->ps_save.tag_vht, false, 0,true);
                } else if(history->who == VPSave::All) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, false,0, true);
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL,history->ps_save.tag_vht, false,0, true);
                }
                return;
        }

        Addr vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(commit_way_fcm, FCM_VHT_INDEX,  history->fcm_save.ip);

        prediction_t &fcm_pred = history->fcm_save.pred;
        prediction_t &ps_pred = history->ps_save.pred;

        unsigned commit_way_vht = ps_commit_VHT[history->ps_save.index_vht].tagLookup(history->ps_save.tag_vht);
        unsigned commit_way_sht = ps_SHT[history->ps_save.index_sht].tagLookup(history->ps_save.tag_sht);

#ifdef NOREAD_AT_COMMIT
        bool outcome_fcm = fcm_pred.first == value;
#else
        bool outcome_fcm = !history->fcm_save.mismatch && history->fcm_save.pred.first == value && history->fcm_save.pred.second == 7;
#endif
        correct_fcm += outcome_fcm && history->who == VPSave::FCM;
        incorrect_fcm += !history->fcm_save.mismatch &&  history->fcm_save.pred.second == 7 && !outcome_fcm && history->who == VPSave::FCM;

#ifdef NOREAD_AT_COMMIT
        bool outcome_ps = ps_pred.first == value;
#else
        bool outcome_ps = !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && history->ps_save.pred.first == value && history->ps_save.pred.second == 7;
#endif

        correct_ps += outcome_ps && history->who == VPSave::PS;
        incorrect_ps += !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && history->ps_save.pred.second == 7 && !outcome_ps && history->who == VPSave::PS;


        correct_all = outcome_ps && outcome_fcm && history->who == VPSave::All;
        incorrect_all = !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && history->ps_save.pred.second == 7 && !outcome_ps
                        && !history->fcm_save.mismatch &&  history->fcm_save.pred.second == 7 && !outcome_fcm && history->who == VPSave::All;




        outcome_ps = commit_way_sht != assoc_ps_sht && commit_way_vht != assoc_ps_vht &&
                        ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht) + ps_SHT[history->ps_save.index_sht].getStride(commit_way_sht) == value;
        outcome_fcm =  commit_way_fcm != assoc_fcm && fcm_VHT[vht_index] == value;

        //Updating the arbiter
        if(outcome_fcm && !outcome_ps && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7) {
                arbiter < 15 ? arbiter++ : 0;
        } else if(!outcome_fcm && outcome_ps  && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7) {
                arbiter > -16 ? arbiter-- : 0;
        }


        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
        }
        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                ps_low_conf_inflight[history->ps_save.index_vht]--;
                assert(ps_low_conf_inflight[history->ps_save.index_vht] >= 0);
        }

        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]--;
                assert(fcm_inflight[history->fcm_save.ht_index] >= 0);
        }

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                ps_inflight[history->ps_save.index_vht]--;
                assert(ps_inflight[history->ps_save.index_vht] >= 0);
        }


        if(commit_way_fcm != assoc_fcm) {
                this->updateVHT(vht_index, value);
                if(outcome_fcm) {
                        this->fcm_conf[vht_index].increment();
                } else {
                        this->fcm_conf[vht_index].decrement();
                }
                //this->fcm_conf[vht_index].updateConf(outcome_fcm);
        }

        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, outcome_fcm, fcm_pred.second);

        Prediction stride;
        if(commit_way_vht != assoc_ps_vht) {
#ifdef NOREAD_AT_COMMIT
                        stride = value - history->ps_save.previous;
#else
                stride = (value - this->ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht));
#endif
        } else {
                stride = Prediction();
        }


        this->ps_SHT[history->ps_save.index_sht].update(stride, NULL, history->ps_save.tag_sht, outcome_ps, ps_pred.second);
        this->ps_commit_VHT[history->ps_save.index_vht].update(value, NULL, history->ps_save.tag_vht,  outcome_ps, ps_pred.second);


        if(commit_way_vht != assoc_ps_vht && ps_inflight[history->ps_save.index_vht] == 0) {
                this->ps_fetch_VHT[history->ps_save.index_vht].restore(value, commit_way_vht);
        }

        if(commit_way_fcm != assoc_fcm && fcm_inflight[history->fcm_save.ht_index] == 0) {
                DPRINTF(ValuePredictor, "Tag mismatch or not other instances in flight: setting speculative history to committed history\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(commit_way_fcm), commit_way_fcm);
        }

        bool outcome = ((history->who == VPSave::PS || history->who == VPSave::All) && history->ps_save.pred.first == value) || (history->who == VPSave::FCM && history->fcm_save.pred.first == value);
        updateStats(*history, outcome);


        delete history;
}

void
FCM_PS::squash(void *vp_history, bool remove) {
        VPSave *history = static_cast<VPSave*>(vp_history);



        DPRINTF(ValuePredictor, "Squashing entry %u in FCM\n", history->fcm_save.ht_index);

        if(remove) {
                if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                        fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                        assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
                }
                if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                        ps_low_conf_inflight[history->ps_save.index_vht]--;
                        assert(ps_low_conf_inflight[history->ps_save.index_vht] >= 0);
                }

                if(!history->fcm_save.mismatch) {
                        fcm_inflight[history->fcm_save.ht_index]--;
                }

                if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                        ps_inflight[history->ps_save.index_vht]--;
                }
        }

        if(!history->fcm_save.mismatch && fcm_inflight[history->fcm_save.ht_index] != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(history->fcm_save.signatures, history->fcm_save.way);
        } else if(!history->fcm_save.mismatch){
                DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(history->fcm_save.way), history->fcm_save.way);
        }

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && ps_inflight[history->ps_save.index_vht] != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                this->ps_fetch_VHT[history->ps_save.index_vht].restore(history->ps_save.previous, history->ps_save.way_vht);
        } else if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                Prediction tmp = this->ps_commit_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht);
                this->ps_fetch_VHT[history->ps_save.index_vht].restore(tmp, history->ps_save.way_vht);
        }

        if(remove) {
                delete history;
        }
}

void
FCM_PS::regStats()
{

        made_by_ps
                .name(name() + ".madeByPS")
                .desc("Number of predictions made by PS")
                ;

        made_by_fcm
        .name(name() + ".madeByFCM")
        .desc("Number of predictions made by FCM")
        ;

        spec_update_ps
                .name(name() + ".specUpdatePS")
                .desc("Number of time PS updated FCM")
                ;

        spec_update_fcm
                .name(name() + ".specUpdateFCM")
                .desc("Number of time FCM updated PS")
                ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_fcm
                .name(name() + ".correctFCM")
                .desc("Number of correct predictions coming from FCM")
                ;

        correct_ps
                        .name(name() + ".correctPS")
                        .desc("Number of correct predictions coming from PS")
                        ;

        correct_all
        .name(name() + ".correctAll")
        .desc("Number of correct predictions coming from All")
        ;

        incorrect_all
        .name(name() + ".incorrectAll")
        .desc("Number of incorrect predictions coming from All")
        ;

        incorrect_fcm
                        .name(name() + ".incorrectFCM")
                        .desc("Number of correct predictions coming from FCM")
                        ;

        incorrect_ps
                        .name(name() + ".incorrectPS")
                        .desc("Number of correct predictions coming from PS")
                        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        ignoredCorrect
               .name(name() + ".ignoredCorrect")
               .desc("Percentage of correct predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ignoredIncorrect
               .name(name() + ".ignoredIncorrect")
               .desc("Percentage of incorrect predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ign_correct
               .name(name() + ".ignCorrect")
               .desc("Number of correct predictions ignored because low conf inflight")
               ;

               ign_incorrect
               .name(name() + ".ignInorrect")
               .desc("Number of incorrect predictions ignored because low conf inflight")
               ;
        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
         ignoredIncorrect = ign_incorrect / attempted;
}

void
FCM_PS::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         ign_correct +=  proceeded && outcome && ((history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) || (history.ps_save.low_conf_inflight && history.who == VPSave::PS));
         ign_incorrect += proceeded && !outcome &&  ((history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) || (history.ps_save.low_conf_inflight && history.who == VPSave::PS));

         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;


         /** Stats  **/

         bool highConf = history.who == VPSave::PS ? history.ps_save.pred.second == 7 : (history.who == VPSave::FCM ? history.fcm_save.pred.second == 7 : false);

         bool transient =  (history.who == VPSave::PS ? history.ps_save.pred.second >= 1 : (history.who == VPSave::FCM ? history.fcm_save.pred.second >= 1 : false))
                         && !highConf;

         bool low_conf =  (history.who == VPSave::PS ? history.ps_save.pred.second == 0 :(history.who == VPSave::FCM ? history.fcm_save.pred.second == 0 : false))
                         && !highConf && !transient;

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}




