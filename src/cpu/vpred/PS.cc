/*
 * PS.cc
 *
 *  Created on: Sep 28, 2012
 *      Author: aperais
 */

#include "cpu/vpred/PS.hh"

#include "cpu/o3/helper.hh"

//#include "boost/dynamic_bitset.hpp"



std::string PSVP::SHTEntry::name;

PSVP::PSVP(std::string &name, unsigned size_VHT, unsigned tag_vht, unsigned size_SHT, unsigned tag_sht, unsigned bhist_width, unsigned counterwidth,
                std::vector<unsigned> &probability, unsigned associativity_vht, unsigned associativity_sht,  std::deque<TageBP::Hist> *bhist) {

        this->bhist = bhist;
        this->proba = probability;
        this->assoc_vht = associativity_vht;
        this->assoc_sht = associativity_sht;
        this->_name = name + ".VPredUnit.PS";
        PSVP::SHTEntry::name = this->_name + ".Entry";

        this->BHIST_LENGTH = bhist_width;

        this->fetch_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_VHT, this->assoc_vht, counterwidth, proba);
        this->commit_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_VHT, this->assoc_vht, counterwidth, proba);

        this->_SHT = AssociativeArray<PSVP, PSVP::SHTEntry>(size_SHT, this->assoc_sht, counterwidth, proba);

        this->_low_conf_inflight.resize(size_VHT);
        this->inflight.resize(size_VHT);

        this->TAG_SIZE_VHT = tag_vht;
        this->TAG_SIZE_SHT = tag_sht;
}


Addr
PSVP::hashPC(Addr ip) {
        std::bitset<640> tmp;
        for(unsigned i = 0; i < BHIST_LENGTH; i++) {
                tmp[i] = (*bhist)[i].dir & 0x1;
        }
        return ((ip << BHIST_LENGTH) + tmp.to_ulong());
}



PSVP::~PSVP() {}


prediction_t
PSVP::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        VPSave *save = new VPSave();
        vp_history = static_cast<void*>(save);

        Addr base_upc_vht =  createUPCindex(micropc, (Addr) log2(this->commit_VHT.size()));
        Addr base_upc_sht =  createUPCindex(micropc, (Addr) log2(this->_SHT.size()));

        save->index_vht = (base_upc_vht ^ ip) & (this->commit_VHT.size() - 1);
        save->index_sht = (hashPC(base_upc_sht ^ ip) & (this->_SHT.size() - 1));

        save->tag_vht =  (ip >> (unsigned) log2(this->commit_VHT.size())) & (((Addr) 1 << TAG_SIZE_VHT) - 1);
        save->tag_sht = (ip >> (unsigned) log2(this->_SHT.size())) & (((Addr) 1 << TAG_SIZE_SHT) -1);

        save->microPC = base_upc_sht;
        save->ip = base_upc_sht ^ ip;

        //Init
        save->low_conf_inflight = false;

        prediction_t result;

        save->way_vht = commit_VHT[save->index_vht].tagLookup(save->tag_vht);
        save->mismatch_vht = save->way_vht == assoc_vht;

        save->way_sht = _SHT[save->index_sht].tagLookup(save->tag_sht);
        save->mismatch_sht = save->way_sht == assoc_sht;

        if(save->mismatch_sht || save->mismatch_vht) {
                save->pred = prediction_t(Prediction(),0);
                return prediction_t(Prediction(),0);
        }

        accessSHT++;
        accessVHT++;
        Prediction val;


        val = this->fetch_VHT[save->index_vht].getPred(save->way_vht) + this->_SHT[save->index_sht].getStride(save->way_sht);
        save->pred = prediction_t(val, this->commit_VHT[save->index_vht].getConfidence(save->way_vht));
        this->fetch_VHT[save->index_vht].update(save->pred.first, *save, save->way_vht);
        inflight[save->index_vht]++;

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
if(this->_low_conf_inflight[index_vht] > 0) {
        save->low_conf_inflight = true;
}

if(save->pred.second < 7 || save->low_conf_inflight) {
        this->_low_conf_inflight[index_vht]++;
}
#endif

DPRINTF(ValuePredictor, "Predicting for inst at 0x%lx. Accessing entry (SHT) %lu (tag: %lu, mismatch: %u)."
                " Accessing entry (VHT) %lu (tag: %lu, mismatch: %u). Predicted %s with confidence %i\n",
                ip, save->index_sht, save->tag_sht, save->mismatch_sht, save->index_vht, save->tag_vht, save->mismatch_vht,
                save->pred.first.tostring(), (int) save->pred.second);

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
if(!save->low_conf_inflight) {
#endif
        return result;
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
} else {
        if(save->pred.second == 7) {
                ignoredLowConfInflight++;
        }
        return prediction_t(Prediction(),0);
}
#endif
}

void
PSVP::squash(void *vp_history, bool remove) {

        VPSave *history = static_cast<VPSave*>(vp_history);


        if(remove) {
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
                if(!history->mismatch_sht && !history->mismatch_vht && (history->pred.second < 7 || history->low_conf_inflight)) {

                        _low_conf_inflight[history->index_vht]--;
                        assert(_low_conf_inflight[history->index_vht] >= 0);
                }
#endif

if(!history->mismatch_sht && !history->mismatch_vht) {
        inflight[history->index_vht]--;
        assert(inflight[history->index_vht] >= 0);
}
        }

        if(!history->mismatch_sht && !history->mismatch_vht && inflight[history->index_vht] != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch history\n");
                accessVHT++;
                this->fetch_VHT[history->index_vht].restore(history->previous, history->way_vht);
        } else if(!history->mismatch_sht && !history->mismatch_vht) {
                DPRINTF(ValuePredictor, "Restoring commit and fetch history\n");
                Prediction tmp = this->commit_VHT[history->index_vht].getPred(history->way_vht);
                accessVHT++;
                this->fetch_VHT[history->index_vht].restore(tmp, history->way_vht);
        }

        if(remove) {
                delete history;
        }
}
void
PSVP::update(Prediction & value, void *vp_history, bool squashed)
{
        VPSave *history = static_cast<VPSave*>(vp_history);

        if(squashed) {
                Prediction tmp = Prediction();
                this->commit_VHT[history->index_vht].update(tmp, NULL,history->tag_vht, false, 0, true);
                return;
        }

#ifdef NOREAD_AT_COMMIT
unsigned commit_way_vht = history->way_vht;
unsigned commit_way_sht = history->way_sht;
#else
unsigned commit_way_vht = commit_VHT[history->index_vht].tagLookup(history->tag_vht);
unsigned commit_way_sht = _SHT[history->index_sht].tagLookup(history->tag_sht);
#endif

accessSHT++;
accessVHT++;


if(commit_way_sht != assoc_sht && commit_way_vht != assoc_vht) {
        if(this->commit_VHT[history->index_vht].getConfidence(commit_way_vht) /*this->commit_VHT[history->index_vht].getConfidence()*/ == 7) {
                if(this->commit_VHT[history->index_vht].getPred(commit_way_vht) + this->_SHT[history->index_sht].getStride(commit_way_sht) == value) {
                        commit_correct++;
                } else {
                        commit_incorrect++;
                }
        }
}

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
if(!history->mismatch_sht && !history->mismatch_vht && (history->pred.second < 7 || history->low_conf_inflight)) {

        this->_low_conf_inflight[history->index_vht]--;
        assert(this->_low_conf_inflight[history->index_vht] >= 0);
}
#endif

#ifndef NOREAD_AT_COMMIT
bool correct =  commit_way_sht != assoc_sht && commit_way_vht != assoc_vht &&
                (this->commit_VHT[history->index_vht].getPred(commit_way_vht) + this->_SHT[history->index_sht].getStride(commit_way_sht)) == value;
#else
bool correct =  !history->mismatch_sht && !history->mismatch_vht && history->pred.first == value;
#endif

DPRINTF(ValuePredictor, "Updating predictor with value %s (correct: %u). Accessing entry (SHT) %lu (tag: %lu, mismatch: %u)."
                " Accessing entry (VHT) %lu (tag: %lu, mismatch: %u).\n",
                history->pred.first.tostring(), correct, history->index_sht, history->tag_sht, history->mismatch_sht, history->index_vht, history->tag_vht, history->mismatch_vht);


Prediction stride;
if(commit_way_vht != assoc_vht) {
#ifdef NOREAD_AT_COMMIT
        stride = value - history->previous;
#else
        stride = (value - this->commit_VHT[history->index_vht].getPred(commit_way_vht));
#endif
} else {
        stride = Prediction();
}

this->_SHT[history->way_vht].update(stride, NULL, history->tag_sht, correct, history->pred.second);


if(!history->mismatch_vht && !history->mismatch_sht) {
        inflight[history->index_vht]--;
}

this->commit_VHT[history->index_vht].update(value, NULL, history->tag_vht, correct, history->pred.second);


if(inflight[history->index_vht] == 0) {
        unsigned way = commit_VHT[history->index_vht].tagLookup(history->tag_vht);
        this->fetch_VHT[history->index_vht].restore(value, way);
}

updateStats(*history,  !history->mismatch_sht && !history->mismatch_vht && history->pred.first == value);
delete history;
}

void PSVP::regStats()
{

        accessVHT
        .name(name() + ".accessesVHT")
        .desc("Accesses to the VHT")
        ;

        accessSHT
        .name(name() + ".accessesSHT")
        .desc("Accesses to the SHT")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        miss_sht
        .name(name() + ".miss_sht")
        .desc("Total number of misses in the sht")
        ;

        miss_vht
        .name(name() + ".miss_vht")
        .desc("Total number of misses in the vht")
        ;

        miss_all
        .name(name() + ".miss_all")
        .desc("Overall number of misses")
        ;
        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;
        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        missRateVHT
        .name(name() + ".missRateVHT")
        .desc("miss rate in the VHT")
        .precision(5)
        ;

        missRateSHT
        .name(name() + ".missRateSHT")
        .desc("miss rate in the SHT")
        .precision(5)
        ;

        missRateAll
        .name(name() + ".missRateAll")
        .desc("Overall miss rate")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;
        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);
        missRateSHT = miss_sht / attempted;
        missRateVHT = miss_vht / attempted;
        missRateAll = miss_all / attempted;
}

void PSVP::updateStats(VPSave &history, bool outcome)
{
        bool proceeded = history.pred.second == 7;

        if (history.mismatch_sht) { miss_sht++; }
        if (history.mismatch_vht) { miss_vht++; }

        if(history.mismatch_sht || history.mismatch_vht) {
                miss_all++;
        }

        ign_correct +=  proceeded && outcome && history.low_conf_inflight ;
        ign_incorrect += proceeded && !outcome &&  history.low_conf_inflight;
        attempted ++;
        correct += outcome && proceeded && !history.low_conf_inflight;
        incorrect += !outcome && proceeded && !history.low_conf_inflight;

        /** Stats  **/

        bool highConf = history.pred.second == 7;

        bool transient =  history.pred.second >= 1 && !highConf;

        bool low_conf =   history.pred.second == 0 && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;
}

