/*
 * vtage_fcm.cc
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */

#include "cpu/vpred/vtage_fcm.hh"

#include "cpu/o3/helper.hh"

VTAGE_FCM::VTAGE_FCM(std::string &name,
                unsigned size_fcm_vht,
                unsigned size_fcm_ht,
                unsigned tag_fcm_vht_size,
                unsigned lhist_length,
                std::deque<TageBP::Hist> *bhist,
                unsigned numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned baseHystShift,
                unsigned instShiftAmt,
                unsigned *phist,
                unsigned counterwidth,
                std::vector<unsigned> &proba_vtage,
                std::vector<unsigned> &proba_fcm,
                unsigned assoc_fcm)
{

       FCM_SPEC_VP::Entry::_lhist_length = lhist_length;
       this->_name = name + ".VPredUnit.VTAGE_FCM";
       this->assoc_fcm = assoc_fcm;
       fcm_proba = proba_fcm;

       FCM_HT_INDEX = log2(size_fcm_ht);
       FCM_VHT_INDEX = log2(size_fcm_vht);
       FCM_TAG_VHT = tag_fcm_vht_size;

       this->fcm_fetch_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);
       this->fcm_commit_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);

       fcm_VHT.resize(size_fcm_vht);
       fcm_conf.resize(size_fcm_vht);
       for(unsigned i = 0; i < size_fcm_vht; i++) {
           fcm_conf[i] = ConfCounter(2, 0, &fcm_proba);
       }

       arbiter = 0;



       fcm_low_conf_inflight.resize(size_fcm_ht);
       fcm_inflight.resize(size_fcm_ht);



       tage = new VTageVP(name,
                   numHistComponents,
                   numLogBaseEntry,
                   minHistSize,
                   maxHistSize,
                   baseHystShift,
                   counterwidth,
                   instShiftAmt,
                   proba_vtage,
                   bhist,
                   phist);

}

prediction_t
VTAGE_FCM::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{

        Addr base_upc = createUPCindex(micropc, FCM_HT_INDEX);

        Addr ht_index = base_upc ^ (ip & (this->fcm_commit_HT.size() - 1));
        Addr tag = (ip >> this->FCM_HT_INDEX) & ((((Addr) 1) << FCM_TAG_VHT) - 1);
        DPRINTF(ValuePredictor, "Index for the prediction: %lx, tag: %lx\n", ht_index, tag);

        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);
        history->who = VPSave::FCM;

        history->fcm_save.ht_index = ht_index;
        history->fcm_save.tag = tag;

        history->isBranch = false;

        void *save = NULL;
        prediction_t vtage_pred = tage->lookup(ip, micropc, save);
        VTageVP::VPSave* vtage_save = static_cast<VTageVP::VPSave*>(save);



        //Init
        history->fcm_save.low_conf_inflight = false;
        history->fcm_save.ip =  ip ^ base_upc;
        history->fcm_save.way = fcm_commit_HT[ht_index].tagLookup(tag);
        history->fcm_save.mismatch =  history->fcm_save.way == assoc_fcm;

        prediction_t pred_fcm;
        if(fcm_inflight[history->fcm_save.ht_index] == 0 && !history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(history->fcm_save.way,FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        } else if(!history->fcm_save.mismatch ){
                history->fcm_save.vht_index = this->fcm_fetch_HT[history->fcm_save.ht_index].getIndex(history->fcm_save.way,FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        }

        history->fcm_save.pred = history->fcm_save.mismatch ? prediction_t(Prediction(),0) : pred_fcm;

        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]++;
        }

        if(!history->fcm_save.mismatch && fcm_low_conf_inflight[history->fcm_save.ht_index] > 0) {
                history->fcm_save.low_conf_inflight = true;
        }

        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]++;
        }


        history->vtage_save = (vtage_save);
        //history->vtage_save->spec_update = false;
        //delete vtage_save;

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->fcm_save.low_conf_inflight = false;
#endif

        //bool vtage = false;
        bool vtage_high = vtage_pred.second == 7;
        bool fcm_high = history->fcm_save.pred.second == 7 && !history->fcm_save.low_conf_inflight;
        if(vtage_high && fcm_high) {
                if(vtage_pred.first == history->fcm_save.pred.first) {
                          this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
                          history->who = VPSave::All;
                          return vtage_pred;
                } else {
//                if(arbiter >= 0) {
//                        vtage = true;
//                } else {
//                        vtage = false;
//                }
//
//                if(vtage) {
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
//
//                        history->vtage_save->spec_update = history->fcm_save.pred.first != vtage_pred.first;
//
//                        history->who = VPSave::VTAGE;
//                        made_by_vtage++;
//                        spec_update_vtage += history->vtage_save->spec_update;
//
//
//                        return vtage_pred;
//
//                } else {
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//
//                        history->who = VPSave::FCM;
//                        made_by_fcm++;
//
//
//
//                                return history->fcm_save.pred;
//
//                }
                        //Nobody is confident enough.
                        if(!history->fcm_save.mismatch) {
                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                        }
                        //this->fcm_fetch_HT[history->fcm_save.vht_index].update(history->fcm_save.pred.first, *history);

                        history->who = VPSave::None;

                        return prediction_t(Prediction(),0);
                }
        } else if(fcm_high) {

                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);


                history->who = VPSave::FCM;
                made_by_fcm++;



                        return history->fcm_save.pred;


        } else if(vtage_high) {
                if(!history->fcm_save.mismatch) {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
                }

                //history->vtage_save->spec_update = true;

                history->who = VPSave::VTAGE;
                made_by_vtage++;
                spec_update_vtage++;


                return vtage_pred;
        } else {
                //Nobody is confident enough.
                if(!history->fcm_save.mismatch) {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                }
                //this->fcm_fetch_HT[history->fcm_save.vht_index].update(history->fcm_save.pred.first, *history);

                history->who = VPSave::None;

                return prediction_t(Prediction(),0);
        }


}


void
VTAGE_FCM::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        void *branch = static_cast<void*>((history->vtage_save));
        tage->flush_branch(branch);
        delete history;
}

void
VTAGE_FCM::updateFoldedHist(bool save, void *&vp_history) {

        VPSave *history = new VPSave();
        vp_history = static_cast<void*>(history);


        void *branch = NULL;
        tage->updateFoldedHist(save, branch);
        VTageVP::VPSave * tmp = static_cast<VTageVP::VPSave*>(branch);
        history->isBranch = true;
        history->vtage_save = tmp;
        //delete tmp;
}

void
VTAGE_FCM::update(Prediction & value, void *vp_history, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way_fcm = fcm_commit_HT[history->fcm_save.ht_index].tagLookup(history->fcm_save.tag);

        if(squashed) {
                if(history->who == VPSave::VTAGE) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value,tmp, false, true);
                } else if(history->who == VPSave::FCM) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, false, 0, true);
                } else if(history->who == VPSave::All) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, false, 0, true);
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value,tmp, false, true);
                }
                return;
        }

        Addr vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(commit_way_fcm, FCM_VHT_INDEX,  history->fcm_save.ip);
        prediction_t &vtage_pred = history->vtage_save->tagePred;
        prediction_t &fcm_pred = history->fcm_save.pred;

        bool outcome_vtage = vtage_pred.first == value && vtage_pred.second == 7;
        correct_vtage += outcome_vtage && history->who == VPSave::VTAGE;
        incorrect_vtage += history->who == VPSave::VTAGE && !outcome_vtage && vtage_pred.second == 7;

#ifdef NOREAD_AT_COMMIT
        bool outcome_fcm = fcm_pred.first == value;
#else
        bool outcome_fcm = !history->fcm_save.mismatch && history->fcm_save.pred.first == value && history->fcm_save.pred.second == 7;
#endif
        correct_fcm += outcome_fcm && history->who == VPSave::FCM;
        incorrect_fcm += history->who == VPSave::FCM && !outcome_fcm && history->fcm_save.pred.second == 7;

        correct_all += outcome_fcm && outcome_vtage && history->who == VPSave::All;
        incorrect_all += !outcome_fcm && !outcome_vtage && history->who == VPSave::All && vtage_pred.second == 7 && history->fcm_save.pred.second == 7;

        Addr ht_index = history->fcm_save.ht_index;


        outcome_vtage = vtage_pred.first == value;
        outcome_fcm =  commit_way_fcm != assoc_fcm && fcm_VHT[vht_index] == value;

        if(outcome_vtage && !outcome_fcm && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 &&  vtage_pred.second == 7) {
                arbiter < 15 ? arbiter++ : 0;
        } else if(!outcome_vtage && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && outcome_fcm &&  vtage_pred.second == 7) {
                arbiter > -16 ? arbiter-- : 0;
        }



        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
        }

        if(!history->fcm_save.mismatch) {
                        fcm_inflight[history->fcm_save.ht_index]--;
        }

        if(commit_way_fcm != assoc_fcm) {
                this->updateVHT(vht_index, value);

                if(outcome_fcm) {
                        this->fcm_conf[vht_index].increment();
                } else {
                        this->fcm_conf[vht_index].decrement();
                }
                //this->fcm_conf[vht_index].updateConf(outcome_fcm);
        }



        this->fcm_commit_HT[ht_index].update(value, NULL, history->fcm_save.tag, outcome_fcm, fcm_pred.second);

        if(commit_way_fcm != assoc_fcm && fcm_inflight[history->fcm_save.ht_index] == 0) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(commit_way_fcm), commit_way_fcm);
        }

        bool outcome = ((history->who == VPSave::VTAGE || history->who == VPSave::All) && vtage_pred.first == value) || (history->who == VPSave::FCM && history->fcm_save.pred.first == value);
        updateStats(*history, outcome);

        void * vtage_hist = static_cast<void*>(history->vtage_save);
        this->tage->update(value, vtage_hist, false, false);

        delete history;
}

void
VTAGE_FCM::squash(void *vp_history, bool remove, bool recompute) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        assert(history);


        void * vtage_hist = static_cast<void*>((history->vtage_save));

        if(history->isBranch) {
                DPRINTF(ValuePredictor, "Deleting a branch history\n");
                this->tage->squash(vtage_hist, remove, recompute);
        } else {
                DPRINTF(ValuePredictor, "Deleting a value history\n");
                this->tage->squash(vtage_hist, remove, recompute);


                if(remove) {
                        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {

                                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
                        }
                        if(!history->fcm_save.mismatch) {
                                fcm_inflight[history->fcm_save.ht_index]--;
                                assert(fcm_inflight[history->fcm_save.ht_index] >= 0);
                        }

                }

                if(!history->fcm_save.mismatch && fcm_inflight[history->fcm_save.ht_index] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                        this->fcm_fetch_HT[history->fcm_save.ht_index].restore(history->fcm_save.signatures, history->fcm_save.way);
                } else if(!history->fcm_save.mismatch) {
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(history->fcm_save.way), history->fcm_save.way);
                }
        }

        if(remove) {
                 delete history;
        }
}

void
VTAGE_FCM::regStats()
{

        made_by_fcm
        .name(name() + ".madeByFCM")
        .desc("Number of predictions made by FCM")
        ;

        made_by_vtage
        .name(name() + ".madeByVTAGE")
        .desc("Number of predictions made by VTAGE")
        ;

        spec_update_vtage
        .name(name() + ".specUpdateVTAGE")
        .desc("Number of time VTAGE updated FCM")
        ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_vtage
                .name(name() + ".correctVTAGE")
                .desc("Number of correct predictions coming from VTAGE")
                ;

        correct_fcm
                        .name(name() + ".correctFCM")
                        .desc("Number of correct predictions coming from FCM")
                        ;

        incorrect_vtage
                        .name(name() + ".incorrectVTAGE")
                        .desc("Number of correct predictions coming from VTAGE")
                        ;

        incorrect_fcm
                        .name(name() + ".incorrectFCM")
                        .desc("Number of correct predictions coming from FCM")
                        ;

        correct_all
        .name(name() + ".correctAll")
        .desc("Number of correct predictions coming from All")
        ;


        incorrect_all
        .name(name() + ".incorrectAll")
        .desc("Number of incorrect predictions coming from All")
        ;


        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;


        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        ignoredCorrect
               .name(name() + ".ignoredCorrect")
               .desc("Percentage of correct predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ignoredIncorrect
               .name(name() + ".ignoredIncorrect")
               .desc("Percentage of incorrect predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ign_correct
               .name(name() + ".ignCorrect")
               .desc("Number of correct predictions ignored because low conf inflight")
               ;

               ign_incorrect
                           .name(name() + ".ignInorrect")
                           .desc("Number of incorrect predictions ignored because low conf inflight")
                           ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;

        tage->regStats();
}

void
VTAGE_FCM::updateVHT(uint16_t index, Prediction & value)
{
        if(this->fcm_conf[index].read() == 0) {
                this->fcm_VHT[index] = value;
        }
}

void
VTAGE_FCM::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         prediction_t &vtage_pred = history.vtage_save->tagePred;

         ign_correct +=  proceeded && outcome && (history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) ;
         ign_incorrect += proceeded && !outcome &&  (history.fcm_save.low_conf_inflight && history.who == VPSave::FCM);

         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/

         bool highConf = history.who == VPSave::FCM ? history.fcm_save.pred.second == 7 : (history.who == VPSave::VTAGE ? vtage_pred.second == 7 : false);

         bool transient =  (history.who == VPSave::FCM ? history.fcm_save.pred.second >= 1 : (history.who == VPSave::VTAGE ? vtage_pred.second >= 1 : false))
                         && !highConf;

         bool low_conf =  (history.who == VPSave::FCM ? history.fcm_save.pred.second == 0 :(history.who == VPSave::VTAGE ? vtage_pred.second == 0 : false))
                         && !highConf && !transient;

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}








