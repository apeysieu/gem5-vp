/*
 * LastVP.cc
 *
 *  Created on: Sep 28, 2012
 *      Author: aperais
 */

#include "cpu/vpred/LVP.hh"

#include "cpu/o3/helper.hh"

std::string LastVP::Entry::name;

LastVP::LastVP(std::string &name, unsigned size, unsigned tag_width, unsigned counterwidth, std::vector<unsigned> &probability, unsigned associativity)
{
        this->_name = name + ".VPredUnit.LastVP";
        this->proba = probability;
        this->assoc = associativity;

        Entry::name = this->_name + ".Entry";

        this->fetch_VHT = AssociativeArray<LastVP, LastVP::Entry>(size, this->assoc, counterwidth, proba);
        this->commit_VHT = AssociativeArray<LastVP, LastVP::Entry>(size, this->assoc, counterwidth, proba);

        this->_low_conf_inflight.resize(size);
        this->inflight.resize(size);
        this->VHT_INDEX_WIDTH = log2(size);
        this->VHT_TAG_WIDTH = tag_width;

}

LastVP::~LastVP() {}


prediction_t
LastVP::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        VPSave *save = new VPSave();

        Addr base_upc = createUPCindex(micropc, VHT_INDEX_WIDTH);

        Addr index = base_upc ^ (ip & ((1 << VHT_INDEX_WIDTH) - 1));

        Addr tag = (ip >> VHT_INDEX_WIDTH) & ((1 << VHT_TAG_WIDTH) - 1);

        save->index = index;
        assert(save->index <= this->commit_VHT.size() - 1);
        save->tag = tag;
        save->way = this->commit_VHT[index].tagLookup(tag);

        access++;
        save->mismatch = save->way == assoc;

        //Init
        save->low_conf_inflight = false;

        vp_history = static_cast<void*>(save);

        if(save->mismatch) {
                save->pred = prediction_t(Prediction(),0);
                return save->pred;
        }

        DPRINTF(ValuePredictor, "Inflight: %i, vht_index: %lu\n", inflight[save->index], index);

        save->pred = prediction_t(this->fetch_VHT[index].getPred(save->way), this->commit_VHT[index].getConfidence(save->way));




#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if(this->_low_conf_inflight[index] > 0) {
                save->low_conf_inflight = true;
        }

        if(save->pred.second < 7 || save->low_conf_inflight) {
                this->_low_conf_inflight[index]++;
        }
#endif

        DPRINTF(ValuePredictor, "Updating fetch to %s\n", save->pred.first.tostring());
        this->fetch_VHT[index].update(save->pred.first, *save, save->way);
        inflight[save->index]++;

        DPRINTF(ValuePredictor, "One more inflight for %lu, inflight: %i\n", save->index, inflight[save->index]);

        DPRINTF(ValuePredictor, "Predicting for inst at 0x%lx. Accessing entry %lu (tag: %lu, mismatch: %u). Predicted %s with confidence %i\n",
                        ip, save->index, save->tag, save->mismatch, save->pred.first.tostring(), (int) save->pred.second);

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if(!save->low_conf_inflight) {
#endif
                return save->pred;
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        } else {
                if(save->pred.second == 7) {
                        ignoredLowConfInflight++;
                }
                return prediction_t(Prediction(),0);
        }
#endif
}

void
LastVP::update(Prediction & value, void *vp_history, bool mispred, bool squashed)
{
        VPSave *history = static_cast<VPSave*>(vp_history);

#ifdef NOREAD_AT_COMMIT
        unsigned commit_way = history->way;
#else
        unsigned commit_way = this->commit_VHT[history->index].tagLookup(history->tag);
#endif

        DPRINTF(ValuePredictor, "Updating the value predictor with value %ls. Accessing entry %lu (tag: %lu, mismatch: %u)\n",
                        value.tostring(), history->index, history->tag, history->mismatch);

        if(commit_way != assoc) {
                DPRINTF(ValuePredictor, "Fetch history was: %s, commit history is: %s\n", history->pred.first.tostring(), this->commit_VHT[history->index].getPred(commit_way).tostring());
        }

        if(squashed) {
                this->commit_VHT[history->index].update(value, NULL,history->tag, false, 0, true);
                return;
        }

        access++;
        if(commit_way != assoc) {
                if(this->commit_VHT[history->index].getConfidence(commit_way) == 7) {
                        if(this->commit_VHT[history->index].getPred(commit_way) == value) {
                                commit_correct++;
                        } else {
                                commit_incorrect++;
                        }
                }
        }

#ifndef NOREAD_AT_COMMIT
bool correct = commit_way != this->assoc && (this->commit_VHT[history->index].getPred(commit_way)) == value && !mispred;
#else
bool correct = !history->mismatch && history->pred.first == value;
#endif

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
        this->_low_conf_inflight[history->index]--;
        assert(this->_low_conf_inflight[history->index] >= 0);
}
#endif

if(!history->mismatch) {
        inflight[history->index]--;
}

this->commit_VHT[history->index].update(value, NULL, history->tag, correct, history->pred.second);

if(inflight[history->index] == 0) {
        unsigned way = this->commit_VHT[history->index].tagLookup(history->tag);
        DPRINTF(ValuePredictor, "Restoring fetch to %s\n", value.tostring());
        this->fetch_VHT[history->index].restore(value, way);
}

updateStats(*history, !history->mismatch && history->pred.first == value);
delete history;
}

void
LastVP::squash(void *vp_history, bool remove){

        VPSave *history = static_cast<VPSave*>(vp_history);

        if(remove) {
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
                if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
                        _low_conf_inflight[history->index]--;
                        assert(_low_conf_inflight[history->index] >= 0);
                }
#endif
if(!history->mismatch) {
        inflight[history->index]--;
        assert(inflight[history->index] >= 0);
}
        }

        if(!history->mismatch && inflight[history->index] != 0) {
                access++;
                DPRINTF(ValuePredictor, "Restoring fetch history\n");
                this->fetch_VHT[history->index].restore(history->previous, history->way);
        } else if(!history->mismatch){
                access++;
                DPRINTF(ValuePredictor, "Tag mismatch, cannot restore value\n");
                Prediction tmp = this->commit_VHT[history->index].getPred(history->way);
                this->fetch_VHT[history->index].restore(tmp, history->way);
        }

        if(remove) {
                delete history;
        }
}

void
LastVP::regStats()
{

        access
        .name(name() + ".accesses")
        .desc("Accesses to the VHT")
        ;

        way_use
        .init(0,3,1)
        .name(name() + ".wayUse")
        .desc("Occupation of the ways")
        ;
        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        ignoredLowConfInflight
        .name(name() + "ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;

        missrate
        .name(name() + ".miss_all")
        .desc("Missrate of the predictor")
        .precision(5)
        ;

        miss
        .name(name() + ".misses")
        .desc("Total number of tag mismatches")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;

        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);

        missrate = miss / attempted;
}

void
LastVP::updateStats(VPSave &history, bool outcome)
{
        bool proceeded = history.pred.second == 7;

        if(!history.mismatch) {
                way_use.sample(history.way);
        }
        miss += history.mismatch;
        ign_correct +=  proceeded && outcome && history.low_conf_inflight ;
        ign_incorrect += proceeded && !outcome &&  history.low_conf_inflight;
        attempted ++;
        correct += outcome && proceeded && !history.low_conf_inflight;
        incorrect += !outcome && proceeded && !history.low_conf_inflight;
        if(!outcome && proceeded && !history.low_conf_inflight) {
                DPRINTF(ValuePredictor, "Prediction was wrong\n");
        }
        /** Stats  **/

        bool highConf = history.pred.second == 7;

        bool transient =  history.pred.second >= 1 && !highConf;

        bool low_conf =   history.pred.second == 0 && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;
}
