/*
 * fcm_stride.hh
 *
 *  Created on: Nov 6, 2012
 *      Author: aperais
 */

#ifndef FCM_STRIDE_HH_
#define FCM_STRIDE_HH_

#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/vpred/associative_array.hh"
#include "cpu/vpred/fcm_spec.hh"
#include "cpu/vpred/stride.hh"
#include "debug/ValuePredictor.hh"

class FCM_Stride
{
protected:


        struct VPSave {
                enum Type {
                        FCM,
                        Stride,
                        All,
                        None
                };

               Type who;
               FCM_SPEC_VP::VPSave fcm_save;
               StrideVP::VPSave stride_save;

        };


public:
        /**
         * Constructor.
         *@param[in] threshold		The threshold of the 2level predictor.
         *@param[in] size		Number of entries in the VHT (power of 2).
         **/
        FCM_Stride(std::string &name, unsigned size_stride_vht, unsigned tag_stride_size, unsigned assoc_stride,
                        unsigned size_fcm_vht, unsigned size_fcm_ht, unsigned tag_fcm_size, unsigned lhist_length, unsigned countWidth, std::vector<unsigned> &proba_fcm, std::vector<unsigned> &proba_stride, unsigned assoc_fcm);

        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        void updateVHT(uint16_t index, Prediction & value);

        void update(Prediction & value, void *vp_history, bool mispred, bool squashed);

        void squash(void *vp_history, bool remove);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);

private:

        /** PRIVATE MEMBERS **/
        unsigned FCM_HT_INDEX, FCM_VHT_INDEX, STRIDE_VHT_INDEX, STRIDE_TAG_LENGTH, FCM_TAG_LENGTH;

        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fcm_fetch_HT;
        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fcm_commit_HT;
        std::vector<Prediction> fcm_VHT;
        std::vector<ConfCounter> fcm_conf;

        AssociativeArray<StrideVP, StrideVP::Entry> stride_fetch_VHT;
        AssociativeArray<StrideVP, StrideVP::Entry> stride_commit_VHT;

        std::string _name;

        std::vector<int> stride_low_conf_inflight, fcm_low_conf_inflight;
        std::vector<int> stride_inflight, fcm_inflight;

        std::vector<unsigned> stride_proba, fcm_proba;

        unsigned assoc_stride, assoc_fcm;

        int arbiter;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct, correct_stride, correct_fcm, correct_all;
        Stats::Scalar incorrect, incorrect_stride, incorrect_fcm, incorrect_all;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Scalar made_by_stride, made_by_fcm;
        Stats::Scalar spec_update_fcm, spec_update_stride;


        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;
};

#endif /* FCM_STRIDE_HH_ */
