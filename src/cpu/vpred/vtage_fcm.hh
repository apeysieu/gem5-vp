/*
 * vtage_fcm.hh
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */

#ifndef VTAGE_FCM_HH_
#define VTAGE_FCM_HH_


#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"
#include "cpu/vpred/VTAGE.hh"
#include "cpu/vpred/associative_array.hh"
#include "cpu/vpred/fcm_spec.hh"
#include "debug/ValuePredictor.hh"

class VTAGE_FCM
{
protected:


        struct VPSave {
                        enum Type {
                                VTAGE,
                                FCM,
                                All,
                                None
                        };

                Type who;
                bool isBranch;
                VTageVP::VPSave *vtage_save;
                FCM_SPEC_VP::VPSave fcm_save;


        };

public:
        /**
         * Constructor.
         *@param[in] threshold		The threshold of the 2level predictor.
         *@param[in] size		Number of entries in the VHT (power of 2).
         **/
        VTAGE_FCM(std::string &name,
                        unsigned size_fcm_vht,
                        unsigned size_fcm_ht,
                        unsigned tag_fcm_size,
                        unsigned lhist_length,
                        std::deque<TageBP::Hist> *bhist,
                        unsigned numHistComponents,
                        unsigned numLogBaseEntry,
                        unsigned minHistSize,
                        unsigned maxHistSize,
                        unsigned baseHystShift,
                        unsigned instShiftAmt,
                        unsigned *phist,
                        unsigned counterwidth,
                        std::vector<unsigned> &proba_vtage,
                        std::vector<unsigned> &proba_fcm,
                        unsigned assoc_fcm);

        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        void flush_branch(void *vp_history);

        void updateFoldedHist(bool save, void *&vp_history);

        void update(Prediction & value, void *vp_history, bool squashed);

        void squash(void *vp_history, bool remove, bool recompute);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);

        void updateVHT(uint16_t index, Prediction & value);


private:

        /** PRIVATE MEMBERS **/
        unsigned FCM_HT_INDEX, FCM_VHT_INDEX, FCM_TAG_VHT;

        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fcm_fetch_HT;
        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fcm_commit_HT;
        std::vector<Prediction> fcm_VHT;
        std::vector<ConfCounter> fcm_conf;

        std::vector<unsigned> fcm_proba;

        VTageVP *tage;

        std::string _name;

        std::vector<int> fcm_low_conf_inflight, fcm_inflight;

        unsigned assoc_fcm;
        int arbiter;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct, correct_fcm, correct_vtage, correct_all;
        Stats::Scalar incorrect, incorrect_fcm, incorrect_vtage, incorrect_all;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Scalar made_by_fcm, made_by_vtage;
        Stats::Scalar spec_update_vtage;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;


        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;
};

#endif /* VTAGE_FCM_HH_ */
