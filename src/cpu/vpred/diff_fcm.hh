/*
 * diff_fcm.hh
 *
 *  Created on: Jan 31, 2013
 *      Author: aperais
 */

#ifndef DIFF_FCM_HH_
#define DIFF_FCM_HH_


#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"

class DiffFCM
{
public:
        std::string _name;

        struct VPSave {
                prediction_t pred;
                Prediction last_value;
                Addr ht_index;
                Addr vht_index;
                Addr tag, ip;
                bool mismatch;
                std::vector<uint16_t> signatures;
                bool spec_update;
                bool low_conf_inflight;
                unsigned way;
        };

        /**
         * Constructor.
         * @param[name] name of the class (will appear in stats).
         * @param[size_vht] Size of the VHT in entries.
         * @param[size_ht] In entries.
         * @param[tag_length_ht] In bits.
         * @param[lhist_length] Order of the fcm predictor.
         * @param[counterWidth] Width of the confidence counters, in bits.
         * @param[proba] Probability vector used to control forward transition of confidence counters.
         * @param[assoc] Associativity of the HT.
         **/
        DiffFCM(std::string &name, unsigned size_vht, unsigned size_ht, unsigned tag_length_ht, unsigned lhist_length, unsigned counterWidth, std::vector<unsigned> &proba, unsigned assoc = 1);

        /**
         * @param[index] Address of the instruction.
         * @param[micropc] uop index.
         * @param[vp_history] Pointer to modify.
         * @post vp_history points to the data structure of LVP describing the current prediction.
         * @return prediction of inst (ip,micropc)
         */
        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        /**
         * @param[index] Entry to update.
         * @param[stride] New stride.
         */
        void updateVHT(uint16_t index, Prediction & stride);

        /**
         * @param[value] Actual result of the instruction (dummy if update on squash).
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[squash] True if update on squash, false if update at retire.
         */
        void update(Prediction & value, void *vp_history, bool mispred, bool squashed);

        /**
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[remove] True if the data structure should be deleted, false otherwise.
         */
        void squash(void *vp_history, bool remove);

        const std::string &name() const { return _name; }

        void regStats();


        void updateStats(VPSave &history, bool outcome);

        /**
         * Class implementing an entry of the VHT of a FCM.
         **/
        class LocalHistEntry
        {
        private:
                Addr _tag;


                /*
                 * Folding function to reduce the size of the values in the history.
                 * @param[to_fold] The prediction to fold
                 */
                static uint16_t fold(Prediction & to_fold)
                {
                        value_t result = 0l;
                        uint64_t tmp = to_fold.getValue();
                        for(unsigned i = 0; i < sizeof(value_t) / sizeof(uint16_t); i++)
                        {
                                result ^=  0xFFFF & (tmp >> (i * 8 * sizeof(uint16_t)));
                        }
                        //Fold to obtain 13 bits.
                       result = ((result & 0xE000) >> 13) ^ (result & 0x1FFF);
                       return (uint16_t) result;
                }

        public:
                std::vector<uint16_t> _signatures;	//Folded Value history.
                static uint32_t _lhist_length;
                static std::string _name;
                Prediction last_value;
                ConfCounter _confidence;


                LocalHistEntry() {}

                /**
                 * @param[counterWidth] In bits
                 * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
                 */
                LocalHistEntry(unsigned counterwidth, std::vector<unsigned> &proba) {
                        this->_tag = 0;
                        this->_signatures.resize(_lhist_length);
                        this->last_value = Prediction();
                        this->_confidence = ConfCounter(counterwidth, 0, &proba);
                }



                /** This is called to restore the value history because of a squash in case the history was speculatively updated**/
                void squash(VPSave &save) {
                        assert(save.spec_update);
                        DPRINTF(ValuePredictor, "Restoring the saved value history because of a value misprediction in entry %lu\n", save.ht_index);
                        DPRINTF(ValuePredictor, "Restoring from 0x%x to 0x%x",  (_signatures)[0], (save.signatures)[0]);

                        //Restoring the value history
                        for(unsigned i = 0; i < _lhist_length; i++) {
                                this->_signatures[i] = save.signatures[i];
                        }
                        last_value = save.last_value;
                }

                /** If the predictor is confident enough, speculatively update the value history.
                 * @param[value] The speculative value to update the predictor with.
                 * @param[stride] The speculative stride to fold and add to the history.
                 * @param[hist] Data structure describing the prediction.
                 **/
                void update(Prediction & value, Prediction & stride, VPSave &hist) {
                        hist.signatures.resize(_lhist_length);

                        for(int i = _lhist_length - 1; i > 0; i--)
                        {
                                hist.signatures[i] = this->_signatures[i];
                                this->_signatures[i] = this->_signatures[i-1];
                        }
                        hist.signatures[0] = this->_signatures[0];
                        this->_signatures[0] = fold(stride);
                        this->last_value = value;

                        //Debug
                        DPRINTF(ValuePredictor, "Speculatively updating the history\nWas: ");
                        for(int i = _lhist_length - 1; i >= 0; i--) {
                                DPRINTF(ValuePredictor, "|| %u\n", hist.signatures[i]);
                        }
                        DPRINTF(ValuePredictor, "\nIs:");
                        for(int i = _lhist_length - 1; i >= 0; i--) {
                                DPRINTF(ValuePredictor, "|| %u\n", this->_signatures[i]);
                        }
                        DPRINTF(ValuePredictor, "\n");
                }

                unsigned getConfidence() const {
                        return _confidence.read();
                }

                /**
                 * @param[signatures] The previously saved history to restore.
                 * @param[last_value] The previously saved last value to restore.
                 */
                void restore(std::vector<uint16_t> &signatures, Prediction & last_value) {
                        this->last_value = last_value;
                        for(unsigned i = 0; i < _lhist_length; i++)
                        {
                                this->_signatures[i] = signatures[i];
                        }
                        DPRINTF(ValuePredictor, "Restoring hist to :\n");
                        for(int i = _lhist_length - 1; i >= 0; i--) {
                                DPRINTF(ValuePredictor, "|| %u\n", this->_signatures[i]);
                        }
                        DPRINTF(ValuePredictor, "\n");
                }

                /**
                 * Returns the address to index the VHT
                 **/
                const uint16_t getIndex(unsigned index_size, Addr ip = 0) const
                {
                        uint32_t index = 0;
                        for(uint i = 0; i < _lhist_length; i++)
                        {
                                index ^= (((uint32_t) this->_signatures.at(i)) << i);
                        }
                        index ^= (ip & 0xffff);
                        return (uint16_t) (index & ((1 << index_size) - 1));
                }

                Prediction & getPred() {
                        return last_value;
                }

                const std::string &name() const { return _name; }


                 bool tagMatches(Addr tag) const
                 {
                                 return this->_tag == tag;
                 }

                 /**
                  * Non speculative update the value history of the entry.
                  * @param[value]	The result of the last instance of the instruction mapping to this entry.
                  * @param[stride] The difference between value and the last value.
                  * @param[tag] The new tag.
                  * @param correct 	The outcome of the prediction.
                  * @param[conf] The confidence seen at fetch.
                  * @param[squashed] True if update on squashed, false otherwise.
                  **/
                 void update(Prediction & value, Prediction *stride, Addr tag, bool correct, unsigned conf, bool squashed = false)
                 {

                         if(tagMatches(tag))
                         {
#ifdef NOREAD_AT_COMMIT
                                  //Update is done with the confidence seen at fetch, so "restore" it
                                 _confidence.set(conf);
#endif
                                 this->_confidence.updateConf(correct);

                                 if(!squashed) {
                                                                         //Update the history
                                                                         for(int i = _lhist_length - 1; i > 0; i--)
                                                                         {
                                                                                         this->_signatures[i] = this->_signatures[i-1];
                                                                         }
                                                                         this->_signatures[0] = fold(*stride);
                                                                         DPRINTF(ValuePredictor, "Folding value %s to %x\n", value.tostring(), this->_signatures[0]);
                                                                         last_value = value;

                                                                         //Debug
                                                                         DPRINTF(ValuePredictor, "Updating (incorrect) hist to :\n");
                                                                         for(int i = _lhist_length - 1; i >= 0; i--) {
                                                                                 DPRINTF(ValuePredictor, "|| %u\n", this->_signatures[i]);
                                                                         }
                                                                         DPRINTF(ValuePredictor, "\n");
                                }
                         } else {
                                 _confidence.set(0);
                                 if(!squashed) {
                                         this->_signatures[0] = fold(*stride);
                                         DPRINTF(ValuePredictor, "Folding value %s to %x\n", value.tostring(), this->_signatures[0]);
                                         last_value = value;
                                 }
                                 this->_tag = tag;
                         }
                 }
        };

private:

        /** PRIVATE MEMBERS **/
        unsigned HT_INDEX, VHT_INDEX, TAG_LENGTH;
        AssociativeArray<DiffFCM, DiffFCM::LocalHistEntry> fetch_HT;
        AssociativeArray<DiffFCM, DiffFCM::LocalHistEntry> commit_HT;
        std::vector<Prediction> _VHT;
        std::vector<ConfCounter> _conf;

        std::vector<unsigned> _proba;
        std::vector<int> _low_conf_inflight;
        std::vector<int> inflight;

        unsigned assoc;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct;
        Stats::Scalar incorrect;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar commit_correct, commit_incorrect;
        Stats::Formula accuracy_ideal, coverage_ideal;

        Stats::Scalar ignoredLowConfInflight;

        Stats::Scalar vhist_repair;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;

        Stats::Scalar indexDiffers, valueDiffers;
};

#endif /* DIFF_FCM_HH_ */
