/*
 * Authors: Arthur Perais.
 */

#include <cmath>
#include <cstdlib>
#include <sstream>

#include "base/intmath.hh"
#include "base/trace.hh"
#include "cpu/o3/helper.hh"
#include "cpu/pred/TAGE.hh"
#include "cpu/vpred/Diff_VTAGE.hh"
#include "debug/TAGEAlloc.hh"
#include "debug/ValuePredictor.hh"

DiffVTAGE::DiffVTAGE(std::string &name,
                unsigned _numHistComponents,
                unsigned numLogBaseEntry,
                unsigned LVT_size,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned _baseHystShift,
                unsigned counterWidth,
                unsigned _instShiftAmt,
                std::vector<unsigned> & _filterProbability,
                std::deque<TageBP::Hist> *bpred_globHist,
                unsigned *bpred_phist,
                TageBP *tage)
: _name(name + ".VPredUnit.DVTAGE"),
  numHistComponents(_numHistComponents),
#ifdef X86_ISA
  baseHystShift(0),
#else
  baseHystShift(_baseHystShift),
#endif
  logCTick(19),
  cTick((1 << (logCTick - 1))),
  phist(bpred_phist),
  globHist(bpred_globHist),
  seed(0),
#ifdef X86_ISA
  instShiftAmt(0)
#else
instShiftAmt(_instShiftAmt)
#endif
{
        proba = _filterProbability;


        unsigned i;
        unsigned size = numHistComponents + 1;

        /** initialize structures */
        this->tage = tage;

        /** logg */
        logg.resize(size);
        for(i = 1; i < size; ++i) {
                logg[i] = 10;
        }

        /** m */
        m.resize(size);
        /** computes the geometric history lengths */
        m[1] = minHistSize;
        m[numHistComponents] = maxHistSize;

        for(i = 2; i < size; i++) {
                m[i] =
                                (int) (((double) minHistSize *
                                                pow ((double) (maxHistSize) / (double) minHistSize,
                                                                (double) (i - 1) / (double) ((numHistComponents - 1)))) + 0.5);

        }

        /** ch_i, ch_t */
        ch_i.resize(size);
        ch_t[0].resize(size);
        ch_t[1].resize(size);

        for(i = 1; i < size; ++i) {
                ch_i[i].init(m[i], logg[i]);
                ch_t[0][i].init(ch_i[i].olength, i+12);
                ch_t[1][i].init(ch_i[i].olength, i+11);
        }


        /** bTable */
        bTable.resize(1 << numLogBaseEntry, Bentry(counterWidth, proba));

        /** gTable */
        gTable.resize(size);
        for(i = 1; i < size; ++i) {
                gTable[i].resize(1 << 10, Gentry(counterWidth, proba));
        }



        /** Compute masks */
        baseMask = ((1 << (numLogBaseEntry)) - 1);
        fetch_valueTable.resize(1 << (LVT_size));
        commit_valueTable.resize(1 << (LVT_size));
        tags.resize(1 << (LVT_size));


        inflight.resize(1 << (LVT_size));

        tagMask.resize(size);
        for(i = 1; i < size; ++i) {
                tagMask[i] = ((1 << (i+12)) - 1);
        }

        gMask.resize(size);
        for(i = 1; i < size; ++i) {
                gMask[i] = ((1 << 10) - 1);
        }

}


void
DiffVTAGE::regStats()
{

        strideOverflow
        .name(name() + ".strideOverflow")
        .desc("Number of times the stride overflowed when computed")
        ;

        mismatch
        .name(name() + ".VHTMismatch")
        .desc("Tag mismatch when trying to get the last value")
        ;

        zero_stride
        .name(name() + ".ZeroStride")
        .desc("Number of predictions made with a stride 0")
        ;
        table
        .name(name() + ".TableSpread")
        .init(0,6,1)
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        taggedHit
        .name(name() + ".taggedHit")
        .desc("Number of hits in a tagged table during a lookup for the standard pred")
        ;

        altTaggedHit
        .name(name() + ".altTaggedHit")
        .desc("Number of hits in a tagged table during a lookup for the altpred")
        ;

        alloc
        .name(name() + ".alloc")
        .desc("Number of allocations in tagged components")
        ;


        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        ;

        nonTagged
        .name(name() + ".nonTagged")
        .desc("Ratio of non tagged predictions out of potential predictions")
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        taggedPred
        .name(name() + ".taggedPred")
        .desc("Number of tagged components predictions")
        ;

        basePred
        .name(name() + ".basePred")
        .desc("Number of base bimodal predictions")
        ;

        standardPred
        .name(name() + ".standardPred")
        .desc("Number of standard predictions")
        ;

        altPred
        .name(name() + ".altPred")
        .desc("Number of predictions given by the alternate prediction")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        baseHighConf
        .name(name() + ".baseHighConf")
        .desc("Number of high confidence base predictions")
        ;

        baseHighConfHit
        .name(name() + ".baseHighConfHit")
        .desc("Number of correct high confidence base redictions")
        ;

        baseHighConfMiss
        .name(name() + ".baseHighConfMiss")
        .desc("Number of incorrect high confidence base predictions.")
        ;

        baseTransientConf
        .name(name() + ".baseTransientConf")
        .desc("Number of transient confidence base predictions")
        ;

        baseTransientConfHit
        .name(name() + ".baseTransientConfHit")
        .desc("Number of correct medium confidence base predictions")
        ;

        baseTransientConfMiss
        .name(name() + ".baseTransientConfMiss")
        .desc("Number of incorrect medium confidence base predictions.")
        ;

        baseLowConf
        .name(name() + ".baseLowConf")
        .desc("Number of low confidence base predictions")
        ;

        baseLowConfHit
        .name(name() + ".baseLowConfHit")
        .desc("Number of correct low confidence base bimodal predictions")
        ;

        baseLowConfMiss
        .name(name() + ".baseLowConfMiss")
        .desc("Number of incorrect low confidence base bimodal predictions.")
        ;

        correctBasePred
        .name(name() + ".correctBasPred")
        .desc("Number of correct predictions that flowed from the base predictors")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        nonTagged = correctBasePred / attempted;

        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);

}

unsigned
DiffVTAGE::F(unsigned hist, unsigned size, unsigned bank)
{
        Addr res, h1, h2;
        res = (Addr)hist;

        res = res & ((1 << size) - 1);
        h1 = (res & gMask[bank]);
        h2 = (res >> logg[bank]);
        h2 = ((h2 << bank) & gMask[bank]) + (h2 >> (logg[bank] - bank));
        res = h1 ^ h2;
        res = ((res << bank) & gMask[bank]) + (res >> (logg[bank] - bank));
        return (unsigned)res;
}

unsigned
DiffVTAGE::gIndex(Addr &branch_addr, unsigned bank)
{
        Addr index;
        unsigned M = (m[bank] > 16) ? 16 : m[bank];
        index = branch_addr ^ (branch_addr >> (abs(logg[bank] - bank) + 1)) ^
                        ch_i[bank].comp ^ F(*phist, M, bank);
        return (unsigned)(index & gMask[bank]);
}

unsigned
DiffVTAGE::gTag(Addr &branch_addr, unsigned bank)
{
        Addr tag = branch_addr ^ ch_t[0][bank].comp ^ (ch_t[1][bank].comp << 1);
        return (unsigned)(tag & tagMask[bank]);
}

prediction_t
DiffVTAGE::getBasePred(Addr &branch_addr)
{
        unsigned index = bIndex(branch_addr);
        unsigned LVT_index = branch_addr & (commit_valueTable.size() - 1);

        if(bTable[index].pred == Prediction()) {
                zero_stride++;
        }

        return prediction_t(fetch_valueTable[LVT_index] + bTable[index].pred, bTable[index].hyst.read());


}

void
DiffVTAGE::baseUpdate(Addr branch_addr, Prediction & val, bool outcome, unsigned conf)
{
        unsigned index = bIndex(branch_addr);
        bTable[index].ctrupdate(outcome, val, conf);
}


prediction_t
DiffVTAGE::lookup(Addr &addr, MicroPC micropc, void *&vp_history)
{

        int i;
        unsigned size = numHistComponents + 1;
        unsigned hitBank = 0;
        unsigned altBank = 0;
        prediction_t tagePred;
        prediction_t altPred;

        bool choseAlt = false;

        // Create DiffVTAGE::VPSave. This is the history for a new instruction.
        VPSave *history = new VPSave();

        // TAGE prediction
        // computes the table addresses and the partial tags
        history->instAddr = addr;
        history->gI.resize(size);
        history->gTag.resize(size);

        Addr lvtaddr = addr ^ createUPCindex(micropc, (Addr) log2(commit_valueTable.size()));

        history->tag = lvtaddr >> ((Addr) log2(commit_valueTable.size()));
        history->LVT_index =lvtaddr & (commit_valueTable.size() - 1);


        Addr baddr = addr ^ createUPCindex(micropc, (Addr) log2(bTable.size()));
        Addr taddr = addr ^ createUPCindex(micropc, (Addr) log2(gTable[1].size()));

        DPRINTF(ValuePredictor, "Commit table: %s, fetch table: %s\n", commit_valueTable[history->LVT_index].tostring(), fetch_valueTable[history->LVT_index].tostring());
        history->gI[0] = bIndex(baddr);


#ifdef TAG_DVTAGE
        history->mismatch = tags[history->LVT_index] != history->tag;
#else
        history->mismatch = false;
#endif

        for (i = 1; i < size; ++i) {
                history->gI[i] = gIndex(taddr, i);
                history->gTag[i] = gTag(taddr, i);
        }

        assert(baddr == lvtaddr);


        //Look for the bank with longest matching history
        for (i = size - 1; i > 0; --i) {
                if (gTable[i][history->gI[i]].tag == history->gTag[i]) {
                        hitBank = i;

                        //DPRINTF(ValuePredictor, "ValuePred: 0x%lx, entry found in bank: %i,"
                        //                " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);

                        break;
                }
        }

        //Look for the alternate bank
        for (i = hitBank - 1; i > 0; --i)  {
                if (gTable[i][history->gI[i]].tag == history->gTag[i]) {
                        altBank = i;

                        //DPRINTF(ValuePredictor, "ValuePred: 0x%lx, alternate entry found in bank: %i,"
                        //                " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);
                        break;
                }
        }

        //computes the prediction and the alternate prediction
        if (hitBank > 0) {
                taggedHit++;
                if (altBank > 0) {
                        altTaggedHit++;
                        altPred = prediction_t(fetch_valueTable[history->LVT_index] + gTable[altBank][history->gI[altBank]].pred, gTable[altBank][history->gI[altBank]].hyst.read());
                }
                else {
                        altPred = getBasePred(lvtaddr);
                }

                tagePred = prediction_t(fetch_valueTable[history->LVT_index] + gTable[hitBank][history->gI[hitBank]].pred, gTable[hitBank][history->gI[hitBank]].hyst.read());

                if(gTable[hitBank][history->gI[hitBank]].pred == Prediction()) {
                        zero_stride++;
                }
                //DPRINTF(ValuePredictor, "Stride: %s\n", gTable[altBank][history->gI[altBank]].pred.tostring());
        }
        else
        {
                altPred = getBasePred(baddr);
                tagePred = prediction_t(altPred.first, altPred.second);
                choseAlt = false;
                //DPRINTF(ValuePredictor, "Stride: %s\n", bTable[history->gI[hitBank]].pred.tostring());
        }



        /* save predictor state */
        if(!history)
                panic("No history data structure to save the predictor state");

        /* save prediction, hit/alt bank, GI and GTAG */
        history->bank = hitBank;
        history->altBank = altBank;
        history->tagePred = tagePred;
        history->altPred = altPred;
        history->isBranch = false;

        if(inflight[history->LVT_index] == 0) {
                history->last_value = commit_valueTable[history->LVT_index];
        } else {
                history->last_value = fetch_valueTable[history->LVT_index];
        }

        //If mismatch, nothint to do.
        if(history->mismatch) {
                DPRINTF(ValuePredictor, "Tag mismatch for prediction \n");
                altPred.second = 0;
                tagePred.second = 0;
                //Else, update speculative state.
        } else {
                fetch_valueTable[history->LVT_index] = choseAlt ? altPred.first : tagePred.first;
                inflight[history->LVT_index]++;
        }


        /*
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, hitBank: %i, altBank: %i, tagePred: %i,"
    " altTaken: %i, predTaken: %i\n", branch_addr, hitBank, altBank, tagePred,
    altTaken, predTaken);
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, confidence:  high: %i, med: %i, low: %i"
    " (bimod: %i) (Stag: %i, NStag: %i, Wtag: %i, NWtag: %i)\n", branch_addr,
    history->highConf, history->medConf, history->lowConf,
    history->bimHighConf || history->bimMedConf || history->bimLowConf,
    history->Stag, history->NStag, history->Wtag, history->NWtag);*/

        //DPRINTF(ValuePredictor, "VPSave :bank: %i, altbank: %i, choseAlt: %x\n",
        //                history->bank, history->altBank, choseAlt);

        vp_history = static_cast<void *>(history);

        //DPRINTF(ValuePredictor, "Commit table: %s, fetch table: %s, inflight: %i\n", commit_valueTable[history->LVT_index].tostring(), fetch_valueTable[history->LVT_index].tostring(), inflight[history->LVT_index]);
        return (choseAlt ? altPred : tagePred);
}


void
DiffVTAGE::updateFoldedHist(bool save, void *&vp_history)
{
        unsigned size = numHistComponents + 1;
        unsigned i = 0;
        DPRINTF(ValuePredictor, "Updating Folded History in VTAGE\n");

        if(save) {
                VPSave *new_record = new VPSave();

                new_record->ch_i_comp.resize(size);
                new_record->ch_t_comp[0].resize(size);
                new_record->ch_t_comp[1].resize(size);

                for (i = 1; i < size; ++i) {
                        new_record->ch_i_comp[i] = ch_i[i].comp;
                        new_record->ch_t_comp[0][i] = ch_t[0][i].comp;
                        new_record->ch_t_comp[1][i] = ch_t[1][i].comp;
                }

                //Don't care since it's a branch
                new_record->bank = 0;
                new_record->altBank = 0;
                new_record->tagePred = prediction_t(Prediction(),0);
                new_record->altPred = prediction_t(Prediction(),0);

                //Careful to set this bool.
                new_record->isBranch = true;

                vp_history = static_cast<void *>(new_record);
        }

        //prepare next index and tag computations
        for (i = 1; i < size; ++i) {
                ch_i[i].update(*globHist);
                ch_t[0][i].update(*globHist);
                ch_t[1][i].update(*globHist);
                DPRINTF(ValuePredictor, "Updating ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
        }
        DPRINTF(ValuePredictor, "ValuePred: GlobHist size: %i, new phist: %x\n", globHist->size(), *phist);
}


void
DiffVTAGE::updatePredictor(/*Addr &addr,*/ Prediction &val, bool outcome, void *vp_history, bool squashed)
{
        unsigned i,j;
        unsigned size = numHistComponents + 1;
        unsigned nrand = myRandom();

        unsigned hitBank;
        unsigned altBank;

        assert(vp_history);

        VPSave *history = static_cast<VPSave *>(vp_history);

        table.sample(history->bank);

        //Propagate the confidence instead of re reading the table.
        unsigned conf = history->tagePred.second;


        if(squashed) {
                //Could update the entry to reset the confidence counter, but it doesn't appear necessary.
                if(history->bank == 0) {
                        baseUpdate(history->gI[0], val, false, 0);
                } else {
                        gTable[history->bank][history->gI[history->bank]].ctrupdate(false, val, 0);
                }
                return;
        }

        if(history->bank == 0) {
                commit_correct += commit_valueTable[history->LVT_index] + bTable[history->gI[0]].pred  == val && bTable[history->gI[0]].hyst.read() == 7 ;
                commit_incorrect += !(commit_valueTable[history->LVT_index] + bTable[history->gI[0]].pred == val) && bTable[history->gI[0]].hyst.read() == 7;
        } else {
                commit_correct += commit_valueTable[history->LVT_index] + gTable[history->bank][history->gI[history->bank]].pred == val && gTable[history->bank][history->gI[history->bank]].hyst.read() == 7;
                commit_incorrect += !(commit_valueTable[history->LVT_index] + gTable[history->bank][history->gI[history->bank]].pred == val) && gTable[history->bank][history->gI[history->bank]].hyst.read() == 7;
        }


        prediction_t &tagePred = history->tagePred;
        prediction_t &altPred = history->altPred;
        hitBank = history->bank;
        altBank = history->altBank;
        Prediction stride;

#ifdef NOREAD_AT_COMMIT_VTAGE
        stride = val - history->last_value;
#else
        stride = val - commit_valueTable[history->LVT_index];
#endif

#ifndef NOREAD_AT_COMMIT_VTAGE
        if(hitBank == 0) {
                outcome = commit_valueTable[history->LVT_index] + bTable[history->gI[0]].pred == val && outcome;
        } else {
                outcome = commit_valueTable[history->LVT_index] + gTable[hitBank][history->gI[hitBank]].pred == val && outcome;
        }
#endif

if(stride.overflow) {
        strideOverflow++;
}

assert(!history->isBranch);

// VTAGE UPDATE
// try to allocate a  new entries only if prediction was wrong
bool alloc = !outcome && (hitBank < numHistComponents) && !history->mismatch && !stride.overflow;
if (hitBank > 0) {
        alloc = alloc && gTable[hitBank][history->gI[hitBank]].valid;
        // Manage the selection between longest matching and alternate matching
        // for "pseudo"-newly allocated longest matching entry
        bool PseudoNewAlloc = (gTable[hitBank][history->gI[hitBank]].hyst.read() == 0);
        // an entry is considered as newly allocated if its prediction counter is weak
        if (PseudoNewAlloc) {
                if (tagePred.first == val) {
                        alloc = false;
                }
                // if it was delivering the correct prediction, no need to allocate a new entry
                //even if the overall prediction was false

        }
} else {
        alloc = alloc && bTable[history->gI[0]].valid;
}

if(alloc) {
        DPRINTF(TAGEAlloc, "Allocating entry for inst 0x%lx\n", history->instAddr);
}

DPRINTF(ValuePredictor, "ValuePred: update, hitBank: %i, altBank: %i, tagePred: %lu,"
                " altPred: %lu, ALLOC: %i\n", hitBank, altBank, tagePred.first.tostring(),
                altPred.first.tostring(), alloc);

if (alloc) {
        // is there some "unuseful" entry to allocate
        unsigned min = 1;
        for (i = numHistComponents; i > hitBank; --i) {
                if (gTable[i][history->gI[i]].u < min) {
                        min = gTable[i][history->gI[i]].u;
                }
        }

        // we allocate an entry with a longer history
        //to  avoid ping-pong, we do not choose systematically the next entry, but among the 3 next entries
        unsigned Y = nrand & ((1 << (numHistComponents - hitBank - 1)) - 1);
        unsigned X = hitBank + 1;
        if (Y & 1) {
                X++;
                if (Y & 2) {
                        X++;
                }
        }
        //NO ENTRY AVAILABLE:  ENFORCES ONE TO BE AVAILABLE
        //TODO Do we need this?

        if(X >= gTable.size()) {
                X = gTable.size() - 1;
        }
        if (min > 0) {
                gTable[X][history->gI[X]].u = 0;
        }

        //Allocate only  one entry
        for (i = X; i < size; ++i) {
                if (gTable[i][history->gI[i]].u == 0) {
                        this->alloc++;

                        gTable[i][history->gI[i]].tag = history->gTag[i];
                        gTable[i][history->gI[i]].hyst.set(0);
                        gTable[i][history->gI[i]].u = 0;
                        //The entry will not contain a consistent stride for the next prediction if this was a mismatch as the last value is wrong.
                        gTable[i][history->gI[i]].valid = !history->mismatch;
                        gTable[i][history->gI[i]].pred = gTable[i][history->gI[i]].valid ? stride : Prediction();

                        //TODO Fix DPRINTF
                        DPRINTF(ValuePredictor, "ValuePred: new entry allocated,"
                                        " table: %i, index: %i, tag: %i val: %s\n", i,
                                        history->gI[i], history->gTag[i], gTable[i][history->gI[i]].valid ? stride.tostring() : Prediction().tostring());

                        break;
                }
        }
}
//periodic reset of u
cTick++;
if ((cTick & ((1 << logCTick) - 1)) == 0) {
        // reset least significant bit
        // most significant bit becomes least significant bit
        for (i = 1; i < size; ++i) {
                for (j = 0; j < (1 << logg[i]); ++j) {
                        gTable[i][j].u = 0;
                }
        }
}

if (hitBank > 0) {
        gTable[hitBank][history->gI[hitBank]].ctrupdate(outcome && !stride.overflow, stride, conf);
        gTable[hitBank][history->gI[hitBank]].valid = !history->mismatch;

        //if the provider entry is not certified to be useful also update the alternate prediction
        if (gTable[hitBank][history->gI[hitBank]].u == 0) {
                if (altBank > 0) {
                        gTable[altBank][history->gI[altBank]].ctrupdate(outcome && !stride.overflow, stride, altPred.second);
                        gTable[altBank][history->gI[altBank]].valid = !history->mismatch;
                }
                if (altBank == 0) {
                        baseUpdate(history->gI[0], stride, outcome && !stride.overflow, altPred.second);
                        bTable[history->gI[0]].valid = !history->mismatch;
                }
        }
}
else {
        baseUpdate(history->gI[0], stride, outcome && !stride.overflow, conf);
        bTable[history->gI[0]].valid = !history->mismatch;

}

// update the u counter
if (!(tagePred.first == altPred.first)) {
        if(tagePred.first == val && hitBank > 0)  {
                gTable[hitBank][history->gI[hitBank]].u = 1;
        } else if(hitBank > 0){
                /* if (useAltOnNA < 0)*/ gTable[hitBank][history->gI[hitBank]].u = 0;
        }
} else if(altPred.second != 7 && tagePred.first == val && hitBank > 0) {
        gTable[hitBank][history->gI[hitBank]].u = 1;
}

//Always update the last value table
commit_valueTable[history->LVT_index] = val;
tags[history->LVT_index] = history->tag;

if(history->mismatch) {
        mismatch++;
} else {
        inflight[history->LVT_index]--;
        assert(inflight[history->LVT_index] >= 0);
}

//Copy the committed state to the speculative state if no prediction inflight
if(inflight[history->LVT_index] == 0) {
        fetch_valueTable[history->LVT_index] = val;
}

DPRINTF(ValuePredictor, "Commit table: %s, fetch table: %s, inflight %i\n", val.tostring(), fetch_valueTable[history->LVT_index].tostring(), inflight[history->LVT_index]);
updateStats(*history, outcome);


}

void
DiffVTAGE::squash(void *vp_history, bool remove, bool recompute)
{
        VPSave *hist = static_cast<VPSave*>(vp_history);
        if(!remove && !recompute) {
                //No history to remove for branches
                assert(!hist->isBranch);
        }
        if(hist->isBranch) {
                DPRINTF(ValuePredictor, "squashing a branch in value predictor\n");
                recoverBHist(vp_history, recompute);
        }

        if(remove) recoverVHist(vp_history);
}

void
DiffVTAGE::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave *>(vp_history);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        delete history;
}

void
DiffVTAGE::recoverBHist(/*Addr &addr,*/ void *vp_history, bool recompute)
{
        unsigned size = numHistComponents + 1;
        unsigned i;

        VPSave *rollback = static_cast<VPSave*>(vp_history);

        /** Restore history */
        assert(vp_history);
        assert(rollback->isBranch);

        for (i = 1; i < size; ++i){
                ch_i[i].comp = rollback->ch_i_comp[i];
                ch_t[0][i].comp = rollback->ch_t_comp[0][i];
                ch_t[1][i].comp = rollback->ch_t_comp[1][i];
                DPRINTF(ValuePredictor, "Restoring ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
        }

        //Call to recompute the cyclic registers with the correct global branch history
        if(recompute) updateFoldedHist(false, vp_history);
}

void
DiffVTAGE::recoverVHist(/*Addr &addr,*/ void *vp_history)
{
        //Essentially we don't have much to do here.
        VPSave *history = static_cast<VPSave *>(vp_history);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);

        if(!history->isBranch && !history->mismatch) {
                //Account for the number of inflight predictions.
                inflight[history->LVT_index]--;
                if(inflight[history->LVT_index] == 0) {
                        fetch_valueTable[history->LVT_index] = commit_valueTable[history->LVT_index];
                } else {
                        fetch_valueTable[history->LVT_index] = history->last_value;
                }
                assert(inflight[history->LVT_index] >= 0);
        }
        delete history;
}

void
DiffVTAGE::update(Prediction &  val, void *vp_history, bool mispred,
                bool squashed)
{

        VPSave *history = static_cast<VPSave *>(vp_history);
        assert(!history->isBranch);

        DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->tagePred.first.tostring());

        updatePredictor(val, val == history->tagePred.first && !mispred, vp_history, squashed);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);

        if(!squashed) {
                delete history;
        }

}



void
DiffVTAGE::updateStats(VPSave &history, bool outcome)
{
        /** Base stats **/

        bool proceeded = history.tagePred.second == 7;
        attempted ++;

        correct += outcome && proceeded;
        incorrect += !outcome && proceeded;


        standardPred++;
        if(history.bank == 0)
        {
                basePred++;
                if(outcome && proceeded) {
                        correctBasePred++;
                }
        } else {
                taggedPred++;
        }


        /** Stats for the tagged components **/

        bool highConf = (/*!history.usedAlt &&*/ history.bank != 0 && history.tagePred.second >= 7);
       // || (history.usedAlt && history.altBank != 0 && history.altPred.second >= 7);

        bool transient = (/*(!history.usedAlt && */history.bank != 0 && history.tagePred.second >= 1);
                        //|| (history.usedAlt && history.altBank != 0 && history.altPred.second >= 1)) && !highConf;

        bool low_conf = (/*(!history.usedAlt &&*/ history.bank != 0 && history.tagePred.second == 0);
                        //|| (history.usedAlt && history.altBank != 0 && history.altPred.second == 0)) && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;


        //Stats for the base component.

        highConf = (/*!history.usedAlt &&*/ history.bank == 0 && history.tagePred.second >= 7);
                                               //                                 || (history.usedAlt && history.altBank == 0 && history.altPred.second >= 7);

        transient = (/*(!history.usedAlt &&*/ history.bank == 0 && history.tagePred.second >= 1);
                       // || (history.usedAlt && history.altBank == 0 && history.altPred.second >= 1)) && !highConf;

        low_conf = (/*(!history.usedAlt &&*/ history.bank == 0 && history.tagePred.second == 0);
                        //|| (history.usedAlt && history.altBank == 0 && history.altPred.second == 0)) && !highConf && !transient;

        baseHighConf += highConf;
        baseHighConfHit += highConf && outcome;
        baseHighConfMiss += highConf && !outcome;

        baseTransientConf += transient;
        baseTransientConfHit += transient && outcome;
        baseTransientConfMiss += transient && !outcome;

        baseLowConf += low_conf;
        baseLowConfHit += low_conf && outcome;
        baseLowConfMiss += low_conf && !outcome;

}

std::string
DiffVTAGE::dump()
{
        std::ostringstream oss;
        oss << "VPred: Branch history: ";
        for(std::deque<TageBP::Hist>::iterator it = globHist->begin(); it != globHist->end(); ++it)
        {
                oss << it->dir;
        }
        oss << std::endl;
        return oss.str();
}


