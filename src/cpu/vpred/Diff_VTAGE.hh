/*
 * Diff_VTAGE.hh
 *
 *  Created on: Jan 31, 2013
 *      Author: aperais
 */

#include <cstdlib>
#include <deque>
#include <vector>

#include "base/statistics.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"

#ifndef Diff_VTAGE_HH_
#define Diff_VTAGE_HH_


class DiffVTAGE
{
public:

    /**
     * Default branch predictor constructor.
     * @param[name] Name of the class (will appear in the stats).
     * @param[numHistComponents] Number of tagged tables.
     * @param[numLogBaseEntry] Log2(#entries) of the base component.
     * @param[LVT_size] Size of the LVT, in entries.
     * @param[minHistSize] Minimum bhist length.
     * @param[maxHistSize] Max bhist length.
     * @param[baseHystShift] In case hysteresis counters have to be shared between entries.
     * @param[counterWidth] Width of the confidence counters.
     * @param[instShiftAmt] Shifts the PC (useful only for RISC or aligned instructions).
     * @param[filterProbability] The probability vector to handle forward transitions of confidence countres.
     * @param[bpred_globHist] Pointer to the global branch history of TAGE.
     * @param[bpred_phist] Pointer to the global path history of TAGE.
     * @param[bpred] Pointer to TAGE.
     */
    DiffVTAGE(std::string &name,
            unsigned numHistComponents,
            unsigned numLogBaseEntry,
            unsigned LVT_size,
            unsigned minHistSize,
            unsigned maxHistSize,
            unsigned baseHystShift,
            unsigned counterWidth,
            unsigned instShiftAmt,
            std::vector<unsigned> &proba,
            std::deque<TageBP::Hist> *bpred_globHist,
            unsigned *bpred_phist,
            TageBP *bpred = NULL);

    const std::string &name() const { return _name; }

        /** this is the cyclic shift register for folding
         * a long global history into a smaller number of bits;
         * see P. Michaud's PPM-like predictor at CBP-1
         */
        class Folded_history
        {
        public:

            Addr comp;
            unsigned clength;
            unsigned olength;
            unsigned outpoint;

            Folded_history ()
            {
            }

            void init (int original_length, int compressed_length)
            {
                comp = 0;
                olength = original_length;
                clength = compressed_length;
                outpoint = olength % clength;
            }

            void update (std::deque<TageBP::Hist> &globHist)
            {

                comp = (comp << 1) | (Addr)globHist[0].dir;
                comp ^= ((Addr)globHist[olength].dir << outpoint);
                comp ^= (comp >> clength);
                comp &= (1 << clength) - 1;
            }
        };

    /** We still have to save the cyclic register when a branch is encountered because the
     * branch predictor does not necessarily use registers of the same size.
     * We also have to save the index and the tag of the entry,
     * as well as the bank and the altbank and which one made the prediction.
     */
    struct VPSave {
                std::vector<Addr> ch_i_comp;
                std::vector<Addr> ch_t_comp[2];
                std::vector<unsigned> gI;
                std::vector<unsigned> gTag;
                Addr LVT_index;
                unsigned bank;
                unsigned altBank;
                prediction_t tagePred;
                prediction_t altPred;
                bool isBranch;
                bool mismatch;
                Addr instAddr, tag;
                Prediction last_value;
    };

    /**
     * Registers statistics.
     */
    void regStats();

    /**
     * Looks up the given address in the branch predictor and returns
     * a pair <value, confidence>.  Also creates a
     * BPHistory object to store any state it will need on squash/update.
     * @param[branch_addr] The address of instruction to look up.
     * @param[micropc] The uop index.
     * @param[vp_history Pointer that will be set to the BPHistory object.
     * @return The predicted result of the instruction.
     */
    prediction_t lookup(Addr &branch_addr, MicroPC micropc, void * &vp_history);

    /**
     * Updates the value predictor with the actual result of an instruction.
     * @param[val] Actual result.
     * @param[vp_history] Pointer to the BPHistory object that was created
     * when the branch was predicted.
     * @param[squashed] is set when this function is called during a squash
     * operation.
     */
    void update(Prediction & val, void *vp_history, bool mispred, bool squashed);

    /**
     * Restores the global value history on a squash.
     * @param[vp_history] Pointer to the VPHistory object that has the
     * previous cyclic registers in it.
     * @param[remove] True if the history should be deleted, false otherwise.
     * @param[recompute] True if the cyclic registers should be recomputed (squashing a branch).
     */
    void squash(void *vp_history, bool remove, bool recompute);

    /** Computes the new folded history
    * @param[save] True if the cyclic registers should be saved.
    * @param[vp_history] Pointer to the VPHistory object that has the
    * previous cyclic registers in it.
    * @pre The buffer managing the branch history has been updated by the branch predictor.
    * @post Folded histories are ready to compute new indexes.
    **/
    void updateFoldedHist(bool save, void *&vp_history);

    /** Update the branch predictor with the committed value.
     * @param[val] The actual result of the instruction.
     * @param[outcome] True if the predicition was correct, false otherwise.
     * @param[vp_history] Pointer to the VPHistory object that has the
     * previous cyclic registers in it.
     * @param[squashed] True if update on squash, false otherwise.
     */
    void updatePredictor(/*Addr &addr,*/ Prediction & val, bool outcome, void *vp_history, bool squashed);

    std::string dump();

    //Basically delete the saved folded history when they are no longer
    //necessary (because we are committing/updating the predictor with a younger instruction).
    void flush_branch(void *vp_history);

private:


    /** Index function for the base table */
    inline unsigned bIndex(Addr &branch_addr) {
        return ((branch_addr >> instShiftAmt) & baseMask);
    }


    /**
     * The index functions for the tagged tables uses
     * path history as in the OGEHL predictor
     */
    /** F serves to mix path history */
    unsigned F(unsigned A, unsigned size, unsigned bank);

    /** gIndex computes a full hash of pc, ghist and phist */
    unsigned gIndex(Addr &branch_addr, unsigned bank);

    /** tags computation */
    unsigned gTag(Addr &branch_addr, unsigned bank);

    /** Base prediction (with the base predictor) */
    prediction_t getBasePred(Addr &branch_addr);

    /**
     * Update the base predictor.
     */
    void baseUpdate(Addr branch_addr, Prediction &  val, bool taken, unsigned conf);

    /**
     * Just a simple pseudo random number generator:
     * a 2-bit counter, used to avoid ping-pong phenomenon
     * on tagged entry allocations
     */
    inline unsigned myRandom()
    {
        ++seed;
        return seed & 3;
    }



    /**
     * Recover from a misprediction:
     * correct the global history
     * @param[recompute] True if the cyclic registers should be recomputed.
     */
    void recoverBHist(/*Addr &addr,*/ void *vp_history, bool recompute);
    void recoverVHist(/*Addr &addr,*/ void *vp_history);


    void updateStats(VPSave &history, bool outcome);




    /** VTAGE base table entry */
    class Bentry
    {
    public:
        ConfCounter hyst;
        Prediction pred;
        bool valid;

        /**
         * @param[counterwidth] Width of the confidence counter in bits.
         * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
         */
        Bentry (unsigned counterwidth, std::vector<unsigned> &proba)
        {
            pred = Prediction();
            hyst = ConfCounter(counterwidth,  0, &proba);
            valid = false;
        }

        /**
           * Updates the hysteresis/confidence counter
           * @param[outcome] True if the prediction was correct, false otherwise.
           * @param[val] The actual result of the instruction.
           * @param[conf] The confidence read at fetch.
           * @param[squashed] True is update on squash, false otherwise.
          **/
        void ctrupdate (bool outcome, Prediction & val, unsigned conf)
        {
#ifdef NOREAD_AT_COMMIT_VTAGE
                hyst.set(conf);
#endif


                if(hyst.read() == 0) {
                        valid ? pred = val : pred = Prediction();
                }

                hyst.updateConf(outcome);
                valid = true;
        }
    };

    /** VTAGE global table entry */
    class Gentry
    {
    public:
        ConfCounter hyst;
        unsigned tag;
        unsigned u;
        Prediction pred;
        bool valid;

        /**
         * @param[counterwidth] Width of the confidence counter in bits.
         * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
         */
        Gentry (unsigned counterwidth, std::vector<unsigned> &proba)
        {
            pred = Prediction();
            tag = 0;
            u = 0;
            hyst = ConfCounter(counterwidth, 0, &proba);
            valid = false;
        }

        /**
         * Updates the hysteresis/confidence counter
         * @param[outcome] True if the prediction was correct, false otherwise.
         * @param[val] The actual result of the instruction.
         * @param[conf] The confidence read at fetch.
         * @param[squashed] True is update on squash, false otherwise.
         **/
        void ctrupdate (bool outcome, Prediction & val, unsigned conf)
        {
#ifdef NOREAD_AT_COMMIT_VTAGE
                hyst.set(conf);
#endif

                if(hyst.read() == 0 && valid) {
                        pred = val;
                }

                hyst.updateConf(outcome);
                valid = true;
        }
    };

    /** Parameters */

    const std::string _name;

    /** Number of Tagged Components */
    unsigned numHistComponents;

    /** sharing an hysteresis bit between 4 bimodal predictor entries */
    unsigned baseHystShift;

    /** Internal structures and variables */



    /** "Use alternate prediction on newly allocated":
     * a 4-bit counter  to determine whether the newly
     * allocated entries should be considered as
     * valid or not for delivering  the prediction */
    int useAltOnNA;

    /** Control counter for the smooth resetting of useful counters */
    unsigned logCTick, cTick;

    /** Use a path history as on  the OGEHL predictor. Use that of the branch predictor. */
    unsigned *phist;
    /** Global history. Use that of the branch predictor. */
    std::deque<TageBP::Hist> *globHist;

    /** Log of number of entries  on each tagged component */
    std::vector<unsigned> logg;

    std::vector<unsigned> proba;


    /** Utility for computing TAGE indices */
    std::vector<Folded_history> ch_i;

    /** Utility for computing TAGE tags */
    std::vector<Folded_history> ch_t[2];

    /** Base VTAGE table */
    std::vector<Bentry> bTable;

    std::vector<Prediction> fetch_valueTable;
    std::vector<Prediction> commit_valueTable;
    std::vector<Addr> tags;

    std::vector<int> inflight;

    /** Tagged VTAGE tables */
    std::vector< std::vector<Gentry> > gTable;

    /** Used for storing the history lengths */
    std::vector<unsigned> m;



    /** For the pseudo-random number generator */
    unsigned seed;

    /** Mask to compute the index in the base bimodal predictor */
    Addr baseMask;

    /** Mask to compute the tag for the different tagged tables */
    std::vector<Addr> tagMask;

    /** Mask to compute the index in the different tagged tables */
    std::vector<Addr> gMask;

    /**
     * Count how many prediction have been made
     * since the last misprediction due to the base predictor
     */
    unsigned sinceBaseMispred;

    /** Number of bits to shift the instruction over to get rid of the word
     *  offset.
     */
    unsigned instShiftAmt;


    /** Global Stats */

    //Stats for frontend predictions
    Stats::Scalar taggedHit;
    Stats::Scalar altTaggedHit;
    Stats::Scalar zero_stride, mismatch;

    //Stats for backend predictions
    /** Stat for number of predictions given by the tagged components. */
    Stats::Scalar taggedPred;
    /** Stat for number of predictions given by the base bimodal predictor. */
    Stats::Scalar basePred;
    /** Stat for number of standard predictions. */
    Stats::Scalar standardPred;
    /** Stat for number of predictions given by the alternate prediction. */
    Stats::Scalar altPred;

    Stats::Scalar alloc;

    Stats::Scalar attempted;
    Stats::Scalar correct;
    Stats::Scalar incorrect;

    Stats::Formula accuracy;
    Stats::Formula coverage;
    Stats::Formula nonTagged;
    Stats::Scalar correctBasePred;
    Stats::Scalar commit_correct, commit_incorrect;
    Stats::Formula accuracy_ideal, coverage_ideal;
    Stats::Distribution table;

    /** Confidence Stats */
    /** Stat for number of high confidence predictions. */
    Stats::Scalar highConf;
    /** Stat for number of correct high confidence predictions. */
    Stats::Scalar highConfHit;
    /** Stat for the number of incorrect high confidence predictions */
    Stats::Scalar highConfMiss;
    /** Stat for number of medium confidence predictions. */
    Stats::Scalar transientConf;
    /** Stat for number of correct medium confidence predictions. */
    Stats::Scalar transientConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar transientConfMiss;
    /** Stat for number of low confidence predictions. */
    Stats::Scalar lowConf;
    /** Stat for number of correct low confidence predictions. */
    Stats::Scalar lowConfHit;
    /** Stat for the number of incorrect medium confidence predictions */
    Stats::Scalar lowConfMiss;

    /** Stat for number of high confidence base predictions. */
    Stats::Scalar baseHighConf;
    /** Stat for number of correct high confidence base bimodal predictions. */
    Stats::Scalar baseHighConfHit;
    /** Stat for the number of incorrect high confidence base bimodal predictions */
    Stats::Scalar baseHighConfMiss;
    /** Stat for number of medium confidence base bimodal predictions. */
    Stats::Scalar baseTransientConf;
    /** Stat for number of correct medium confidence base bimodal predictions. */
    Stats::Scalar baseTransientConfHit;
    /** Stat for the number of incorrect medium confidence base bimodal predictions */
    Stats::Scalar baseTransientConfMiss;
    /** Stat for number of low confidence base bimodal predictions. */
    Stats::Scalar baseLowConf;
    /** Stat for number of correct low confidence base bimodal predictions. */
    Stats::Scalar baseLowConfHit;
    /** Stat for the number of incorrect medium confidence base bimodal predictions */
    Stats::Scalar baseLowConfMiss;

    Stats::Scalar strideOverflow;

    TageBP *tage;

};

#endif
