/*
 * vtage_ps.hh
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */

#ifndef VTAGE_PS_HH_
#define VTAGE_PS_HH_



#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"
#include "cpu/vpred/PS.hh"
#include "cpu/vpred/VTAGE.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"

class VTAGE_PS
{
protected:


        struct VPSave {
                        enum Type {
                                VTAGE,
                                PS,
                                All,
                                None
                        };

                Type who;
                bool isBranch;
                PSVP::VPSave ps_save;
                VTageVP::VPSave *vtage_save;

        };



public:
        /**
         * Constructor.
         *@param[in] threshold		The threshold of the 2level predictor.
         *@param[in] size		Number of entries in the VHT (power of 2).
         **/
        VTAGE_PS(std::string &name,
                        unsigned size_ps_vht,
                        unsigned size_ps_sht,
                        unsigned tag_ps_vht_size,
                        unsigned tag_ps_sht_size,
                        unsigned bhist_length,
                        unsigned associativity_vht,
                        unsigned associativity_sht,
                        std::deque<TageBP::Hist> *bhist,
                        unsigned numHistComponents,
                        unsigned numLogBaseEntry,
                        unsigned minHistSize,
                        unsigned maxHistSize,
                        unsigned baseHystShift,
                        unsigned instShiftAmt,
                        unsigned *phist,
                        unsigned counterwidth,
                        std::vector<unsigned> &proba_vtage,
                        std::vector<unsigned> &proba_ps);

        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        void flush_branch(void *vp_history);

        void updateFoldedHist(bool save, void *&vp_history);

        void update(Prediction & value, void *vp_history, bool squashed);

        void squash(void *vp_history, bool remove, bool recompute);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);

        Addr hashPC(Addr ip);

private:

        /** PRIVATE MEMBERS **/
        unsigned PS_VHT_INDEX, PS_SHT_INDEX, PS_TAG_SHT, PS_TAG_VHT, BHIST_LENGTH;


        AssociativeArray<PSVP, PSVP::VHTEntry> ps_fetch_VHT;
        AssociativeArray<PSVP, PSVP::VHTEntry> ps_commit_VHT;
        AssociativeArray<PSVP, PSVP::SHTEntry> ps_SHT;

        std::vector<unsigned> ps_proba;
        unsigned assoc_ps_vht, assoc_ps_sht;

        VTageVP *tage;

        std::deque<TageBP::Hist> *bhist;

        std::string _name;

        std::vector<int> ps_low_conf_inflight, ps_inflight;


       int arbiter;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct, correct_ps, correct_vtage, correct_all;
        Stats::Scalar incorrect, incorrect_ps, incorrect_vtage, incorrect_all;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Scalar made_by_ps, made_by_vtage;
        Stats::Scalar spec_update_vtage;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;


        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;
};

#endif /* VTAGE_PS_HH_ */
