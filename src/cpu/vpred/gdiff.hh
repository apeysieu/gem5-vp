/*
 * gDiffVP.hh
 *
 *  Created on: Aug 17, 2013
 *      Author: aperais
 */

#ifndef gDiffVP_HH_
#define gDiffVP_HH_

#include <deque>
#include <string>
#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/vpred/VTAGE.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"
#include "params/DerivO3CPU.hh"

/**
* Class implementing a gDiffVP predictor.
**/
class gDiffVP
{

public:
    typedef pair<uint64_t,Prediction> gEntry;

        struct VPSave {
                Addr index_vht;
                Addr tag_vht;
                prediction_t pred;
                bool mismatch, low_conf_inflight;
                Prediction previous;
                gEntry oldGValue;
                bool spec_update;
                unsigned way;
                Addr ip, micropc;
                std::deque<Prediction> fetch_hist;
                bool isBranch, tagePredicted;
                void * tageHist;
        };


        class Entry
        {
          private:
            Addr _tag;
            //gDiffVP
            std::vector<Prediction> strides;
            unsigned offset;

            //Stride predictor
            Prediction last_value, stride;

          public:
            ConfCounter _confidence;


            Entry()
            {
            }

            Entry(unsigned counterwidth, std::vector<unsigned> &proba, unsigned order)
            {
                strides.resize(order);
                this->_tag = 0;
                this->_confidence = ConfCounter(counterwidth, 0, &proba);
                this->offset = 0;
            }

            Prediction getStride()
            {
              return strides[offset];
            }

            unsigned getOffset()
            {
                return offset;
            }

            Prediction getPred() {
                return last_value + stride;
            }

            Prediction getLastValue() {
                return last_value;
            }

            /**
            * Return true of the tag matches the tag of the entry.
            *@param[in] tag		The tag to compare against the tag of the entry.
            **/
            bool tagMatches(Addr tag) const
            {
              return this->_tag == tag;
            }

            /**
             * Returns the value of the confidence counter associated with the entry.
             **/
            unsigned getConfidence() const
            {
              return this->_confidence.read();
            }

            void update(Prediction &value, VPSave &save) {
                save.previous = last_value;
                last_value = value;
            }

            void restore(Prediction &value) {
                last_value = value;
            }

            /**
             * Updates the entry with a new value.
             *@param[in] value		The new value the entry will contain.
             *@param[in] tag		The new tag of the entry (if eviction).
             *@param[in] hit		True if the current entry was a hit, false otherwise.
             *@param[in] correct        True if the prediction was correct.
             **/
            void update(std::vector<Prediction> &new_strides, Prediction &value, Prediction *fetch_value, Addr tag, bool outcome, int conf, bool squashed = false)
            {
                if(!tagMatches(tag)) {
                        this->_confidence.set(0);
                        if(!squashed) {
                                this->_tag = tag;
                                last_value = value;
                                stride = Prediction();
                        }
                } else {
                        if(!squashed) {


#ifdef NOREAD_AT_COMMIT
                                _confidence.set(conf);
#endif
                                //Updating Stride part
                                if(_confidence.read() == 0) {
#ifdef NOREAD_AT_COMMIT
                                        stride = value - *fetch_value;
#else
                                        stride = value - last_value;
#endif
                                }
                                last_value = value;

                                //Updating gDiffVP
                                for(int i = 0; i < strides.size(); i++) {
                                        if(strides[i] == new_strides[i]) {
                                                offset = i;
                                        }
                                        strides[i] = new_strides[i];
                                }
                                _confidence.updateConf(outcome);
                        } else {
                                _confidence.set(0);
                        }
                }
            }
        };

        /** PRIVATE MEMBERS **/

        AssociativeArray<gDiffVP, gDiffVP::Entry> fetch_VHT;
        AssociativeArray<gDiffVP, gDiffVP::Entry> commit_VHT;

        unsigned TAG_SIZE;
        std::string _name;

        unsigned assoc, order;

        std::vector<int> _low_conf_inflight;
        std::vector<int> inflight;
        std::vector<unsigned> proba;



        std::deque<gEntry> fetch_gHistory;
        std::deque<Prediction> commit_gHistory;


        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct;
        Stats::Scalar incorrect;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;
        Stats::Scalar commit_correct, commit_incorrect;
        Stats::Scalar miss;
        Stats::Formula accuracy_ideal, coverage_ideal;
        Stats::Formula missrate;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;

        Stats::Scalar access;
        Stats::Scalar nonZeroStride;

        Stats::Distribution diff_with_commit;

        Stats::Scalar madeByVtage, madeByGDiff;


        gDiffVP(std::string &name, unsigned order, unsigned size_VHT, unsigned tag_vht, unsigned assoc,  unsigned counterwidth, std::vector<unsigned> &probability,
                        DerivO3CPUParams *params, std::deque<TageBP::Hist> *bpred_globHist,unsigned *bpred_phist, TageBP *tage);
        ~gDiffVP();
        prediction_t lookup(Addr ip, MicroPC micropc, void *&vp_history, uint64_t seqnum);
        void update(Prediction & value, void *vp_history, bool squashed);
        void squash(void *vp_history, bool remove, bool recompute);

        void updateFoldedHist(bool save, void *&vp_history);
        prediction_t updateGHistDP(uint64_t seqnum, VPSave &history);
        void updateGHistWB(uint64_t seqnum, Prediction &value);

        const std::string &name() const { return _name; }
        void flush_branch(void *vp_history);

        void regStats();
        void updateStats(VPSave &history, bool outcome);

        VTageVP * vtage;

};


#endif /* gDiffVP_HH_ */
