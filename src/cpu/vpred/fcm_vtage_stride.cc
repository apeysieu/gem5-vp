/*
 * fcm_vtage_stride.cc
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */

#include "cpu/vpred/fcm_vtage_stride.hh"

#include "cpu/o3/helper.hh"

FCM_VTAGE_Stride::FCM_VTAGE_Stride(std::string &name,
                //Stride
                unsigned size_stride_vht,
                unsigned tag_stride_size,
                std::vector<unsigned> &proba_stride,
                unsigned assoc_stride,
                //FCM
                unsigned size_fcm_vht,
                unsigned size_fcm_ht,
                unsigned tag_fcm_size,
                unsigned lhist_length,
                std::vector<unsigned> &proba_fcm,
                unsigned assoc_fcm,
                //VTAGE
                unsigned numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned baseHystShift,
                unsigned instShiftAmt,
                std::deque<TageBP::Hist> *bhist,
                unsigned *phist,
                std::vector<unsigned> &proba_vtage,
                //Misc
                unsigned counterwidth)
{

       this->_name = name + ".VPredUnit.FCM_VTAGE_Stride";
       FCM_SPEC_VP::Entry::_lhist_length = lhist_length;

       FCM_HT_INDEX = log2(size_fcm_ht);
       FCM_VHT_INDEX = log2(size_fcm_vht);
       STRIDE_VHT_INDEX = log2(size_stride_vht);
       STRIDE_TAG_LENGTH = tag_stride_size;
       FCM_TAG_LENGTH = tag_fcm_size;

       stride_proba = proba_stride;
       fcm_proba = proba_fcm;

       this->assoc_stride = assoc_stride;
       this->assoc_fcm = assoc_fcm;

       this->fcm_fetch_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);
       this->fcm_commit_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);

       std::cerr << size_fcm_ht << std::endl;

       fcm_VHT.resize(size_fcm_vht);
       fcm_conf.resize(size_fcm_vht);
       for(unsigned i = 0; i < size_fcm_vht; i++) {
           fcm_conf[i] = ConfCounter(2,  0, &fcm_proba);
       }


       this->stride_fetch_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_stride_vht, this->assoc_stride, counterwidth, stride_proba);
       this->stride_commit_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_stride_vht, this->assoc_stride, counterwidth, stride_proba);

       arbiter.resize(3);
       arbiter[0] = 0;
       arbiter[2] = 0;
       arbiter[1] = 0;

       stride_low_conf_inflight.resize(size_stride_vht);
       stride_inflight.resize(size_stride_vht);
       fcm_low_conf_inflight.resize(size_fcm_ht);
       fcm_inflight.resize(size_fcm_ht);

       tage = new VTageVP(name,
                   numHistComponents,
                   numLogBaseEntry,
                   minHistSize,
                   maxHistSize,
                   baseHystShift,
                   counterwidth,
                   instShiftAmt,
                   proba_vtage,
                   bhist,
                   phist);

       //This assert is here because of the way we compute the index of the VHT.
       //assert(pow((double) 2, (double) (8 + lhist_length - 1)) == size_fcm_vht);
}

prediction_t
FCM_VTAGE_Stride::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{

        Addr base_upc_fcm = createUPCindex(micropc, FCM_VHT_INDEX);
        Addr base_upc_stride = createUPCindex(micropc, (Addr)log2(this->stride_commit_VHT.size()));

        Addr ht_index = base_upc_fcm ^ (ip & (this->fcm_commit_HT.size() - 1));
        Addr tag = (ip >> this->FCM_HT_INDEX) & ((((Addr) 1) << FCM_TAG_LENGTH) - 1);
        DPRINTF(ValuePredictor, "Index for the prediction: %lx, tag: %lx\n", ht_index, tag);

        Addr stride_vht_index = base_upc_stride ^ (ip & (this->stride_fetch_VHT.size() - 1));
        Addr stride_tag = (ip >> this->STRIDE_VHT_INDEX) & ((((Addr) 1) << STRIDE_TAG_LENGTH) - 1);

        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);
        history->who = VPSave::FCM;

        history->isBranch = false;
        history->fcm_save.ht_index = ht_index;
        history->fcm_save.tag = tag;

        history->stride_save.index_vht = stride_vht_index;
        history->stride_save.tag_vht = stride_tag;

        void *save = NULL;
        prediction_t pred_vtage = tage->lookup(ip, micropc, save);
        VTageVP::VPSave* vtage_save = static_cast<VTageVP::VPSave*>(save);




        history->fcm_save.spec_update = false;
        history->stride_save.spec_update = false;
        history->stride_save.low_conf_inflight = false;
        history->fcm_save.low_conf_inflight = false;


        //Init
        history->fcm_save.low_conf_inflight = false;
        history->stride_save.low_conf_inflight = false;

        history->fcm_save.way = fcm_commit_HT[ht_index].tagLookup(tag);
        history->fcm_save.mismatch =  history->fcm_save.way == assoc_fcm;
        history->stride_save.way = this->stride_commit_VHT[stride_vht_index].tagLookup(stride_tag);
        history->stride_save.mismatch = history->stride_save.way == this->assoc_stride;
        history->fcm_save.ip =  ip ^ base_upc_fcm;

        prediction_t pred_fcm, pred_stride;
        prediction_t &vtage_pred = pred_vtage;

        if(fcm_inflight[history->fcm_save.ht_index] == 0 && !history->fcm_save.mismatch ) {
                history->fcm_save.vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        } else if(!history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_fetch_HT[history->fcm_save.ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        }

        if(stride_inflight[history->stride_save.index_vht] == 0 && ! history->stride_save.mismatch) {
                pred_stride = prediction_t(this->stride_commit_VHT[history->stride_save.index_vht].getPred(history->stride_save.way) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(history->stride_save.way),this->stride_commit_VHT[history->stride_save.index_vht].getConfidence(history->stride_save.way));
        } else if(! history->stride_save.mismatch) {
                pred_stride = prediction_t(this->stride_fetch_VHT[history->stride_save.index_vht].getPred(history->stride_save.way) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(history->stride_save.way),this->stride_commit_VHT[history->stride_save.index_vht].getConfidence(history->stride_save.way));
        }

        history->fcm_save.pred = (history->fcm_save.mismatch ? prediction_t(Prediction(),0) : pred_fcm);
        history->stride_save.pred = (history->stride_save.mismatch) ? prediction_t(Prediction(),0) : pred_stride;

        if(!history->fcm_save.mismatch && fcm_low_conf_inflight[history->fcm_save.ht_index] > 0) {
                history->fcm_save.low_conf_inflight = true;
        }

        if(!history->stride_save.mismatch && stride_low_conf_inflight[history->stride_save.index_vht] > 0) {
                history->stride_save.low_conf_inflight = true;
        }

        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]++;
        }

        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                stride_low_conf_inflight[history->stride_save.index_vht]++;
        }


        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]++;
        }

        if(!history->stride_save.mismatch) {
                stride_inflight[history->stride_save.index_vht]++;
        }

        history->vtage_save = (vtage_save);
        //history->vtage_save->spec_update = false;

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->fcm_save.low_conf_inflight = false;
        history->stride_save.low_conf_inflight = false;
#endif
       // delete vtage_save;

        //Arbitration if needed
        bool vtage_high = pred_vtage.second == 7;
        bool stride_high = history->stride_save.pred.second == 7 && !history->stride_save.low_conf_inflight;
        bool fcm_high = history->fcm_save.pred.second == 7 && !history->fcm_save.low_conf_inflight;
        if(vtage_high && stride_high && fcm_high) {
                if(history->stride_save.pred.first != history->fcm_save.pred.first &&
                                history->stride_save.pred.first != vtage_pred.first &&
                                history->fcm_save.pred.first != vtage_pred.first) {
                        //All predictors confident and disagree, no prediction
                        history->who = VPSave::None;
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save,history->fcm_save.way);
                        return prediction_t(Prediction(),0);
                } else if((history->stride_save.pred.first != history->fcm_save.pred.first && history->stride_save.pred.first ==vtage_pred.first) ||
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
//                        history->who = VPSave::STRIDE_VTAGE;
//                        return vtage_pred;
                (history->stride_save.pred.first == history->fcm_save.pred.first && history->stride_save.pred.first != vtage_pred.first) ||
//                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        history->who = VPSave::FCM_STRIDE;
//                        return history->fcm_save.pred;
                (vtage_pred.first == history->fcm_save.pred.first && history->stride_save.pred.first != history->fcm_save.pred.first)) {
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
//                        history->who = VPSave::FCM_VTAGE;
//                        return vtage_pred;
                        if(!history->fcm_save.mismatch) {
                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);

                        }
                        if(!history->stride_save.mismatch) {
                                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        }
                        history->who = VPSave::None;
                        return prediction_t(Prediction(),0);
                } else {
                        //Everybody is equal
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                        history->who = VPSave::All;
                        return vtage_pred;
                }
        } else if(stride_high && fcm_high) {
                if(history->stride_save.pred.first != history->fcm_save.pred.first) {
                        return chooseBetweenFCMAndStride(history);
                } else {
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);

                        history->who = VPSave::FCM_STRIDE;
                        return history->fcm_save.pred;

                }
        } else if(fcm_high && vtage_high) {
                if(vtage_pred.first != history->fcm_save.pred.first) {
                        return chooseBetweenFCMAndVTage(history, vtage_pred);
                } else {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
                        history->who = VPSave::FCM_VTAGE;
                        return vtage_pred;
                }
        } else if(stride_high && vtage_high) {
                if(vtage_pred.first != history->stride_save.pred.first) {
                        return chooseBetweenStrideAndVTage(history, vtage_pred);
                } else {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        history->who = VPSave::STRIDE_VTAGE;
                        return vtage_pred;
                }
        } else if(stride_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first,history->stride_save, history->stride_save.way);
                history->stride_save.spec_update = true;
                made_by_stride++;
                spec_update_stride++;
                history->who = VPSave::Stride;


                return history->stride_save.pred;


        } else if(fcm_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
                history->fcm_save.spec_update = true;
                made_by_fcm++;
                spec_update_fcm++;
                history->who = VPSave::FCM;


                return history->fcm_save.pred;


        } else if(vtage_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
                this->stride_fetch_VHT[history->stride_save.index_vht].update(vtage_pred.first, history->stride_save, history->stride_save.way);
                //history->vtage_save->spec_update = true;
                made_by_vtage++;
                spec_update_vtage++;
                history->who = VPSave::VTAGE;
                return vtage_pred;
        } else {
                if(!history->fcm_save.mismatch) {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);

                }
                if(!history->stride_save.mismatch) {
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                }
                history->who = VPSave::None;
                return prediction_t(Prediction(),0);
        }

}

prediction_t
FCM_VTAGE_Stride::chooseBetweenFCMAndVTage(VPSave *history, prediction_t &vtage_pred)
{
//	bool fcm = false;
//	if(arbiter[FCM_VTAGE] >= 0) {
//		fcm = true;
//	} else {
//		fcm = false;
//	}
//
//	if(fcm) {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
//
//		history->fcm_save.spec_update = true;
//
//		history->who = VPSave::FCM;
//		made_by_fcm++;
//		spec_update_fcm++;
//
//
//		return history->fcm_save.pred;
//
//	} else {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
//		this->stride_fetch_VHT[history->stride_save.index_vht].update(vtage_pred.first, history->stride_save, history->stride_save.way);
//
//		history->vtage_save->spec_update = true;
//
//		history->who = VPSave::VTAGE;
//		made_by_vtage++;
//		spec_update_vtage++;
//
//		return vtage_pred;
//
//	}
        if(!history->fcm_save.mismatch) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);

        }
        if(!history->stride_save.mismatch) {
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
        }
        history->who = VPSave::None;
        return prediction_t(Prediction(),0);
}

prediction_t
FCM_VTAGE_Stride::chooseBetweenFCMAndStride(VPSave *history)
{
//	bool fcm = false;
//	if(arbiter[FCM_STRIDE] >= 0) {
//		fcm = true;
//	} else {
//		fcm = false;
//	}
//
//	if(fcm) {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
//
//		history->fcm_save.spec_update = true;
//
//		history->who = VPSave::FCM;
//		made_by_fcm++;
//		spec_update_fcm++;
//
//
//		return history->fcm_save.pred;
//
//	} else {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
//
//		history->stride_save.spec_update = true;
//
//		history->who = VPSave::Stride;
//		made_by_stride++;
//		spec_update_stride++;
//
//
//		return history->stride_save.pred;
//
//
//	}
        if(!history->fcm_save.mismatch) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);

        }
        if(!history->stride_save.mismatch) {
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
        }
        history->who = VPSave::None;
        return prediction_t(Prediction(),0);
}
prediction_t
FCM_VTAGE_Stride::chooseBetweenStrideAndVTage(VPSave *history, prediction_t &vtage_pred)
{
//	bool stride = false;
//	if(arbiter[STRIDE_VTAGE] >= 0) {
//		stride = true;
//	} else {
//		stride = false;
//	}
//
//	if(stride) {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
//
//		history->stride_save.spec_update = true;
//
//		history->who = VPSave::Stride;
//		made_by_stride++;
//		spec_update_stride++;
//
//
//		return history->stride_save.pred;
//
//	} else {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
//		this->stride_fetch_VHT[history->stride_save.index_vht].update(vtage_pred.first, history->stride_save, history->stride_save.way);
//
//		history->vtage_save->spec_update = true;
//
//		history->who = VPSave::VTAGE;
//		made_by_vtage++;
//		spec_update_vtage++;
//
//		return vtage_pred;
//
//	}
        if(!history->fcm_save.mismatch) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);

        }
        if(!history->stride_save.mismatch) {
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
        }
        history->who = VPSave::None;
        return prediction_t(Prediction(),0);
}

void
FCM_VTAGE_Stride::updateVHT(uint16_t index, Prediction & value)
{
        if(this->fcm_conf[index].read() == 0) {
                this->fcm_VHT[index] = value;
        }
}

void
FCM_VTAGE_Stride::update(Prediction & value, void *vp_history, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way_stride = this->stride_commit_VHT[history->stride_save.index_vht].tagLookup(history->stride_save.tag_vht);
        unsigned commit_way_fcm = fcm_commit_HT[history->fcm_save.ht_index].tagLookup(history->fcm_save.tag);

        if(squashed) {
                    assert(history->who != VPSave::None);
                if(history->who == VPSave::FCM) {
                         this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false, 0,true);
                } else if(history->who == VPSave::Stride) {
                                this->stride_commit_VHT[history->stride_save.index_vht].update(value, NULL,history->stride_save.tag_vht, false, 0,true);
                } else if(history->who == VPSave::VTAGE) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value,tmp, false, true);
                } else if(history->who == VPSave::FCM_STRIDE) {
                         this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false, 0,true);
                         this->stride_commit_VHT[history->stride_save.index_vht].update(value,NULL, history->stride_save.tag_vht, false, 0,true);
                } else if(history->who == VPSave::FCM_VTAGE) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value,NULL, history->fcm_save.tag, false,0,true);
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value,tmp, false, true);
                } else if(history->who == VPSave::STRIDE_VTAGE) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value,tmp, false, true);
                        this->stride_commit_VHT[history->stride_save.index_vht].update(value, NULL,history->stride_save.tag_vht, false,0, true);
                } else if(history->who == VPSave::All) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value,tmp, false, true);
                        this->stride_commit_VHT[history->stride_save.index_vht].update(value, NULL,history->stride_save.tag_vht, false, 0,true);
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false, 0,true);
                }
                return;
        }

        Addr fcm_vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(commit_way_fcm, FCM_VHT_INDEX,  history->fcm_save.ip);
        prediction_t &vtage_pred = history->vtage_save->tagePred;
        prediction_t &fcm_pred = history->fcm_save.pred;
        prediction_t &stride_pred = history->stride_save.pred;


#ifdef NOREAD_AT_COMMIT
        bool outcome_stride = stride_pred.first == value;
        bool outcome_fcm = fcm_pred.first == value;
#else
        bool outcome_fcm = !history->fcm_save.low_conf_inflight && !history->fcm_save.mismatch && history->fcm_save.pred.first == value && history->fcm_save.pred.second == 7;
        bool outcome_stride = !history->stride_save.low_conf_inflight && !history->stride_save.mismatch && history->stride_save.pred.first == value && history->stride_save.pred.second == 7;
#endif
        bool outcome_vtage = vtage_pred.first == value && vtage_pred.second == 7;

        correct_fcm += outcome_fcm && history->who == VPSave::FCM;
        incorrect_fcm += !outcome_fcm && history->who == VPSave::FCM && history->fcm_save.pred.second == 7;

        correct_stride += outcome_stride && history->who == VPSave::Stride;
        incorrect_stride += !outcome_stride && history->who == VPSave::Stride && history->stride_save.pred.second == 7;

        correct_vtage += outcome_vtage && history->who == VPSave::VTAGE;
        incorrect_vtage += !outcome_vtage && history->who == VPSave::VTAGE  && vtage_pred.second == 7;

        correct_fcm_stride += outcome_fcm && outcome_stride && history->who == VPSave::FCM_STRIDE;
        incorrect_fcm_stride += !outcome_fcm && !outcome_stride && history->who == VPSave::FCM_STRIDE && history->stride_save.pred.second == 7 && history->fcm_save.pred.second == 7;

        correct_fcm_vtage += outcome_fcm && outcome_vtage && history->who == VPSave::FCM_VTAGE;
        incorrect_fcm_vtage += !outcome_fcm && !outcome_vtage && history->who == VPSave::FCM_VTAGE  && vtage_pred.second == 7 && history->fcm_save.pred.second == 7;

        correct_stride_vtage += outcome_vtage && outcome_stride && history->who == VPSave::STRIDE_VTAGE;
        incorrect_stride_vtage += !outcome_vtage && !outcome_stride && history->who == VPSave::STRIDE_VTAGE  && vtage_pred.second == 7 && history->stride_save.pred.second == 7;

        correct_all += outcome_vtage && outcome_stride && outcome_fcm && history->who == VPSave::All;
        incorrect_all += !outcome_vtage && !outcome_stride && !outcome_fcm && history->who == VPSave::All  && vtage_pred.second == 7 && history->stride_save.pred.second == 7 && history->fcm_save.pred.second == 7;

        outcome_fcm =  commit_way_fcm != assoc_fcm && fcm_VHT[fcm_vht_index] == value;
        outcome_stride =  commit_way_stride != assoc_stride && stride_commit_VHT[history->stride_save.index_vht].getPred(commit_way_stride) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(commit_way_stride)== value;
        outcome_vtage = vtage_pred.first == value;

        if(outcome_fcm && !outcome_stride && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7  && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7) {
                arbiter[FCM_STRIDE] < 15 ? arbiter[FCM_STRIDE]++ : 0;
        } else if(!outcome_fcm && outcome_stride && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 &&  stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7) {
                arbiter[FCM_STRIDE] > -16 ? arbiter[FCM_STRIDE]-- : 0;
        }

        if(outcome_fcm && !outcome_vtage && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && vtage_pred.second == 7) {
                arbiter[FCM_VTAGE] < 15 ? arbiter[FCM_VTAGE]++ : 0;
        } else if(!outcome_fcm && outcome_vtage && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && vtage_pred.second == 7) {
                arbiter[FCM_VTAGE] > -16 ? arbiter[FCM_VTAGE]-- : 0;
        }

        if(outcome_stride && !outcome_vtage &&  stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7 && vtage_pred.second == 7) {
                arbiter[STRIDE_VTAGE] < 15 ? arbiter[STRIDE_VTAGE]++ : 0;
        } else if(!outcome_stride && outcome_vtage  &&  stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7 && vtage_pred.second == 7) {
                arbiter[STRIDE_VTAGE] > -16 ? arbiter[STRIDE_VTAGE]-- : 0;
        }


        //Updating the arbiter


        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
        }
        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                stride_low_conf_inflight[history->stride_save.index_vht]--;
                assert(stride_low_conf_inflight[history->stride_save.index_vht] >= 0);
        }

        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]--;
                assert(fcm_inflight[history->fcm_save.ht_index] >= 0);
        }

        if(!history->stride_save.mismatch && !history->stride_save.mismatch) {
                stride_inflight[history->stride_save.index_vht]--;
                assert(stride_inflight[history->stride_save.index_vht] >= 0);
        }


        if(commit_way_fcm != assoc_fcm) {
                this->updateVHT(fcm_vht_index, value);
                if(outcome_fcm) {
                        this->fcm_conf[fcm_vht_index].increment();
                } else {
                        this->fcm_conf[fcm_vht_index].decrement();
                }
                //this->fcm_conf[fcm_vht_index].updateConf(outcome_fcm);
        }

        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, outcome_fcm, fcm_pred.second);
        this->stride_commit_VHT[history->stride_save.index_vht].update(value, &history->stride_save.previous, history->stride_save.tag_vht, outcome_stride, stride_pred.second);


        if(commit_way_stride != this->assoc_stride && stride_inflight[history->stride_save.index_vht] == 0) {
                this->stride_fetch_VHT[history->stride_save.index_vht].restore(value, commit_way_stride);
        }

        if(commit_way_fcm != assoc_fcm && fcm_inflight[history->fcm_save.ht_index] == 0) {
                DPRINTF(ValuePredictor, "Tag mismatch or not other instances in flight: setting speculative history to committed history\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(commit_way_fcm), commit_way_fcm);
        }

        bool outcome = ((history->who == VPSave::VTAGE || history->who == VPSave::FCM_VTAGE || history->who == VPSave::STRIDE_VTAGE || history->who == VPSave::All) && outcome_vtage)
                        || ((history->who == VPSave::Stride || history->who == VPSave::FCM_STRIDE) && outcome_stride)
                        || (history->who == VPSave::FCM && outcome_fcm);

        updateStats(*history,outcome);
        void * vtage_hist = static_cast<void*>(history->vtage_save);
        this->tage->update(value, vtage_hist,  false, false);

        delete history;
}

void
FCM_VTAGE_Stride::squash(void *vp_history, bool remove, bool recompute) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        void * vtage_hist = static_cast<void*>((history->vtage_save));
        if(history->isBranch) {
                  this->tage->squash(vtage_hist, remove, recompute);
        } else {
                this->tage->squash(vtage_hist, remove, recompute);

                if(remove) {

                        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
                        }
                        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                                stride_low_conf_inflight[history->stride_save.index_vht]--;
                                assert(stride_low_conf_inflight[history->stride_save.index_vht] >= 0);
                        }

                        if(!history->fcm_save.mismatch) {
                                fcm_inflight[history->fcm_save.ht_index]--;
                        }

                        if(!history->stride_save.mismatch) {
                                stride_inflight[history->stride_save.index_vht]--;
                        }

                }


                if(!history->fcm_save.mismatch && fcm_inflight[history->fcm_save.ht_index] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                        this->fcm_fetch_HT[history->fcm_save.ht_index].restore(history->fcm_save.signatures, history->fcm_save.way);
                } else if(!history->fcm_save.mismatch) {
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(history->fcm_save.way), history->fcm_save.way);
                }

                if(!history->stride_save.mismatch && stride_inflight[history->stride_save.index_vht] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                        this->stride_fetch_VHT[history->stride_save.index_vht].restore(history->stride_save.previous, history->stride_save.way);
                } else if(!history->stride_save.mismatch) {
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        Prediction tmp = this->stride_commit_VHT[history->stride_save.index_vht].getPred(history->stride_save.way);
                        this->stride_fetch_VHT[history->stride_save.index_vht].restore(tmp, history->stride_save.way);
                }
        }

        if(remove) {
                  delete history;
        }
}



void
FCM_VTAGE_Stride::updateFoldedHist(bool save, void *&vp_history) {

        VPSave *history = new VPSave();
        vp_history = static_cast<void*>(history);


        void *branch = NULL;
        tage->updateFoldedHist(save, branch);
        VTageVP::VPSave * tmp = static_cast<VTageVP::VPSave*>(branch);
        history->isBranch = true;
        history->vtage_save = tmp;
        //delete tmp;
}

void
FCM_VTAGE_Stride::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        void *branch = static_cast<void*>((history->vtage_save));
        tage->flush_branch(branch);
        delete history;
}

void
FCM_VTAGE_Stride::regStats()
{

        made_by_stride
                .name(name() + ".madeByStride")
                .desc("Number of predictions made by stride")
                ;

        made_by_fcm
        .name(name() + ".madeByFCM")
        .desc("Number of predictions made by FCM")
        ;

        made_by_vtage
                .name(name() + ".madeByVTAGE")
                .desc("Number of predictions made by VTAGE")
                ;

        spec_update_stride
                .name(name() + ".specUpdateStride")
                .desc("Number of time Stride updated FCM")
                ;

        spec_update_fcm
                .name(name() + ".specUpdateFCM")
                .desc("Number of time FCM updated Stride")
                ;

        spec_update_vtage
                        .name(name() + ".specUpdateVTAGE")
                        .desc("Number of time VTAGE updated the others")
                        ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_fcm
        .name(name() + ".correctFCM")
        .desc("Number of correct predictions coming from FCM")
        ;

        correct_stride
        .name(name() + ".correctStride")
        .desc("Number of correct predictions coming from Stride")
        ;

        correct_vtage
        .name(name() + ".correctVTAGE")
        .desc("Number of correct predictions coming from Vtage")
        ;

        correct_fcm_vtage
        .name(name() + ".correctFCM_VTAGE")
        .desc("Number of correct predictions coming from FCM and Vtage")
        ;

        correct_stride_vtage
        .name(name() + ".correctStride_VTAGE")
        .desc("Number of correct predictions coming from stride and Vtage")
        ;

        correct_fcm_stride
        .name(name() + ".correctFCM_Stride")
        .desc("Number of correct predictions coming from fcm and stride")
        ;

        correct_all
        .name(name() + ".correctALL")
        .desc("Number of correct predictions coming from all components")
        ;


        incorrect_fcm_vtage
        .name(name() + ".incorrectFCM_VTAGE")
        .desc("Number of incorrect predictions coming from FCM and Vtage")
        ;

        incorrect_stride_vtage
        .name(name() + ".incorrectStride_VTAGE")
        .desc("Number of incorrect predictions coming from stride and Vtage")
        ;

        incorrect_fcm_stride
        .name(name() + ".incorrectFCM_Stride")
        .desc("Number of incorrect predictions coming from fcm and stride")
        ;

        incorrect_all
        .name(name() + ".incorrectALL")
        .desc("Number of incorrect predictions coming from all components")
        ;

        incorrect_fcm
        .name(name() + ".incorrectFCM")
        .desc("Number of correct predictions coming from FCM")
        ;

        incorrect_vtage
        .name(name() + ".incorrectVTAGE")
        .desc("Number of correct predictions coming from VTAGE")
        ;

        incorrect_stride
        .name(name() + ".incorrectStride")
        .desc("Number of correct predictions coming from Stride")
        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;


        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        ignoredCorrect
               .name(name() + ".ignoredCorrect")
               .desc("Percentage of correct predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ignoredIncorrect
               .name(name() + ".ignoredIncorrect")
               .desc("Percentage of incorrect predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ign_correct
               .name(name() + ".ignCorrect")
               .desc("Number of correct predictions ignored because low conf inflight")
               ;

               ign_incorrect
                           .name(name() + ".ignInorrect")
                           .desc("Number of incorrect predictions ignored because low conf inflight")
                           ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;



        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;

        tage->regStats();
}

void
FCM_VTAGE_Stride::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         prediction_t &vtage_pred = history.vtage_save->tagePred;
         ign_correct +=  proceeded && outcome && ((history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) || (history.stride_save.low_conf_inflight && history.who == VPSave::Stride));
         ign_incorrect += proceeded && !outcome &&  ((history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) || (history.stride_save.low_conf_inflight && history.who == VPSave::Stride));

         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/

         bool highConf = false, transient = false, low_conf = false;

         if(history.who == VPSave::Stride && history.stride_save.pred.second == 7) {
                 highConf = true;
         } else if(history.who == VPSave::FCM && history.fcm_save.pred.second == 7) {
                 highConf = true;
         } else if(history.who == VPSave::VTAGE && vtage_pred.second == 7) {
                 highConf = true;
         } else {
                 highConf = false;
         }

         if(!highConf) {
                         if(history.who == VPSave::Stride && history.stride_save.pred.second >= 1) {
                                 transient = true;
                         } else if(history.who == VPSave::FCM && history.fcm_save.pred.second >= 1) {
                                 transient = true;
                         } else if(history.who == VPSave::VTAGE && vtage_pred.second >= 1) {
                                 transient = true;
                         } else {
                                 transient = false;
                         }
         }

         if(!transient) {
                 if(history.who == VPSave::Stride && history.stride_save.pred.second == 0) {
                         low_conf = true;
                 } else if(history.who == VPSave::FCM && history.fcm_save.pred.second == 0) {
                         low_conf = true;
                 } else if(history.who == VPSave::VTAGE && vtage_pred.second == 0) {
                         low_conf = true;
                 } else {
                         low_conf = false;
                 }
         }

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         this->lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}




