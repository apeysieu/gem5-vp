/*
 * fcm_spec.hh
 *
 *  Created on: Oct 10, 2012
 *      Author: aperais
 */

#ifndef FCM_SPEC_HH_
#define FCM_SPEC_HH_


#define CONFIDENCE_IN_VHT

#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"

class FCM_SPEC_VP
{
public:
        std::string _name;

        struct VPSave {
                prediction_t pred;
                Addr ht_index;
                Addr vht_index;
                Addr tag;
                bool mismatch;
                std::vector<uint16_t> signatures;
                bool spec_update;
                bool low_conf_inflight;
                bool vht_mismatch;
                unsigned way;
                Addr ip;

        };

public:
        /**
         * Constructor.
         * @param[name] name of the class (will appear in stats).
         * @param[size_vht] Size of the VHT in entries.
         * @param[size_ht] In entries.
         * @param[tag_length_ht] In bits.
         * @param[lhist_length] Order of the fcm predictor.
         * @param[counterWidth] Width of the confidence counters, in bits.
         * @param[proba] Probability vector used to control forward transition of confidence counters.
         * @param[assoc] Associativity of the HT.
         **/
        FCM_SPEC_VP(std::string &name, unsigned size_vht, unsigned size_ht, unsigned tag_length_ht, unsigned lhist_length, unsigned countWidth, std::vector<unsigned> &proba, unsigned assoc);

        /**
         * @param[index] Address of the instruction.
         * @param[micropc] uop index.
         * @param[vp_history] Pointer to modify.
         * @post vp_history points to the data structure of LVP describing the current prediction.
         * @return prediction of inst (ip,micropc)
         */
        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        /**
         * @param[index] Entry to update.
         * @param[value] New value.
         */
        void updateVHT(uint16_t index, Prediction & value);

        /**
         * @param[value] Actual result of the instruction (dummy if update on squash).
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[squash] True if update on squash, false if update at retire.
         */
        void update(Prediction & value, void *vp_history, bool mispred, bool squashed);

        /**
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[remove] True if the data structure should be deleted, false otherwise.
         */
        void squash(void *vp_history, bool remove);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);

        /**
         * Class implementing an entry of the VHT of a FCM.
         **/
        class Entry
        {
        private:
                Addr _tag;


                /**
                 * Folding function to reduce the size of the values in the history.
                 */
                static uint16_t fold(Prediction & to_fold)
                {
                        value_t result = 0;
                        for(unsigned i = 0; i < sizeof(value_t) / sizeof(uint16_t); i++)
                        {
                                result ^=  0xFFFF & (to_fold.getValue() >> (i * 8 * sizeof(uint16_t)));

                        }
                        return (uint16_t) result;
                }

        public:
                std::vector<uint16_t> _signatures;	//Folded Value history.
                static uint32_t _lhist_length;
                static std::string _name;
                ConfCounter _confidence;

                Entry() { }

                /**
                 * @param[counterWidth] In bits
                 * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
                 */
                Entry(unsigned counterwidth, std::vector<unsigned> &proba) {
                           this->_tag = 0;
                           this->_signatures.resize(_lhist_length);
                           _confidence = ConfCounter(counterwidth,  0, &proba);
                }



                unsigned getConfidence() const {
                        return _confidence.read();
                }

                /** This is called to restore the value history because of a squash in case the history was speculatively updated**/
                void squash(VPSave &save) {
                        assert(save.spec_update);
                        DPRINTF(ValuePredictor, "Restoring the saved value history because of a value misprediction in entry %lu\n", save.ht_index);
                        DPRINTF(ValuePredictor, "Restoring from 0x%x to 0x%x",  (_signatures)[0], (save.signatures)[0]);
                        for(unsigned i = 0; i < _lhist_length; i++) {
                                this->_signatures.at(i) = save.signatures.at(i);
                        }
                }

                /** If the predictor is confident enough, speculatively update the value history.
                 * @param[value] The speculative value to update the predictor with.
                 * @param[hist] Data structure describing the prediction.
                 **/
                void update(Prediction & value, VPSave &hist) {
                        hist.signatures.resize(_lhist_length);
                        for(int i = _lhist_length - 1; i > 0; i--) {
                                hist.signatures.at(i) = this->_signatures.at(i);
                                this->_signatures.at(i) = this->_signatures[i-1];
                        }
                        hist.signatures[0] = this->_signatures[0];
                        this->_signatures[0] = fold(value);


                        DPRINTF(ValuePredictor, "Speculatively updating the history\nWas: ");
                        for(int i = _lhist_length - 1; i >= 0; i--) {
                                DPRINTF(ValuePredictor, "|| %u\n", hist.signatures.at(i));
                        }
                        DPRINTF(ValuePredictor, "\nIs:");
                        for(int i = _lhist_length - 1; i >= 0; i--) {
                                DPRINTF(ValuePredictor, "|| %u\n", this->_signatures.at(i));
                        }
                        DPRINTF(ValuePredictor, "\n");
                }

                /**
                 * @param[signatures] The previously saved history to restore.
                 * @param[last_value] The previously saved last value to restore.
                 */
                void restore(std::vector<uint16_t> & signatures) {
                        for(unsigned i = 0; i < signatures.size(); i++)
                        {
                                this->_signatures.at(i) = signatures.at(i);
                        }
                        DPRINTF(ValuePredictor, "Restoring hist to :\n");
                        for(int i = _lhist_length - 1; i >= 0; i--) {
                                DPRINTF(ValuePredictor, "|| %u\n", this->_signatures.at(i));
                        }
                        DPRINTF(ValuePredictor, "\n");
                }

                /**
                 * Returns the address to index the VHT
                 **/
                const uint16_t getIndex(unsigned index_size, Addr ip = 0) const
                {
                        uint16_t index = 0;
                        for(uint i = 0; i < _lhist_length; i++)
                        {
                                DPRINTF(ValuePredictor, "Current hashed value %x\n", this->_signatures.at(i));
                                index ^= (this->_signatures.at(i) << i);

                        }
                        index ^= (ip & 0xffff);
                        return index & ((1 << index_size) - 1);
                }

                const std::string &name() const { return _name; }

                 /**
                  * Returns wether the tag matches the tag of the entry.
                  **/
                 bool tagMatches(Addr tag) const
                 {
                         return this->_tag == tag;
                 }


                 /**
                  * Non speculative update the value history of the entry.
                  * @param[value]	The result of the last instance of the instruction mapping to this entry.
                  * @param[dummy] Reserved.
                  * @param[tag] The new tag.
                  * @param[correct] The outcome of the prediction.
                  * @param[conf] The confidence seen at fetch.
                  * @param[squashed] True if update on squashed, false otherwise.
                  **/
                 void update(Prediction &value, Prediction *dummy, Addr tag, bool correct, unsigned conf, bool squashed = false)
                 {
                         if(tagMatches(tag))
                         {
#ifdef NOREAD_AT_COMMIT
                                 //If the confidence is propagated, set it to the value seen at fetch here
                                 _confidence.set(conf);
#endif
                                 _confidence.updateConf(correct);

                                 if(!squashed) {
                                         for(int i = _lhist_length - 1; i > 0; i--)
                                         {
                                                 this->_signatures.at(i) = this->_signatures[i-1];
                                         }
                                         this->_signatures[0] = fold(value);


                                         DPRINTF(ValuePredictor, "Folding value %s to %x\n", value.tostring(), this->_signatures[0]);
                                         DPRINTF(ValuePredictor, "Updating (correct) hist to :\n");
                                         for(int i = _lhist_length - 1; i >= 0; i--) {
                                                 DPRINTF(ValuePredictor, "|| %u\n", this->_signatures.at(i));
                                         }
                                         DPRINTF(ValuePredictor, "\n");



                                 }
                         } else {
                                 _confidence.set(0);
                                 if(!squashed) {
                                         this->_signatures[0] = fold(value);
                                 }
                                 this->_tag = tag;
                         }
                 }
        };

private:

        /** PRIVATE MEMBERS **/
        unsigned HT_INDEX, VHT_INDEX, TAG_LENGTH;
        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> fetch_HT;
        AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry> commit_HT;
        std::vector<Prediction> _VHT;

        unsigned assoc;

        std::vector<ConfCounter> _conf;
        std::vector<unsigned> _proba;

        std::vector<unsigned> _vht_tag;
        std::vector<int> _low_conf_inflight;
        std::vector<int> inflight;
        std::vector<bool> _last_wrong;
        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct;
        Stats::Scalar incorrect;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar commit_correct, commit_incorrect;
        Stats::Formula accuracy_ideal, coverage_ideal;

        Stats::Scalar ignoredLowConfInflight;
        Stats::Scalar selectiveCascade;


        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;

        Stats::Scalar indexDiffers, valueDiffers, vhtTagDiffers;

        Stats::Distribution vpt_dist;

        Stats::Scalar miss;
        Stats::Formula missrate;

        Stats::Scalar accessVHT, accessVPT;
};


#endif /* FCM_SPEC_HH_ */
