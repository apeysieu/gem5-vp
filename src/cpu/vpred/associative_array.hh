/*
 * associative_array.hh
 *
 *  Created on: Mar 15, 2013
 *      Author: aperais
 */

#ifndef ASSOCIATIVE_ARRAY_HH_
#define ASSOCIATIVE_ARRAY_HH_

#include <cassert>
#include <list>
#include <vector>

#include "debug/ValuePredictor.hh"

using namespace std;


template <typename Pred, typename Entry>
class AssociativeArray {

        typedef typename Pred::VPSave VPSave;

public:
        class VecWrapper {
        public:
                vector<Entry> _set;
                list<unsigned> _LRUtracker;
                unsigned _assoc;

                VecWrapper() {}

                void resize(unsigned assoc, Entry entry) {
                        _set.resize(assoc, entry);
                        for(unsigned j = 0; j < assoc; j++) {
                                _LRUtracker.push_back(j);
                        }
                        _assoc = assoc;
                        assert(_LRUtracker.size() == _assoc);
                }

                unsigned tagLookup(Addr tag) {
                        for(unsigned i = 0; i < _assoc; i++) {
                                if(_set[i].tagMatches(tag)) {
                                        return i;
                                }
                        }
                        return _assoc;
                }

                Prediction getStride(unsigned way) {
                        return _set[way].getStride();
                }

                Prediction getStride2(unsigned way) {
                                      return _set[way].getStride2();
                }

                Prediction getPred(unsigned way) {
                        return _set[way].getPred();
                }

                Prediction getLastValue(unsigned way) {
                        return _set[way].getLastValue();
                }

                unsigned getOffset(unsigned way) {
                        return _set[way].getOffset();
                }

                Addr getIndex(unsigned way, unsigned size, Addr ip = 0) {
                        if(way == _assoc) {
                                return 0;
                        }
                        return _set[way].getIndex(size, ip);
                }

                std::vector<uint16_t> &getSignatures(unsigned way) {
                        return _set[way]._signatures;
                }

                void update(Prediction &value, Prediction *fetch_value, Addr tag, bool outcome, int conf, bool squashed = false) {
                                unsigned way = tagLookup(tag);

                                if(squashed && way != this->_assoc) {
                                        _set[way].update(value, fetch_value, tag, outcome, conf, squashed);
                                        return;
                                } else if(squashed) {
                                        return;
                                }

                                if(way == _assoc) {
                                        unsigned lru = _LRUtracker.front();
                                        assert(lru >= 0 && lru < _assoc);
                                        _set[lru].update(value, fetch_value, tag, outcome, conf, squashed);
                                        _LRUtracker.push_back(lru);
                                        _LRUtracker.pop_front();
                                        assert(_LRUtracker.size() == _assoc);
                                } else {
                                        _set[way].update(value, fetch_value, tag, outcome, conf, squashed);
                                        //std::cerr << "Way: " << way << ", list: ";
                                        for(auto it = _LRUtracker.begin(); it != _LRUtracker.end(); ++it) {
                                                //std::cerr << *it << ", ";
                                                if(*it == way) {
                                                        _LRUtracker.erase(it);
                                                        break;
                                                }
                                        }
                                        //std::cerr << endl;
                                        _LRUtracker.push_back(way);
                                        assert(_LRUtracker.size() == _assoc);
                                }
                }

                void update(std::vector<Prediction> &new_strides, Prediction &value, Prediction *fetch_value, Addr tag, bool outcome, int conf, bool squashed = false) {
                        unsigned way = tagLookup(tag);

                        if(squashed && way != this->_assoc) {
                                _set[way].update(new_strides, value, fetch_value, tag, outcome, conf, squashed);
                                return;
                        } else if(squashed) {
                                return;
                        }

                        if(way == _assoc) {
                                unsigned lru = _LRUtracker.front();
                                assert(lru >= 0 && lru < _assoc);
                                _set[lru].update(new_strides, value, fetch_value, tag, outcome, conf, squashed);
                                _LRUtracker.push_back(lru);
                                _LRUtracker.pop_front();
                                assert(_LRUtracker.size() == _assoc);
                        } else {
                                _set[way].update(new_strides, value, fetch_value, tag, outcome, conf, squashed);
                                //std::cerr << "Way: " << way << ", list: ";
                                for(auto it = _LRUtracker.begin(); it != _LRUtracker.end(); ++it) {
                                        //std::cerr << *it << ", ";
                                        if(*it == way) {
                                                _LRUtracker.erase(it);
                                                break;
                                        }
                                }
                                //std::cerr << endl;
                                _LRUtracker.push_back(way);
                                assert(_LRUtracker.size() == _assoc);
                        }
                }

                void restore(Prediction &value, unsigned way) {
                        _set[way].restore(value);
                }

                void restore(vector<uint16_t> &signatures, unsigned way) {
                        _set[way].restore(signatures);
                }

                void restore(vector<uint16_t> &signatures, Prediction &last_value, unsigned way) {
                         _set[way].restore(signatures, last_value);
                }

                void update(Prediction &value, VPSave &save, unsigned way) {
                        if(way == _assoc) {
                                        DPRINTF(ValuePredictor, "Returning because way is %u\n", way);
                                return;
                        }
                        DPRINTF(ValuePredictor, "Updating way %u with %s\n", way, value.tostring());
                        _set[way].update(value, save);
                }

                void update(Prediction &value, Prediction & stride, VPSave &save, unsigned way) {
                        if(way == _assoc) {
                                DPRINTF(ValuePredictor, "Returning because way is %u\n", way);
                                return;
                        }
                        DPRINTF(ValuePredictor, "Updating way %u with %s\n", way, value.tostring());
                        _set[way].update(value, stride, save);
                }

                unsigned getConfidence(unsigned way) const {
                        return _set[way].getConfidence();
                }

        };

        vector<VecWrapper> _cache;
        unsigned assoc, sets;

        AssociativeArray() {}

        AssociativeArray(unsigned size, unsigned associativity, unsigned counterwidth, std::vector<unsigned> &proba) {
                sets = size;
                assoc = associativity;
                _cache.resize(size);
                for(unsigned i = 0; i < size; i++) {
                        _cache[i].resize(assoc, Entry(counterwidth, proba));
                }
        }

        AssociativeArray(unsigned size, unsigned associativity, unsigned counterwidth, std::vector<unsigned> &proba, unsigned order) {
                    sets = size;
                    assoc = associativity;
                    _cache.resize(size);
                    for(unsigned i = 0; i < size; i++) {
                            _cache[i].resize(assoc, Entry(counterwidth, proba, order));
                    }
            }

        VecWrapper &operator[](Addr index) {
                return _cache.at(index);
        }

        inline unsigned size() {
                return sets;
        }


};


#endif /* ASSOCIATIVE_ARRAY_HH_ */
