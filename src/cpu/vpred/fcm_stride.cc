/*
 * fcm_stride.cc
 *
 *  Created on: Nov 6, 2012
 *      Author: aperais
 */


#include "cpu/vpred/fcm_stride.hh"

#include "cpu/o3/helper.hh"

FCM_Stride::FCM_Stride(std::string &name, unsigned size_stride_vht, unsigned tag_stride_size, unsigned assoc_stride,
                       unsigned size_fcm_vht, unsigned size_fcm_ht, unsigned tag_fcm_size, unsigned lhist_length, unsigned counterwidth, std::vector<unsigned> &proba_fcm, std::vector<unsigned> &proba_stride, unsigned assoc_fcm)
{

       this->_name = name + ".VPredUnit.FCM_Stride";

       FCM_SPEC_VP::Entry::_lhist_length = lhist_length;

       stride_proba = proba_stride;
       fcm_proba = proba_fcm;
       this->assoc_stride = assoc_stride;
       this->assoc_fcm = assoc_fcm;


       FCM_HT_INDEX = log2(size_fcm_ht);
       FCM_VHT_INDEX = log2(size_fcm_vht);
       STRIDE_VHT_INDEX = log2(size_stride_vht);
       STRIDE_TAG_LENGTH = tag_stride_size;
       FCM_TAG_LENGTH = tag_fcm_size;

       this->fcm_fetch_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);
       this->fcm_commit_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);

       fcm_VHT.resize(size_fcm_vht);
       fcm_conf.resize(size_fcm_vht);

       for(unsigned i = 0; i < size_fcm_vht; i++) {
           this->fcm_conf.at(i) = ConfCounter(2, 0, &fcm_proba);
       }
       arbiter = 0;


       this->stride_fetch_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_stride_vht, this->assoc_stride, counterwidth, stride_proba);
       this->stride_commit_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_stride_vht, this->assoc_stride, counterwidth, stride_proba);

       stride_low_conf_inflight.resize(size_stride_vht);
       stride_inflight.resize(size_stride_vht);
       fcm_low_conf_inflight.resize(size_fcm_ht);
       fcm_inflight.resize(size_fcm_ht);


       //This assert is here because of the way we compute the index of the VHT.
       //assert(pow((double) 2, (double) (8 + lhist_length - 1)) == size_fcm_vht);
}

prediction_t
FCM_Stride::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{

        Addr base_upc_fcm = createUPCindex(micropc, (Addr) log2(this->fcm_commit_HT.size()));
        Addr base_upc_stride = createUPCindex(micropc, (Addr)log2(this->stride_commit_VHT.size()));

        Addr fcm_ht_index = base_upc_fcm ^ (ip & (this->fcm_commit_HT.size() - 1));
        Addr fcm_tag = (ip >> this->FCM_HT_INDEX) & ((((Addr) 1) << FCM_TAG_LENGTH) - 1);

        Addr stride_vht_index = (base_upc_stride ^ ip) & (this->stride_fetch_VHT.size() - 1);
        Addr stride_tag = (ip >> this->STRIDE_VHT_INDEX) & ((((Addr) 1) << STRIDE_TAG_LENGTH) - 1);



        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);


        history->fcm_save.spec_update = false;
        history->stride_save.spec_update = false;
        history->stride_save.low_conf_inflight = false;
        history->fcm_save.low_conf_inflight = false;

        history->fcm_save.ht_index = fcm_ht_index;
        history->fcm_save.tag = fcm_tag;
        history->fcm_save.ip =  ip ^ base_upc_fcm;

        history->fcm_save.way = fcm_commit_HT[fcm_ht_index].tagLookup(fcm_tag);
        history->fcm_save.mismatch =  history->fcm_save.way == assoc_fcm;

        history->stride_save.low_conf_inflight = false;


        history->stride_save.index_vht = stride_vht_index;
        history->stride_save.tag_vht = stride_tag;

        history->stride_save.way = stride_commit_VHT[stride_vht_index].tagLookup(stride_tag);
        history->stride_save.mismatch = history->stride_save.way == assoc_stride;


        prediction_t pred_fcm, pred_stride;
        if(fcm_inflight[history->fcm_save.ht_index] == 0 && !history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_commit_HT[fcm_ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        } else if(!history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_fetch_HT[fcm_ht_index].getIndex(history->fcm_save.way,FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        }

        if(stride_inflight[history->stride_save.index_vht] == 0 && ! history->stride_save.mismatch) {
                pred_stride = prediction_t(this->stride_commit_VHT[history->stride_save.index_vht].getPred(history->stride_save.way) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(history->stride_save.way),this->stride_commit_VHT[history->stride_save.index_vht].getConfidence(history->stride_save.way));
        } else if(! history->stride_save.mismatch) {
                pred_stride = prediction_t(this->stride_fetch_VHT[history->stride_save.index_vht].getPred(history->stride_save.way) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(history->stride_save.way),this->stride_commit_VHT[history->stride_save.index_vht].getConfidence(history->stride_save.way));
        }


        if(history->stride_save.mismatch && history->fcm_save.mismatch) {
                history->who = VPSave::None;
                history->fcm_save.pred = prediction_t(Prediction(),0);
                history->stride_save.pred = prediction_t(Prediction(),0);
                return prediction_t(Prediction(),0);
        } else if (history->stride_save.mismatch) {
                history->stride_save.pred = prediction_t(Prediction(),0);
                //If only PS misses, FCM has one more in flight.
                fcm_inflight[history->fcm_save.ht_index]++;
        } else if(history->fcm_save.mismatch) {
                history->fcm_save.pred = prediction_t(Prediction(),0);
                //If only FCM misses, STRIDE has one more in flight;
                stride_inflight[history->stride_save.index_vht]++;
        } else {
                //No miss
                fcm_inflight[history->fcm_save.ht_index]++;
                stride_inflight[history->stride_save.index_vht]++;
        }



        history->fcm_save.pred = (history->fcm_save.mismatch? prediction_t(Prediction(),0) : pred_fcm);
        history->stride_save.pred = (history->stride_save.mismatch) ? prediction_t(Prediction(),0) : pred_stride;


        if(!history->stride_save.mismatch && stride_low_conf_inflight[history->stride_save.index_vht] > 0) {
                history->stride_save.low_conf_inflight = true;
        }

        if(!history->fcm_save.mismatch && fcm_low_conf_inflight[history->fcm_save.ht_index] > 0) {
                history->fcm_save.low_conf_inflight = true;
        }

        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                stride_low_conf_inflight[history->stride_save.index_vht]++;
        }
        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]++;
        }

        //Arbitration if needed

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->fcm_save.low_conf_inflight = false;
        history->stride_save.low_conf_inflight = false;
#endif

        //bool fcm = false;
        bool stride_high = history->stride_save.pred.second == 7 && !history->stride_save.low_conf_inflight;
        bool fcm_high = history->fcm_save.pred.second == 7 && !history->fcm_save.low_conf_inflight;

        if(stride_high && fcm_high) {
                if(history->stride_save.pred.first == history->fcm_save.pred.first) {
                         this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                         this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
                         history->who = VPSave::All;
                         return pred_fcm;
                } else {
//                if(arbiter >= 0) {
//                        fcm = true;
//                } else {
//                        fcm = false;
//                }
//
//                if(fcm) {
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);
//
//                        history->fcm_save.spec_update = history->fcm_save.pred.first != history->fcm_save.pred.first;
//
//                        history->who = VPSave::FCM;
//                        made_by_fcm++;
//                        spec_update_fcm += history->fcm_save.spec_update;
//
//
//                        return pred_fcm;
//
//                } else {
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
//
//                        history->stride_save.spec_update = history->fcm_save.pred.first != history->fcm_save.pred.first;
//
//                        history->who = VPSave::Stride;
//                        made_by_stride++;
//                        spec_update_stride += history->stride_save.spec_update;
//
//
//                        return pred_stride;
//
//                }
                        if(!history->stride_save.mismatch) {
                                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        }
                        if(!history->fcm_save.mismatch) {
                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                        }



                        history->who = VPSave::None;

                        return prediction_t(Prediction(),0);
                }
        } else if(stride_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->stride_save.pred.first, history->fcm_save, history->fcm_save.way);
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);

                history->stride_save.spec_update = true;

                history->who = VPSave::Stride;
                made_by_stride++;
                spec_update_stride++;


                return pred_stride;

        } else if(fcm_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->fcm_save.pred.first, history->stride_save, history->stride_save.way);

                history->fcm_save.spec_update = true;

                history->who = VPSave::FCM;
                made_by_fcm++;
                spec_update_fcm++;


                return pred_fcm;
        } else {
                if(!history->stride_save.mismatch) {
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                }
                if(!history->fcm_save.mismatch) {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                }



                history->who = VPSave::None;

                return prediction_t(Prediction(),0);
        }


}

void
FCM_Stride::updateVHT(uint16_t index, Prediction & value)
{
        if(this->fcm_conf[index].read() == 0) {
                this->fcm_VHT[index] = value;
        }
}

void
FCM_Stride::update(Prediction & value, void *vp_history, bool mispred, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way_stride = this->stride_commit_VHT[history->stride_save.index_vht].tagLookup(history->stride_save.tag_vht);
        unsigned commit_way_fcm = fcm_commit_HT[history->fcm_save.ht_index].tagLookup(history->fcm_save.tag);
        if(squashed) {
                if(history->who == VPSave::FCM) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value,NULL, history->fcm_save.tag, false,0, true);
                } else if(history->who == VPSave::Stride) {
                        this->stride_commit_VHT[history->stride_save.index_vht].update(value,NULL, history->stride_save.tag_vht, false,0, true);
                } else if(history->who == VPSave::All) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, false, 0,true);
                        this->stride_commit_VHT[history->stride_save.index_vht].update(value, NULL,history->stride_save.tag_vht, false, 0,true);
                }
                return;
        }

        Addr fcm_vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(commit_way_fcm, FCM_VHT_INDEX,  history->fcm_save.ip);
        prediction_t &fcm_pred = history->fcm_save.pred;
        prediction_t &stride_pred = history->stride_save.pred;


#ifdef NOREAD_AT_COMMIT
        bool outcome_fcm = fcm_pred.first == value;
#else
        bool outcome_fcm = !history->fcm_save.mismatch && history->fcm_save.pred.first == value && history->fcm_save.pred.second == 7 && (history->who != VPSave::FCM || !mispred);
#endif
        correct_fcm += outcome_fcm && history->who == VPSave::FCM;
        incorrect_fcm += !history->fcm_save.mismatch && !outcome_fcm && history->who == VPSave::FCM && history->fcm_save.pred.second == 7;

#ifdef NOREAD_AT_COMMIT
        bool outcome_stride = stride_pred.first == value;
#else
        bool outcome_stride = !history->stride_save.mismatch && history->stride_save.pred.first == value && history->stride_save.pred.second == 7  && (history->who != VPSave::Stride || !mispred);
#endif
        correct_stride += outcome_stride && history->who == VPSave::Stride;
        incorrect_stride += !history->stride_save.mismatch && !outcome_stride && history->who == VPSave::Stride && history->stride_save.pred.second == 7;

        correct_all += outcome_stride && outcome_fcm && history->who == VPSave::All;
        incorrect_all += !outcome_stride && !outcome_fcm && history->who == VPSave::All && history->fcm_save.pred.second == 7 && history->stride_save.pred.second == 7;


        outcome_fcm = commit_way_fcm != assoc_fcm && fcm_VHT[fcm_vht_index] == value && (history->who != VPSave::FCM || !mispred);;
        outcome_stride = commit_way_stride != this->assoc_stride && stride_commit_VHT[history->stride_save.index_vht].getPred(commit_way_stride) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(commit_way_stride)== value && (history->who != VPSave::Stride || !mispred);;

        //Updating the arbiter
        if(outcome_fcm && !outcome_stride  && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7) {
                arbiter < 15 ? arbiter++ : 0;
        } else if(!outcome_fcm && outcome_stride  && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7) {
                arbiter > -16 ? arbiter-- : 0;
        }


        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
        }
        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                stride_low_conf_inflight[history->stride_save.index_vht]--;
                assert(stride_low_conf_inflight[history->stride_save.index_vht] >= 0);
        }

        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]--;
                assert(fcm_inflight[history->fcm_save.ht_index] >= 0);
        }

        if(!history->stride_save.mismatch && !history->stride_save.mismatch) {
                stride_inflight[history->stride_save.index_vht]--;
                assert(stride_inflight[history->stride_save.index_vht] >= 0);
        }

        if(commit_way_fcm != assoc_fcm) {
                this->updateVHT(fcm_vht_index, value);
                if(outcome_fcm) {
                        this->fcm_conf[fcm_vht_index].increment();
                } else {
                        this->fcm_conf[fcm_vht_index].decrement();
                }
                //this->fcm_conf[fcm_vht_index].updateConf(outcome_fcm);
        }

        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, outcome_fcm, fcm_pred.second);
        this->stride_commit_VHT[history->stride_save.index_vht].update(value, &history->stride_save.previous, history->stride_save.tag_vht, outcome_stride, stride_pred.second);

        if(commit_way_stride != this->assoc_stride &&  stride_inflight[history->stride_save.index_vht] == 0) {
                this->stride_fetch_VHT[history->stride_save.index_vht].restore(value, commit_way_stride);
        }

        if(commit_way_fcm != assoc_fcm && fcm_inflight[history->fcm_save.ht_index] == 0) {
                DPRINTF(ValuePredictor, "Tag mismatch or not other instances in flight: setting speculative history to committed history\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(commit_way_fcm), commit_way_fcm);
        }

        bool outcome = ((history->who == VPSave::FCM || history->who == VPSave::All) && history->fcm_save.pred.first == value) || (history->who == VPSave::Stride && history->stride_save.pred.first == value);
        updateStats(*history, outcome);


        delete history;
}

void
FCM_Stride::squash(void *vp_history, bool remove) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        if(remove) {
                if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                        fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                        assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
                }
                if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                        stride_low_conf_inflight[history->stride_save.index_vht]--;
                        assert(stride_low_conf_inflight[history->stride_save.index_vht] >= 0);
                }

                if(!history->fcm_save.mismatch) {
                        fcm_inflight[history->fcm_save.ht_index]--;
                }

                if(!history->stride_save.mismatch) {
                        stride_inflight[history->stride_save.index_vht]--;
                }
        }

        if(!history->fcm_save.mismatch && fcm_inflight[history->fcm_save.ht_index] != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(history->fcm_save.signatures, history->fcm_save.way);
        } else if(!history->fcm_save.mismatch) {
                DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(history->fcm_save.way), history->fcm_save.way);
        }

        if(!history->stride_save.mismatch && stride_inflight[history->stride_save.index_vht] != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                this->stride_fetch_VHT[history->stride_save.index_vht].restore(history->stride_save.previous, history->stride_save.way);
        } else if(!history->stride_save.mismatch) {
                DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                Prediction tmp = this->stride_commit_VHT[history->stride_save.index_vht].getPred(history->stride_save.way);
                this->stride_fetch_VHT[history->stride_save.index_vht].restore(tmp, history->stride_save.way);
        }

        if(remove) {
                delete history;
        }
}

void
FCM_Stride::regStats()
{

        made_by_stride
                .name(name() + ".madeByStride")
                .desc("Number of predictions made by Stride")
                ;

        made_by_fcm
        .name(name() + ".madeByFCM")
        .desc("Number of predictions made by FCM")
        ;

        spec_update_stride
                .name(name() + ".specUpdateStride")
                .desc("Number of time Stride updated FCM")
                ;

        spec_update_fcm
                .name(name() + ".specUpdateFCM")
                .desc("Number of time FCM updated Stride")
                ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_fcm
        .name(name() + ".correctFCM")
        .desc("Number of correct predictions coming from FCM")
        ;

        correct_stride
        .name(name() + ".correctStride")
        .desc("Number of correct predictions coming from Stride")
        ;


        correct_all
        .name(name() + ".correctAll")
        .desc("Number of correct predictions coming from All")
        ;


        incorrect_all
        .name(name() + ".incorrectAll")
        .desc("Number of incorrect predictions coming from All")
        ;

        incorrect_fcm
        .name(name() + ".incorrectFCM")
        .desc("Number of correct predictions coming from FCM")
        ;

        incorrect_stride
        .name(name() + ".incorrectStride")
        .desc("Number of correct predictions coming from Stride")
        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;


        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;
        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
               ignoredIncorrect = ign_incorrect / attempted;
}

void
FCM_Stride::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         ign_correct +=  proceeded && outcome && ((history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) || (history.stride_save.low_conf_inflight && history.who == VPSave::Stride));
         ign_incorrect += proceeded && !outcome &&  ((history.fcm_save.low_conf_inflight && history.who == VPSave::FCM) || (history.stride_save.low_conf_inflight && history.who == VPSave::Stride));
         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/

         bool highConf = history.who == VPSave::Stride ? history.stride_save.pred.second == 7 : (history.who == VPSave::FCM ? history.fcm_save.pred.second == 7 : false);

         bool transient =  (history.who == VPSave::Stride ? history.stride_save.pred.second >= 1 : (history.who == VPSave::FCM ? history.fcm_save.pred.second >= 1 : false))
                         && !highConf;

         bool low_conf =  (history.who == VPSave::Stride ? history.stride_save.pred.second == 0 :(history.who == VPSave::FCM ? history.fcm_save.pred.second == 0 : false))
                         && !highConf && !transient;

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}

