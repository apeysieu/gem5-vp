/*
 * stride.cc
 *
 *  Created on: Oct 12, 2012
 *      Author: aperais
 */

#include "cpu/vpred/stride.hh"

#include "cpu/o3/helper.hh"

std::string StrideVP::Entry::name;

/**
 * Constructor.
 *@param[in] size	Number of entries in the predictor (power of 2).
 *@param[in] assoc	Associativity of the table.
 **/
StrideVP::StrideVP(std::string &name, unsigned size_VHT, unsigned tag_vht, unsigned assoc, unsigned counterwidth,
                std::vector<unsigned> &probability) {

        this->_name = name + ".VPredUnit.Stride";
        StrideVP::Entry::name = this->_name + ".Entry";
        this->proba = probability;
        this->assoc = assoc;

        this->fetch_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_VHT, this->assoc, counterwidth, proba);
        this->commit_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_VHT, this->assoc, counterwidth, proba);

        this->_low_conf_inflight.resize(size_VHT, 0);
        this->inflight.resize(size_VHT, 0);
        this->TAG_SIZE_VHT = tag_vht;
}


StrideVP::~StrideVP() {}


prediction_t
StrideVP::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{

        Addr base_upc = createUPCindex(micropc, (Addr)log2(this->commit_VHT.size()));
        Addr index_vht = (base_upc ^ ip) & (this->fetch_VHT.size() - 1);


        Addr tag_VHT = ((ip >> (unsigned) log2(this->fetch_VHT.size())) & (((Addr) 1 << TAG_SIZE_VHT) - 1));


        VPSave *save = new VPSave();
        vp_history = static_cast<void*>(save);

        save->index_vht = index_vht;
        DPRINTF(ValuePredictor, "Trying to access entry %u with tag %u\n", index_vht, tag_VHT);

        save->tag_vht = tag_VHT;
        save->way = this->commit_VHT[index_vht].tagLookup(tag_VHT);
        save->mismatch = save->way == assoc;

        //Init
        save->low_conf_inflight = false;
        save->ip = ip;

        prediction_t result;

        access++;

        if(save->mismatch) {
                DPRINTF(ValuePredictor, "Tag mismatch, returning\n");
                save->pred = prediction_t(Prediction(),0);
                return save->pred;
        }

#ifndef NO_SPECULATIVE_HISTORY
        Prediction val = (inflight[save->index_vht] == 0 ? this->commit_VHT[index_vht].getPred(save->way) + this->commit_VHT[index_vht].getStride(save->way) : this->fetch_VHT[index_vht].getPred(save->way)  + this->commit_VHT[index_vht].getStride(save->way));
#else
        Prediction val = this->commit_VHT[index_vht].getPred(save->way) + this->commit_VHT[index_vht].getStride(save->way);
#endif

        result = prediction_t(val, this->commit_VHT[index_vht].getConfidence(save->way));
        DPRINTF(ValuePredictor, "Accessing table, value in fetch: %s, value in commit: %s, stride: %s\n", this->fetch_VHT[index_vht].getPred(save->way).tostring() , this->commit_VHT[index_vht].getPred(save->way).tostring(), this->commit_VHT[index_vht].getStride(save->way).tostring());

        save->pred = result;
        if(this->commit_VHT[index_vht].getStride(save->way).getValue() != 0) {
                nonZeroStride++;
        }

        if(this->_low_conf_inflight[index_vht] > 0) {
                        DPRINTF(ValuePredictor, "Low conf prediction inflight, ignoring this one \n");
                save->low_conf_inflight = true;
        }

#ifdef PARTIAL_TRACKING
        if((save->pred.second < 7)) {
#else
        if((save->pred.second < 7 || save->low_conf_inflight)) {
#endif
                this->_low_conf_inflight[index_vht]++;
        }

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        //std::cerr << here << std::endl;
        save->low_conf_inflight = false;
#endif

        inflight[save->index_vht]++;
        DPRINTF(ValuePredictor, "Updating speculative history with %s, saving %s, inflight for entry %u: %u\n", save->pred.first.tostring(), this->fetch_VHT[save->index_vht].getPred(save->way).tostring(), save->index_vht, inflight[save->index_vht]);
        this->fetch_VHT[save->index_vht].update(save->pred.first, *save, save->way);
        assert(save->index_vht == index_vht);
        assert(this->fetch_VHT[index_vht].getPred(save->way) == save->pred.first);


        DPRINTF(ValuePredictor, "Predicting for inst at 0x%lx."
                        " Accessing entry (VHT) %lu (tag: %lu, mismatch: %u). Predicted %s with confidence %i\n",
                                ip, save->index_vht, save->tag_vht, save->mismatch,
                                 save->pred.first.tostring(), (int) save->pred.second);

        if(!save->low_conf_inflight) {
                return save->pred;
        } else {
                if(save->pred.second == 7) {
                        ignoredLowConfInflight++;
                }
                return prediction_t(Prediction(),0);
        }
}

void
StrideVP::squash(void *vp_history, bool remove) {

         VPSave *history = static_cast<VPSave*>(vp_history);

         if(remove) {
#ifdef PARTIAL_TRACKING
                 if(!history->mismatch && (history->pred.second < 7)) {
#else
                 if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
#endif
                         _low_conf_inflight[history->index_vht]--;
                         assert(_low_conf_inflight[history->index_vht] >= 0);
                 }
                 if(!history->mismatch) {
                         inflight[history->index_vht]--;
                         assert(inflight[history->index_vht] >= 0);
                 }
         }

         if(!history->mismatch && inflight[history->index_vht] != 0) {
                 access++;
                 DPRINTF(ValuePredictor, "Restoring entry %u in fetch history with %s\n", history->index_vht, history->previous.tostring());
                 this->fetch_VHT[history->index_vht].restore(history->previous, history->way);
         } else if(!history->mismatch) {
                          Prediction tmp = this->commit_VHT[history->index_vht].getPred(history->way);
                         DPRINTF(ValuePredictor, "Restoring entry %u in fetch and commit histories\n", history->index_vht, tmp.tostring());
                 access++;

                 this->fetch_VHT[history->index_vht].restore(tmp, history->way);
         }

         if(remove) {
                 delete history;
         }
}
void
StrideVP::update(Prediction & value, void *vp_history, bool mispred, bool squashed)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way = this->commit_VHT[history->index_vht].tagLookup(history->tag_vht);

        if(squashed) {
                this->commit_VHT[history->index_vht].update(value, NULL,history->tag_vht, false, 0, squashed);
                return;
        }

        access++;
        if(commit_way != assoc) {
                if(this->commit_VHT[history->index_vht].getConfidence(commit_way) == 7) {
                        if(this->commit_VHT[history->index_vht].getPred(commit_way) + this->commit_VHT[history->index_vht].getStride(commit_way) == value) {
                                commit_correct++;
                        } else {
                                commit_incorrect++;
                        }
                }
        }

#ifdef PARTIAL_TRACKING
                 if(!history->mismatch && (history->pred.second < 7)) {
#else
                 if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
#endif
                  this->_low_conf_inflight[history->index_vht]--;
                  assert(this->_low_conf_inflight[history->index_vht] >= 0);
        }

#ifndef NOREAD_AT_COMMIT
        bool correct = (commit_way != this->assoc && (this->commit_VHT[history->index_vht].getPred(commit_way) + this->commit_VHT[history->index_vht].getStride(commit_way)) == value) && !mispred;
#else
        bool correct = !history->mismatch && history->pred.first == value) && !mispred;
#endif

        DPRINTF(ValuePredictor, "Updating predictor with %s (correct: %u)."
                        " Accessing entry (VHT) %lu (tag: %lu, mismatch: %u). Way: %u\n",
                         value.tostring(), correct, history->index_vht, history->tag_vht, history->mismatch, commit_way);

        //Update the SHT first because we need the value in the VHT to compute the stride.

        if(!history->mismatch) {
                inflight[history->index_vht]--;
                DPRINTF(ValuePredictor, "Inflight for entry %u : %u\n",history->index_vht, inflight[history->index_vht]);
        }


        DPRINTF(ValuePredictor, "Updating %s with %s\n",  this->commit_VHT[history->index_vht].getPred(commit_way).tostring(), value.tostring());
        DPRINTF(ValuePredictor, "Strides are %s and %s\n", this->commit_VHT[history->index_vht].getStride(commit_way).tostring(),  this->commit_VHT[history->index_vht].getStride2(commit_way).tostring());
        DPRINTF(ValuePredictor, "New Stride is %s\n", (this->commit_VHT[history->index_vht].getPred(commit_way) - value).tostring());
        this->commit_VHT[history->index_vht].update(value, &history->previous, history->tag_vht, correct, history->pred.second);


        if(commit_way != this->assoc && inflight[history->index_vht] == 0) {
                         DPRINTF(ValuePredictor, "Restoring entry %u in fetch history with %s\n", history->index_vht, value.tostring());
                this->fetch_VHT[history->index_vht].restore(value, commit_way);
        }

        updateStats(*history, !history->mismatch && history->pred.first == value && !mispred);
        delete history;
}

void StrideVP::regStats()
{


        access
                .name(name() + ".accesses")
                .desc("Accesses to the VHT")
                ;

        nonZeroStride
        .name(name() + ".nonZeroStride")
        .desc("Number of predictions with a non zero stride")
        ;

           accuracy_ideal
                .name(name() + ".accuracy_ideal")
                .desc("Accuracy ideal")
                ;

                coverage_ideal
                .name(name() + ".coverage_ideal")
                .desc("Coverage ideal")
                ;

                commit_correct
                .name(name() + ".commit_correct")
                .desc("Total number of correct predictions ideal")
                ;

                commit_incorrect
                .name(name() + ".commit_incorrect")
                .desc("Total number of incorrect predictions ideal")
                ;

        ignoredLowConfInflight
                .name(name() + "ignoredBecauseLowConf")
                .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
                ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        missrate
        .name(name() + ".miss_all")
        .desc("Missrate of the predictor")
        .precision(5)
        ;

        miss
             .name(name() + ".misses")
             .desc("Total number of tag mismatches")
             ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;



        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        ignoredCorrect
               .name(name() + ".ignoredCorrect")
               .desc("Percentage of correct predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ignoredIncorrect
               .name(name() + ".ignoredIncorrect")
               .desc("Percentage of incorrect predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ign_correct
               .name(name() + ".ignCorrect")
               .desc("Number of correct predictions ignored because low conf inflight")
               ;

               ign_incorrect
                           .name(name() + ".ignInorrect")
                           .desc("Number of incorrect predictions ignored because low conf inflight")
                           ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;
        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);
        missrate = miss / attempted;
}

void StrideVP::updateStats(VPSave &history, bool outcome)
{
        bool proceeded = history.pred.second == 7;

        miss += history.mismatch;
        ign_correct +=  proceeded && outcome && history.low_conf_inflight ;
        ign_incorrect += proceeded && !outcome &&  history.low_conf_inflight;
        attempted ++;

        DPRINTF(ValuePredictor, "Updating stat, outcome %u, proceeded: %u, low conf inflight: %u\n", outcome, proceeded, history.low_conf_inflight);
        if(outcome && proceeded && !history.low_conf_inflight) {
        } else if(outcome && proceeded && history.low_conf_inflight) {
                DPRINTF(ValuePredictor, "Committing low conf inflight predicton\n");
        }

        correct += outcome && proceeded && !history.low_conf_inflight;
        incorrect += !outcome && proceeded && !history.low_conf_inflight;

        /** Stats  **/

        bool highConf = history.pred.second == 7;

        bool transient =  history.pred.second >= 1 && !highConf;

        bool low_conf =   history.pred.second == 0 && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;
}
