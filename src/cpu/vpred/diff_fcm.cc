/*
 * diff_fcm.cc
 *
 *  Created on: Jan 31, 2013
 *      Author: aperais
 */


#include "./diff_fcm.hh"
#include "base/statistics.hh"
#include "cpu/o3/helper.hh"
#include "cpu/vpred/fcm_spec.hh"
#include "debug/ValuePredictor.hh"

uint32_t DiffFCM::LocalHistEntry::_lhist_length;
std::string DiffFCM::LocalHistEntry::_name;

DiffFCM::DiffFCM(std::string &name, unsigned size_vht, unsigned size_ht, unsigned tag_length_ht, unsigned lhist_length, unsigned counterWidth, std::vector<unsigned> &proba, unsigned assoc)
{
        this->_name = name + ".VPredUnit.DiffFCM";
        DiffFCM::LocalHistEntry::_lhist_length = lhist_length;
        DiffFCM::LocalHistEntry::_name = this->_name + ".Entry";


        HT_INDEX = log2(size_ht);
        VHT_INDEX = log2(size_vht);
        TAG_LENGTH = tag_length_ht;
        this->_proba =proba;
        this->assoc = assoc;

        this->fetch_HT = AssociativeArray<DiffFCM, DiffFCM::LocalHistEntry>(size_ht, this->assoc, counterWidth, _proba);
        this->commit_HT = AssociativeArray<DiffFCM, DiffFCM::LocalHistEntry>(size_ht, this->assoc, counterWidth, _proba);

        _low_conf_inflight.resize(size_ht, 0);
        inflight.resize(size_ht, 0);

        this->_VHT.resize(size_ht);
        this->_conf.resize(size_vht, ConfCounter(2, 0, &(this->_proba)));

}

prediction_t DiffFCM::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);

        history->ip = createUPCindex(micropc, (Addr)log2(this->commit_HT.size())) ^ ip;
        history->ht_index = (history->ip & (this->commit_HT.size() - 1));
        history->tag = (ip >> this->HT_INDEX) & ((((Addr) 1) << TAG_LENGTH) - 1);
        DPRINTF(ValuePredictor, "Index for the prediction: %lx, tag: %lx\n", history->ht_index, history->tag);

        //Init
        history->low_conf_inflight = false;
        history->way = this->commit_HT[history->ht_index].tagLookup(history->tag);
        history->mismatch = history->way == assoc;

        //Don't predict on a tag mismatch
        if(history->mismatch) {
                DPRINTF(ValuePredictor, "Tag mismatch when getting prediction for inst at 0x%lx in entry %lu (tag %lu)\n",
                                ip, history->ht_index, history->tag);
                history->pred = prediction_t(Prediction(),0);
                return prediction_t(Prediction(),0);
        }


        history->vht_index = this->fetch_HT[history->ht_index].getIndex(history->way, VHT_INDEX, history->ip);
        history->last_value = this->fetch_HT[history->ht_index].getPred(history->way);
        history->pred = prediction_t(this->_VHT[history->vht_index] + this->fetch_HT[history->ht_index].getPred(history->way), this->commit_HT[history->ht_index].getConfidence(history->way));

        DPRINTF(ValuePredictor, "Inflight: %i, vht_index: %lu\n", inflight[history->ht_index], history->vht_index);
        DPRINTF(ValuePredictor, "Predicting %s with conf %i\n", history->pred.first.tostring(), (int) history->pred.second);

        //Track if there are low confidence predictions in flight for the same index
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if( _low_conf_inflight[history->ht_index] > 0) {
                DPRINTF(ValuePredictor, "Pred is low conf\n");
                history->low_conf_inflight = true;
        }

        if( (history->pred.second < 7 || history->low_conf_inflight)) {
                _low_conf_inflight[history->ht_index]++;
                DPRINTF(ValuePredictor, "One more low conf inflight: %i\n", _low_conf_inflight[history->ht_index]);
        }
#endif

        inflight[history->ht_index]++;
        DPRINTF(ValuePredictor, "One more inflight for %lu, inflight: %i\n", history->ht_index, inflight[history->ht_index]);

        //Speculative update of the table (to track speculative last values)
        this->fetch_HT[history->ht_index].update(history->pred.first, this->_VHT[history->vht_index], *history, history->way);



#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if(!history->low_conf_inflight) {
#endif
                DPRINTF(ValuePredictor, "Returning pred\n");
                return history->pred;
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        } else {

                if(history->pred.second == 7) {
                        ignoredLowConfInflight++;
                }
                DPRINTF(ValuePredictor, "Returning dummy because low conf\n");
                return prediction_t(Prediction(),0);
        }
#endif
}

void DiffFCM::updateVHT(uint16_t index, Prediction & stride)
{
        //Replace the stride if hysteresis counter is 0.
        if(_conf[index].read() == 0) {
                this->_VHT[index] = stride;
        }
}

void DiffFCM::update(Prediction & value, void *vp_history, bool mispred, bool squashed)
{
        assert(vp_history);
        VPSave *history = static_cast<VPSave*>(vp_history);

        //Theoretically, has to check that the entry is still valid. Could have been replaced.
#ifdef NOREAD_AT_COMMIT
        unsigned commit_way_fcm = history->way;
#else
        unsigned commit_way_fcm = commit_HT[history->ht_index].tagLookup(history->tag);
#endif

        //If squashing, don't fully update (will be done in order at commit) but forbid the entry from predicting by resetting confidence counter.
        if(squashed) {
                DPRINTF(ValuePredictor, "squashing counter in entry %lx\n", history->ht_index);
                this->commit_HT[history->ht_index].update(value, NULL, history->tag, false, 0, true);
                return;
        }

        //Re-compute the vht index if needed.
#ifndef NOREAD_AT_COMMIT
        //Stats
        if(history->vht_index != this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip) && history->pred.second == 7 && !(history->pred.first == value)) {
                indexDiffers++;
        }

        if(commit_way_fcm != assoc && this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip)
                        == history->vht_index && history->pred.second == 7
                        && ((this->_VHT[this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip)]
                                        + this->commit_HT[history->ht_index].getPred(commit_way_fcm)) == value)
                                        && !(history->pred.first == value)) {
                DPRINTF(ValuePredictor, "Value in pred : %s, value in table : %s\n", history->pred.first.tostring(), this->_VHT[history->vht_index].tostring());
                valueDiffers++;
        }


        if(commit_way_fcm != this->assoc) {
                history->vht_index = this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip);
        }
#endif


        if(commit_way_fcm != this->assoc) {
                if(this->commit_HT[history->ht_index].getConfidence(commit_way_fcm) == 7) {
                        if(this->_VHT[this->commit_HT[history->ht_index].getIndex(commit_way_fcm, VHT_INDEX, history->ip)] + this->commit_HT[history->ht_index].getPred(commit_way_fcm) == value) {
                                commit_correct++;
                        } else {
                                commit_incorrect++;
                        }
                }
        }

#ifndef NOREAD_AT_COMMIT
        bool outcome = commit_way_fcm != assoc  && (this->_VHT[history->vht_index] + this->commit_HT[history->ht_index].getPred(commit_way_fcm)) == value && !mispred;
#else
        bool outcome = !history->mismatch && history->pred.first == value;
#endif
        DPRINTF(ValuePredictor, "Update, value predicted: %s, value in commit: %s, value: %s, outcome: %lu\n", history->pred.first.tostring(), this->_VHT[history->vht_index].tostring(), value.tostring(), outcome);

#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
        if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
                _low_conf_inflight[history->ht_index]--;
                DPRINTF(ValuePredictor, "One less low conf in flight: %lu, at %lu\n", _low_conf_inflight[history->ht_index], history->ht_index);
                assert(_low_conf_inflight[history->ht_index] >= 0);
        }
#endif

        //Track the number of inflight predictions for the index.
        if(!history->mismatch) {
                inflight[history->ht_index]--;
                DPRINTF(ValuePredictor, "Accessing entry %lx, inflight: %i\n", history->ht_index, inflight[history->ht_index]);
                assert(inflight[history->ht_index] >= 0);
        }

        //Compute the new stride
        Prediction stride;
#ifdef NOREAD_AT_COMMIT
        if(!history->mismatch) {
                stride = value - history->last_value;
        }
#else
        if(commit_way_fcm != this->assoc) {
                stride = value - this->commit_HT[history->ht_index].getPred(commit_way_fcm);
        }
#endif

if(commit_way_fcm != assoc) {
        this->updateVHT(history->vht_index, stride);
        if(outcome) {
                this->_conf[history->vht_index].increment();
        } else {
                this->_conf[history->vht_index].decrement();
        }
}

DPRINTF(ValuePredictor, "Updating value predictor with value %s (outcome: %u). Accessing entry %lu (tag: %lu, mismatch: %u).\n",
                value.tostring(), outcome, history->ht_index, history->tag, history->mismatch);

this->commit_HT[history->ht_index].update(value, &stride, history->tag, outcome, history->pred.second);

//If there is no speculative state, copy the committed last value into the speculative last value.
if(inflight[history->ht_index] == 0) {
                unsigned way = this->commit_HT[history->ht_index].tagLookup(history->tag);
        DPRINTF(ValuePredictor, "Tag mismatch or not other instances in flight: setting speculative history to committed history\n");
        Prediction tmp = this->commit_HT[history->ht_index].getPred(commit_way_fcm);
        this->fetch_HT[history->ht_index].restore(this->commit_HT[history->ht_index].getSignatures(way), tmp, way);
}

updateStats(*history, !history->mismatch && (history->pred.first == value));
delete history;
}

void DiffFCM::squash(void *vp_history, bool remove)
{
        VPSave *history = static_cast<VPSave*>(vp_history);

        DPRINTF(ValuePredictor, "Squash, restoring fetch history for ht: %lx, vht %lx\n", history->ht_index, history->vht_index);

        if(remove) {
#ifndef NO_INFLIGHT_CONFIDENCE_TRACKING
                if(!history->mismatch && (history->pred.second < 7 || history->low_conf_inflight)) {
                        _low_conf_inflight.at(history->ht_index)--;
                        assert(_low_conf_inflight.at(history->ht_index) >= 0);
                }
#endif
if(!history->mismatch) {
        inflight.at(history->ht_index)--;
        assert(inflight.at(history->ht_index) >= 0);
}
        }

        //Restore last value of the entry from history or from committed state depending on the number of inflight predictions.
        if(!history->mismatch && inflight.at(history->ht_index) != 0) {
                DPRINTF(ValuePredictor, "Restoring fetch history\n");
                this->fetch_HT[history->ht_index].restore(history->signatures, history->last_value, history->way);
        } else if(!history->mismatch) {
                DPRINTF(ValuePredictor, "Restoring commit and fetch history\n");
                Prediction tmp = this->commit_HT[history->ht_index].getPred(history->way);
                this->fetch_HT[history->ht_index].restore(this->commit_HT[history->ht_index].getSignatures(history->way), tmp, history->way);
        }

        if(remove) {
                delete history;
        }
}

void DiffFCM::regStats()
{

        ignoredLowConfInflight
        .name(name() + "ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight")
        ;

        indexDiffers
        .name(name() + ".VHTIndexDiffers")
        .desc("Number of times the index of the VHT computed at fetch differed from the one computed at commit")
        ;

        valueDiffers
        .name(name() + ".VHTValueDiffers")
        .desc("Number of times the value in the VHT computed at fetch differed from the one computed at commit")
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        .flags(Stats::total)
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        .flags(Stats::total)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        .flags(Stats::total)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        .flags(Stats::total)
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        .flags(Stats::total)
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        vhist_repair
        .name(name() + ".vhist_repair")
        .desc("Total number of value history repair (should be the same as the number of proceeding mispredictions)")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;

        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);

        ignoredIncorrect = ign_incorrect / attempted;
}

void DiffFCM::updateStats(VPSave &history, bool outcome)
{
        bool proceeded = history.pred.second == 7;

        ign_correct +=  proceeded && outcome && history.low_conf_inflight ;
        ign_incorrect += proceeded && !outcome &&  history.low_conf_inflight;
        attempted ++;
        correct += outcome && proceeded && !history.low_conf_inflight;
        incorrect += !outcome && proceeded && !history.low_conf_inflight;

        /** Stats  **/

        bool highConf = history.pred.second == 7;

        bool transient =  history.pred.second >= 1 && !highConf;

        bool low_conf =   history.pred.second == 0 && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;
}


