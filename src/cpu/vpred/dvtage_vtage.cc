/*
 * dvtage_vtage.cc
 *
 *  Created on: Jun 21, 2013
 *      Author: aperais
 */

#include "cpu/vpred/dvtage_vtage.hh"



DVTAGE_VTAGE_VP::DVTAGE_VTAGE_VP(std::string &name,
        std::deque<TageBP::Hist> *bhist,
        unsigned numHistComponents,
        unsigned numLogBaseEntry,
        unsigned dvtage_base,
        unsigned minHistSize,
        unsigned maxHistSize,
        unsigned baseHystShift,
        unsigned instShiftAmt,
        unsigned *phist,
        unsigned counterwidth,
        std::vector<unsigned> &proba_vtage)
{

       this->_name = name + ".VPredUnit.DVTAGE_VTAGE_VP";

       arbiter = 0;

       vtage = new VTageVP(name,
                   numHistComponents,
                   numLogBaseEntry,
                   minHistSize,
                   maxHistSize,
                   baseHystShift,
                   counterwidth,
                   instShiftAmt,
                   proba_vtage,
                   bhist,
                   phist);

       dvtage = new DiffVTAGE(name,
                         numHistComponents,
                         numLogBaseEntry,
                         dvtage_base,
                         minHistSize,
                         maxHistSize,
                         baseHystShift,
                         counterwidth,
                         instShiftAmt,
                         proba_vtage,
                         bhist,
                         phist);

}

prediction_t
DVTAGE_VTAGE_VP::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        VPSave *history = new VPSave();

        vp_history = static_cast<void *>(history);

        //Lookup will allocate save.
        void *vsave = NULL;
        prediction_t vtage_pred = vtage->lookup(ip, micropc, vsave);
        VTageVP::VPSave* vtage_save = static_cast<VTageVP::VPSave*>(vsave);

        void *dvsave;
        prediction_t dvtage_pred = dvtage->lookup(ip, micropc, dvsave);
        DiffVTAGE::VPSave* dvtage_save = static_cast<DiffVTAGE::VPSave*>(dvsave);


        history->isBranch = false;

        //Arbitration if needed
        history->vtage_save = vtage_save;
        history->dvtage_save = dvtage_save;

        bool vtage_high = vtage_pred.second == 7;
        bool dvtage_high = dvtage_pred.second == 7;
        if(vtage_high && dvtage_high) {
                if(dvtage_pred.first == vtage_pred.first) {
                        history->who = VPSave::All;
                        return dvtage_pred;
                } else {
                        history->who = VPSave::None;

                        return prediction_t(Prediction(),0);
                }
        } else if(dvtage_high) {
                history->who = VPSave::DVTAGE;
                made_by_dvtage++;
                return dvtage_pred;


        } else if(vtage_high) {


                history->who = VPSave::VTAGE;
                made_by_vtage++;
                return vtage_pred;
        } else {
                history->who = VPSave::None;

                return prediction_t(Prediction(),0);
        }

}


void
DVTAGE_VTAGE_VP::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        void *branch = static_cast<void*>(history->vtage_save);
        vtage->flush_branch(branch);
        branch = static_cast<void*>(history->dvtage_save);
        dvtage->flush_branch(branch);
        delete history;
}

void
DVTAGE_VTAGE_VP::updateFoldedHist(bool save, void *&vp_history) {

        VPSave *history = new VPSave();
        vp_history = static_cast<void*>(history);


        void *branch = NULL;
        vtage->updateFoldedHist(save, branch);
        VTageVP::VPSave * tmp = static_cast<VTageVP::VPSave*>(branch);
        void *dvbranch = NULL;
        dvtage->updateFoldedHist(save, dvbranch);
        DiffVTAGE::VPSave * dvtmp = static_cast<DiffVTAGE::VPSave*>(dvbranch);
        history->isBranch = true;
        history->vtage_save = tmp;
        history->dvtage_save = dvtmp;
}

void
DVTAGE_VTAGE_VP::update(Prediction & value, void *vp_history, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);


        if(squashed) {
                if(history->who == VPSave::VTAGE) {
                        void * tmp = static_cast<void*>(history->vtage_save);
                        this->vtage->update(value,tmp, false, true);
                } else if(history->who == VPSave::DVTAGE) {
                        void * tmp = static_cast<void*>(history->dvtage_save);
                        this->dvtage->update(value,tmp, false, true);
                } else if(history->who == VPSave::All) {
                        void * tmp = static_cast<void*>(history->vtage_save);
                        this->vtage->update(value,tmp, false, true);
                        tmp = static_cast<void*>(history->dvtage_save);
                        this->dvtage->update(value,tmp, false, true);
                }
                return;
        }

        prediction_t &vtage_pred = history->vtage_save->tagePred;
        prediction_t &dvtage_pred = history->dvtage_save->tagePred;

        bool outcome_vtage = vtage_pred.first == value && vtage_pred.second == 7;
        correct_vtage += outcome_vtage && history->who == VPSave::VTAGE;
        incorrect_vtage += !outcome_vtage && history->who == VPSave::VTAGE && vtage_pred.second == 7;

        bool outcome_dvtage = dvtage_pred.first == value && dvtage_pred.second == 7;
        correct_dvtage += outcome_dvtage && history->who == VPSave::VTAGE;
        incorrect_dvtage += !outcome_dvtage && history->who == VPSave::VTAGE && dvtage_pred.second == 7;

        correct_all += outcome_dvtage && outcome_vtage && history->who == VPSave::All && dvtage_pred.second == 7 && vtage_pred.second == 7;
        incorrect_all += !outcome_dvtage && !outcome_vtage && history->who == VPSave::All && dvtage_pred.second == 7 && vtage_pred.second == 7;


        outcome_vtage = vtage_pred.first == value;
        outcome_dvtage = dvtage_pred.first == value;

        //Updating the arbiter
#ifdef TOURNAMENT_VPRED
        if(outcome_vtage && !outcome_dvtage && dvtage_commit_VHT[history->dvtage_save.index_vht].getConfidence(commit_way_dvtage) == 7 &&  vtage_pred.second == 7) {
                arbiter[history->dvtage_save.index_vht] < 15 ? arbiter[history->dvtage_save.index_vht]++ : 0;
        } else if(!outcome_vtage && outcome_dvtage && dvtage_commit_VHT[history->dvtage_save.index_vht].getConfidence(commit_way_dvtage) &&  vtage_pred.second == 7) {
                arbiter[history->dvtage_save.index_vht] > -16 ? arbiter[history->dvtage_save.index_vht]-- : 0;
        }
#else
        if(outcome_vtage && !outcome_dvtage && dvtage_pred.second == 7 &&  vtage_pred.second == 7) {
                arbiter < 15 ? arbiter++ : 0;
        } else if(!outcome_vtage && outcome_dvtage &&  dvtage_pred.second == 7 &&  vtage_pred.second == 7) {
                arbiter > -16 ? arbiter-- : 0;
        }
#endif



        bool outcome = ((history->who == VPSave::VTAGE || history->who == VPSave::All) && vtage_pred.first == value) || (history->who == VPSave::DVTAGE && dvtage_pred.first == value);
        updateStats(*history, outcome);

        void * vtage_hist = static_cast<void*>(history->vtage_save);
        this->vtage->update(value, vtage_hist, false, false);
        void * dvtage_hist = static_cast<void*>(history->dvtage_save);
        this->dvtage->update(value, dvtage_hist, false, false);


        delete history;
}

void
DVTAGE_VTAGE_VP::squash(void *vp_history, bool remove, bool recompute) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        assert(history);


        void * vtage_hist = static_cast<void*>(history->vtage_save);
        void * dvtage_hist = static_cast<void*>(history->dvtage_save);

        if(history->isBranch) {
                DPRINTF(ValuePredictor, "Deleting a branch history\n");
                this->vtage->squash(vtage_hist, remove, recompute);
                this->dvtage->squash(dvtage_hist, remove, recompute);
        } else {
                DPRINTF(ValuePredictor, "Deleting a value history\n");
                this->vtage->squash(vtage_hist, remove, recompute);
                this->dvtage->squash(dvtage_hist, remove, recompute);

        }


        if(remove) {
                delete history;
        }

}

void
DVTAGE_VTAGE_VP::regStats()
{
                vtage->regStats();
                dvtage->regStats();
        made_by_dvtage
        .name(name() + ".madeByDVTAGE")
        .desc("Number of predictions made by Stride")
        ;

        made_by_vtage
        .name(name() + ".madeByVTAGE")
        .desc("Number of predictions made by VTAGE")
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_vtage
                .name(name() + ".correctVTAGE")
                .desc("Number of correct predictions coming from VTAGE")
                ;

        correct_dvtage
                        .name(name() + ".correctDVTAGE")
                        .desc("Number of correct predictions coming from Stride")
                        ;

        incorrect_vtage
                        .name(name() + ".incorrectVTAGE")
                        .desc("Number of correct predictions coming from VTAGE")
                        ;

        incorrect_dvtage
                        .name(name() + ".incorrectDVTAGE")
                        .desc("Number of correct predictions coming from PS")
                        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        correct_all
        .name(name() + ".correctAll")
        .desc("Number of correct predictions coming from All")
        ;

        incorrect_all
        .name(name() + ".incorrectAll")
        .desc("Number of incorrect predictions coming from All")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;


        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);

}

void
DVTAGE_VTAGE_VP::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         prediction_t &vtage_pred = history.vtage_save->tagePred;
         prediction_t &dvtage_pred = history.dvtage_save->tagePred;

         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/


         bool highConf =  (history.who == VPSave::DVTAGE && dvtage_pred.second == 7) || (history.who == VPSave::VTAGE && vtage_pred.second == 7);

         bool transient =  ((history.who == VPSave::DVTAGE && dvtage_pred.second >= 1) || (history.who == VPSave::VTAGE && vtage_pred.second >= 1)) && !highConf;


         bool low_conf =  ((history.who == VPSave::DVTAGE && dvtage_pred.second == 0) || (history.who == VPSave::VTAGE && vtage_pred.second == 0))
                         && !highConf && !transient;

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}

