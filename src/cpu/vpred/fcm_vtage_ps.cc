/*
 * fcm_vtage_ps.cc
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */


#include "cpu/vpred/fcm_vtage_ps.hh"

//#include "boost/dynamic_bitset.hpp"
#include "cpu/o3/helper.hh"

VTAGE_FCM_PS::VTAGE_FCM_PS(std::string &name,
                //PS
                                unsigned size_ps_vht,
                                unsigned size_ps_sht,
                                unsigned tag_ps_vht_size,
                                unsigned tag_ps_sht_size,
                                unsigned bhist_length,
                                std::vector<unsigned> &proba_ps,
                                unsigned ps_associativity_vht,
                                unsigned ps_associativity_sht,
                                //FCM
                                unsigned size_fcm_vht,
                                unsigned size_fcm_ht,
                                unsigned tag_fcm_size,
                                unsigned lhist_length,
                                std::vector<unsigned> &proba_fcm,
                                unsigned assoc_fcm,
                                //VTAGE
                                unsigned numHistComponents,
                                unsigned numLogBaseEntry,
                                unsigned minHistSize,
                                unsigned maxHistSize,
                                unsigned baseHystShift,
                                unsigned instShiftAmt,
                                std::deque<TageBP::Hist> *bhist,
                                unsigned *phist,
                                std::vector<unsigned> &proba_vtage,
                                //Misc
                                unsigned counterwidth)
{

       this->_name = name + ".VPredUnit.VTAGE_FCM_PS";

       FCM_SPEC_VP::Entry::_lhist_length = lhist_length;

       FCM_HT_INDEX = log2(size_fcm_ht);
       FCM_VHT_INDEX = log2(size_fcm_vht);
       PS_VHT_INDEX = log2(size_ps_vht);
       PS_SHT_INDEX = log2(size_ps_sht);
       PS_VHT_TAG_LENGTH = tag_ps_vht_size;
       PS_SHT_TAG_LENGTH = tag_ps_sht_size;
       FCM_TAG_LENGTH = tag_fcm_size;
       BHIST_LENGTH = bhist_length;

       fcm_proba = proba_fcm;
       ps_proba = proba_ps;

       this->assoc_ps_vht = ps_associativity_vht;
       this->assoc_ps_sht = ps_associativity_sht;
       this->assoc_fcm = assoc_fcm;

       this->bhist = bhist;

       this->fcm_fetch_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);
       this->fcm_commit_HT = AssociativeArray<FCM_SPEC_VP, FCM_SPEC_VP::Entry>(size_fcm_ht, this->assoc_fcm, counterwidth, fcm_proba);

       fcm_VHT.resize(size_fcm_vht);
       fcm_conf.resize(size_fcm_vht);
       for(unsigned i = 0; i < size_fcm_vht; i++) {
           fcm_conf[i] = ConfCounter(2, 0, &fcm_proba);
       }


       this->ps_fetch_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_ps_vht, this->assoc_ps_vht, counterwidth, ps_proba);
       this->ps_commit_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_ps_vht, this->assoc_ps_vht, counterwidth, ps_proba);

       this->ps_SHT = AssociativeArray<PSVP, PSVP::SHTEntry>(size_ps_sht, this->assoc_ps_sht, counterwidth, ps_proba);

       arbiter.resize(3);
       arbiter[0] = 0;
       arbiter[2] = 0;
       arbiter[1] = 0;





       ps_low_conf_inflight.resize(size_ps_vht);
       ps_inflight.resize(size_ps_vht);
       fcm_low_conf_inflight.resize(size_fcm_ht);
       fcm_inflight.resize(size_fcm_ht);



       for(unsigned i = 0; i < fcm_commit_HT.size(); i++)
       {
               this->fcm_conf[i] = ConfCounter(counterwidth, 0, &fcm_proba);
       }

       tage = new VTageVP(name,
                   numHistComponents,
                   numLogBaseEntry,
                   minHistSize,
                   maxHistSize,
                   baseHystShift,
                   counterwidth,
                   instShiftAmt,
                   proba_vtage,
                   bhist,
                   phist);

       //This assert is here because of the way we compute the index of the VHT.
       //assert(pow((double) 2, (double) (8 + lhist_length - 1)) == size_fcm_vht);
}

Addr
VTAGE_FCM_PS::hashPC(Addr ip)
{
        std::bitset<640> tmp;
        for(unsigned i = 0; i < BHIST_LENGTH; i++) {
                tmp[i] = (*bhist)[i].dir;
        }
        return ((ip << BHIST_LENGTH) + tmp.to_ulong());
}

prediction_t
VTAGE_FCM_PS::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        Addr base_upc_vht_ps = createUPCindex(micropc, (Addr) log2(this->ps_commit_VHT.size()));
        Addr base_upc_sht_ps = createUPCindex(micropc, (Addr) log2(this->ps_SHT.size()));

        Addr base_upc_fcm = createUPCindex(micropc, FCM_VHT_INDEX);

        Addr fcm_ht_index = base_upc_fcm ^ (ip & (this->fcm_commit_HT.size() - 1));
        Addr fcm_tag = (ip >> this->FCM_HT_INDEX) & ((((Addr) 1) << FCM_TAG_LENGTH) - 1);

        Addr ps_vht_index = (base_upc_vht_ps ^ ip) & (this->ps_fetch_VHT.size() - 1);
        Addr ps_vht_tag = ((ip >> this->PS_VHT_INDEX) & ((((Addr) 1) << PS_VHT_TAG_LENGTH) - 1));

        Addr ps_sht_index = hashPC(base_upc_sht_ps ^ ip) % (this->ps_SHT.size());
        Addr ps_sht_tag = (ip >> (unsigned) log2(this->ps_SHT.size())) & (((Addr) 1 << PS_SHT_TAG_LENGTH) -1);
        //Addr ps_sht_tag = (hashPC(ip) >> (unsigned) log2(this->ps_SHT.size())) & (((Addr) 1 << PS_SHT_TAG_LENGTH) -1);


        VPSave *history = new VPSave();
        vp_history = static_cast<void *>(history);

        //Looking for the prediction of tage
        void *save = NULL;
        prediction_t vtage_pred = tage->lookup(ip, micropc, save);
        VTageVP::VPSave* vtage_save = static_cast<VTageVP::VPSave*>(save);


        history->isBranch = false;

        history->fcm_save.spec_update = false;
        history->ps_save.spec_update = false;

        history->ps_save.low_conf_inflight = false;
        history->fcm_save.low_conf_inflight = false;

        history->fcm_save.ht_index = fcm_ht_index;
        history->fcm_save.tag = fcm_tag;
        history->fcm_save.ip =  ip ^ base_upc_fcm;

        history->fcm_save.way = fcm_commit_HT[fcm_ht_index].tagLookup(fcm_tag);
        history->fcm_save.mismatch =  history->fcm_save.way == assoc_fcm;


        history->ps_save.index_vht = ps_vht_index;
        history->ps_save.tag_vht = ps_vht_tag;
        history->ps_save.index_sht = ps_sht_index;
        history->ps_save.tag_sht = ps_sht_tag;

        history->ps_save.way_vht = ps_commit_VHT[ps_vht_index].tagLookup(ps_vht_tag);
        history->ps_save.mismatch_vht = history->ps_save.way_vht == assoc_ps_vht;

        history->ps_save.way_sht = ps_SHT[ps_sht_index].tagLookup(ps_sht_tag);
        history->ps_save.mismatch_sht = history->ps_save.way_sht == assoc_ps_sht;



        DPRINTF(ValuePredictor, "FCM predicts using entry %u\n", history->fcm_save.ht_index);

        prediction_t pred_fcm, pred_ps;
        if(fcm_inflight[history->fcm_save.ht_index] == 0 && ! history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_commit_HT[fcm_ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        } else if(!history->fcm_save.mismatch) {
                history->fcm_save.vht_index = this->fcm_fetch_HT[fcm_ht_index].getIndex(history->fcm_save.way, FCM_VHT_INDEX,  history->fcm_save.ip);
                pred_fcm = prediction_t(this->fcm_VHT[history->fcm_save.vht_index], this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(history->fcm_save.way));
        }

        if(ps_inflight[history->ps_save.index_vht] == 0 && !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                pred_ps = prediction_t(this->ps_commit_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht) + this->ps_SHT[history->ps_save.index_sht].getStride(history->ps_save.way_sht),this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(history->ps_save.way_vht));
        } else if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                pred_ps = prediction_t(this->ps_fetch_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht) + this->ps_SHT[history->ps_save.index_sht].getStride(history->ps_save.way_sht),this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(history->ps_save.way_vht));
        }


        history->fcm_save.pred = (history->fcm_save.mismatch ? prediction_t(Prediction(),0) : pred_fcm);
        history->ps_save.pred = (history->ps_save.mismatch_vht || history->ps_save.mismatch_sht) ? prediction_t(Prediction(),0) : pred_ps;


        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && ps_low_conf_inflight[history->ps_save.index_vht] > 0) {
                history->ps_save.low_conf_inflight = true;
        }
        if(!history->fcm_save.mismatch && fcm_low_conf_inflight[history->fcm_save.ht_index] > 0) {
                history->fcm_save.low_conf_inflight = true;
        }

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht &&  (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                ps_low_conf_inflight[history->ps_save.index_vht]++;
        }
        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]++;
        }

        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]++;
        }

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                ps_inflight[history->ps_save.index_vht]++;
        }
        //Arbitration if needed

        history->vtage_save = (vtage_save);
        //history->vtage_save->spec_update = false;
        //delete vtage_save;

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->fcm_save.low_conf_inflight = false;
        history->ps_save.low_conf_inflight = false;
#endif

        bool vtage_high = vtage_pred.second == 7;
        bool ps_high = history->ps_save.pred.second == 7 && !history->ps_save.low_conf_inflight;
        bool fcm_high = history->fcm_save.pred.second == 7 && !history->fcm_save.low_conf_inflight;
        //Arbitration if needed
        if(vtage_high && ps_high && fcm_high) {
                if(history->ps_save.pred.first != history->fcm_save.pred.first &&
                                history->ps_save.pred.first != vtage_pred.first &&
                                history->fcm_save.pred.first != vtage_pred.first) {
                        //All predictors confident and disagree, no prediction
                        history->who = VPSave::None;
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                        return prediction_t(Prediction(),0);
                } else if((history->ps_save.pred.first != history->fcm_save.pred.first && history->ps_save.pred.first == vtage_pred.first)  ||
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->ps_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
//                        history->who = VPSave::PS_VTAGE;
//                        return vtage_pred;
                (history->ps_save.pred.first == history->fcm_save.pred.first && history->ps_save.pred.first != vtage_pred.first)  ||
//                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        history->who = VPSave::FCM_PS;
//                        return history->fcm_save.pred;
                (vtage_pred.first == history->fcm_save.pred.first && history->ps_save.pred.first != history->fcm_save.pred.first)) {
//                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->fcm_save.pred.first, history->ps_save, history->ps_save.way_vht);
//                        history->who = VPSave::FCM_VTAGE;
//                        return vtage_pred;
                        history->who = VPSave::None;
                        if(!history->fcm_save.mismatch) {
                                this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                        }
                        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        }
                        return prediction_t(Prediction(), 0);
                } else {
                        //Everybody is equal
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                        history->who = VPSave::All;
                        return vtage_pred;
                }
        } else if(ps_high && fcm_high) {
                if(history->ps_save.pred.first != history->fcm_save.pred.first) {
                        return chooseBetweenFCMAndPS(history);
                } else {
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                        history->who = VPSave::FCM_PS;
                        return history->fcm_save.pred;

                }
        } else if(fcm_high && vtage_high) {
                if(vtage_pred.first != history->fcm_save.pred.first) {
                        return chooseBetweenFCMAndVTage(history, vtage_pred);
                } else {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->fcm_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        history->who = VPSave::FCM_VTAGE;
                        return vtage_pred;
                }
        } else if(ps_high && vtage_high) {
                if(vtage_pred.first != history->ps_save.pred.first) {
                        return chooseBetweenPSAndVTage(history, vtage_pred);
                } else {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->ps_save.pred.first, history->fcm_save, history->fcm_save.way);
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        history->who = VPSave::PS_VTAGE;
                        return vtage_pred;
                }
        } else if(ps_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->ps_save.pred.first, history->fcm_save, history->fcm_save.way);
                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                history->ps_save.spec_update = true;
                made_by_ps++;
                spec_update_ps++;
                history->who = VPSave::PS;


                return history->ps_save.pred;


        } else if(fcm_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->fcm_save.pred.first, history->ps_save, history->ps_save.way_vht);
                history->fcm_save.spec_update = true;
                made_by_fcm++;
                spec_update_fcm++;
                history->who = VPSave::FCM;


                return history->fcm_save.pred;


        } else if(vtage_high) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
                this->ps_fetch_VHT[history->ps_save.index_vht].update(vtage_pred.first, history->ps_save, history->ps_save.way_vht);
                //history->vtage_save->spec_update = true;
                made_by_vtage++;
                spec_update_vtage++;
                history->who = VPSave::VTAGE;
                return vtage_pred;
        } else {
                history->who = VPSave::None;
                if(!history->fcm_save.mismatch) {
                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(pred_fcm.first, history->fcm_save, history->fcm_save.way);
                }
                if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                }
                return prediction_t(Prediction(), 0);
        }

}

prediction_t
VTAGE_FCM_PS::chooseBetweenFCMAndVTage(VPSave *history, prediction_t &vtage_pred)
{
        //                bool fcm = false;
        //                if(arbiter[FCM_VTAGE] >= 0) {
        //                        fcm = true;
        //                } else {
        //                        fcm = false;
        //                }
        //
        //                if(fcm) {
        //                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
        //                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->fcm_save.pred.first, history->ps_save, history->ps_save.way_vht);
        //
        //                        history->fcm_save.spec_update = true;
        //
        //                        history->who = VPSave::FCM;
        //                        made_by_fcm++;
        //                        spec_update_fcm++;
        //
        //
        //                        return history->fcm_save.pred;
        //
        //                } else {
        //                        this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
        //                        this->ps_fetch_VHT[history->ps_save.index_vht].update(vtage_pred.first, history->ps_save, history->ps_save.way_vht);
        //
        //                        history->vtage_save->spec_update = true;
        //
        //                        history->who = VPSave::VTAGE;
        //                        made_by_vtage++;
        //                        spec_update_vtage++;
        //
        //                        return vtage_pred;
        //
        //                }
        history->who = VPSave::None;
        if(!history->fcm_save.mismatch) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
        }
        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
        }
        return prediction_t(Prediction(), 0);
}

prediction_t
VTAGE_FCM_PS::chooseBetweenFCMAndPS(VPSave *history)
{
//	bool fcm = false;
//	if(arbiter[FCM_PS] >= 0) {
//		fcm = true;
//	} else {
//		fcm = false;
//	}
//
//	if(fcm) {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->ps_fetch_VHT[history->ps_save.index_vht].update(history->fcm_save.pred.first, history->ps_save, history->ps_save.way_vht);
//
//		history->fcm_save.spec_update = true;
//
//		history->who = VPSave::FCM;
//		made_by_fcm++;
//		spec_update_fcm++;
//
//
//		return history->fcm_save.pred;
//
//	} else {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->ps_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
//
//		history->ps_save.spec_update = true;
//
//		history->who = VPSave::PS;
//		made_by_ps++;
//		spec_update_ps++;
//
//
//		return history->ps_save.pred;
//
//
//	}
        history->who = VPSave::None;
        if(!history->fcm_save.mismatch) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
        }
        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
        }
        return prediction_t(Prediction(), 0);
}
prediction_t
VTAGE_FCM_PS::chooseBetweenPSAndVTage(VPSave *history, prediction_t &vtage_pred)
{
//	bool ps = false;
//	if(arbiter[PS_VTAGE] >= 0) {
//		ps = true;
//	} else {
//		ps = false;
//	}
//
//	if(ps) {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->ps_save.pred.first, history->fcm_save, history->fcm_save.way);
//		this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
//
//		history->ps_save.spec_update = true;
//
//		history->who = VPSave::PS;
//		made_by_ps++;
//		spec_update_ps++;
//
//
//		return history->ps_save.pred;
//
//	} else {
//		this->fcm_fetch_HT[history->fcm_save.ht_index].update(vtage_pred.first, history->fcm_save, history->fcm_save.way);
//		this->ps_fetch_VHT[history->ps_save.index_vht].update(vtage_pred.first, history->ps_save, history->ps_save.way_vht);
//
//		history->vtage_save->spec_update = true;
//
//		history->who = VPSave::VTAGE;
//		made_by_vtage++;
//		spec_update_vtage++;
//
//		return vtage_pred;
//
//	}
        history->who = VPSave::None;
        if(!history->fcm_save.mismatch) {
                this->fcm_fetch_HT[history->fcm_save.ht_index].update(history->fcm_save.pred.first, history->fcm_save, history->fcm_save.way);
        }
        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
        }
        return prediction_t(Prediction(), 0);
}

void
VTAGE_FCM_PS::updateVHT(uint16_t index, Prediction & value)
{
        if(this->fcm_conf[index].read() == 0) {
                this->fcm_VHT[index] = value;
        }
}

void
VTAGE_FCM_PS::update(Prediction & value, void *vp_history, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);

        if(squashed) {
                assert(history->who != VPSave::None);
                if(history->who == VPSave::FCM) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false, 0,true);
                } else if(history->who == VPSave::PS) {
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL,history->ps_save.tag_vht, false, 0,true);
                } else if(history->who == VPSave::VTAGE) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value, tmp, false, true);
                } else if(history->who == VPSave::FCM_PS) {
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false, 0,true);
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL,history->ps_save.tag_vht, false, 0,true);
                } else if(history->who == VPSave::FCM_VTAGE) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value, tmp, false, true);
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false,0, true);
                } else if(history->who == VPSave::PS_VTAGE) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value, tmp, false, true);
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL,history->ps_save.tag_vht, false, 0,true);
                } else if(history->who == VPSave::All) {
                        void * tmp = static_cast<void*>((history->vtage_save));
                        this->tage->update(value, tmp, false, true);
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL,history->ps_save.tag_vht, false,0, true);
                        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL,history->fcm_save.tag, false, 0,true);
                }
                return;
        }

        prediction_t &pred_vtage = history->vtage_save->tagePred;
        prediction_t &fcm_pred = history->fcm_save.pred;
        prediction_t &ps_pred = history->ps_save.pred;

        unsigned commit_way_vht = ps_commit_VHT[history->ps_save.index_vht].tagLookup(history->ps_save.tag_vht);
        unsigned commit_way_sht = ps_SHT[history->ps_save.index_sht].tagLookup(history->ps_save.tag_sht);
        unsigned commit_way_fcm = fcm_commit_HT[history->fcm_save.ht_index].tagLookup(history->fcm_save.tag);

        Addr vht_index = this->fcm_commit_HT[history->fcm_save.ht_index].getIndex(commit_way_fcm, FCM_VHT_INDEX,  history->fcm_save.ip);

#ifdef NOREAD_AT_COMMIT
        bool outcome_fcm = fcm_pred.first == value;
        bool outcome_ps = ps_pred.first == value;
#else
        bool outcome_fcm = !history->fcm_save.low_conf_inflight && !history->fcm_save.mismatch && history->fcm_save.pred.first == value && history->fcm_save.pred.second == 7;
        bool outcome_ps = !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && history->ps_save.pred.first == value && history->ps_save.pred.second == 7;
#endif


        bool outcome_vtage = pred_vtage.first == value && pred_vtage.second == 7;

        correct_fcm += outcome_fcm && history->who == VPSave::FCM;
        incorrect_fcm += !outcome_fcm && history->who == VPSave::FCM && history->fcm_save.pred.second == 7;

        correct_ps += outcome_ps && history->who == VPSave::PS;
        incorrect_ps += !outcome_ps && history->who == VPSave::PS  && history->ps_save.pred.second == 7;

        correct_vtage += outcome_vtage && history->who == VPSave::VTAGE;
        incorrect_vtage += !outcome_vtage && history->who == VPSave::VTAGE && pred_vtage.second == 7;

        correct_fcm_ps += outcome_fcm && outcome_ps && history->who == VPSave::FCM_PS;
        incorrect_fcm_ps += !outcome_fcm && !outcome_ps && history->who == VPSave::FCM_PS  && history->ps_save.pred.second == 7 && history->fcm_save.pred.second == 7;

        correct_fcm_vtage += outcome_fcm && outcome_vtage && history->who == VPSave::FCM_VTAGE;
        incorrect_fcm_vtage += !outcome_fcm && !outcome_vtage && history->who == VPSave::FCM_VTAGE && pred_vtage.second == 7 && history->fcm_save.pred.second == 7;

        correct_ps_vtage += outcome_vtage && outcome_ps && history->who == VPSave::PS_VTAGE;
        incorrect_ps_vtage += !outcome_vtage && !outcome_ps && history->who == VPSave::PS_VTAGE && pred_vtage.second == 7  && history->ps_save.pred.second == 7;

        correct_all += outcome_vtage && outcome_ps && outcome_fcm && history->who == VPSave::All;
        incorrect_all += !outcome_vtage && !outcome_ps && !outcome_fcm && history->who == VPSave::All && pred_vtage.second == 7  && history->ps_save.pred.second == 7 && history->fcm_save.pred.second == 7;


        outcome_fcm = commit_way_fcm != assoc_fcm && fcm_VHT[vht_index] == value;
        outcome_ps =  commit_way_sht != assoc_ps_sht && commit_way_vht != assoc_ps_vht &&
                        ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht) + ps_SHT[history->ps_save.index_sht].getStride(commit_way_sht) == value;
        outcome_vtage = pred_vtage.first == value;

        if(outcome_fcm && !outcome_ps && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7 && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7) {
                arbiter[FCM_PS] < 15 ? arbiter[FCM_PS]++ : 0;
        } else if(!outcome_fcm && outcome_ps &&  this->fcm_conf[history->fcm_save.vht_index].read() == 7 && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7 && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7) {
                arbiter[FCM_PS] > -16 ? arbiter[FCM_PS]-- : 0;
        }

        if(outcome_fcm && !outcome_vtage && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && pred_vtage.second == 7) {
                arbiter[FCM_VTAGE] < 15 ? arbiter[FCM_VTAGE]++ : 0;
        } else if(!outcome_fcm && outcome_vtage &&  this->fcm_conf[history->fcm_save.vht_index].read() == 7 && this->fcm_commit_HT[history->fcm_save.ht_index].getConfidence(commit_way_fcm) == 7 && pred_vtage.second == 7) {
                arbiter[FCM_VTAGE] > -16 ? arbiter[FCM_VTAGE]-- : 0;
        }

        if(outcome_ps && !outcome_vtage  && pred_vtage.second == 7 && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7) {
                arbiter[PS_VTAGE] < 15 ? arbiter[PS_VTAGE]++ : 0;
        } else if(!outcome_ps && outcome_vtage &&  pred_vtage.second == 7 && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7) {
                arbiter[PS_VTAGE] > -16 ? arbiter[PS_VTAGE]-- : 0;
        }


        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
        }
        if(!history->ps_save.mismatch_vht && !history->ps_save.mismatch_sht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                ps_low_conf_inflight[history->ps_save.index_vht]--;
                assert(ps_low_conf_inflight[history->ps_save.index_vht] >= 0);
        }

        if(!history->fcm_save.mismatch) {
                fcm_inflight[history->fcm_save.ht_index]--;
                assert(fcm_inflight[history->fcm_save.ht_index] >= 0);
        }


        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                ps_inflight[history->ps_save.index_vht]--;
                assert(ps_inflight[history->ps_save.index_vht] >= 0);
        }


        if(commit_way_fcm != assoc_fcm) {
                this->updateVHT(vht_index, value);
                if(outcome_fcm) {
                        this->fcm_conf[vht_index].increment();
                } else {
                        this->fcm_conf[vht_index].decrement();
                }
                //this->fcm_conf[vht_index].updateConf(outcome_fcm);
        }


        this->fcm_commit_HT[history->fcm_save.ht_index].update(value, NULL, history->fcm_save.tag, outcome_fcm, fcm_pred.second);

        Prediction stride;
        if(commit_way_vht != assoc_ps_vht) {
#ifdef NOREAD_AT_COMMIT
                        stride = value - history->ps_save.previous;
#else
                stride = (value - this->ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht));
#endif
        } else {
                stride = Prediction();
        }

        this->ps_SHT[history->ps_save.index_sht].update(stride, NULL, history->ps_save.tag_sht, outcome_ps, ps_pred.second);

        if(commit_way_fcm != assoc_fcm && fcm_inflight[history->fcm_save.ht_index] == 0) {
                DPRINTF(ValuePredictor, "Tag mismatch or not other instances in flight: setting speculative history to committed history\n");
                this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(commit_way_fcm), commit_way_fcm);
        }

        this->ps_commit_VHT[history->ps_save.index_vht].update(value, NULL, history->ps_save.tag_vht, outcome_ps, ps_pred.second);

        if(commit_way_vht != assoc_ps_vht && ps_inflight[history->ps_save.index_vht] == 0) {
                Prediction restore = this->ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht);
                this->ps_fetch_VHT[history->ps_save.index_vht].restore(restore, commit_way_vht);
        }

        bool outcome = ((history->who == VPSave::VTAGE || history->who == VPSave::FCM_VTAGE || history->who == VPSave::PS_VTAGE || history->who == VPSave::All) && outcome_vtage)
                        || ((history->who == VPSave::PS || history->who == VPSave::FCM_PS) && outcome_ps)
                        || (history->who == VPSave::FCM && outcome_fcm);

        updateStats(*history, outcome);

        void * vtage_hist = static_cast<void*>(history->vtage_save);
        this->tage->update(value,vtage_hist,  false, false);


        delete history;
}

void
VTAGE_FCM_PS::squash(void *vp_history, bool remove, bool recompute) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        void * vtage_hist = static_cast<void*>((history->vtage_save));

        DPRINTF(ValuePredictor, "Squashing entry %u in FCM\n", history->fcm_save.ht_index);

        if(history->isBranch) {
                this->tage->squash(vtage_hist, remove, recompute);
        } else {
                this->tage->squash(vtage_hist, remove, recompute);

                if(remove) {

                        if(!history->fcm_save.mismatch && (history->fcm_save.pred.second < 7 || history->fcm_save.low_conf_inflight)) {
                                fcm_low_conf_inflight[history->fcm_save.ht_index]--;
                                assert(fcm_low_conf_inflight[history->fcm_save.ht_index] >= 0);
                        }
                        if(!history->ps_save.mismatch_vht && !history->ps_save.mismatch_sht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                                ps_low_conf_inflight[history->ps_save.index_vht]--;
                                assert(ps_low_conf_inflight[history->ps_save.index_vht] >= 0);
                        }

                        if(!history->fcm_save.mismatch) {
                                fcm_inflight[history->fcm_save.ht_index]--;
                        }

                        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                                ps_inflight[history->ps_save.index_vht]--;
                        }

                }


                if(!history->fcm_save.mismatch && fcm_inflight[history->fcm_save.ht_index] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch history\n");
                        this->fcm_fetch_HT[history->fcm_save.ht_index].restore(history->fcm_save.signatures, history->fcm_save.way);
                } else if(!history->fcm_save.mismatch) {
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        this->fcm_fetch_HT[history->fcm_save.ht_index].restore(this->fcm_commit_HT[history->fcm_save.ht_index].getSignatures(history->fcm_save.way), history->fcm_save.way);
                }

                if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && ps_inflight[history->ps_save.index_vht] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                        this->ps_fetch_VHT[history->ps_save.index_vht].restore(history->ps_save.previous, history->ps_save.way_vht);
                } else if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        Prediction tmp = this->ps_commit_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht);
                        this->ps_fetch_VHT[history->ps_save.index_vht].restore(tmp, history->ps_save.way_vht);
                }
        }

        if(remove) {
                delete history;
        }



}



void
VTAGE_FCM_PS::updateFoldedHist(bool save, void *&vp_history) {

        VPSave *history = new VPSave();
        vp_history = static_cast<void*>(history);

        void *branch = NULL;
        tage->updateFoldedHist(save, branch);
        VTageVP::VPSave * tmp = static_cast<VTageVP::VPSave*>(branch);
        history->isBranch = true;
        history->vtage_save = tmp;
        //delete tmp;
}

void
VTAGE_FCM_PS::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        void *branch = static_cast<void*>((history->vtage_save));
        tage->flush_branch(branch);
        delete history;
}

void
VTAGE_FCM_PS::regStats()
{
                tage->regStats();
        made_by_ps
                .name(name() + ".madeByPS")
                .desc("Number of predictions made by PS")
                ;

        made_by_fcm
        .name(name() + ".madeByFCM")
        .desc("Number of predictions made by FCM")
        ;

        made_by_vtage
                .name(name() + ".madeByVTAGE")
                .desc("Number of predictions made by VTAGE")
                ;

        spec_update_ps
                .name(name() + ".specUpdateStride")
                .desc("Number of time Stride updated FCM")
                ;

        spec_update_fcm
                .name(name() + ".specUpdateFCM")
                .desc("Number of time FCM updated Stride")
                ;

        spec_update_vtage
                        .name(name() + ".specUpdateVTAGE")
                        .desc("Number of time VTAGE updated the others")
                        ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_fcm
        .name(name() + ".correctFCM")
        .desc("Number of correct predictions coming from FCM")
        ;

        correct_ps
        .name(name() + ".correctPS")
        .desc("Number of correct predictions coming from PS")
        ;

        correct_vtage
        .name(name() + ".correctVTAGE")
        .desc("Number of correct predictions coming from Vtage")
        ;


        incorrect_fcm
        .name(name() + ".incorrectFCM")
        .desc("Number of correct predictions coming from FCM")
        ;

        incorrect_vtage
        .name(name() + ".incorrectVTAGE")
        .desc("Number of correct predictions coming from VTAGE")
        ;

        incorrect_ps
        .name(name() + ".incorrectPS")
        .desc("Number of correct predictions coming from PS")
        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        correct_fcm_vtage
        .name(name() + ".correctFCM_VTAGE")
        .desc("Number of correct predictions coming from FCM and Vtage")
        ;

        correct_ps_vtage
        .name(name() + ".correctPS_VTAGE")
        .desc("Number of correct predictions coming from PS and Vtage")
        ;

        correct_fcm_ps
        .name(name() + ".correctFCM_PS")
        .desc("Number of correct predictions coming from fcm and ps")
        ;

        correct_all
        .name(name() + ".correctALL")
        .desc("Number of correct predictions coming from all components")
        ;


        incorrect_fcm_vtage
        .name(name() + ".incorrectFCM_VTAGE")
        .desc("Number of incorrect predictions coming from FCM and Vtage")
        ;

        incorrect_ps_vtage
        .name(name() + ".incorrectPS_VTAGE")
        .desc("Number of incorrect predictions coming from stride and Vtage")
        ;

        incorrect_fcm_ps
        .name(name() + ".incorrectFCM_PS")
        .desc("Number of incorrect predictions coming from fcm and ps")
        ;

        incorrect_all
        .name(name() + ".incorrectALL")
        .desc("Number of incorrect predictions coming from all components")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        ignoredCorrect
        .name(name() + ".ignoredCorrect")
        .desc("Percentage of correct predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ignoredIncorrect
        .name(name() + ".ignoredIncorrect")
        .desc("Percentage of incorrect predictions ignored because of low conf inflight")
        .precision(5)
        ;

        ign_correct
        .name(name() + ".ignCorrect")
        .desc("Number of correct predictions ignored because low conf inflight")
        ;

        ign_incorrect
        .name(name() + ".ignInorrect")
        .desc("Number of incorrect predictions ignored because low conf inflight")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
               ignoredIncorrect = ign_incorrect / attempted;
}

void
VTAGE_FCM_PS::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         prediction_t &vtage_pred =  history.vtage_save->tagePred;

         ign_correct +=  proceeded && outcome;
         ign_incorrect += proceeded && !outcome;
         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/

         bool highConf = false, transient = false, low_conf = false;

         if(history.who == VPSave::PS && history.ps_save.pred.second == 7) {
                 highConf = true;
         } else if(history.who == VPSave::FCM && history.fcm_save.pred.second == 7) {
                 highConf = true;
         } else if(history.who == VPSave::VTAGE && vtage_pred.second == 7) {
                 highConf = true;
         } else {
                 highConf = false;
         }

         if(!highConf) {
                 if(history.who == VPSave::PS && history.ps_save.pred.second >= 1) {
                         transient = true;
                 } else if(history.who == VPSave::FCM && history.fcm_save.pred.second >= 1) {
                         transient = true;
                 } else if(history.who == VPSave::VTAGE && vtage_pred.second >= 1) {
                         transient = true;
                 } else {
                         transient = false;
                 }
         }

         if(!transient) {
                 if(history.who == VPSave::PS && history.ps_save.pred.second == 0) {
                         low_conf = true;
                 } else if(history.who == VPSave::FCM && history.fcm_save.pred.second == 0) {
                         low_conf = true;
                 } else if(history.who == VPSave::VTAGE && vtage_pred.second == 0) {
                         low_conf = true;
                 } else {
                         low_conf = false;
                 }
         }

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         this->lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}






