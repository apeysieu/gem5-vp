#ifndef _CPU_PSVP_HH
#define _CPU_PSVP_HH


#include <vector>

#include "base/statistics.hh"
#include "base/trace.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"
#include "cpu/vpred/associative_array.hh"
#include "debug/ValuePredictor.hh"

/**
* Class implementing a finite PS predictor.
**/
class PSVP
{

public:

        struct VPSave {
                Addr index_vht;
                Addr index_sht;
                Addr tag_vht;
                Addr tag_sht;
                prediction_t pred;
                Prediction previous;
                bool mismatch_sht, mismatch_vht, low_conf_inflight;
                Addr ip, microPC;
                unsigned way_sht, way_vht;
                bool spec_update;
        };

        class SHTEntry
        {
        private:
                Addr _tag;
                Prediction _stride1;
                Prediction _stride2;
                ConfCounter _conf;

        public:
                static std::string name;

                SHTEntry() {}

                /**
                 * @param[counterwidth] Width of the confidence counter in bits.
                 * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
                 */
                SHTEntry(unsigned counterwidth, std::vector<unsigned> &proba) {
                        _tag = 0;
                        _stride1 = Prediction();
                        _stride2 = Prediction();
                        _conf = ConfCounter(counterwidth, 0, &proba);
                }

                Prediction getStride() const
                {
                        return this->_stride1;
                }

                bool tagMatches(Addr tag) const
                {
                        return this->_tag == tag;
                }

                 unsigned getConfidence() const {
                         return _conf.read();
                 }


                 /**
                  * @param[value] The actual result of the instruction.
                  * @param[dummy] Reserved.
                  * @param[tag] The new tag.
                  * @param[outcome] True if the predictor predicted correctly, false otherwise.
                  * @param[conf] The confidence seen at fetch (may differ from the confidence currently in the entry).
                  * @param[squashed] True if this is an update on squash, fale is this is an update at retire.
                  */
                 void update(Prediction &new_stride, Prediction *dummy, Addr tag, bool correct, unsigned conf, bool squashed = false)
                 {
                         if(!tagMatches(tag)) {
                                 this->_tag = tag;
                                 this->_stride1 = Prediction();
                                 this->_stride2 = Prediction();
                         }
                         else {
                                 if(!squashed) {
                                         if(new_stride == this->_stride2) {
                                                 this->_stride1 = this->_stride2;
                                         }
                                         this->_stride2 = new_stride;
                                 }
#ifdef NOREAD_AT_COMMIT
                                 _conf.set(conf);
#endif
                                 this->_conf.updateConf(correct);
                         }
                 }


        };

        /**
         * Class implementing an entry of the PS predictor.
         **/
        class VHTEntry
        {
        private:
                Prediction _value;
                Addr _tag;

        public:

                ConfCounter _confidence;

                VHTEntry()
                {
                }

                /**
                 * @param[counterwidth] Width of the confidence counter in bits.
                 * @param[proba] Vector containing the 2^counterbits probabilities controlling the forward transitions of the counter.
                 */
                VHTEntry(unsigned counterwidth, std::vector<unsigned> &proba) {
                        _value = Prediction();
                        _tag = 0;
                        _confidence = ConfCounter(counterwidth, 0, &proba);
                }

                bool tagMatches(Addr tag) const
                {
                        return this->_tag == tag;
                }


                Prediction getPred() const
                {
                        return this->_value;
                }

                /**
                 * @param[value] The speculative value to update the entry with.
                 * @param[save] Structure to save the "old" last value.
                 */
                void update(Prediction & value, VPSave &save) {
                        save.previous = this->_value;
                        this->_value = value;
                }

                /**
                 * @param[value] Restore a previously saved last value.
                 */
                void restore(Prediction & value) {
                        this->_value = value;
                }

                /**
                 * @param[value] The actual result of the instruction.
                 * @param[dummy] Reserved.
                 * @param[tag] The new tag.
                 * @param[outcome] True if the predictor predicted correctly, false otherwise.
                 * @param[conf] The confidence seen at fetch (may differ from the confidence currently in the entry).
                 * @param[squashed] True if this is an update on squash, false is this is an update at retire.
                 */
                void update(Prediction & value, Prediction *dummy, Addr tag, bool correct, unsigned conf, bool squashed = false)
                {
                        if(tagMatches(tag)) {
                                _confidence.updateConf(correct);
                                if(!squashed) {
                                        this->_value = value;
                                }
                        } else {
                                if(!squashed) {
                                        this->_tag = tag;
                                        this->_value = value;
                                }
                                this->_confidence.set(0);
                        }
                }

                unsigned getConfidence() const
                {
                        return this->_confidence.read();
                }

        };

        /** PRIVATE MEMBERS **/

        AssociativeArray<PSVP, PSVP::VHTEntry> fetch_VHT;
        AssociativeArray<PSVP, PSVP::VHTEntry> commit_VHT;

        AssociativeArray<PSVP, PSVP::SHTEntry> _SHT;
        std::vector<int> _low_conf_inflight, inflight;

        std::vector<unsigned> proba;

        std::deque<TageBP::Hist> *bhist;

        unsigned BHIST_LENGTH, TAG_SIZE_VHT, TAG_SIZE_SHT;
        unsigned assoc_vht, assoc_sht;

        std::string _name;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct;
        Stats::Scalar incorrect;

        Stats::Formula accuracy;
        Stats::Formula coverage;
        Stats::Scalar ignoredLowConfInflight;

        Stats::Formula ignoredCorrect, ignoredIncorrect;
        Stats::Scalar ign_correct, ign_incorrect;
        Stats::Scalar commit_correct, commit_incorrect;
        Stats::Formula accuracy_ideal, coverage_ideal;

        Stats::Scalar miss_sht, miss_vht, miss_all;
        Stats::Formula missRateSHT, missRateVHT, missRateAll;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;

        Stats::Scalar accessVHT, accessSHT;

        /*
         * @return the PC hashed with branch history bits to index the SHT at fetch
         */
        Addr hashPC(Addr ip);

        /*
         * @return the PC hashed with branch history bits to index the SHT at commit
         */
        Addr hashPCNonSpec(Addr ip);

        /**
         * @param[name] Name of the class (will appear in stats).
         * @param[size_VHT] Size of the VHT in entries.
         * @param[tag_vht] In bits.
         * @param[size_SHT] Size of the SHT in entries.
         * @param[tag_Sht] In bits.
         * @param[bhist_width] Number of branc history bits to use in the hash function.
         * @param[counterWidth] In bits.
         * @param[proba] Probability vector used to control forward transition of confidence counters.
         * @param[associativity_vht] Associativity of the VHT.
         * @param[associativity_sht] Associativity of the SHT.
         * @param[nonspec] Iterator to the global branch history (first is newer).
         * @param[bhist] Pointer to the global branch history.
         */
        PSVP(std::string &name, unsigned size_VHT, unsigned tag_vht, unsigned size_SHT, unsigned tag_sht,
                        unsigned bhist_width, unsigned counterwidth, std::vector<unsigned> &probability, unsigned associativity_vht, unsigned associativity_sht,
                        std::deque<TageBP::Hist> *bhist);
        ~PSVP();

        /**
         * @param[ip] Address of the instruction.
         * @param[micropc] uop index.
         * @param[vp_history] Pointer to modify.
         * @post vp_history points to the data structure of LVP describing the current prediction.
         * @return prediction of inst (ip,micropc)
         */
        prediction_t lookup(Addr ip, MicroPC micropc, void *&vp_history);

        /**
         * @param[value] Actual result of the instruction (dummy if update on squash).
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[squash] True if update on squash, false if update at retire.
         */
        void update(Prediction & value, void *vp_history, bool squashed);


        /**
         * @param[vp_history] Pointer to the data structure describing the prediction. Must be cast.
         * @param[remove] True if the data structure should be deleted, false otherwise.
         */
        void squash(void *vp_history, bool remove);

        const std::string &name() const { return _name; }

        /**
         * @pre Stats are registered with the gem5 infrastructure.
         */
        void regStats();

        /**
         * @param[history] Structure describing the prediction.
         * @param[outcome] True if the prediction was correct, false otherwise.
         */
        void updateStats(VPSave &history, bool outcome);
};

#endif
