/*
 * vtage_stride.cc
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */



#include "cpu/vpred/vtage_stride.hh"

#include "cpu/o3/helper.hh"

VTAGE_Stride::VTAGE_Stride(std::string &name,
                unsigned size_stride_vht,
                unsigned tag_stride_vht_size,
                unsigned assoc_stride,
                std::deque<TageBP::Hist> *bhist,
                unsigned numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned baseHystShift,
                unsigned instShiftAmt,
                unsigned *phist,
                unsigned counterwidth,
                std::vector<unsigned> &proba_vtage,
                std::vector<unsigned> &proba_stride)
{

       this->_name = name + ".VPredUnit.VTAGE_Stride";
       this->assoc_stride = assoc_stride;

       STRIDE_VHT_INDEX = log2(size_stride_vht);
       STRIDE_TAG_VHT = tag_stride_vht_size;

       stride_proba = proba_stride;

       this->stride_fetch_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_stride_vht, this->assoc_stride, counterwidth, stride_proba);
       this->stride_commit_VHT = AssociativeArray<StrideVP, StrideVP::Entry>(size_stride_vht, this->assoc_stride, counterwidth, stride_proba);



       stride_low_conf_inflight.resize(size_stride_vht);
       stride_inflight.resize(size_stride_vht);


#ifdef TOURNAMENT_VPRED
       arbiter.resize(size_stride_vht, 0 );
#else
       arbiter = 0;
#endif

       tage = new VTageVP(name,
                   numHistComponents,
                   numLogBaseEntry,
                   minHistSize,
                   maxHistSize,
                   baseHystShift,
                   counterwidth,
                   instShiftAmt,
                   proba_vtage,
                   bhist,
                   phist);

}

prediction_t
VTAGE_Stride::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{

        Addr base_upc = createUPCindex(micropc, (Addr)log2(this->stride_commit_VHT.size()));

        Addr stride_vht_index = (base_upc ^ ip) & (this->stride_commit_VHT.size() - 1);
        Addr stride_vht_tag = (ip >> (unsigned) log2(this->stride_commit_VHT.size())) & (((Addr) 1 << STRIDE_TAG_VHT) - 1);



        VPSave *history = new VPSave();

        vp_history = static_cast<void *>(history);

        //Lookup will allocate save.
        void *save = NULL;
        prediction_t vtage_pred = tage->lookup(ip, micropc, save);
        VTageVP::VPSave* vtage_save = static_cast<VTageVP::VPSave*>(save);


        history->isBranch = false;

        history->stride_save.low_conf_inflight = false;


        history->stride_save.index_vht = stride_vht_index;
        history->stride_save.tag_vht = stride_vht_tag;

        history->stride_save.way = stride_commit_VHT[stride_vht_index].tagLookup(stride_vht_tag);
        history->stride_save.mismatch = history->stride_save.way == assoc_stride;

        prediction_t pred_stride = prediction_t(Prediction(),0);
        if(stride_inflight[history->stride_save.index_vht] == 0 && ! history->stride_save.mismatch) {
                pred_stride = prediction_t(this->stride_commit_VHT[history->stride_save.index_vht].getPred(history->stride_save.way) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(history->stride_save.way),this->stride_commit_VHT[history->stride_save.index_vht].getConfidence(history->stride_save.way));
        } else if(! history->stride_save.mismatch) {
                pred_stride = prediction_t(this->stride_fetch_VHT[history->stride_save.index_vht].getPred(history->stride_save.way) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(history->stride_save.way),this->stride_commit_VHT[history->stride_save.index_vht].getConfidence(history->stride_save.way));
        }

        history->stride_save.pred = (history->stride_save.mismatch) ? prediction_t(Prediction(),0) : pred_stride;

        if(!history->stride_save.mismatch && stride_low_conf_inflight[history->stride_save.index_vht] > 0) {
                history->stride_save.low_conf_inflight = true;
        }
        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                stride_low_conf_inflight[history->stride_save.index_vht]++;
        }
        if(!history->stride_save.mismatch) {
                stride_inflight[history->stride_save.index_vht]++;
        }

        //Arbitration if needed
        history->vtage_save = vtage_save;
        //history->vtage_save->spec_update = false;

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->stride_save.low_conf_inflight = false;
#endif
        //delete vtage_save;

        //bool vtage = false;
        bool vtage_high = vtage_pred.second == 7;
        bool stride_high = history->stride_save.pred.second == 7 && !history->stride_save.low_conf_inflight;
        if(vtage_high && stride_high) {
                if(history->stride_save.pred.first == vtage_pred.first) {
                        history->who = VPSave::All;
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(vtage_pred.first, history->stride_save, history->stride_save.way);
                        return pred_stride;
                } else {
                                //Both high conf? Don't predict
                        if(!history->stride_save.mismatch) {
                                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                        }

                        history->who = VPSave::None;

                        return prediction_t(Prediction(),0);
                }
        } else if(stride_high) {

                this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);


                history->who = VPSave::STRIDE;
                made_by_stride++;



                return pred_stride;


        } else if(vtage_high) {

                if(!history->stride_save.mismatch) {
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(vtage_pred.first, history->stride_save, history->stride_save.way);
                }

                //history->vtage_save->spec_update = true;

                history->who = VPSave::VTAGE;
                made_by_vtage++;
                spec_update_vtage++;


                return vtage_pred;
        } else {
                //Nobody is confident enough.
                if(!history->stride_save.mismatch) {
                        this->stride_fetch_VHT[history->stride_save.index_vht].update(history->stride_save.pred.first, history->stride_save, history->stride_save.way);
                }

                history->who = VPSave::None;

                return prediction_t(Prediction(),0);
        }

}


void
VTAGE_Stride::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        void *branch = static_cast<void*>(history->vtage_save);
        tage->flush_branch(branch);
        delete history;
}

void
VTAGE_Stride::updateFoldedHist(bool save, void *&vp_history) {

        VPSave *history = new VPSave();
        vp_history = static_cast<void*>(history);


        void *branch = NULL;
        tage->updateFoldedHist(save, branch);
        VTageVP::VPSave * tmp = static_cast<VTageVP::VPSave*>(branch);
        history->isBranch = true;
        history->vtage_save = tmp;
        //delete tmp;
}

void
VTAGE_Stride::update(Prediction & value, void *vp_history, bool mispred, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);
        unsigned commit_way_stride = this->stride_commit_VHT[history->stride_save.index_vht].tagLookup(history->stride_save.tag_vht);

        if(squashed) {
                if(history->who == VPSave::VTAGE) {
                        void * tmp = static_cast<void*>(history->vtage_save);
                        this->tage->update(value,tmp, false, true);
                } else if(history->who == VPSave::STRIDE) {
                        this->stride_commit_VHT[history->stride_save.index_vht].update(value, NULL, history->stride_save.tag_vht, false, 0, true);
                } else if(history->who == VPSave::All) {
                        void * tmp = static_cast<void*>(history->vtage_save);
                        this->tage->update(value,tmp, false, true);
                        this->stride_commit_VHT[history->stride_save.index_vht].update(value, NULL, history->stride_save.tag_vht, false, 0, true);
                }
                return;
        }

        prediction_t &vtage_pred = history->vtage_save->tagePred;
        prediction_t &stride_pred = history->stride_save.pred;

        bool outcome_vtage = vtage_pred.first == value && vtage_pred.second == 7;
        correct_vtage += outcome_vtage && history->who == VPSave::VTAGE;
        incorrect_vtage += !outcome_vtage && history->who == VPSave::VTAGE && vtage_pred.second == 7;

        bool outcome_stride = !history->stride_save.mismatch && history->stride_save.pred.first == value && history->stride_save.pred.second == 7;
        correct_stride += outcome_stride && history->who == VPSave::STRIDE;
        incorrect_stride += !history->stride_save.mismatch && !outcome_stride && history->who == VPSave::STRIDE && history->stride_save.pred.second == 7;

        correct_all += outcome_stride && outcome_vtage && history->who == VPSave::All;
        incorrect_all += !outcome_stride && !outcome_vtage && history->who == VPSave::All && history->stride_save.pred.second == 7 && vtage_pred.second == 7;

#ifdef NOREAD_AT_COMMIT
        outcome_stride = stride_pred.first == value && !mispred;
#else
        outcome_stride = commit_way_stride != assoc_stride && stride_commit_VHT[history->stride_save.index_vht].getPred(commit_way_stride) + this->stride_commit_VHT[history->stride_save.index_vht].getStride(commit_way_stride)== value && (history->who != VPSave::STRIDE || !mispred);// history->stride_save.pred.first == value;
#endif
        outcome_vtage = vtage_pred.first == value  && (history->who != VPSave::VTAGE || !mispred);

        //Updating the arbiter
#ifdef TOURNAMENT_VPRED
        if(outcome_vtage && !outcome_stride && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7 &&  vtage_pred.second == 7) {
                arbiter[history->stride_save.index_vht] < 15 ? arbiter[history->stride_save.index_vht]++ : 0;
        } else if(!outcome_vtage && outcome_stride && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) &&  vtage_pred.second == 7) {
                arbiter[history->stride_save.index_vht] > -16 ? arbiter[history->stride_save.index_vht]-- : 0;
        }
#else
        if(outcome_vtage && !outcome_stride && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) == 7 &&  vtage_pred.second == 7) {
                arbiter < 15 ? arbiter++ : 0;
        } else if(!outcome_vtage && outcome_stride && stride_commit_VHT[history->stride_save.index_vht].getConfidence(commit_way_stride) &&  vtage_pred.second == 7) {
                arbiter > -16 ? arbiter-- : 0;
        }
#endif


        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                stride_low_conf_inflight[history->stride_save.index_vht]--;
                assert(stride_low_conf_inflight[history->stride_save.index_vht] >= 0);
        }
        if(!history->stride_save.mismatch) {
                stride_inflight[history->stride_save.index_vht]--;
                assert(stride_inflight[history->stride_save.index_vht] >= 0);
        }



        this->stride_commit_VHT[history->stride_save.index_vht].update(value, &history->stride_save.previous, history->stride_save.tag_vht, outcome_stride, stride_pred.second);




        if(commit_way_stride != assoc_stride && stride_inflight[history->stride_save.index_vht] == 0) {
                this->stride_fetch_VHT[history->stride_save.index_vht].restore(value, commit_way_stride);
        }

        bool outcome = ((history->who == VPSave::VTAGE || history->who == VPSave::All) && vtage_pred.first == value && !mispred) || (history->who == VPSave::STRIDE && history->stride_save.pred.first == value && !mispred);
        updateStats(*history, outcome);

        void * vtage_hist = static_cast<void*>(history->vtage_save);
        this->tage->update(value, vtage_hist, mispred, false);


        delete history;
}

void
VTAGE_Stride::squash(void *vp_history, bool remove, bool recompute) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        assert(history);


        void * vtage_hist = static_cast<void*>(history->vtage_save);

        if(history->isBranch) {
                DPRINTF(ValuePredictor, "Deleting a branch history\n");
                this->tage->squash(vtage_hist, remove, recompute);
        } else {
                DPRINTF(ValuePredictor, "Deleting a value history\n");
                this->tage->squash(vtage_hist, remove, recompute);


                if(remove) {

                        if(!history->stride_save.mismatch && (history->stride_save.pred.second < 7 || history->stride_save.low_conf_inflight)) {
                                stride_low_conf_inflight[history->stride_save.index_vht]--;
                                assert(stride_low_conf_inflight[history->stride_save.index_vht] >= 0);
                        }

                        if(!history->stride_save.mismatch) {
                                stride_inflight[history->stride_save.index_vht]--;
                        }


                }

                if(!history->stride_save.mismatch && stride_inflight[history->stride_save.index_vht] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                        this->stride_fetch_VHT[history->stride_save.index_vht].restore(history->stride_save.previous, history->stride_save.way);
                } else if(!history->stride_save.mismatch){
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        Prediction tmp = this->stride_commit_VHT[history->stride_save.index_vht].getPred(history->stride_save.way);
                        this->stride_fetch_VHT[history->stride_save.index_vht].restore(tmp, history->stride_save.way);
                }
        }


        if(remove) {
                delete history;
        }

}

void
VTAGE_Stride::regStats()
{

        tage->regStats();

        made_by_stride
        .name(name() + ".madeByStride")
        .desc("Number of predictions made by Stride")
        ;

        made_by_vtage
        .name(name() + ".madeByVTAGE")
        .desc("Number of predictions made by VTAGE")
        ;

        spec_update_vtage
        .name(name() + ".specUpdateVTAGE")
        .desc("Number of time VTAGE updated PS")
        ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_vtage
                .name(name() + ".correctVTAGE")
                .desc("Number of correct predictions coming from VTAGE")
                ;

        correct_stride
                        .name(name() + ".correctStride")
                        .desc("Number of correct predictions coming from Stride")
                        ;

        incorrect_vtage
                        .name(name() + ".incorrectVTAGE")
                        .desc("Number of correct predictions coming from VTAGE")
                        ;

        incorrect_stride
                        .name(name() + ".incorrectPS")
                        .desc("Number of correct predictions coming from PS")
                        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;


        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        correct_all
        .name(name() + ".correctAll")
        .desc("Number of correct predictions coming from All")
        ;

        incorrect_all
        .name(name() + ".incorrectAll")
        .desc("Number of incorrect predictions coming from All")
        ;

        ignoredCorrect
               .name(name() + ".ignoredCorrect")
               .desc("Percentage of correct predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ignoredIncorrect
               .name(name() + ".ignoredIncorrect")
               .desc("Percentage of incorrect predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ign_correct
               .name(name() + ".ignCorrect")
               .desc("Number of correct predictions ignored because low conf inflight")
               ;

               ign_incorrect
                           .name(name() + ".ignInorrect")
                           .desc("Number of incorrect predictions ignored because low conf inflight")
                           ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
               ignoredIncorrect = ign_incorrect / attempted;
}

void
VTAGE_Stride::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         prediction_t &vtage_pred = history.vtage_save->tagePred;

         ign_correct +=  proceeded && outcome && (history.stride_save.low_conf_inflight && history.who == VPSave::STRIDE) ;
         ign_incorrect += proceeded && !outcome &&  (history.stride_save.low_conf_inflight && history.who == VPSave::STRIDE);
         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/


         bool highConf =  (history.who == VPSave::STRIDE && history.stride_save.pred.second == 7) || (history.who == VPSave::VTAGE && vtage_pred.second == 7);

         bool transient =  ((history.who == VPSave::STRIDE && history.stride_save.pred.second >= 1) || (history.who == VPSave::VTAGE && vtage_pred.second >= 1)) && !highConf;


         bool low_conf =  ((history.who == VPSave::STRIDE && history.stride_save.pred.second == 0) || (history.who == VPSave::VTAGE && vtage_pred.second == 0))
                         && !highConf && !transient;

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}






