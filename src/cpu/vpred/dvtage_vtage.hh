/*
 * dvtage_vtage.hh
 *
 *  Created on: Jun 21, 2013
 *      Author: aperais
 */



#include "cpu/pred/TAGE.hh"
#include "cpu/vpred/Diff_VTAGE.hh"
#include "cpu/vpred/VTAGE.hh"

//#ifndef DVTAGE_VTAGE_HH_
//#define DVTAGE_VTAGE_HH_

class DVTAGE_VTAGE_VP
{
protected:


        struct VPSave {
                enum Type {
                        DVTAGE,
                        VTAGE,
                        All,
                        None
                };

                Type who;
                VTageVP::VPSave *vtage_save;
                DiffVTAGE::VPSave *dvtage_save;

                bool isBranch;
        };


public:
        /**
         * Constructor.
         *@param[in] threshold		The threshold of the 2level predictor.
         *@param[in] size		Number of entries in the VHT (power of 2).
         **/
        DVTAGE_VTAGE_VP(std::string &name,
                std::deque<TageBP::Hist> *bhist,
                unsigned numHistComponents,
                unsigned numLogBaseEntry,
                unsigned base_dvtage,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned baseHystShift,
                unsigned instShiftAmt,
                unsigned *phist,
                unsigned counterwidth,
                std::vector<unsigned> &proba_vtage);

        prediction_t lookup(Addr index, MicroPC micropc, void *&vp_history);

        void updateVHT(uint16_t index, Prediction & value);

        void flush_branch(void *vp_history);

        void updateFoldedHist(bool save, void *&vp_history);

        void update(Prediction & value, void *vp_history, bool squashed);

        void squash(void *vp_history, bool remove, bool recompute);

        const std::string &name() const { return _name; }

        void regStats();

        void updateStats(VPSave &history, bool outcome);

        Addr hashPC(Addr ip);

private:

        /** PRIVATE MEMBERS **/

        std::deque<TageBP::Hist> *bhist;
        VTageVP *vtage;
        DiffVTAGE *dvtage;

        std::string _name;

        int arbiter;

        /** Global Stats */
        Stats::Scalar attempted;
        Stats::Scalar correct, correct_dvtage, correct_vtage, correct_all;
        Stats::Scalar incorrect, incorrect_dvtage, incorrect_vtage, incorrect_all;

        Stats::Formula accuracy;
        Stats::Formula coverage;

        Stats::Scalar made_by_dvtage, made_by_vtage;

        /** Confidence Stats */
        /** Stat for number of high confidence predictions. */
        Stats::Scalar highConf;
        /** Stat for number of correct high confidence predictions. */
        Stats::Scalar highConfHit;
        /** Stat for the number of incorrect high confidence predictions */
        Stats::Scalar highConfMiss;
        /** Stat for number of medium confidence predictions. */
        Stats::Scalar transientConf;
        /** Stat for number of correct medium confidence predictions. */
        Stats::Scalar transientConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar transientConfMiss;
        /** Stat for number of low confidence predictions. */
        Stats::Scalar lowConf;
        /** Stat for number of correct low confidence predictions. */
        Stats::Scalar lowConfHit;
        /** Stat for the number of incorrect medium confidence predictions */
        Stats::Scalar lowConfMiss;
};

//#endif /* DVTAGE_VTAGE_HH_ */
