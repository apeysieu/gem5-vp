/*
 * Authors: NathanaÃ«l PrÃ©millieu
 */

#include "cpu/vpred/VTAGE.hh"

#include <cmath>
#include <cstdlib>
#include <sstream>

#include "base/intmath.hh"
#include "base/trace.hh"
#include "cpu/o3/helper.hh"
#include "cpu/pred/TAGE.hh"
#include "debug/TAGEAlloc.hh"
#include "debug/ValuePredictor.hh"


VTageVP::VTageVP(std::string &name,
                unsigned _numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned _baseHystShift,
                unsigned counterWidth,
                unsigned _instShiftAmt,
                std::vector<unsigned> & _filterProbability,
                std::deque<TageBP::Hist> *bpred_globHist,
                unsigned *bpred_phist,
                TageBP *tage)
: _name(name + ".VPredUnit.VTAGE"),
  numHistComponents(_numHistComponents),
  baseHystShift(0),
  useAltOnNA(0),
  logCTick(19),
  cTick((1 << (logCTick - 1))),
  phist(bpred_phist),
  globHist(bpred_globHist),
  seed(0),
  instShiftAmt(0)
{
        proba = _filterProbability;

        //For sanity checking.
        created = deleted = 0;

        unsigned i;
        unsigned size = numHistComponents + 1;

        /** initialize structures */
        this->tage = tage;
        /** logg */
        logg.resize(size);
        for(i = 1; i < size; ++i) {
                logg[i] = 10;
        }

        /** m */
        m.resize(size);
        /** computes the geometric history lengths */
        m[1] = minHistSize;
        m[numHistComponents] = maxHistSize;


        for(i = 2; i < size; i++) {
                m[i] =
                                (int) (((double) minHistSize *
                                                pow ((double) (maxHistSize) / (double) minHistSize,
                                                                (double) (i - 1) / (double) ((numHistComponents - 1)))) + 0.5);
                std::cerr << m[i] << std::endl;
        }

        /** ch_i, ch_t */
        ch_i.resize(size);
        ch_t[0].resize(size);
        ch_t[1].resize(size);


        for(i = 1; i < size; ++i) {
                ch_i[i].init(m[i], logg[i]);
                ch_t[0][i].init(ch_i[i].olength, 12+i);
                ch_t[1][i].init(ch_i[i].olength, 12+i - 1);
        }


        /** bTable */
        bTable.resize(1 << numLogBaseEntry, Bentry(counterWidth, proba));


        /** gTable */
        gTable.resize(size);
        for(i = 1; i < size; ++i) {
                gTable[i].resize(1 << logg[i], Gentry(counterWidth, proba));
        }

        /** Compute masks */
        baseMask = ((1 << (numLogBaseEntry)) - 1);

        tagMask.resize(size);
        for(i = 1; i < size; ++i) {
                tagMask[i] = ((1 << (i+12)) - 1);
        }

        gMask.resize(size);
        for(i = 1; i < size; ++i) {
                gMask[i] = ((1 << logg[i]) - 1);
        }

}



void
VTageVP::regStats()
{
        table
        .name(name() + ".TableSpread")
        .init(0,6,1)
        ;

        accesses
        .init(0,6,1)
        .name(name() + ".accesses")
        .desc("Number of accesses for each table")
        ;

        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        taggedHit
        .name(name() + ".taggedHit")
        .desc("Number of hits in a tagged table during a lookup for the standard pred")
        ;

        altTaggedHit
        .name(name() + ".altTaggedHit")
        .desc("Number of hits in a tagged table during a lookup for the altpred")
        ;


        alloc
        .name(name() + ".alloc")
        .desc("Number of allocations in tagged components")
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;

        accuracy_ideal
        .name(name() + ".accuracy_ideal")
        .desc("Accuracy ideal")
        ;

        coverage_ideal
        .name(name() + ".coverage_ideal")
        .desc("Coverage ideal")
        ;

        commit_correct
        .name(name() + ".commit_correct")
        .desc("Total number of correct predictions ideal")
        ;

        commit_incorrect
        .name(name() + ".commit_incorrect")
        .desc("Total number of incorrect predictions ideal")
        ;

        taggedPred
        .name(name() + ".taggedPred")
        .desc("Number of tagged components predictions")
        ;

        basePred
        .name(name() + ".basePred")
        .desc("Number of base bimodal predictions")
        ;

        standardPred
        .name(name() + ".standardPred")
        .desc("Number of standard predictions")
        ;

        altPred
        .name(name() + ".altPred")
        .desc("Number of predictions given by the alternate prediction")
        ;

        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        baseHighConf
        .name(name() + ".baseHighConf")
        .desc("Number of high confidence base predictions")
        ;

        baseHighConfHit
        .name(name() + ".baseHighConfHit")
        .desc("Number of correct high confidence base redictions")
        ;

        baseHighConfMiss
        .name(name() + ".baseHighConfMiss")
        .desc("Number of incorrect high confidence base predictions.")
        ;

        baseTransientConf
        .name(name() + ".baseTransientConf")
        .desc("Number of transient confidence base predictions")
        ;

        baseTransientConfHit
        .name(name() + ".baseTransientConfHit")
        .desc("Number of correct medium confidence base predictions")
        ;

        baseTransientConfMiss
        .name(name() + ".baseTransientConfMiss")
        .desc("Number of incorrect medium confidence base predictions.")
        ;

        baseLowConf
        .name(name() + ".baseLowConf")
        .desc("Number of low confidence base predictions")
        ;

        baseLowConfHit
        .name(name() + ".baseLowConfHit")
        .desc("Number of correct low confidence base bimodal predictions")
        ;

        baseLowConfMiss
        .name(name() + ".baseLowConfMiss")
        .desc("Number of incorrect low confidence base bimodal predictions.")
        ;

        nonTagged
        .name(name() + ".nonTagged")
        .desc("Number of correct predictions that flowed from the base predictor")
        ;

        correctBasePred
        .name(name() + ".correctBasPred")
        .desc("Number of correct predictions that flowed from the base predictors")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        nonTagged = correctBasePred / attempted;

        coverage_ideal = commit_correct / attempted;
        accuracy_ideal = commit_correct / (commit_correct + commit_incorrect);
}

unsigned
VTageVP::F(unsigned hist, unsigned size, unsigned bank)
{
        Addr res, h1, h2;
        res = (Addr)hist;

        res = res & ((1 << size) - 1);
        h1 = (res & gMask[bank]);
        h2 = (res >> logg[bank]);
        h2 = ((h2 << bank) & gMask[bank]) + (h2 >> (logg[bank] - bank));
        res = h1 ^ h2;
        res = ((res << bank) & gMask[bank]) + (res >> (logg[bank] - bank));
        return (unsigned)res;
}

unsigned
VTageVP::gIndex(Addr &branch_addr, unsigned bank)
{
        Addr index;
        unsigned M = (m[bank] > 16) ? 16 : m[bank];
        index = branch_addr ^ (branch_addr >> (abs(logg[bank] - bank) + 1)) ^
                        ch_i[bank].comp ^ F(*phist, M, bank);
        return (unsigned)(index & gMask[bank]);
}

unsigned
VTageVP::gTag(Addr &branch_addr, unsigned bank)
{
        Addr tag = branch_addr ^ ch_t[0][bank].comp ^ (ch_t[1][bank].comp << 1);
        return (unsigned)(tag & tagMask[bank]);
}

prediction_t
VTageVP::getBasePred(Addr &branch_addr, bool &saturated)
{
        unsigned index = bIndex(branch_addr);

        saturated = bTable[index].hyst.saturated();
        return prediction_t(bTable[index].pred, bTable[index].hyst.read());
}

void
VTageVP::baseUpdate(Addr branch_addr, Prediction & val, bool outcome, unsigned conf, bool squashed)
{
        unsigned index = bIndex(branch_addr);
        bTable[index].ctrupdate(outcome, val, conf);
}


prediction_t
VTageVP::lookup(Addr &addr, MicroPC micropc, void *&vp_history, uint64_t seqNum)
{

        int i;
        unsigned size = numHistComponents + 1;
        unsigned hitBank = 0;
        unsigned altBank = 0;
        prediction_t tagePred;
        prediction_t altPred;

        bool choseAlt;
        bool baseSaturated = false;

        // Create VTageVP::VPSave. This is the history for a new instruction.
        VPSave *history = new VPSave();
        ++created;


        // TAGE prediction
        // computes the table addresses and the partial tags
        history->gI.resize(size);
        history->gTag.resize(size);

        Addr taddr = addr ^ micropc;

        history->gI[0] = bIndex(taddr);

        for (i = 1; i < size; ++i)
        {
                history->gI[i] = gIndex(taddr, i);
                history->gTag[i] = gTag(taddr, i);
        }

        //Look for the bank with longest matching history
        for (i = size - 1; i > 0; --i)
        {
                if (gTable[i][history->gI[i]].tag == history->gTag[i])
                {

                        hitBank = i;

                        DPRINTF(ValuePredictor, "ValuePred: 0x%lx, entry found in bank: %i,"
                                        " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);

                        break;
                }
        }

        accesses.sample(0);
        accesses.sample(1);
        accesses.sample(2);
        accesses.sample(3);
        accesses.sample(4);
        accesses.sample(5);
        accesses.sample(6);

        //Look for the alternate bank
        for (i = hitBank - 1; i > 0; --i)
        {
                if (gTable[i][history->gI[i]].tag == history->gTag[i])
                        //TODO Check this condition
                        //if ((useAltOnNA < 0) || (gTable[i][history->gI[i]].hyst.read()) > 0)
                        //{
                        altBank = i;

                DPRINTF(ValuePredictor, "ValuePred: 0x%lx, alternate entry found in bank: %i,"
                                " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);
                break;
                //}
}

        //computes the prediction and the alternate prediction
        if (hitBank > 0)
        {
                if (altBank > 0) {
                        altPred = prediction_t(gTable[altBank][history->gI[altBank]].pred, (uint8_t) gTable[altBank][history->gI[altBank]].hyst.read());
                        //            choseBase = false;
                }
                else
                {
                        altPred = getBasePred(addr, baseSaturated);
                        //            choseBase = true;

                        //TODO Fix DPRINTF
                        DPRINTF(ValuePredictor, "ValuePred: 0x%x, alternate prediction by base comp,"
                                        " index: %i, sat: %i, pred: %i\n", addr, bIndex(addr), baseSaturated,
                                        0);
                }
                //if the entry is recognized as a newly allocated entry and
                //useAltOnNA is positive  use the alternate prediction
                tagePred = prediction_t(gTable[hitBank][history->gI[hitBank]].pred, (uint8_t) gTable[hitBank][history->gI[hitBank]].hyst.read());

                if ((useAltOnNA < 0)
                                || gTable[hitBank][history->gI[hitBank]].hyst.read()  > 0)
                {
                        choseAlt = false;


                        DPRINTF(ValuePredictor, "ValuePred: 0x%lx, standard prediction used, vtagePred %lu\n",
                                        addr, tagePred.first.getValue());
                }
                else
                {
                        choseAlt = true;

                        //TODO Fix DPRINTF
                        //            DPRINTF(ValuePredictor, "ValuePred: 0x%x, alternate prediction used, tagePred %i,"
                        //            " basePred: %i\n", addr, tagePred.first, altPred.first);
                }
        }
        else
        {
                altPred = getBasePred(addr, baseSaturated);
                tagePred = prediction_t(altPred.first, altPred.second);

                //        choseBase = true;
                choseAlt = false;

                //TODO Fix DPRINTF
                //        DPRINTF(ValuePredictor, "ValuePred: 0x%x, no tagged prediction, use base"
                //        " base prediction, tagePred: %i, index: %i, sat: %i\n", addr, tagePred.first,
                //        bIndex(addr), baseSaturated);
        }

        //    history->usedBimod = choseBase;


        /* save predictor state */
        if(!history)
                panic("No history data structure to save the predictor state");

        /* save prediction, hit/alt bank, GI and GTAG */
        history->bank = hitBank;
        history->altBank = altBank;
        history->tagePred = tagePred;
        history->altPred = altPred;
        history->isBranch = false;

        /*
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, hitBank: %i, altBank: %i, tagePred: %i,"
    " altTaken: %i, predTaken: %i\n", branch_addr, hitBank, altBank, tagePred,
    altTaken, predTaken);
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, confidence:  high: %i, med: %i, low: %i"
    " (bimod: %i) (Stag: %i, NStag: %i, Wtag: %i, NWtag: %i)\n", branch_addr,
    history->highConf, history->medConf, history->lowConf,
    history->bimHighConf || history->bimMedConf || history->bimLowConf,
    history->Stag, history->NStag, history->Wtag, history->NWtag);*/

        DPRINTF(ValuePredictor, "VPSave :bank: %i, altbank: %i, choseAlt: %x\n",
                        history->bank, history->altBank, choseAlt);

        vp_history = static_cast<void *>(history);
        hists.insert(std::pair<void*, uint64_t>(vp_history, seqNum));
        return (choseAlt ? altPred : tagePred);
}


void
VTageVP::updateFoldedHist(bool save, void *&vp_history, uint64_t seqnum)
{
        unsigned size = numHistComponents + 1;
        unsigned i = 0;
        DPRINTF(ValuePredictor, "Updating Folded History in VTAGE\n");

        if(save)
        {
                VPSave *new_record = new VPSave();
                ++created;




                new_record->ch_i_comp.resize(size);
                new_record->ch_t_comp[0].resize(size);
                new_record->ch_t_comp[1].resize(size);

                for (i = 1; i < size; ++i)
                {
                        //DPRINTF(ValuePredictor, "Saving folded history %u: %lu\n", i, ch_i[i].comp);

                        new_record->ch_i_comp[i] = ch_i[i].comp;
                        new_record->ch_t_comp[0][i] = ch_t[0][i].comp;
                        new_record->ch_t_comp[1][i] = ch_t[1][i].comp;
                }


                //        	std::stringstream ss;
                //        	for(unsigned j = 0; j < 64; j++) {
                //        		ss << (*globHist)[j].dir;
                //        	}
                //        	DPRINTF(ValuePredictor, "History is: %s\n", ss.str());

                new_record->bank = 0;
                new_record->altBank = 0;
                new_record->tagePred = prediction_t(Prediction(),0);
                new_record->altPred = prediction_t(Prediction(),0);
                new_record->isBranch = true;

                vp_history = static_cast<void *>(new_record);
        }

        //prepare next index and tag computations
        for (i = 1; i < size; ++i)
        {
                ch_i[i].update(*globHist);
                //DPRINTF(ValuePredictor, "VTAGE: Updated folded history %u: %lu\n", i, ch_i[i].comp);
                ch_t[0][i].update(*globHist);
                ch_t[1][i].update(*globHist);
                DPRINTF(ValuePredictor, "Updating ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
        }
        hists.insert(std::pair<void*, uint64_t>(vp_history, seqnum));
        DPRINTF(ValuePredictor, "ValuePred: GlobHist size: %i, new phist: %x\n", globHist->size(), *phist);
}


void
VTageVP::updatePredictor(Prediction & val, bool outcome, void *vp_history, bool squashed)
{
        unsigned i,j;
        unsigned size = numHistComponents + 1;
        unsigned nrand = myRandom();
        prediction_t tagePred;
        prediction_t altPred;


        unsigned hitBank;
        unsigned altBank;

        assert(vp_history);

        VPSave *history = static_cast<VPSave *>(vp_history);
        table.sample(history->bank);
        //Propagate the confidence instead of re reading the table.
        unsigned conf = history->tagePred.second;


        if(squashed) {
                if(history->bank == 0) {
                        baseUpdate(history->gI[0], val, false, 0, true);
                } else {
                        gTable[history->bank][history->gI[history->bank]].ctrupdate(false, val, 0);
                }
                return;
        }

        /*if(history->usedAlt) {
                if(history->altBank == 0) {
                        commit_correct += bTable[history->gI[0]].pred == val && bTable[history->gI[0]].hyst.read() == 7;
                        commit_incorrect += !(bTable[history->gI[0]].pred == val) && bTable[history->gI[0]].hyst.read() == 7;
                } else {
                        commit_correct += gTable[history->altBank][history->gI[history->altBank]].pred == val && gTable[history->altBank][history->gI[history->altBank]].hyst.read() == 7;
                        commit_incorrect += !(gTable[history->altBank][history->gI[history->altBank]].pred == val) && gTable[history->altBank][history->gI[history->altBank]].hyst.read() == 7;

                }
        } else {*/
                if(history->bank == 0) {
                        commit_correct += bTable[history->gI[0]].pred == val && bTable[history->gI[0]].hyst.read() == 7 ;
                        commit_incorrect += !(bTable[history->gI[0]].pred == val) && bTable[history->gI[0]].hyst.read() == 7;
                } else {
                        commit_correct += gTable[history->bank][history->gI[history->bank]].pred == val && gTable[history->bank][history->gI[history->bank]].hyst.read() == 7;
                        commit_incorrect += !(gTable[history->bank][history->gI[history->bank]].pred == val) && gTable[history->bank][history->gI[history->bank]].hyst.read() == 7;
                }
        //}

        tagePred = history->tagePred;
        altPred = history->altPred;
        hitBank = history->bank;
        altBank = history->altBank;

#ifndef NOREAD_AT_COMMIT_VTAGE
if(hitBank == 0) {
        outcome = bTable[history->gI[0]].pred == val && outcome;
} else {
        outcome = gTable[hitBank][history->gI[hitBank]].pred == val && outcome;
}
#endif

// VTAGE UPDATE
// try to allocate a  new entries only if prediction was wrong
bool alloc = !outcome && (hitBank < numHistComponents);
if (hitBank > 0) {
        // Manage the selection between longest matching and alternate matching
        // for "pseudo"-newly allocated longest matching entry
        bool PseudoNewAlloc = (gTable[hitBank][history->gI[hitBank]].hyst.read() == 0);
        // an entry is considered as newly allocated if its prediction counter is weak
        if (PseudoNewAlloc) {
                if (tagePred.first == val) {
                        alloc = false;
                }
                // if it was delivering the correct prediction, no need to allocate a new entry
                //even if the overall prediction was false
                if(altPred.second == 7) {
                        if (!(altPred.first == tagePred.first)) {
                                if (altPred.first == val) {
                                        if (useAltOnNA < 7) {
                                                useAltOnNA++;
                                        }
                                } else if (useAltOnNA > -8) {
                                        useAltOnNA--;
                                }
                        }
                }
        }
}

if (alloc) {
        DPRINTF(TAGEAlloc, "Allocating entry for inst 0x%lx\n", history->instAddr);
        // is there some "unuseful" entry to allocate
        unsigned min = 1;
        for (i = numHistComponents; i > hitBank; --i) {
                accesses.sample(i);
                if (gTable[i][history->gI[i]].u < min) {
                        min = gTable[i][history->gI[i]].u;
                }

        }
        // we allocate an entry with a longer history
        //to  avoid ping-pong, we do not choose systematically the next entry, but among the 3 next entries
        unsigned Y = nrand & ((1 << (numHistComponents - hitBank - 1)) - 1);
        unsigned X = hitBank + 1;
        if (Y & 1)
        {
                X++;
                if (Y & 2)
                        X++;
        }
        //NO ENTRY AVAILABLE:  ENFORCES ONE TO BE AVAILABLE
        //TODO Do we need this?

                        if (min > 0) {
                                gTable[X][history->gI[X]].u = 0;
                        }

                        //Allocate only  one entry
                        for (i = X; i < size; ++i) {
                                accesses.sample(i);
                                if (gTable[i][history->gI[i]].u == 0) {
                                        this->alloc++;

                                        gTable[i][history->gI[i]].tag = history->gTag[i];
                                        gTable[i][history->gI[i]].hyst.set(0);
                                        gTable[i][history->gI[i]].u = 0;
                                        gTable[i][history->gI[i]].pred = val;

                                        //TODO Fix DPRINTF
                                        DPRINTF(ValuePredictor, "ValuePred: new entry allocated,"
                                                        " table: %i, index: %i, tag: %i\n", i,
                                                        history->gI[i], history->gTag[i]);

                                        break;
                                }
                        }
}
//periodic reset of u
cTick++;
if ((cTick & ((1 << logCTick) - 1)) == 0) {
        // reset least significant bit
        // most significant bit becomes least significant bit
        for (i = 1; i < size; ++i) {
                for (j = 0; j < (1 << logg[i]); ++j) {
                        gTable[i][j].u = 0;
                }
        }
}

if (hitBank > 0) {
        accesses.sample(hitBank);
        gTable[hitBank][history->gI[hitBank]].ctrupdate(outcome, val, conf);

        //if the provider entry is not certified to be useful also update the alternate prediction
        if (gTable[hitBank][history->gI[hitBank]].u == 0) {
                if (altBank > 0) {
                        accesses.sample(altBank);
                        gTable[altBank][history->gI[altBank]].ctrupdate(outcome, val, altPred.second);
                }
                if (altBank == 0) {
                        //baseUpdate(addr, val, altPred.first == val);
                        accesses.sample(altBank);
                        baseUpdate(history->gI[0], val, outcome,  altPred.second);
                }
        }
} else {
        accesses.sample(0);
        //baseUpdate(addr, val, altPred.first == val);
        baseUpdate(history->gI[0], val, outcome, conf);

}

// update the u counter
if (!(tagePred.first == altPred.first)) {
        if(tagePred.first == val && hitBank > 0) {
                gTable[hitBank][history->gI[hitBank]].u = 1;
        } else if(hitBank > 0) {
                gTable[hitBank][history->gI[hitBank]].u = 0;
        }
} else if(altPred.second != 7 && tagePred.first == val && hitBank > 0) {
        gTable[hitBank][history->gI[hitBank]].u = 1;
}
updateStats(*history, outcome);
}

void
VTageVP::squash(void *vp_history, bool remove, bool recompute)
{
        VPSave *hist = static_cast<VPSave*>(vp_history);
        if(!remove && !recompute) {
                assert(!hist->isBranch);
        }
        if(hist->isBranch) {
                DPRINTF(ValuePredictor, "squashing a branch in value predictor\n");
                recoverBHist(vp_history, recompute);
        }
        if(remove) recoverVHist(vp_history);
}

void
VTageVP::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave *>(vp_history);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        hists.erase(vp_history);
        delete history;++deleted;
        assert(created - deleted < 512);
}

void
VTageVP::recoverBHist(/*Addr &addr,*/ void *vp_history, bool recompute)
{
        unsigned size = numHistComponents + 1;
        unsigned i;

        VPSave *rollback = static_cast<VPSave*>(vp_history);

        /** Restore history */
        assert(vp_history);
        assert(rollback->isBranch);

        for (i = 1; i < size; ++i)
        {
                ch_i[i].comp = rollback->ch_i_comp[i];
                ch_t[0][i].comp = rollback->ch_t_comp[0][i];
                ch_t[1][i].comp = rollback->ch_t_comp[1][i];

                DPRINTF(ValuePredictor, "Restoring ch_i[%u] to %u, ch_t[0][%u] to %u and ch_t[1][%u] to %u\n", i, ch_i[i].comp, i, ch_t[0][i].comp, i, ch_t[1][i].comp);
        }

        //Call to recompute the cyclic registers with the correct global branch history
        //Only if needed.
        if(recompute) updateFoldedHist(false, vp_history);
}

void
VTageVP::recoverVHist(/*Addr &addr,*/ void *vp_history)
{
        //Essentially we don't have much to do here.
        VPSave *history = static_cast<VPSave *>(vp_history);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        hists.erase(vp_history);
        delete history;
        deleted++;
        assert(created - deleted < 512);
}

void
VTageVP::update(Prediction &  val, void *vp_history, bool mispred,
                bool squashed)
{

        VPSave *history = static_cast<VPSave *>(vp_history);
        assert(!history->isBranch);

        //if(history->usedAlt) DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->altPred.first.tostring());
        /*else*/ DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->tagePred.first.tostring());

        updatePredictor(/*addr,*/ val, !mispred && /*(history->usedAlt ? (val == history->altPred.first) :*/ (val == history->tagePred.first), vp_history, squashed);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        if(!squashed) {
#ifdef DEBUG
                assert(hists.find(vp_history) != hists.end());
                uint64_t seqNum = hists[vp_history];
                for(auto it = hists.begin(); it != hists.end(); ++it) {
                        if(it->second < seqNum) {
                                std::cerr << "Should not happen, inst " << std::dec << it->second << "it still here, is branch : " << static_cast<VPSave*>(it->first)->isBranch << std::endl;
                                fatal("oups");
                        }
                }
#endif
                delete history;
                hists.erase(vp_history);
                deleted++;
                assert(created - deleted < 512);

        }

}


void
VTageVP::updateStats(VPSave &history, bool outcome)
{
        /** Base stats **/

        bool proceeded = /*(history.usedAlt && history.altPred.second == 7) || (!history.usedAlt &&*/ (history.tagePred.second == 7);
        attempted ++;

        correct += outcome && proceeded;
        incorrect += !outcome && proceeded;

        /*if(history.usedAlt)
        {
                altPred++;
                if(history.altBank == 0)
                {
                        basePred++;
                        if(outcome && proceeded) {
                                correctBasePred++;
                        }
                } else {
                        taggedPred++;
                }
        } else {*/
                standardPred++;
                if(history.bank == 0)
                {
                        basePred++;
                        if(outcome && proceeded) {
                                correctBasePred++;
                        }
                } else {
                        taggedPred++;
                }
        //}

        /** Stats for the tagged components **/

        bool highConf = (/*!history.usedAlt &&*/ history.bank != 0 && history.tagePred.second >= 7);
        //|| (history.usedAlt && history.altBank != 0 && history.altPred.second >= 7);

        bool transient = /*((!history.usedAlt &&*/ (history.bank != 0 && history.tagePred.second >= 1);
                        //|| (history.usedAlt && history.altBank != 0 && history.altPred.second >= 1)) && !highConf;

        bool low_conf = /*((!history.usedAlt &&*/ (history.bank != 0 && history.tagePred.second == 0);
                       // || (history.usedAlt && history.altBank != 0 && history.altPred.second == 0)) && !highConf && !transient;

        this->highConf += highConf;
        highConfHit += highConf && outcome;
        highConfMiss += highConf && !outcome;

        transientConf += transient;
        transientConfHit += transient && outcome;
        transientConfMiss += transient && !outcome;

        lowConf += low_conf;
        lowConfHit += low_conf && outcome;
        lowConfMiss += low_conf && !outcome;


        //Stats for the base component.

        highConf = (!history.usedAlt && history.bank == 0 && history.tagePred.second >= 7)
                                                                                                || (history.usedAlt && history.altBank == 0 && history.altPred.second >= 7);

        transient = ((!history.usedAlt && history.bank == 0 && history.tagePred.second >= 1)
                        || (history.usedAlt && history.altBank == 0 && history.altPred.second >= 1)) && !highConf;

        low_conf = ((!history.usedAlt && history.bank == 0 && history.tagePred.second == 0)
                        || (history.usedAlt && history.altBank == 0 && history.altPred.second == 0)) && !highConf && !transient;

        baseHighConf += highConf;
        baseHighConfHit += highConf && outcome;
        baseHighConfMiss += highConf && !outcome;

        baseTransientConf += transient;
        baseTransientConfHit += transient && outcome;
        baseTransientConfMiss += transient && !outcome;

        baseLowConf += low_conf;
        baseLowConfHit += low_conf && outcome;
        baseLowConfMiss += low_conf && !outcome;

}

std::string
VTageVP::dump()
{
        std::ostringstream oss;
        oss << "VPred: Branch history: ";
        for(std::deque<TageBP::Hist>::iterator it = globHist->begin(); it != globHist->end(); ++it)
        {
                oss << it->dir;
        }
        oss << std::endl;
        return oss.str();
}


//#ifdef DEBUG
//int
//VTageVP::VPHistory::newCount = 0;
//#endif
