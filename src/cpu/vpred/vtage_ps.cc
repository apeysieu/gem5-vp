/*
 * vtage_ps.cc
 *
 *  Created on: Nov 7, 2012
 *      Author: aperais
 */


#include "cpu/vpred/vtage_ps.hh"

#include "cpu/o3/helper.hh"

VTAGE_PS::VTAGE_PS(std::string &name,
                unsigned size_ps_vht,
                unsigned size_ps_sht,
                unsigned tag_ps_vht_size,
                unsigned tag_ps_sht_size,
                unsigned bhist_length,
                unsigned associativity_vht,
                unsigned associativity_sht,
                std::deque<TageBP::Hist> *bhist,
                unsigned numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned baseHystShift,
                unsigned instShiftAmt,
                unsigned *phist,
                unsigned counterwidth,
                std::vector<unsigned> &proba_vtage,
                std::vector<unsigned> &proba_ps)
{

       this->_name = name + ".VPredUnit.VTAGE_PS";

       PS_VHT_INDEX = log2(size_ps_vht);
       PS_SHT_INDEX = log2(size_ps_sht);
       PS_TAG_SHT = tag_ps_sht_size;
       PS_TAG_VHT = tag_ps_vht_size;
       BHIST_LENGTH = bhist_length;

       this->bhist = bhist;
       ps_proba = proba_ps;
       assoc_ps_sht = associativity_sht;
       assoc_ps_vht = associativity_vht;

       this->ps_fetch_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_ps_vht, this->assoc_ps_vht, counterwidth, ps_proba);
       this->ps_commit_VHT = AssociativeArray<PSVP, PSVP::VHTEntry>(size_ps_vht, this->assoc_ps_vht, counterwidth, ps_proba);

       this->ps_SHT = AssociativeArray<PSVP, PSVP::SHTEntry>(size_ps_sht, this->assoc_ps_sht, counterwidth, ps_proba);


       ps_low_conf_inflight.resize(size_ps_vht);
       ps_inflight.resize(size_ps_vht);

       arbiter = 0;
       tage = new VTageVP(name,
                   numHistComponents,
                   numLogBaseEntry,
                   minHistSize,
                   maxHistSize,
                   baseHystShift,
                   counterwidth,
                   instShiftAmt,
                   proba_vtage,
                   bhist,
                   phist);

       std::cerr << "Size vht " << ps_fetch_VHT.size() << ", sht " << ps_SHT.size() << std::endl;
       std::cerr << "Assoc vht" << associativity_vht << ", assoc sht " << associativity_sht << std::endl;

}

Addr
VTAGE_PS::hashPC(Addr ip)
{
        std::bitset<640> tmp;
        for(unsigned i = 0; i < BHIST_LENGTH; i++) {
                tmp[i] = (*bhist)[i].dir;
        }
        return ((ip << BHIST_LENGTH) + tmp.to_ulong());
}

prediction_t
VTAGE_PS::lookup(Addr ip, MicroPC micropc, void *&vp_history)
{
        Addr base_upc_vht = createUPCindex(micropc, (Addr) log2(this->ps_commit_VHT.size()));
        Addr base_upc_sht = createUPCindex(micropc, (Addr) log2(this->ps_SHT.size()));

        Addr ps_vht_index = (base_upc_vht ^ ip) & (this->ps_fetch_VHT.size() - 1);
        Addr ps_vht_tag = ((ip >> this->PS_VHT_INDEX) & ((((Addr) 1) << PS_TAG_VHT) - 1));

        Addr ps_sht_index = hashPC(base_upc_sht ^ ip) % (this->ps_SHT.size());
        Addr ps_sht_tag = (ip >> (unsigned) log2(this->ps_SHT.size())) & (((Addr) 1 << PS_TAG_SHT) -1);
        //Addr ps_sht_tag = (hashPC(ip) >> (unsigned) log2(this->ps_SHT.size())) & (((Addr) 1 << PS_TAG_SHT) -1);

        //std::cerr << "Index vht: " << ps_vht_index << ", index sht : " << ps_sht_index << std::endl;

        VPSave *history = new VPSave();


        vp_history = static_cast<void *>(history);

        //Lookup will allocate save.
        void *save = NULL;
        prediction_t vtage_pred = tage->lookup(ip, micropc, save);
        VTageVP::VPSave* vtage_save = static_cast<VTageVP::VPSave*>(save);

        history->isBranch = false;

        history->ps_save.low_conf_inflight = false;


        history->ps_save.index_vht = ps_vht_index;
        history->ps_save.tag_vht = ps_vht_tag;
        history->ps_save.index_sht = ps_sht_index;
        history->ps_save.tag_sht = ps_sht_tag;

        history->ps_save.way_vht = ps_commit_VHT[ps_vht_index].tagLookup(ps_vht_tag);
        history->ps_save.mismatch_vht = history->ps_save.way_vht == assoc_ps_vht;

        history->ps_save.way_sht = ps_SHT[ps_sht_index].tagLookup(ps_sht_tag);
        history->ps_save.mismatch_sht = history->ps_save.way_sht == assoc_ps_sht;

        //std::cerr << "Way VHT : " <<  history->ps_save.way_vht << ", Way SHT : " <<  history->ps_save.way_sht << std::endl;

        prediction_t pred_ps;
        if(ps_inflight[history->ps_save.index_vht] == 0 && !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                pred_ps = prediction_t(this->ps_commit_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht) + this->ps_SHT[history->ps_save.index_sht].getStride(history->ps_save.way_sht),this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(history->ps_save.way_vht));
        } else if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                pred_ps = prediction_t(this->ps_fetch_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht) + this->ps_SHT[history->ps_save.index_sht].getStride(history->ps_save.way_sht),this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(history->ps_save.way_vht));
        }

        history->ps_save.pred = (history->ps_save.mismatch_vht || history->ps_save.mismatch_sht) ? prediction_t(Prediction(),0) : pred_ps;

        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && ps_low_conf_inflight[history->ps_save.index_vht] > 0) {
                history->ps_save.low_conf_inflight = true;
        }
        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht &&  (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                ps_low_conf_inflight[history->ps_save.index_vht]++;
        }
        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                ps_inflight[history->ps_save.index_vht]++;
        }

        //Arbitration if needed
        history->vtage_save = vtage_save;
        //history->vtage_save->spec_update = false;
        //delete vtage_save;

#ifdef NO_INFLIGHT_CONFIDENCE_TRACKING
        history->ps_save.low_conf_inflight = false;
#endif

        //bool vtage = false;
        bool vtage_high =vtage_pred.second == 7;
        bool ps_high = history->ps_save.pred.second == 7 && !history->ps_save.low_conf_inflight;

        if(ps_high && vtage_high) {
                if(vtage_pred.first == history->ps_save.pred.first) {
                         this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                         history->who = VPSave::All;
                         return vtage_pred;
                } else {
//                if(arbiter >= 0) {
//                        vtage = true;
//                } else {
//                        vtage = false;
//                }
//                if(vtage) {
//                        this->ps_fetch_VHT[history->ps_save.index_vht].update(vtage_pred.first, history->ps_save, history->ps_save.way_vht);
//
//                        history->vtage_save->spec_update = history->ps_save.pred.first != vtage_pred.first;
//
//                        history->who = VPSave::VTAGE;
//                        made_by_vtage++;
//                        spec_update_vtage += history->vtage_save->spec_update;
//
//
//                        return vtage_pred;
//
//                } else {
//                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
//
//                        history->who = VPSave::PS;
//                        made_by_ps++;
//
//
//
//                                return pred_ps;
//
//                }
                        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                        }

                        history->who = VPSave::None;

                        return prediction_t(Prediction(),0);
                }
        } else if(ps_high) {

                this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);


                history->who = VPSave::PS;
                made_by_ps++;



                        return pred_ps;


        } else if(vtage_high) {
               this->ps_fetch_VHT[history->ps_save.index_vht].update(vtage_pred.first, history->ps_save, history->ps_save.way_vht);

                //history->vtage_save->spec_update = true;

                history->who = VPSave::VTAGE;
                made_by_vtage++;
                spec_update_vtage++;


                return vtage_pred;
        } else {
                //Nobody is confident enough.
                if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                        this->ps_fetch_VHT[history->ps_save.index_vht].update(history->ps_save.pred.first, history->ps_save, history->ps_save.way_vht);
                }

                history->who = VPSave::None;

                return prediction_t(Prediction(),0);
        }

}


void
VTAGE_PS::flush_branch(void *vp_history)
{
        VPSave *history = static_cast<VPSave*>(vp_history);
        void *branch = static_cast<void*>(history->vtage_save);
        tage->flush_branch(branch);
        delete history;
}

void
VTAGE_PS::updateFoldedHist(bool save, void *&vp_history) {

        VPSave *history = new VPSave();
        vp_history = static_cast<void*>(history);


        void *branch = NULL;
        tage->updateFoldedHist(save, branch);
        VTageVP::VPSave * tmp = static_cast<VTageVP::VPSave*>(branch);
        history->isBranch = true;
        history->vtage_save = tmp;
        //delete tmp;
}

void
VTAGE_PS::update(Prediction & value, void *vp_history, bool squashed)
{
        assert(vp_history);

        VPSave *history = static_cast<VPSave*>(vp_history);

        if(squashed) {
                if(history->who == VPSave::VTAGE) {
                        void * tmp = static_cast<void*>(history->vtage_save);
                        this->tage->update(value,tmp, false, true);
                } else if(history->who == VPSave::PS) {
                          Prediction dummy = Prediction();
                          this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL, history->ps_save.tag_vht, false, 0, true);
                } else if(history->who == VPSave::All) {
                        void * tmp = static_cast<void*>(history->vtage_save);
                        this->tage->update(value,tmp, false, true);
                        Prediction dummy = Prediction();
                        this->ps_commit_VHT[history->ps_save.index_vht].update(dummy, NULL, history->ps_save.tag_vht, false, 0, true);
                }
                return;
        }

        prediction_t &vtage_pred = history->vtage_save->tagePred;
        prediction_t &ps_pred = history->ps_save.pred;

        unsigned commit_way_vht = ps_commit_VHT[history->ps_save.index_vht].tagLookup(history->ps_save.tag_vht);
        unsigned commit_way_sht = ps_SHT[history->ps_save.index_sht].tagLookup(history->ps_save.tag_sht);

        bool outcome_vtage = vtage_pred.first == value && vtage_pred.second == 7;
        correct_vtage += outcome_vtage && history->who == VPSave::VTAGE;
        incorrect_vtage += !outcome_vtage && history->who == VPSave::VTAGE && vtage_pred.second == 7;
#ifdef NOREAD_AT_COMMIT
        bool outcome_ps = ps_pred.first == value;
#else
        bool outcome_ps = !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && history->ps_save.pred.first == value && history->ps_save.pred.second == 7;
#endif
        correct_ps += outcome_ps && history->who == VPSave::PS;
        incorrect_ps += !history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && !outcome_ps && history->who == VPSave::PS && history->ps_save.pred.second == 7;

        correct_all += outcome_ps && outcome_vtage && history->who == VPSave::All;
        incorrect_all += !outcome_ps && !outcome_vtage && history->who == VPSave::All && vtage_pred.second == 7 && history->ps_save.pred.second == 7;

        outcome_ps =  commit_way_sht != assoc_ps_sht && commit_way_vht != assoc_ps_vht &&
                        ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht) + ps_SHT[history->ps_save.index_sht].getStride(commit_way_sht) == value;
        outcome_vtage = vtage_pred.first == value;

        //Updating the arbiter
        if(outcome_vtage && !outcome_ps && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7 &&  vtage_pred.second == 7) {
                arbiter < 15 ? arbiter++ : 0;
        } else if(!outcome_vtage && outcome_ps && this->ps_commit_VHT[history->ps_save.index_vht].getConfidence(commit_way_vht) == 7 &&  vtage_pred.second == 7) {
                arbiter > -16 ? arbiter-- : 0;
        }


        if(!history->ps_save.mismatch_vht && !history->ps_save.mismatch_sht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                ps_low_conf_inflight[history->ps_save.index_vht]--;
                assert(ps_low_conf_inflight[history->ps_save.index_vht] >= 0);
        }


        if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                ps_inflight[history->ps_save.index_vht]--;
                assert(ps_inflight[history->ps_save.index_vht] >= 0);
        }


        Prediction stride;
        if(commit_way_vht != assoc_ps_vht) {
#ifdef NOREAD_AT_COMMIT
                        stride = value - history->ps_save.previous;
#else
                stride = (value - this->ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht));
#endif
        } else {
                stride = Prediction();
        }

        this->ps_SHT[history->ps_save.index_sht].update(stride, NULL, history->ps_save.tag_sht, outcome_ps, ps_pred.second);

        this->ps_commit_VHT[history->ps_save.index_vht].update(value, NULL, history->ps_save.tag_vht, outcome_ps, ps_pred.second);

        if(commit_way_vht != assoc_ps_vht && ps_inflight[history->ps_save.index_vht] == 0) {
                Prediction restore = this->ps_commit_VHT[history->ps_save.index_vht].getPred(commit_way_vht);
                this->ps_fetch_VHT[history->ps_save.index_vht].restore(restore, commit_way_vht);
        }

        bool outcome =  ((history->who == VPSave::VTAGE || history->who == VPSave::All) && vtage_pred.first == value) || (history->who == VPSave::PS && history->ps_save.pred.first == value);
        updateStats(*history, outcome);

        void * vtage_hist = static_cast<void*>(history->vtage_save);
        this->tage->update(value, vtage_hist, false, false);

        delete history;
}

void
VTAGE_PS::squash(void *vp_history, bool remove, bool recompute) {
        VPSave *history = static_cast<VPSave*>(vp_history);

        assert(history);

        void * vtage_hist = static_cast<void*>(history->vtage_save);

        if(history->isBranch) {
                 this->tage->squash(vtage_hist, remove, recompute);
        } else {
                this->tage->squash(vtage_hist, remove, recompute);


                if(remove) {
                          if(!history->ps_save.mismatch_vht && !history->ps_save.mismatch_sht && (history->ps_save.pred.second < 7 || history->ps_save.low_conf_inflight)) {
                                  ps_low_conf_inflight[history->ps_save.index_vht]--;
                                  assert(ps_low_conf_inflight[history->ps_save.index_vht] >= 0);
                          }
                          if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                                  ps_inflight[history->ps_save.index_vht]--;
                          }

                }

                if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht && ps_inflight[history->ps_save.index_vht] != 0) {
                        DPRINTF(ValuePredictor, "Restoring fetch historyt\n");
                        this->ps_fetch_VHT[history->ps_save.index_vht].restore(history->ps_save.previous, history->ps_save.way_vht);
                } else if(!history->ps_save.mismatch_sht && !history->ps_save.mismatch_vht) {
                        DPRINTF(ValuePredictor, "Restoring commit and fetch historyt\n");
                        Prediction tmp = this->ps_commit_VHT[history->ps_save.index_vht].getPred(history->ps_save.way_vht);
                        this->ps_fetch_VHT[history->ps_save.index_vht].restore(tmp, history->ps_save.way_vht);
                }

        }

        if(remove) {
                delete history;
        }
}

void
VTAGE_PS::regStats()
{

        made_by_ps
        .name(name() + ".madeByPS")
        .desc("Number of predictions made by PS")
        ;

        made_by_vtage
        .name(name() + ".madeByVTAGE")
        .desc("Number of predictions made by VTAGE")
        ;

        spec_update_vtage
        .name(name() + ".specUpdateVTAGE")
        .desc("Number of time VTAGE updated PS")
        ;

        ignoredLowConfInflight
        .name(name() + ".ignoredBecauseLowConf")
        .desc("Number of high conf predictions ignored because a low conf prediction was in flight");
        ;


        accuracy
        .name(name() + ".VPaccuracyAll")
        .desc("Accuracy of the predictor (committed predictions)")
        .precision(5)
        ;

        coverage
        .name(name() + ".VPcoverageAll")
        .desc("Coverage of the predictor (committed prediction)")
        .precision(5)
        ;

        attempted
        .name(name() + ".attempted")
        .desc("Total number of attempted predictions that proceeded to commit")
        ;

        correct
        .name(name() + ".correct")
        .desc("Total number of correct predictions that proceeded to commit")
        ;

        correct_vtage
                .name(name() + ".correctVTAGE")
                .desc("Number of correct predictions coming from VTAGE")
                ;

        correct_ps
                        .name(name() + ".correctPS")
                        .desc("Number of correct predictions coming from PS")
                        ;

        correct_all
        .name(name() + ".correctAll")
        .desc("Number of correct predictions coming from All")
        ;

        incorrect_all
        .name(name() + ".incorrectAll")
        .desc("Number of incorrect predictions coming from All")
        ;


        incorrect_vtage
                        .name(name() + ".incorrectVTAGE")
                        .desc("Number of correct predictions coming from VTAGE")
                        ;

        incorrect_ps
                        .name(name() + ".incorrectPS")
                        .desc("Number of correct predictions coming from PS")
                        ;
        incorrect
        .name(name() + ".incorrect")
        .desc("Total number of incorrect predictions that proceeded to commit")
        ;


        highConf
        .name(name() + ".highConf")
        .desc("Number of high confidence predictions")
        ;

        highConfHit
        .name(name() + ".highConfHit")
        .desc("Number of correct high confidence predictions")
        ;

        highConfMiss
        .name(name() + ".highConfMiss")
        .desc("Number of incorrect high confidence predictions.")
        ;

        transientConf
        .name(name() + ".transientConf")
        .desc("Number of transient confidence predictions")
        ;

        transientConfHit
        .name(name() + ".transientConfHit")
        .desc("Number of correct transient confidence predictions")
        ;

        transientConfMiss
        .name(name() + ".transientConfMiss")
        .desc("Number of incorrect transient confidence predictions.")
        ;

        ignoredCorrect
               .name(name() + ".ignoredCorrect")
               .desc("Percentage of correct predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ignoredIncorrect
               .name(name() + ".ignoredIncorrect")
               .desc("Percentage of incorrect predictions ignored because of low conf inflight")
               .precision(5)
               ;

               ign_correct
               .name(name() + ".ignCorrect")
               .desc("Number of correct predictions ignored because low conf inflight")
               ;

               ign_incorrect
                           .name(name() + ".ignInorrect")
                           .desc("Number of incorrect predictions ignored because low conf inflight")
                           ;

        lowConf
        .name(name() + ".lowConf")
        .desc("Number of low confidence predictions")
        ;

        lowConfHit
        .name(name() + ".lowConfHit")
        .desc("Number of correct low confidence predictions")
        ;

        lowConfMiss
        .name(name() + ".lowConfMiss")
        .desc("Number of incorrect low confidence predictions.")
        ;

        coverage = correct / attempted;
        accuracy = correct / (correct + incorrect);
        ignoredCorrect = ign_correct / attempted;
        ignoredIncorrect = ign_incorrect / attempted;

        tage->regStats();
}

void
VTAGE_PS::updateStats(VPSave &history, bool outcome)
{
         bool proceeded = history.who != VPSave::None;

         prediction_t &vtage_pred = history.vtage_save->tagePred;

         ign_correct +=  proceeded && outcome && (history.ps_save.low_conf_inflight && history.who == VPSave::PS) ;
         ign_incorrect += proceeded && !outcome &&  (history.ps_save.low_conf_inflight && history.who == VPSave::PS);
         attempted ++;
         correct += outcome && proceeded;
         incorrect += !outcome && proceeded;

         /** Stats  **/

         bool highConf = history.who == VPSave::PS ? history.ps_save.pred.second == 7 : (history.who == VPSave::VTAGE ? vtage_pred.second == 7 : false);

         bool transient =  (history.who == VPSave::PS ? history.ps_save.pred.second >= 1 : (history.who == VPSave::VTAGE ? vtage_pred.second >= 1 : false))
                         && !highConf;

         bool low_conf =  (history.who == VPSave::PS ? history.ps_save.pred.second == 0 :(history.who == VPSave::VTAGE ? vtage_pred.second == 0 : false))
                         && !highConf && !transient;

         this->highConf += highConf;
         highConfHit += highConf && outcome;
         highConfMiss += highConf && !outcome;

         transientConf += transient;
         transientConfHit += transient && outcome;
         transientConfMiss += transient && !outcome;

         lowConf += low_conf;
         lowConfHit += low_conf && outcome;
         lowConfMiss += low_conf && !outcome;

}





