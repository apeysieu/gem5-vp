/*
 * vtage_component.hh
 *
 *  Created on: Apr 2, 2013
 *      Author: aperais
 */

#ifndef VTAGE_COMPONENT_HH_
#define VTAGE_COMPONENT_HH_

#include <cstdlib>
#include <deque>
#include <vector>

#include "base/statistics.hh"
#include "base/types.hh"
#include "cpu/o3/ConfCounter.hh"
#include "cpu/pred/TAGE.hh"

/**
 * Implements a TAGE branch predictor, winner of
 * the 2nd JILP Championship Branch Prediction Competition.
 * AndrÃ© Seznec
 */
class VTAGEComponent
{
public:

    /**
     * Default branch predictor constructor.
     *@param name
     *@param numHistComponents
     *@param numLogBaseEntry
     *@param minHistSize
     *@param maxHistSize
     *@param tagWidth
     *@param logHistComponentsEntry
     *@param baseHystShift In case hysteresis counters have to be shared between entries.
     *@param counterWidth
     *@param instShiftAmt Shifts the PC (useful only for RISC or aligned instructions).
     *@param filterProbability The probability of the confidence counter saturating is 1/filterProbability.
     */
        VTAGEComponent(std::string &name,
            unsigned numHistComponents,
            unsigned numLogBaseEntry,
            unsigned minHistSize,
            unsigned maxHistSize,
            unsigned baseHystShift,
            unsigned counterWidth,
            unsigned instShiftAmt,
            std::vector<unsigned> &filterProbability,
            std::deque<TageBP::Hist> *bpred_globHist,
            unsigned *bpred_phist);

    const std::string &name() const { return _name; }

    /** this is the cyclic shift register for folding
         * a long global history into a smaller number of bits;
         * see P. Michaud's PPM-like predictor at CBP-1
         */
        class Folded_history
        {
        public:

            Addr comp;
            unsigned clength;
            unsigned olength;
            unsigned outpoint;

            Folded_history ()
            {
            }

            void init (int original_length, int compressed_length)
            {
                comp = 0;
                olength = original_length;
                clength = compressed_length;
                outpoint = olength % clength;
            }

            void update (std::deque<TageBP::Hist> &globHist)
            {

                comp = (comp << 1) | (Addr)globHist[0].dir;
                comp ^= ((Addr)globHist[olength].dir << outpoint);
                comp ^= (comp >> clength);
                comp &= (1 << clength) - 1;
            }
        };

    /** We still have to save the cyclic register when a branch is encountered because the
     * branch predictor does not necessarily use registers of the same size.
     * We also have to save the index and the tag of the entry,
     * as well as the bank and the altbank and which one made the prediction.
     */
    struct VPSave {
                std::vector<Addr> ch_i_comp;
                std::vector<Addr> ch_t_comp[2];
                std::vector<unsigned> gI;
                std::vector<unsigned> gTag;
                unsigned bank;
                unsigned altBank;
                prediction_t tagePred;
                prediction_t altPred;
                bool usedAlt;
                bool isBranch;
                Addr instAddr;
                bool spec_update;
    };

    /**
     * Looks up the given address in the branch predictor and returns
     * a pair <value, confidence>.  Also creates a
     * BPHistory object to store any state it will need on squash/update.
     * @param branch_addr The address of instruction to look up.
     * @param vp_history Pointer that will be set to the BPHistory object.
     * @return The predicted value of the instruction.
     */
    prediction_t lookup(Addr &branch_addr, MicroPC micropc, void * &vp_history);

    /**
     * Updates the value predictor with the actual result of an instruction.
     * @param branch_addr The address of the instruction to update.
     * @param val Actual result.
     * @param bp_history Pointer to the BPHistory object that was created
     * when the branch was predicted.
     * @param squashed is set when this function is called during a squash
     * operation.
     */
    void update(Prediction & val, void *vp_history, bool squashed);

    /**
     * Restores the global value history on a squash.
     * @param bp_history Pointer to the BPHistory object that has the
     * previous global branch history in it.
     */
    void squash(void *vp_history, bool remove, bool recompute);

    /** Computes the new folded history
    *@param branch_add The address of the committed branch.
    *@pre The buffer managing the branch history has been updated by the branch predictor.
    *@post Folded histories are ready to compute new indexes.
    **/
    void updateFoldedHist(bool save, void *&vp_history);

    /** Update the branch predictor with the committed value. */
    void updatePredictor(/*Addr &addr,*/ Prediction val, bool outcome, void *vp_history, bool squashed);

    std::string dump();

    //Basically delete the saved folded history when they are no longer
    //necessary (because we are committing/updating the predictor with a younger instruction).
    void flush_branch(void *vp_history);

private:


    /** Index function for the base table */
    inline unsigned bIndex(Addr &branch_addr) {
        return ((branch_addr >> instShiftAmt) & baseMask);
    }


    /**
     * The index functions for the tagged tables uses
     * path history as in the OGEHL predictor
     */
    /** F serves to mix path history */
    unsigned F(unsigned A, unsigned size, unsigned bank);

    /** gIndex computes a full hash of pc, ghist and phist */
    unsigned gIndex(Addr &branch_addr, unsigned bank);

    /** tags computation */
    unsigned gTag(Addr &branch_addr, unsigned bank);

    /** Base prediction (with the base predictor) */
    prediction_t getBasePred(Addr &branch_addr, bool &saturated);

    /**
     * Update the base predictor.
     */
    void baseUpdate(Addr branch_addr, Prediction &  val, bool taken, bool squashed = false);

    /**
     * Just a simple pseudo random number generator:
     * a 2-bit counter, used to avoid ping-pong phenomenon
     * on tagged entry allocations
     */
    inline unsigned myRandom()
    {
        ++seed;
        return seed & 3;
    }



    /**
     * Recover from a misprediction:
     * correct the global history
     */
    void recoverBHist(/*Addr &addr,*/ void *vp_history, bool recompute);
    void recoverVHist(/*Addr &addr,*/ void *vp_history);





    /** VTAGE base table entry */
    class Bentry
    {
    public:
        ConfCounter hyst;
        Prediction pred;

        Bentry (unsigned counterwidth, std::vector<unsigned> &proba)
        {
            pred = Prediction();
            hyst = ConfCounter(counterwidth,  0, &proba);
        }

        /**
         * Updates the hysteresis/confidence counter
         *@param outcome True if the prediction was correct, false otherwise.
        **/
        void ctrupdate (bool outcome, Prediction & val, bool squashed = false)
        {
                if(hyst.read() == 0 && !squashed) {
                        pred = val;
                }
                hyst.updateConf(outcome);
        }
    };

    /** VTAGE global table entry */
    class Gentry
    {
    public:
        ConfCounter hyst;
        unsigned tag;
        unsigned u;
        Prediction pred;

        Gentry (unsigned counterwidth, std::vector<unsigned> & proba)
        {
            hyst = ConfCounter(counterwidth, 0, &proba);
            pred = Prediction();
            tag = 0;
            u = 0;
        }

        /**
        * Updates the hysteresis/confidence counter
        *@param outcome True if the prediction was correct, false otherwise.
        **/
        void ctrupdate (bool outcome, Prediction & val, bool squashed = false)
        {
                if(hyst.read() == 0 && !squashed) {
                        pred = val;
                }
                hyst.updateConf(outcome);
        }
    };

    /** Parameters */

    const std::string _name;

    /** Number of Tagged Components */
    unsigned numHistComponents;

    /** sharing an hysteresis bit between 4 bimodal predictor entries */
    unsigned baseHystShift;

    /** Internal structures and variables */



    /** "Use alternate prediction on newly allocated":
     * a 4-bit counter  to determine whether the newly
     * allocated entries should be considered as
     * valid or not for delivering  the prediction */
    int useAltOnNA;

    /** Control counter for the smooth resetting of useful counters */
    unsigned logCTick, cTick;

    /** Use a path history as on  the OGEHL predictor. Use that of the branch predictor. */
    unsigned *phist;
    /** Global history. Use that of the branch predictor. */
    std::deque<TageBP::Hist> *globHist;

    /** Log of number of entries  on each tagged component */
    std::vector<unsigned> logg;




    //std::deque<BPSave> foldedHistSave;
    //std::deque<VPSave> valueHist;

    /** Utility for computing TAGE indices */
    std::vector<Folded_history> ch_i;

    /** Utility for computing TAGE tags */
    std::vector<Folded_history> ch_t[2];

    /** Base VTAGE table */
    std::vector<Bentry> bTable;

    /** Tagged VTAGE tables */
    std::vector< std::vector<Gentry> > gTable;

    /** Used for storing the history lengths */
    std::vector<unsigned> m;



    /** For the pseudo-random number generator */
    unsigned seed;

    /** Mask to compute the index in the base bimodal predictor */
    Addr baseMask;

    /** Mask to compute the tag for the different tagged tables */
    std::vector<Addr> tagMask;

    /** Mask to compute the index in the different tagged tables */
    std::vector<Addr> gMask;
    std::vector<unsigned> proba;
    /**
     * Count how many prediction have been made
     * since the last misprediction due to the base predictor
     */
    unsigned sinceBaseMispred;

    /** Number of bits to shift the instruction over to get rid of the word
     *  offset.
     */
    unsigned instShiftAmt;
};

#endif /* VTAGE_COMPONENT_HH_ */
