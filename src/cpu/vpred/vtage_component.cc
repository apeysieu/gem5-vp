/*
 * vtage_component.cc
 *
 *  Created on: Apr 2, 2013
 *      Author: aperais
 */



/*
 * Authors: Arthur Perais.
 */

#include "cpu/vpred/vtage_component.hh"

#include <cmath>
#include <cstdlib>
#include <sstream>

#include "base/intmath.hh"
#include "base/trace.hh"
#include "cpu/o3/helper.hh"
#include "cpu/pred/TAGE.hh"
#include "debug/ValuePredictor.hh"

VTAGEComponent::VTAGEComponent(std::string &name,
                unsigned _numHistComponents,
                unsigned numLogBaseEntry,
                unsigned minHistSize,
                unsigned maxHistSize,
                unsigned _baseHystShift,
                unsigned counterWidth,
                unsigned _instShiftAmt,
                std::vector<unsigned> &_filterProbability,
                std::deque<TageBP::Hist> *bpred_globHist,
                unsigned *bpred_phist)
: _name(name + ".VPredUnit.VTAGE"),
  numHistComponents(_numHistComponents),
  //baseHystShift(_baseHystShift),
  baseHystShift(0),
  useAltOnNA(0),
  logCTick(19),
  cTick((1 << (logCTick - 1))),
  phist(bpred_phist),
  globHist(bpred_globHist),
  seed(0),
  //instShiftAmt(_instShiftAmt),
  instShiftAmt(0)
{
        proba = _filterProbability;

    unsigned i;
    unsigned size = numHistComponents + 1;
    /** initialize structures */

    /** logg */
    logg.resize(size);
    for(i = 1; i < size; ++i) {
        logg[i] = 10;
    }
    /** m */
    m.resize(size);
    /** computes the geometric history lengths */
    m[1] = minHistSize;
    m[numHistComponents] = maxHistSize;

    for(i = 2; i < size; i++) {
        m[i] =
                (int) (((double) minHistSize *
                        pow ((double) (maxHistSize) / (double) minHistSize,
                                (double) (i - 1) / (double) ((numHistComponents - 1)))) + 0.5);
    }

    /** ch_i, ch_t */
    ch_i.resize(size);
    ch_t[0].resize(size);
    ch_t[1].resize(size);

    for(i = 1; i < size; ++i) {
        ch_i[i].init(m[i], 13);
        ch_t[0][i].init(ch_i[i].olength, i+12);
        ch_t[1][i].init(ch_i[i].olength, i+11);
    }


    /** bTable */
    bTable.resize(1 << numLogBaseEntry, Bentry(counterWidth, proba));
    std::cerr << bTable.size() << std::endl;

    /** gTable */
    gTable.resize(size);
    for(i = 1; i < size; ++i) {
        gTable[i].resize(1 << 10, Gentry(counterWidth, proba));
        std::cerr << gTable[i].size() << std::endl;
    }



    /** Compute masks */
    baseMask = ((1 << (numLogBaseEntry)) - 1);

    tagMask.resize(size);
    for(i = 1; i < size; ++i) {
        tagMask[i] = ((1 << (i+12)) - 1);
    }

    gMask.resize(size);
    for(i = 1; i < size; ++i) {
        gMask[i] = ((1 << 10) - 1);
    }

}


unsigned
VTAGEComponent::F(unsigned hist, unsigned size, unsigned bank)
{
    Addr res, h1, h2;
    res = (Addr)hist;

    res = res & ((1 << size) - 1);
    h1 = (res & gMask[bank]);
    h2 = (res >> logg[bank]);
    h2 = ((h2 << bank) & gMask[bank]) + (h2 >> (logg[bank] - bank));
    res = h1 ^ h2;
    res = ((res << bank) & gMask[bank]) + (res >> (logg[bank] - bank));
    return (unsigned)res;
}

unsigned
VTAGEComponent::gIndex(Addr &branch_addr, unsigned bank)
{
    Addr index;
    unsigned M = (m[bank] > 16) ? 16 : m[bank];
    index = branch_addr ^ (branch_addr >> (abs(logg[bank] - bank) + 1)) ^
            ch_i[bank].comp ^ F(*phist, M, bank);
    return (unsigned)(index & gMask[bank]);
}

unsigned
VTAGEComponent::gTag(Addr &branch_addr, unsigned bank)
{
    Addr tag = branch_addr ^ ch_t[0][bank].comp ^ (ch_t[1][bank].comp << 1);
    return (unsigned)(tag & tagMask[bank]);
}

prediction_t
VTAGEComponent::getBasePred(Addr &branch_addr, bool &saturated)
{
    unsigned index = bIndex(branch_addr);

    saturated = bTable[index].hyst.read() == 7;
    return prediction_t(bTable[index].pred, bTable[index].hyst.read());
}

void
VTAGEComponent::baseUpdate(Addr branch_addr, Prediction & val, bool outcome, bool squashed)
{
    unsigned index = bIndex(branch_addr);
    unsigned conf = bTable[index].hyst.read();

    //Hack to compile gem5.fast;
    conf = conf;

    bTable[index].ctrupdate(outcome, val, squashed);

    DPRINTF(ValuePredictor, "ValuePred: base update, index: %i,"
    " pred: %s (counter: before: %i, after: %i)\n", index,
    bTable[index].pred.tostring(), conf, bTable[index].hyst.read());
}


prediction_t
VTAGEComponent::lookup(Addr &addr, MicroPC micropc, void *&vp_history)
{

    int i;
    unsigned size = numHistComponents + 1;
    unsigned hitBank = 0;
    unsigned altBank = 0;
    prediction_t tagePred;
    prediction_t altPred;

    bool choseAlt;
    bool baseSaturated = false;

    // Create VTAGEComponent::VPSave. This is the history for a new instruction.
    VPSave *history = new VPSave();

    // TAGE prediction
    // computes the table addresses and the partial tags
    history->instAddr = addr;

    history->gI.resize(size);
    history->gTag.resize(size);



    Addr base_upc = createUPCindex(micropc, (Addr) log2(bTable.size()));
    Addr tagged_upc =createUPCindex(micropc, (Addr) log2(gTable[1].size()));


    DPRINTF(ValuePredictor, "Predicting for address %lx, hashing to 0x%lx for base and 0x%lx for tagged\n", addr, addr ^ base_upc, addr ^ tagged_upc);

    Addr baddr = addr ^ base_upc;
    Addr taddr = addr ^ tagged_upc;

    history->gI[0] = bIndex(baddr);

    for (i = 1; i < size; ++i)
    {
        history->gI[i] = gIndex(taddr, i);
        history->gTag[i] = gTag(taddr, i);
    }

    //Look for the bank with longest matching history
    for (i = size - 1; i > 0; --i)
    {
        if (gTable[i][history->gI[i]].tag == history->gTag[i])
        {
            hitBank = i;

            DPRINTF(ValuePredictor, "ValuePred: 0x%lx, entry found in bank: %i,"
            " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);

            break;
        }
    }

    //Look for the alternate bank
    for (i = hitBank - 1; i > 0; --i)
    {
        if (gTable[i][history->gI[i]].tag == history->gTag[i])
            //TODO Check this condition
            //if ((useAltOnNA < 0) || (gTable[i][history->gI[i]].hyst.read()) > 0)
            {
                altBank = i;

                DPRINTF(ValuePredictor, "ValuePred: 0x%lx, alternate entry found in bank: %i,"
                " gI: %i, gTag: %i\n", addr, i, history->gI[i], history->gTag[i]);
                break;
            }
    }

    //computes the prediction and the alternate prediction
    if (hitBank > 0)
    {

        if (altBank > 0) {

            altPred = prediction_t(gTable[altBank][history->gI[altBank]].pred, gTable[altBank][history->gI[altBank]].hyst.read());
//            choseBase = false;
        }
        else
        {
            altPred = getBasePred(baddr, baseSaturated);
//            choseBase = true;

            //TODO Fix DPRINTF
//            DPRINTF(ValuePredictor, "ValuePred: 0x%x, alternate prediction by base comp,"
//            " index: %i, sat: %i, pred: %i\n", addr, bIndex(addr), baseSaturated,
//            0);
        }
        //if the entry is recognized as a newly allocated entry and
        //useAltOnNA is positive  use the alternate prediction
        tagePred = prediction_t(gTable[hitBank][history->gI[hitBank]].pred, gTable[hitBank][history->gI[hitBank]].hyst.read());

        if ((useAltOnNA < 0)
                || gTable[hitBank][history->gI[hitBank]].hyst.read()  > 0)
        {
            choseAlt = false;


            DPRINTF(ValuePredictor, "ValuePred: 0x%lx, standard prediction used, vtagePred %s\n",
            addr, tagePred.first.tostring());
        }
        else
        {
            choseAlt = true;

            //TODO Fix DPRINTF
//            DPRINTF(ValuePredictor, "ValuePred: 0x%x, alternate prediction used, tagePred %i,"
//            " basePred: %i\n", addr, tagePred.first, altPred.first);
        }
    }
    else
    {
        altPred = getBasePred(baddr, baseSaturated);
        tagePred = prediction_t(altPred.first, altPred.second);

//        choseBase = true;
        choseAlt = false;

        //TODO Fix DPRINTF
//        DPRINTF(ValuePredictor, "ValuePred: 0x%x, no tagged prediction, use base"
//        " base prediction, tagePred: %i, index: %i, sat: %i\n", addr, tagePred.first,
//        bIndex(addr), baseSaturated);
    }

//    history->usedBimod = choseBase;
    history->usedAlt = choseAlt;


    /* save predictor state */
    if(!history)
        panic("No history data structure to save the predictor state");

    /* save prediction, hit/alt bank, GI and GTAG */
    history->bank = hitBank;
    history->altBank = altBank;
    history->tagePred = tagePred;
    history->altPred = altPred;
    history->isBranch = false;


    /*
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, hitBank: %i, altBank: %i, tagePred: %i,"
    " altTaken: %i, predTaken: %i\n", branch_addr, hitBank, altBank, tagePred,
    altTaken, predTaken);
    DPRINTF(ValuePredictor, "BranchPred: 0x%x, confidence:  high: %i, med: %i, low: %i"
    " (bimod: %i) (Stag: %i, NStag: %i, Wtag: %i, NWtag: %i)\n", branch_addr,
    history->highConf, history->medConf, history->lowConf,
    history->bimHighConf || history->bimMedConf || history->bimLowConf,
    history->Stag, history->NStag, history->Wtag, history->NWtag);*/

    DPRINTF(ValuePredictor, "VPSave :bank: %i, altbank: %i, choseAlt: %x\n",
                history->bank, history->altBank, choseAlt);

    vp_history = static_cast<void *>(history);

    return (choseAlt ? altPred : tagePred);
}


void
VTAGEComponent::updateFoldedHist(bool save, void *&vp_history)
{
        unsigned size = numHistComponents + 1;
        unsigned i = 0;
        DPRINTF(ValuePredictor, "Updating Folded History in VTAGE\n");

        if(save)
        {
               VPSave *new_record = new VPSave();




                new_record->ch_i_comp.resize(size);
                new_record->ch_t_comp[0].resize(size);
                new_record->ch_t_comp[1].resize(size);

                for (i = 1; i < size; ++i)
                {
                        //DPRINTF(ValuePredictor, "Saving folded history %u: %lu\n", i, ch_i[i].comp);

                        new_record->ch_i_comp[i] = ch_i[i].comp;
                        new_record->ch_t_comp[0][i] = ch_t[0][i].comp;
                        new_record->ch_t_comp[1][i] = ch_t[1][i].comp;
                }


//        	std::stringstream ss;
//        	for(unsigned j = 0; j < 64; j++) {
//        		ss << (*globHist)[j].dir;
//        	}
//        	DPRINTF(ValuePredictor, "History is: %s\n", ss.str());

                new_record->bank = 0;
                new_record->altBank = 0;
                new_record->tagePred = prediction_t(Prediction(),0);
                new_record->altPred = prediction_t(Prediction(),0);
                new_record->usedAlt = false;
                new_record->isBranch = true;

                vp_history = static_cast<void *>(new_record);
        }

    //prepare next index and tag computations
    for (i = 1; i < size; ++i)
    {
        ch_i[i].update(*globHist);
        //DPRINTF(ValuePredictor, "VTAGE: Updated folded history %u: %lu\n", i, ch_i[i].comp);
        ch_t[0][i].update(*globHist);
        ch_t[1][i].update(*globHist);
    }

    DPRINTF(ValuePredictor, "ValuePred: GlobHist size: %i, new phist: %x\n", globHist->size(), *phist);
}


void
VTAGEComponent::updatePredictor(/*Addr &addr,*/ Prediction val, bool outcome, void *vp_history, bool squashed)
{
    unsigned i,j;
    unsigned size = numHistComponents + 1;
    unsigned nrand = myRandom();
    prediction_t tagePred;
    prediction_t altPred;
    //bool choseAlt;

    unsigned hitBank;
    unsigned altBank;

    assert(vp_history);

    VPSave *history = static_cast<VPSave *>(vp_history);


    if(squashed) {
//         if(history->bank == 0) {
//                 baseUpdate(history->gI[0], val, false, true);
//         } else {
//                 gTable[history->bank][history->gI[history->bank]].ctrupdate(false, val, true);
//         }
         return;
     }




    tagePred = history->tagePred;
    altPred = history->altPred;
    hitBank = history->bank;
    altBank = history->altBank;
    //choseAlt = history->usedAlt;

    //TODO Temporary to remove branch instruction value history.
    if(history->isBranch) return;


    // VTAGE UPDATE
    // try to allocate a  new entries only if prediction was wrong
    bool alloc = !outcome && (hitBank < numHistComponents);
    if (hitBank > 0)
    {
        // Manage the selection between longest matching and alternate matching
        // for "pseudo"-newly allocated longest matching entry
        //TODO Look in VTAGE code to fix that
        //bool LongestMatchPred = (gTable[hitBank][history->gI[hitBank]].ctr.read() >= 0);
        bool PseudoNewAlloc =
                (gTable[hitBank][history->gI[hitBank]].hyst.read() == 0);
        // an entry is considered as newly allocated if its prediction counter is weak
        if (PseudoNewAlloc)
        {
            if (tagePred.first == val) {
                alloc = false;
            }
            // if it was delivering the correct prediction, no need to allocate a new entry
            //even if the overall prediction was false
            if(altPred.second == 7) {
                if (!(altPred.first == tagePred.first))
                {
                        if (altPred.first == val)
                        {
                                if (useAltOnNA < 7)
                                        useAltOnNA++;
                        }
                        else if (useAltOnNA > -8)
                                useAltOnNA--;
                }
            }
            //wtf
//            if (useAltOnNA >= 0)
//                tagePred = LongestMatchPred;
        }
    }


//    DPRINTF(ValuePredictor, "ValuePred: update, hitBank: %i, altBank: %i, tagePred: %lu,"
//    " altPred: %lu, ALLOC: %i\n", hitBank, altBank, tagePred.first,
//    altPred.first, alloc);

    if (alloc)
    {
        // is there some "unuseful" entry to allocate
        unsigned min = 1;
        for (i = numHistComponents; i > hitBank; --i) {
            if (gTable[i][history->gI[i]].u < min) {
                min = gTable[i][history->gI[i]].u;
            }

        }
        // we allocate an entry with a longer history
        //to  avoid ping-pong, we do not choose systematically the next entry, but among the 3 next entries
        unsigned Y = nrand & ((1 << (numHistComponents - hitBank - 1)) - 1);
        unsigned X = hitBank + 1;
        if (Y & 1)
        {
            X++;
            if (Y & 2)
                X++;
        }
        //NO ENTRY AVAILABLE:  ENFORCES ONE TO BE AVAILABLE
        //TODO Do we need this?

        if (min > 0)
            gTable[X][history->gI[X]].u = 0;

        //Allocate only  one entry
        for (i = X; i < size; ++i)
            if (gTable[i][history->gI[i]].u == 0)
            {


                gTable[i][history->gI[i]].tag = history->gTag[i];
                gTable[i][history->gI[i]].hyst.set(0);
                gTable[i][history->gI[i]].u = 0;
                gTable[i][history->gI[i]].pred = val;

                //TODO Fix DPRINTF
                DPRINTF(ValuePredictor, "ValuePred: new entry allocated,"
                " table: %i, index: %i, tag: %i\n", i,
               history->gI[i], history->gTag[i]);

                break;
            }
    }
    //periodic reset of u
    cTick++;
    if ((cTick & ((1 << logCTick) - 1)) == 0)
        // reset least significant bit
        // most significant bit becomes least significant bit
        for (i = 1; i < size; ++i)
            for (j = 0; j < (1 << logg[i]); ++j)
                gTable[i][j].u = 0;

    if (hitBank > 0)
    {

        gTable[hitBank][history->gI[hitBank]].ctrupdate(tagePred.first == val, val);

        //if the provider entry is not certified to be useful also update the alternate prediction
        if (gTable[hitBank][history->gI[hitBank]].u == 0)
        {
            if (altBank > 0)

                gTable[altBank][history->gI[altBank]].ctrupdate(altPred.first == val, val);

            if (altBank == 0)
                //baseUpdate(addr, val, altPred.first == val);

                baseUpdate(history->gI[0], val, altPred.first == val);

        }
    }
    else {
        //baseUpdate(addr, val, altPred.first == val);
        baseUpdate(history->gI[0], val, altPred.first == val);

    }

    // update the u counter
    if (!(tagePred.first == altPred.first))
    {
        if(tagePred.first == val && hitBank > 0)
        {
            gTable[hitBank][history->gI[hitBank]].u = 1;
        } else {
           // if (useAltOnNA < 0) gTable[hitBank][history->gI[hitBank]].u = 0;
        }
    } else if(altPred.second != 7 && tagePred.first == val && hitBank > 0) {
         gTable[hitBank][history->gI[hitBank]].u = 1;
    }

}

void
VTAGEComponent::squash(void *vp_history, bool remove, bool recompute)
{
        VPSave *hist = static_cast<VPSave*>(vp_history);
        if(!remove && !recompute) {
                assert(!hist->isBranch);
        }
        if(hist->isBranch) {
                DPRINTF(ValuePredictor, "squashing a branch in value predictor\n");
                recoverBHist(vp_history, recompute);
        }
        if(remove) recoverVHist(vp_history);
}

void
VTAGEComponent::flush_branch(void *vp_history)
{
         VPSave *history = static_cast<VPSave *>(vp_history);
         DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
         delete history;
}

void
VTAGEComponent::recoverBHist(/*Addr &addr,*/ void *vp_history, bool recompute)
{
    unsigned size = numHistComponents + 1;
    unsigned i;

    VPSave *rollback = static_cast<VPSave*>(vp_history);

    /** Restore history */
    assert(vp_history);
    assert(rollback->isBranch);

    //TODO Fix DPRINTF
//    DPRINTF(ValuePredictor, "ValuePred: 0x%x, recover: phist: %x",
//    addr, phist);


    for (i = 1; i < size; ++i)
    {
        ch_i[i].comp = rollback->ch_i_comp[i];
        //DPRINTF(ValuePredictor, "Restoring folded history %u: %lu\n", i, ch_i[i].comp);
        ch_t[0][i].comp = rollback->ch_t_comp[0][i];
        ch_t[1][i].comp = rollback->ch_t_comp[1][i];
    }
//    DPRINTF(ValuePredictor, "History is:");
//    for(unsigned j = 0; j < 64; j++) {
//    	printf("%u", (*globHist)[j].dir);
//    }
//    printf("\n");

    //Call to recompute the cyclic registers with the correct global branch history
    //Only if needed.
    if(recompute) updateFoldedHist(false, vp_history);
}

void
VTAGEComponent::recoverVHist(/*Addr &addr,*/ void *vp_history)
{
        //Essentially we don't have much to do here.
        VPSave *history = static_cast<VPSave *>(vp_history);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        delete history;
}

void
VTAGEComponent::update(Prediction &  val, void *vp_history,
        bool squashed)
{

        VPSave *history = static_cast<VPSave *>(vp_history);
        assert(!history->isBranch);

        if(history->usedAlt) DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->altPred.first.tostring());
        else DPRINTF(ValuePredictor, "Updating the predictor with %s, Predicted %s\n", val.tostring(), history->tagePred.first.tostring());

        updatePredictor(/*addr,*/ val, (history->usedAlt ? (val == history->altPred.first) : (val == history->tagePred.first)), vp_history, squashed);
        DPRINTF(ValuePredictor, "Deleting 0x%lx\n", history);
        if(!squashed) {
                delete history;
        }

}


std::string
VTAGEComponent::dump()
{
        std::ostringstream oss;
        oss << "VPred: Branch history: ";
        for(std::deque<TageBP::Hist>::iterator it = globHist->begin(); it != globHist->end(); ++it)
        {
                oss << it->dir;
        }
        oss << std::endl;
        return oss.str();
}


//#ifdef DEBUG
//int
//VTAGEComponent::VPHistory::newCount = 0;
//#endif

