# Copyright (c) 2012 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Ron Dreslinski


from m5.objects import *


class O3_Haswell_Port0(FUDesc):
    opList = [ OpDesc(opClass='IntAlu', opLat=1),
               OpDesc(opClass='IntMult', opLat=3),
               OpDesc(opClass='FloatMult', opLat=5),
               OpDesc(opClass='SimdFloatMult', opLat=5),
               OpDesc(opClass='SimdFloatMultAcc', opLat=5),
               OpDesc(opClass='FloatMult', opLat=5),
               OpDesc(opClass='FloatDiv', opLat=10, pipelined=False),
               OpDesc(opClass='FloatSqrt', opLat=10, pipelined=False),
               OpDesc(opClass='SimdMult', opLat=3),
               OpDesc(opClass='SimdMultAcc', opLat=3),
               OpDesc(opClass='SimdFloatDiv', opLat=10, pipelined=False),
               OpDesc(opClass='SimdAlu', opLat=1),
               OpDesc(opClass='SimdCmp', opLat=1),
               OpDesc(opClass='SimdMisc', opLat=1),
               OpDesc(opClass='SimdShift', opLat=1),
               OpDesc(opClass='SimdShiftAcc', opLat=1),
               OpDesc(opClass='IntDiv', opLat=25, pipelined=False),
               OpDesc(opClass='SimdSqrt', opLat=25, pipelined=False) ]
    count = 1


class O3_Haswell_Port1(FUDesc):
    opList = [ OpDesc(opClass='IntAlu', opLat=1),
               OpDesc(opClass='SimdFloatMultAcc', opLat=5),
               OpDesc(opClass='SimdMultAcc', opLat=3),
               OpDesc(opClass='FloatAdd', opLat=3),
               OpDesc(opClass='SimdFloatAlu', opLat=3),
               OpDesc(opClass='SimdFloatAdd', opLat=3),
               OpDesc(opClass='SimdFloatCmp', opLat=3),
               OpDesc(opClass='SimdFloatCvt', opLat=3),
               OpDesc(opClass='FloatCmp', opLat=3),
               OpDesc(opClass='FloatCvt', opLat=3),
               OpDesc(opClass='SimdAdd', opLat=1),
               OpDesc(opClass='SimdAddAcc', opLat=1),
               OpDesc(opClass='SimdAlu', opLat=1),
               OpDesc(opClass='SimdCmp', opLat=1),
               OpDesc(opClass='SimdMisc', opLat=1) ]
    count = 1

class O3_Haswell_Port2(FUDesc):
    opList = [ OpDesc(opClass='MemRead'), OpDesc(opClass='MemWrite') ]
    count = 1

class O3_Haswell_Port3(FUDesc):
    opList = [ OpDesc(opClass='MemRead'), OpDesc(opClass='MemWrite') ]
    count = 1

class O3_Haswell_Port1_Bis(FUDesc):
    opList = [ OpDesc(opClass='SimdAdd', opLat=1),
               OpDesc(opClass='FloatAdd', opLat=3),
               OpDesc(opClass='SimdFloatAlu', opLat=3),
               OpDesc(opClass='SimdFloatAdd', opLat=3) ]
    count = 1

class O3_Haswell_Port0_Bis(FUDesc):
    opList = [ OpDesc(opClass='FloatMult', opLat=5),
               OpDesc(opClass='FloatDiv', opLat=10, pipelined=False),
               OpDesc(opClass='SimdFloatMult', opLat=5),
               OpDesc(opClass='SimdFloatDiv', opLat=10, pipelined=False),
               OpDesc(opClass='SimdMult', opLat=3) ]
    count = 1

class O3_Haswell_Port5(FUDesc):
    opList = [ OpDesc(opClass='IntAlu', opLat=1),
               OpDesc(opClass='SimdAdd', opLat=1),
               OpDesc(opClass='SimdAddAcc', opLat=1),
               OpDesc(opClass='SimdAlu', opLat=1),
               OpDesc(opClass='SimdCmp', opLat=1),
               OpDesc(opClass='SimdMisc', opLat=1) ]
    count = 1

class O3_Haswell_Port6(FUDesc):
    opList = [ OpDesc(opClass='IntAlu', opLat=1) ]
    count = 1

class O3_Haswell_Port7(FUDesc):
    opList = [ OpDesc(opClass='MemWrite') ]
    count = 1


class O3_BASE_WIDE_IprPort(FUDesc):
    opList = [ OpDesc(opClass='IprAccess', opLat = 3) ]
    count = 1

class O3_BASE_WIDE_FUP(FUPool):
    FUList = [O3_Haswell_Port0(), O3_Haswell_Port1(),
              O3_Haswell_Port2(), O3_Haswell_Port3(),
              O3_Haswell_Port0_Bis(), O3_Haswell_Port1_Bis(), O3_Haswell_Port5(),
              O3_Haswell_Port6(), O3_Haswell_Port7(), O3_BASE_WIDE_IprPort()]

depth = 1

class O3_BASE_WIDE(DerivO3CPU):
    v_validation_width = 8
    LQEntries = 72
    SQEntries = 42
    LSQDepCheckShift = 0
    LFSTSize = 1024
    SSITSize = 1024
    SSITAssoc = 1
    decodeToFetchDelay = 1
    renameToFetchDelay = 1
    iewToFetchDelay = 1
    commitToFetchDelay = 1
    renameToDecodeDelay = 1
    iewToDecodeDelay = 1
    commitToDecodeDelay = 1
    iewToRenameDelay = 1
    commitToRenameDelay = 1
    commitToIEWDelay = 1
    fetchWidth = 8
    fetchBufferSize = 16
    fetchToDecodeDelay = 1
    decodeWidth = 8
    decodeToRenameDelay = 1
    renameWidth = 8
    renameToIEWDelay = 1
    issueToExecuteDelay = 1
    dispatchWidth = 8
    issueWidth = 6
    wbWidth = 8
    fuPool = O3_BASE_WIDE_FUP()
    iewToCommitDelay = 1
    renameToROBDelay = 1
    commitWidth = 8
    squashWidth = 256
    trapLatency = 13
    backComSize = 5*depth
    forwardComSize = 5*depth
    numPhysIntRegs = 256
    numPhysFloatRegs = 256
    numPhysCCRegs = 256 * 6
    numIQEntries = 60
    numROBEntries = 192
    rename_ee_alu_depth = 2
    commit_le_width = 8
    v_validation_width = 8
    switched_out = False
    numIntBanks = 4
    numFloatBanks = 4
    commit_ports = 16
    v_numLogBaseEntry = 13
    v_LVT_size = 13


# Instruction Cache
class O3_BASE_WIDE_ICache(Cache):
    hit_latency = 1
    response_latency = 1
    mshrs = 64
    tgts_per_mshr = 64
    write_buffers = 32
    size = '32kB'
    assoc = 8

# Data Cache
class O3_BASE_WIDE_DCache(Cache):
    hit_latency = 4
    response_latency = 4
    mshrs = 64
    tgts_per_mshr = 64
    size = '32kB'
    assoc = 8
    write_buffers = 32

# TLB Cache
# Use a cache as a L2 TLB
class O3_BASE_WIDEWalkCache(Cache):
    hit_latency = 1
    response_latency = 1
    mshrs = 64
    tgts_per_mshr = 64
    size = '1kB'
    assoc = 8
    write_buffers = 32


# L2 Cache
class O3_BASE_WIDE_L2Cache(Cache):
    hit_latency = 12
    response_latency = 12
    mshrs = 64
    tgts_per_mshr = 64
    size = '1MB'
    assoc = 16
    write_buffers = 64
    prefetch_on_access = 'true'
    # Simple stride prefetcher
    prefetcher = StridePrefetcher(degree=8, latency = 1)
